﻿
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reactive.Subjects;
using System.Text.RegularExpressions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineSpecific;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Data.Producibles;
using PackNet.ScandataService.Scanning;
using PackNet.Services.SelectionAlgorithm.BoxLast;

using Testing.Specificity;

namespace ServicesTests.SelectionAlgorithm.BoxLast
{
    [TestClass]
    public class BoxLastSelectionAlgorithmServiceTests
    {
        BoxLastSelectionAlgorithmService service = null;

        Mock<IRestrictionResolverService> restrictionResolverMock;
        Mock<IBoxLastRepository> repoMock;
        Mock<IProductionGroupService> productionGroupServiceMock;
        Mock<IMachineGroupService> machineGroupServiceMock;
        Mock<IUserNotificationService> userNotificationServiceMock;
        Mock<IServiceLocator> serviceLocatorMock;
        Mock<IUICommunicationService> uiCommunicationServiceMock;
        Mock<IOptimalCorrugateCalculator> optimalCorrugateCalculator;
        Mock<IAggregateMachineService> machineServiceMock;
        Mock<ILogger> loggerMock;

        readonly EventAggregator aggregator = new EventAggregator();

        private static ProductionGroup pg1;
        private static MachineGroup mg1;

        [TestInitialize]
        public void Setup()
        {
            restrictionResolverMock = new Mock<IRestrictionResolverService>();
            repoMock = new Mock<IBoxLastRepository>();
            productionGroupServiceMock = GetProductionGroupMock();
            machineGroupServiceMock = new Mock<IMachineGroupService>();
            userNotificationServiceMock = new Mock<IUserNotificationService>(); 
            uiCommunicationServiceMock = GetUserNotificationServiceMock();
            optimalCorrugateCalculator = GetOptimalCorrugateCalculatorMock();
            machineServiceMock = new Mock<IAggregateMachineService>();
            loggerMock = new Mock<ILogger>();
            serviceLocatorMock = GetServiceLocatorMock();

            service = new BoxLastSelectionAlgorithmService(restrictionResolverMock.Object, aggregator, aggregator,
                productionGroupServiceMock.Object, repoMock.Object, optimalCorrugateCalculator.Object,
                machineGroupServiceMock.Object, machineServiceMock.Object, serviceLocatorMock.Object,
                uiCommunicationServiceMock.Object, loggerMock.Object);
        }

        private Mock<IServiceLocator> GetServiceLocatorMock()
        {
            var mock = new Mock<IServiceLocator>();

            mock.Setup(m => m.Locate<IUserNotificationService>()).Returns(userNotificationServiceMock.Object);

            return mock;
        }

        private static Mock<IUICommunicationService> GetUserNotificationServiceMock()
        {
            var mock = new Mock<IUICommunicationService>();

            mock.Setup(m => m.UIEventObservable).Returns(new Subject<Tuple<MessageTypes, string>>());

            return mock;
        }

        private Mock<IOptimalCorrugateCalculator> GetOptimalCorrugateCalculatorMock()
        {
            var mock = new Mock<IOptimalCorrugateCalculator>();

            return mock;
        }

        private static Mock<IProductionGroupService> GetProductionGroupMock()
        {
            var mock = new Mock<IProductionGroupService>();

            mg1 = new MachineGroup()
            {
                Id = Guid.NewGuid()
            };

            pg1 = new ProductionGroup()
            {
                SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast,
                Alias = "BestPg",
                ConfiguredMachineGroups = new ConcurrentList<Guid>() { mg1.Id }
            };

            mock.Setup(m => m.GetProductionGroupForMachineGroup(It.IsAny<Guid>())).Returns(pg1);
            mock.Setup(m => m.ProductionGroups).Returns(new List<ProductionGroup>() { pg1 });

            return mock;
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 10928")]
        public void NotificationToUiWhenSkippingAllProduciblesShouldNotContainDuplicateRestrictionEntries()
        {
            restrictionResolverMock.Setup(m => m.Resolve(It.IsAny<IRestriction>(), It.IsAny<IEnumerable<ICapability>>()))
                .Returns(false);

            var producibles = new List<BoxLastProducible>()
            {
                new BoxLastProducible()
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = new Carton()
                    {
                        CustomerUniqueId = "1",
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    },
                    TimeTriggered = DateTime.Now,
                    ProductionGroupAlias = pg1.Alias
                },
                new BoxLastProducible()
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = new Carton()
                    {
                        CustomerUniqueId = "2",
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    },
                    TimeTriggered = DateTime.Now,
                    ProductionGroupAlias = pg1.Alias
                }
            };

            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            
            service.AddProducibles(producibles);
            
            var producible = service.GetNextJobForMachineGroup(mg1);

            Specify.That(producible).Should.BeNull("A producible was returned even though that the Machine Group does not satisfy the restrictions");            
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReproduceFailedJobAutomaticallyWhenLabelIsNotPrinted()
        {
            restrictionResolverMock.Setup(m => m.Resolve(It.IsAny<IRestriction>(), It.IsAny<IEnumerable<ICapability>>())).Returns(true);

            var c1 = new Corrugate();
            var m1 = new FusionMachine();
            m1.Tracks = new ConcurrentList<Track>();
            m1.Tracks.Add(new Track(){LoadedCorrugate = c1, TrackNumber = 1, TrackOffset = 0});
            mg1.ConfiguredMachines.Add(m1.Id);

            machineServiceMock.Setup(ms => ms.Machines).Returns(new List<IMachine>() { m1 });
            optimalCorrugateCalculator.Setup(m => m.GetOptimalCorrugate(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(), It.IsAny<ICarton>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new CartonOnCorrugate() {Corrugate = c1});

            var producibles = new List<BoxLastProducible>()
            {
                new BoxLastProducible()
                {
                    Id = Guid.NewGuid(),
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = GetCartonWithLabel("1", c1),
                    TimeTriggered = DateTime.Now,
                    ProductionGroupAlias = pg1.Alias
                },
                new BoxLastProducible()
                {
                    Id = Guid.NewGuid(),
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = GetCartonWithLabel("2", c1),
                    TimeTriggered = DateTime.Now.AddSeconds(1),
                    ProductionGroupAlias = pg1.Alias
                }
            };

            producibles.ForEach(p => p.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(new CartonOnCorrugate())));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(new CartonOnCorrugate())));
            
            service.AddProducibles(producibles);

            var producible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(producible).Should.Not.BeNull();
            Specify.That(producible.CustomerUniqueId).Should.BeEqualTo("1");
            producible.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;

            var secondProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(secondProducible).Should.Not.BeNull();
            Specify.That(secondProducible.CustomerUniqueId).Should.BeEqualTo("1");
            Specify.That(secondProducible.Id).Should.BeEqualTo(producible.Id);
            secondProducible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            var thirdProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(thirdProducible).Should.Not.BeNull();
            Specify.That(thirdProducible.Id).Should.Not.BeEqualTo(producible.Id);
            Specify.That(thirdProducible.CustomerUniqueId).Should.BeEqualTo("2");
            thirdProducible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            var fourthProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(fourthProducible).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReproduceRemovedJobAutomaticallyWhenLabelIsNotPrinted()
        {
            restrictionResolverMock.Setup(m => m.Resolve(It.IsAny<IRestriction>(), It.IsAny<IEnumerable<ICapability>>())).Returns(true);

            var c1 = new Corrugate();
            var m1 = new FusionMachine();
            m1.Tracks = new ConcurrentList<Track>();
            m1.Tracks.Add(new Track() { LoadedCorrugate = c1, TrackNumber = 1, TrackOffset = 0 });
            mg1.ConfiguredMachines.Add(m1.Id);

            machineServiceMock.Setup(ms => ms.Machines).Returns(new List<IMachine>() { m1 });
            optimalCorrugateCalculator.Setup(m => m.GetOptimalCorrugate(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(), It.IsAny<ICarton>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new CartonOnCorrugate() { Corrugate = c1 });

            var producibles = new List<BoxLastProducible>()
            {
                new BoxLastProducible()
                {
                    Id = Guid.NewGuid(),
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = GetCartonWithLabel("1", c1),
                    TimeTriggered = DateTime.Now,
                    ProductionGroupAlias = pg1.Alias
                },
                new BoxLastProducible()
                {
                    Id = Guid.NewGuid(),
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = GetCartonWithLabel("2", c1),
                    TimeTriggered = DateTime.Now.AddSeconds(1),
                    ProductionGroupAlias = pg1.Alias
                }
            };

            producibles.ForEach(p => p.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(new CartonOnCorrugate())));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(new CartonOnCorrugate())));

            service.AddProducibles(producibles);

            var producible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(producible).Should.Not.BeNull();
            Specify.That(producible.CustomerUniqueId).Should.BeEqualTo("1");
            producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

            var secondProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(secondProducible).Should.Not.BeNull();
            Specify.That(secondProducible.CustomerUniqueId).Should.BeEqualTo("1");
            Specify.That(secondProducible.Id).Should.BeEqualTo(producible.Id);
            secondProducible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            var thirdProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(thirdProducible).Should.Not.BeNull();
            Specify.That(thirdProducible.Id).Should.Not.BeEqualTo(producible.Id);
            Specify.That(thirdProducible.CustomerUniqueId).Should.BeEqualTo("2");
            thirdProducible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            var fourthProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(fourthProducible).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotReproduceFailedJobAutomaticallyIfLabelIsPrinted()
        {
            restrictionResolverMock.Setup(m => m.Resolve(It.IsAny<IRestriction>(), It.IsAny<IEnumerable<ICapability>>())).Returns(true);

            var c1 = new Corrugate();
            var m1 = new FusionMachine();
            m1.Tracks = new ConcurrentList<Track>();
            m1.Tracks.Add(new Track() { LoadedCorrugate = c1, TrackNumber = 1, TrackOffset = 0 });
            mg1.ConfiguredMachines.Add(m1.Id);

            machineServiceMock.Setup(ms => ms.Machines).Returns(new List<IMachine>() { m1 });
            optimalCorrugateCalculator.Setup(m => m.GetOptimalCorrugate(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(), It.IsAny<ICarton>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new CartonOnCorrugate() { Corrugate = c1 });

            var producibles = new List<BoxLastProducible>()
            {
                new BoxLastProducible()
                {
                    Id = Guid.NewGuid(),
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = GetCartonWithLabel("1", c1),
                    TimeTriggered = DateTime.Now,
                    ProductionGroupAlias = pg1.Alias
                },
                new BoxLastProducible()
                {
                    Id = Guid.NewGuid(),
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = GetCartonWithLabel("2", c1),
                    TimeTriggered = DateTime.Now.AddSeconds(1),
                    ProductionGroupAlias = pg1.Alias
                }
            };

            producibles.ForEach(p => p.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(new CartonOnCorrugate())));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(new CartonOnCorrugate())));

            service.AddProducibles(producibles);

            var producible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(producible).Should.Not.BeNull();
            Specify.That(producible.CustomerUniqueId).Should.BeEqualTo("1");
            var kit = producible as Kit;
            var label = kit.ItemsToProduce.First(i => i.GetType() == typeof(Label));
            var carton = kit.ItemsToProduce.First(i => i.GetType() == typeof(Carton));
            label.ProducibleStatus = ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;
            carton.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;
            producible.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;

            var secondProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(secondProducible).Should.Not.BeNull();
            Specify.That(secondProducible.CustomerUniqueId).Should.BeEqualTo("2");
            secondProducible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            var thirdProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(thirdProducible).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotReproduceRemovedJobAutomaticallyIfLabelIsPrinted()
        {
            restrictionResolverMock.Setup(m => m.Resolve(It.IsAny<IRestriction>(), It.IsAny<IEnumerable<ICapability>>())).Returns(true);

            var c1 = new Corrugate();
            var m1 = new FusionMachine();
            m1.Tracks = new ConcurrentList<Track>();
            m1.Tracks.Add(new Track() { LoadedCorrugate = c1, TrackNumber = 1, TrackOffset = 0 });
            mg1.ConfiguredMachines.Add(m1.Id);

            machineServiceMock.Setup(ms => ms.Machines).Returns(new List<IMachine>() { m1 });
            optimalCorrugateCalculator.Setup(m => m.GetOptimalCorrugate(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(), It.IsAny<ICarton>(), It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(new CartonOnCorrugate() { Corrugate = c1 });

            var producibles = new List<BoxLastProducible>()
            {
                new BoxLastProducible()
                {
                    Id = Guid.NewGuid(),
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = GetCartonWithLabel("1", c1),
                    TimeTriggered = DateTime.Now,
                    ProductionGroupAlias = pg1.Alias
                },
                new BoxLastProducible()
                {
                    Id = Guid.NewGuid(),
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = GetCartonWithLabel("2", c1),
                    TimeTriggered = DateTime.Now.AddSeconds(1),
                    ProductionGroupAlias = pg1.Alias
                }
            };

            producibles.ForEach(p => p.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(new CartonOnCorrugate())));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new MustProduceWithOptimalCorrugateRestriction(new CartonOnCorrugate())));

            service.AddProducibles(producibles);

            var producible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(producible).Should.Not.BeNull();
            Specify.That(producible.CustomerUniqueId).Should.BeEqualTo("1");
            var kit = producible as Kit;
            var label = kit.ItemsToProduce.First(i => i.GetType() == typeof(Label));
            var carton = kit.ItemsToProduce.First(i => i.GetType() == typeof(Carton));
            label.ProducibleStatus = ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;
            carton.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
            producible.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

            var secondProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(secondProducible).Should.Not.BeNull();
            Specify.That(secondProducible.CustomerUniqueId).Should.BeEqualTo("2");
            secondProducible.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            var thirdProducible = service.GetNextJobForMachineGroup(mg1);
            Specify.That(thirdProducible).Should.BeNull();
        }

        private Kit GetCartonWithLabel(string customerUniqueId, Corrugate corrugateToUse)
        {
            var kit = new Kit { CustomerUniqueId = customerUniqueId };
            kit.ItemsToProduce.Add(new Carton()
            {
                CustomerUniqueId = customerUniqueId,
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                CartonOnCorrugate = new CartonOnCorrugate() { Corrugate = corrugateToUse}
            });
            kit.ItemsToProduce.Add(new Label()
            {
                CustomerUniqueId = customerUniqueId,
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged
            });

            return kit;
        }
    }
}


//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using PackNet.Business.Classifications;
//using PackNet.Business.PackagingDesigns;
//using PackNet.Common.Eventing;
//using PackNet.Common.Interfaces.DTO;
//using PackNet.Common.Interfaces.DTO.Carton;
//using PackNet.Common.Interfaces.DTO.Corrugates;
//using PackNet.Common.Interfaces.DTO.Machines;
//using PackNet.Common.Interfaces.DTO.Messaging;
//using PackNet.Common.Interfaces.DTO.PackagingDesigns;
//using PackNet.Common.Interfaces.DTO.ProductionGroups;
//using PackNet.Common.Interfaces.DTO.Scanning;
//using PackNet.Common.Interfaces.Enums;
//using PackNet.Common.Interfaces.Enums.ProducibleStates;
//using PackNet.Common.Interfaces.Eventing;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Common.Interfaces.Producible;
//using PackNet.Common.Interfaces.Services;
//using PackNet.Common.Interfaces.Services.Machines;
//using PackNet.Common.Interfaces.Utils;
//using PackNet.Common.Utils;
//using PackNet.Data.Carton;
//using PackNet.Data.Producibles;
//using PackNet.Services;
//using PackNet.Services.SelectionAlgorithm.BoxLast;
//using System;
//using System.Collections.Generic;
//using System.Globalization;
//using System.Linq;
//using System.Reactive.Linq;
//using System.Reactive.Subjects;
//using System.Threading;
//using Testing.Specificity;
//using Classification = PackNet.Common.Interfaces.DTO.Classifications.Classification;
//using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

//namespace ServicesTests.SelectionAlgorithm.BoxLast
//{
//    [TestClass]
//    public class BoxLastSelectionAlgorithmServiceTests
//    {
//        private readonly Guid corrugate1Id = Guid.NewGuid();
//        private readonly Guid corrugate2Id = Guid.NewGuid();
//        private readonly Guid corrugate3Id = Guid.NewGuid();

//        private List<ProductionGroup> productionGroups;
//        private FusionMachine machine;
//        private List<Guid> configuredCorrugates;
//        private List<BoxLastProducible> requests;
//        private Mock<ILogger> logger;
//        private EventAggregator eventAggregator;
//        private Mock<IRestrictionResolverService> resolverService;
//        private Mock<IOptimalCorrugateCalculator> corrugateCalculator;
//        private Mock<ICartonService> cartonService;
//        private Mock<ICorrugateService> corrugateService;
//        private Mock<IProductionGroupService> productionGroupService;
//        private Mock<IPackagingDesignManager> designManager;
//        private Mock<IBoxLastRepository> BoxLastProducibleRepository;
//        private Mock<IClassificationsManager> classificationsManager;
//        private Mock<IServiceLocator> serviceLocator;
//        private Mock<IMachineGroupService> machineGroupService;
//        private Mock<IMachineService> machineService;
//        private Mock<IUICommunicationService> uiCommunicationService;
//        private Subject<Tuple<MessageTypes, string>> eventSubject;
//        private Guid machine1Guid = Guid.NewGuid();
//        private Guid machine2Guid = Guid.NewGuid();
//        private Guid machine3Guid = Guid.NewGuid();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            machine = new FusionMachine()
//            {
//                Tracks = new List<Track>{new Track
//                {
//                    TrackNumber = 1,
//                    LoadedCorrugate = new Corrugate { Alias = "c1", Id = corrugate1Id, Quality = 1, Thickness = 1, Width = 1 }
//                }}
//            };

//            configuredCorrugates = new List<Guid>
//            {
//                //new Corrugate {Alias = "c1", Id = corrugate1Id, Quality = 1, Thickness = 1, Width = 1},
//                //new Corrugate {Alias = "c2", Id = corrugate2Id, Quality = 2, Thickness = 2, Width = 2},
//                //new Corrugate {Alias = "c3", Id = corrugate3Id, Quality = 3, Thickness = 3, Width = 3}
//            };
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup
//                {
//                    Alias = "BoxLast",
//                    SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast,
//                    ConfiguredCorrugates = configuredCorrugates,
//                    ConfiguredMachineGroups = new List<Guid> {}
//                }
//            };

//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
                    
//                    Producible = new Carton(),
//                    ProductionGroupAlias = "BoxLast",
                    
//                },
//            };

//            logger = new Mock<ILogger>();
//            eventAggregator = new EventAggregator();
//            corrugateCalculator = new Mock<IOptimalCorrugateCalculator>();
//            cartonService = new Mock<ICartonService>();
//            resolverService = new Mock<IRestrictionResolverService>();
//            cartonService.Setup(c => c.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid>())).Returns(true);
//            productionGroupService = new Mock<IProductionGroupService>();
//            designManager = new Mock<IPackagingDesignManager>();
//            corrugateService = new Mock<ICorrugateService>();
//            BoxLastProducibleRepository = new Mock<IBoxLastRepository>();
//            classificationsManager = new Mock<IClassificationsManager>();
//            serviceLocator = new Mock<IServiceLocator>();
//            machineGroupService = new Mock<IMachineGroupService>();
//            machineService = new Mock<IMachineService>();
//            uiCommunicationService = new Mock<IUICommunicationService>();
//            eventSubject = new Subject<Tuple<MessageTypes, string>>();
//            uiCommunicationService.SetupGet(u => u.UIEventObservable).Returns(eventSubject.AsObservable);

//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(Guid.Empty))
//                .Returns(productionGroups.First());
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ValidateAllTestsInThisFile()
//        {
//            Assert.Fail("all tests in this class need to be validated for correct logic.");
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenSelectionAlgorithmSortReceived_ThenCartonsAdded()
//        {
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = "BoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast},
//                new ProductionGroup {Alias = "BoxLast2", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast},
//                new ProductionGroup {Alias = "Orders", SelectionAlgorithm = SelectionAlgorithmTypes.Order}
//            };

//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
//                    Producible = new Carton(),
                    
//                    ProductionGroupAlias = "BoxLast"
//                },
//                new BoxLastProducible
//                {
//                    Producible = new Carton()
//                    ,
//                    ProductionGroupAlias = "BoxLast2"
//                },
//                new BoxLastProducible
//                {
//                    Producible = new Carton()
//                    ,
//                    ProductionGroupAlias = "Orders"
//                }
//            };
//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(
//                resolverService.Object,
//                corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastCarton
//            });

//            Specify.That(boxLastSelector.Producibles.Count).Should.BeEqualTo(2);
//            Specify.That(boxLastSelector.Producibles.Count(r => r.ProductionGroupAlias == "BoxLast")).Should.BeEqualTo(1, "Box last request should have been imported");
//            Specify.That(boxLastSelector.Producibles.Count(r => r.ProductionGroupAlias == "BoxLast2")).Should.BeEqualTo(1, "Box last request should have been imported");
//            Specify.That(boxLastSelector.Producibles.All(r => r.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleImported));
//            BoxLastProducibleRepository.Verify(x => x.Create(It.IsAny<BoxLastProducible>()), Times.Exactly(2));
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenStartingUp_TriggeredCartons_AreRetriggered()
//        {
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = "BoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast}
//            };

//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
//                    TimeTriggered = DateTime.Now.AddHours(-1),
//                    Producible = new Carton(),
                    
//                    ProductionGroupAlias = "BoxLast"
//                }
             
//            };
//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);

//            BoxLastProducibleRepository.Setup(x => x.All()).Returns(requests);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(
//                resolverService.Object,
//                corrugateService.Object,
//                eventAggregator,
//                eventAggregator,
//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            Specify.That(boxLastSelector.TriggeredRequests.Count).Should.BeEqualTo(1, "Previously triggered request was not added to PG!");

//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenSelectionAlgorithmSortReceived_ThenCartonsStateAddedToSystem()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            requests.ForEach(r => r.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStatusUnknown);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,

//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });
//            Thread.Sleep(100);

//            Specify.That(requests.All(r => r.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleImported)).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenTriggerPublished_CartonSentToProductionQueue()
//        {
//            var id = Guid.NewGuid();
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = "BoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast}
//            };

//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
                    
//                    Producible = new Carton(),
//                    ProductionGroupAlias = "BoxLast",
                    
//                },
//            };

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate() });

//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.IsAny<IEnumerable<ICarton>>(),
//                        It.IsAny<CartonOnCorrugate>(),
//                        It.Is<IPacksizeCutCreaseMachine>(m => m.Id == id),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton() {  });

//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(Guid.Empty))
//                .Returns(productionGroups.First());

//            var triggered = false;
//            eventAggregator.GetEvent<IMessage<BoxLastTrigger>>().Subscribe(_ => triggered = true);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });

//            //When event is triggered Store the request in the corresponding PG queue
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });

//            Retry.For(() => triggered, TimeSpan.FromSeconds(2));

//            var nextJobForMachine = (ICarton)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { id } });

//            //Make sure that the job gets returned
//            Assert.Fail("Refactoring needs to fix or remove test.");
//            //Specify.That(nextJobForMachine.Requests.First().CustomerUniqueId).Should.BeEqualTo(requests.First().CustomerUniqueId, "Triggered request should be returned");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenRetriggerRequest_CartonProductionGroupChanged_AndCartonMovedToTriggeredProductionGroup()
//        {
//            var id = Guid.NewGuid();
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = "BoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast},
//                new ProductionGroup {Alias = "AnotherBoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast}
//            };

//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
//                    Producible = new Carton(),
//                    ProductionGroupAlias = "BoxLast",
                    
//                },
//            };

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate() });

//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.IsAny<IEnumerable<ICarton>>(),
//                        It.IsAny<CartonOnCorrugate>(),
//                        It.Is<IPacksizeCutCreaseMachine>(m => m.Id == id),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton {  });

//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(Guid.Empty))
//                .Returns(productionGroups.First());

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var eventCalledCount = 0;
//            //eventAggregator.GetEvent<IMessage>()
//            //    .Where(m => m.MessageType == MessageTypes.FillMachineQueues)
//            //    .Subscribe(e => { eventCalledCount++; });

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });

//            //When event is triggered Store the request in the corresponding PG queue
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = "BoxLast",
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });

//            Thread.Sleep(100);

//            // Validate carton request is in BoxLast PG triggers but not in AnotherBoxLast PG
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.ProductionGroupAlias == "BoxLast")).Should.BeEqualTo(requests.First());
//            Specify.That(boxLastSelector.TriggeredRequests.Any(r => r.ProductionGroupAlias == "AnotherBoxLast")).Should.BeFalse();

//            // Re-trigger the request to another PG
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = "AnotherBoxLast",
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });

//            Thread.Sleep(100);

//            // Validate carton request is now in AnotherBoxLast PG triggers and no longer in BoxLast PG
//            Specify.That(boxLastSelector.TriggeredRequests.Any(r => r.ProductionGroupAlias == "BoxLast")).Should.BeFalse();
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.ProductionGroupAlias == "AnotherBoxLast")).Should.BeEqualTo(requests.First());

//            Retry.For(() => eventCalledCount == 2, TimeSpan.FromSeconds(2));

//            Specify.That(eventCalledCount).Should.BeEqualTo(2);
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenTriggeredCartonNotFound_LogWritten()
//        {
//            var id = Guid.NewGuid();
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = "BoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast}
//            };

//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
//                    Producible = new Carton(),
//                    ProductionGroupAlias = "OtherPG",
                    
//                },
//            };

//            var loggedMessage = "";

//            logger
//                .Setup(l => l.Log(LogLevel.Error, It.IsAny<string>()))
//                .Callback<LogLevel, string>((ll, s) =>
//                {
//                    loggedMessage = s;
//                });

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate() });

//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.IsAny<IEnumerable<ICarton>>(),
//                        It.IsAny<CartonOnCorrugate>(),
//                        It.Is<IPacksizeCutCreaseMachine>(m => m.Id == id),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton {  });

//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(Guid.Empty))
//                .Returns(productionGroups.First());

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = requests.First().ProductionGroupAlias, //"BoxLast"
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }

//            });

//            Thread.Sleep(100);

//            Specify.That(loggedMessage.Contains(requests.First().CustomerUniqueId) &&
//                         loggedMessage.Contains(requests.First().ProductionGroupAlias))
//                .Should.BeTrue("Expecting log message to contain S/N " + requests.First().CustomerUniqueId +
//                               " and PG alias: " + requests.First().ProductionGroupAlias + " actual log message was : " +
//                               loggedMessage);
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonIsTriggerd_ButProductionGroupIsNotFound_CartonIsAddedBackToCurrentReqeustsAndLogWritten()
//        {
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = "BoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast}
//            };

//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
//                    Producible = new Carton(),
//                    ProductionGroupAlias = "BoxLast",
                    
//                },
//            };

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate() });

//            //cartonService.Setup(
//            //    manager => manager.CreateCartonRequestOrder(It.IsAny<IEnumerable<ICarton>>(),
//            //        It.IsAny<CartonOnCorrugate>(),
//            //        It.IsAny<IPacksizeCutCreaseMachine>(),
//            //        It.IsAny<Classification>(),
//            //        It.IsAny<bool>(),
//            //        It.IsAny<bool>(),
//            //        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//            //        It.IsAny<int?>(),
//            //        It.IsAny<bool>())).Returns<
//            //            IEnumerable<ICarton>,
//            //            Corrugate,
//            //            IPacksizeCutCreaseMachine,
//            //            Classification,
//            //            bool,
//            //            bool,
//            //            Func<DesignQueryParam, IPackagingDesign>,
//            //            int?,
//            //            bool>(
//            //                (a, b, c, d, e, f, g, h, i) =>
//            //                    new Carton
//            //                    {
//            //                        Requests = a,
//            //                        Corrugate = b,
//            //                        MachineId = c.Id,
//            //                        InternalPrinting = f,
//            //                        PackStation = h
//            //                    });

//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(Guid.Empty))
//                .Returns(productionGroups.First());

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });

//            Specify.That(boxLastSelector.Producibles.Count()).Should.BeEqualTo(1);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = "OtherPG",
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });

//            Specify.That(boxLastSelector.Producibles.Count()).Should.BeEqualTo(1);
//            logger.Verify(l => l.Log(LogLevel.Error, String.Format(
//                "Failed to retrieve production group queue {0}, SerialNo:{1} added back into the pool",
//                requests.First().ProductionGroupAlias,
//                requests.First().CustomerUniqueId)));
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonIsTriggered_SuccessMessagePublished()
//        {
//            var id = Guid.NewGuid();
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = "BoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast},
//                new ProductionGroup {Alias = "AnotherBoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast}
//            };

//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
//                    Producible = new Carton(),
//                    ProductionGroupAlias = "BoxLast",
                    
//                },
//            };

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate() });

//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.IsAny<IEnumerable<ICarton>>(),
//                        It.IsAny<CartonOnCorrugate>(),
//                        It.Is<IPacksizeCutCreaseMachine>(m => m.Id == id),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton {  });

//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(Guid.Empty))
//                .Returns(productionGroups.First());

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });

//            //When event is triggered Store the request in the corresponding PG queue
//            var called = false;
//            eventAggregator.GetEvent<IMessage<ScanMessage>>().Subscribe(e =>
//            {
//                Specify.That(e.Data.Message).Should.Contain(requests.First().CustomerUniqueId);
//                Specify.That(e.Data.Message).Should.Contain("Successfully triggered carton with Serial Number:");
//                Specify.That(e.Data.ProductionGroup).Should.Contain("BoxLast");
//                called = true;
//            });

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = "BoxLast",
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });

//            Thread.Sleep(100);

//            // Validate carton request is in BoxLast PG triggers but not in AnotherBoxLast PG
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.ProductionGroupAlias == "BoxLast")).Should.BeEqualTo(requests.First());
//            Specify.That(boxLastSelector.TriggeredRequests.Any(r => r.ProductionGroupAlias == "AnotherBoxLast")).Should.BeFalse();
//            Specify.That(called).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonIsReTriggered_SuccessMessagePublished()
//        {
//            var id = Guid.NewGuid();
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = "BoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast},
//                new ProductionGroup {Alias = "AnotherBoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast}
//            };

//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
//                    Producible = new Carton(),
//                    ProductionGroupAlias = "BoxLast",
                    
//                },
//            };

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate() });

//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.IsAny<IEnumerable<ICarton>>(),
//                        It.IsAny<CartonOnCorrugate>(),
//                        It.Is<IPacksizeCutCreaseMachine>(m => m.Id == id),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton {  });

//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(Guid.Empty))
//                .Returns(productionGroups.First());

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = "BoxLast",
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });

//            Thread.Sleep(100);

//            //When event is triggered Store the request in the corresponding PG queue
//            var called = false;
//            eventAggregator.GetEvent<IMessage<ScanMessage>>().Subscribe(e =>
//            {
//                Specify.That(e.Data.Message).Should.Contain(requests.First().CustomerUniqueId);
//                Specify.That(e.Data.Message).Should.Contain("Successfully retriggered carton with Serial Number:");
//                Specify.That(e.Data.ProductionGroup).Should.Contain("AnotherBoxLast");
//                called = true;
//            });

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = "AnotherBoxLast",
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });


//            Thread.Sleep(100);

//            Specify.That(called).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenRequestRetractedOrReproduced_ReAddedBasedOnTimeTriggered()
//        {
//            // Arrange
//            const string pg = "BoxLast";
//            productionGroups = new List<ProductionGroup> { new ProductionGroup { Alias = pg, SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast } };
//            productionGroupService.SetupGet(p => p.ProductionGroups).Returns(productionGroups);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var carton1 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "1"}, ProductionGroupAlias = pg };
//            var carton2 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "2"}, ProductionGroupAlias = pg };
//            var carton3 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "3"}, ProductionGroupAlias = pg };

//            // Add cartons to producibles
//            requests = new List<BoxLastProducible> { carton1, carton2, carton3 };
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });

//            // Trigger carton1
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger { ProductionGroupAlias = pg, TriggeredOn = new DateTime(2014, 7, 22, 9, 9, 9, 101), BarcodeData = carton1.CustomerUniqueId }
//            });
//            Thread.Sleep(100);

//            // Trigger carton2
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger { ProductionGroupAlias = pg, TriggeredOn = new DateTime(2014, 7, 22, 9, 9, 9, 103), BarcodeData = carton2.CustomerUniqueId }
//            });
//            Thread.Sleep(100);

//            // Trigger carton3
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger { ProductionGroupAlias = pg, TriggeredOn = new DateTime(2014, 7, 22, 9, 9, 9, 105), BarcodeData = carton3.CustomerUniqueId }
//            });
//            Thread.Sleep(100);

//            // Retract request that was triggered between carton1 and carton2
//            var carton4 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "4"}, ProductionGroupAlias = pg, TimeTriggered = new DateTime(2014, 7, 22, 9, 9, 9, 102) };
//            requests = new List<BoxLastProducible> { carton4 };

//            // Add cartons to producibles
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.RetractedFromMachine, Data = requests });
//            Thread.Sleep(100);

//            // Assert: That retracted request (carton4) is added back to triggered requests in the right position
//            Specify.That(boxLastSelector.TriggeredRequests.Count).Should.BeEqualTo(4);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(0)).Should.BeLogicallyEqualTo(carton1);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(1)).Should.BeLogicallyEqualTo(carton4);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(2)).Should.BeLogicallyEqualTo(carton2);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(3)).Should.BeLogicallyEqualTo(carton3);
//            Specify.That(carton4.State == ProducibleState.QueuedForReproduction).Should.BeTrue();

//            // Act
//            // Reproduce request that was triggered after all cartons
//            var carton5 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "5", ProductionGroupAlias = pg, TimeTriggered = new DateTime(2014, 7, 22, 9, 9, 9, 106) };
//            requests = new List<BoxLastProducible> { carton5 };
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.FlaggedForReproduction, Data = requests });
//            Thread.Sleep(100);

//            // Assert: That reproduced request is added back to triggered requests in the right position
//            Specify.That(boxLastSelector.TriggeredRequests.Count).Should.BeEqualTo(5);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(0)).Should.BeLogicallyEqualTo(carton1);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(1)).Should.BeLogicallyEqualTo(carton4);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(2)).Should.BeLogicallyEqualTo(carton2);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(3)).Should.BeLogicallyEqualTo(carton3);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(4)).Should.BeLogicallyEqualTo(carton5);
//            Specify.That(carton5.State == ProducibleState.QueuedForReproduction).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenRequestRetractedOrReproduced_ButHasNoProductionGroupAssigned_ItIsNotAddedToAnyProductionGroupQueues_AndMessageLogged()
//        {
//            // Arrange
//            const string pg = "BoxLast";
//            productionGroups = new List<ProductionGroup> { new ProductionGroup { Alias = pg, SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast } };
//            productionGroupService.SetupGet(p => p.ProductionGroups).Returns(productionGroups);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            // Retract carton1 that has no production group assigned
//            var carton1 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "1"}, ProductionGroupAlias = null, TimeTriggered = DateTime.UtcNow };
//            requests = new List<BoxLastProducible> { carton1 };

//            // Add cartons to producibles
//            eventAggregator.Publish(new Message<IEnumerable<BoxLastProducible>> { MessageType = CartonMessages.RetractedFromMachine, Data = requests });
//            Thread.Sleep(100);

//            // Assert: That retracted request (carton4) is added back to triggered requests in the right position
//            Specify.That(boxLastSelector.TriggeredRequests.Any()).Should.BeFalse();


//            // Act
//            // Reproduce carton1 that has no production group assigned
//            eventAggregator.Publish(new Message<IEnumerable<IProducible>> { MessageType = CartonMessages.FlaggedForReproduction, Data = requests });
//            Thread.Sleep(100);

//            // Assert: That reproduced request is added back to triggered requests in the right position
//            Specify.That(boxLastSelector.TriggeredRequests.Any()).Should.BeFalse();
//            logger.Verify(l => l.Log(LogLevel.Info, "Carton SerialNo:1 had no associated production group, cannot re-add to triggered requests"), Times.Exactly(2));

//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenReproducedOrRetractRequestWithNullTriggerTime_ReAddedToFrontOfQueue()
//        {
//            // Arrange
//            const string pg = "BoxLast";
//            productionGroups = new List<ProductionGroup> { new ProductionGroup { Alias = pg, SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast } };
//            productionGroupService.SetupGet(p => p.ProductionGroups).Returns(productionGroups);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var carton1 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "1"}, ProductionGroupAlias = pg };
//            var carton2 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "2"}, ProductionGroupAlias = pg };
//            var carton3 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "3"}, ProductionGroupAlias = pg };

//            // Add cartons to producibles
//            requests = new List<BoxLastProducible> { carton1, carton2, carton3 };
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });

//            // Trigger carton1
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger { ProductionGroupAlias = pg, TriggeredOn = new DateTime(2014, 7, 22, 9, 9, 9, 101), BarcodeData = carton1.CustomerUniqueId }
//            });
//            Thread.Sleep(100);

//            // Trigger carton2
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger { ProductionGroupAlias = pg, TriggeredOn = new DateTime(2014, 7, 22, 9, 9, 9, 103), BarcodeData = carton2.CustomerUniqueId }
//            });
//            Thread.Sleep(100);

//            // Trigger carton3
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger { ProductionGroupAlias = pg, TriggeredOn = new DateTime(2014, 7, 22, 9, 9, 9, 105), BarcodeData = carton3.CustomerUniqueId }
//            });
//            Thread.Sleep(100);

//            // Act
//            // Retract request with Null Trigger Time
//            var cartonWithNullTriggerTime = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "4"}, ProductionGroupAlias = pg, TimeTriggered = null };
//            requests = new List<BoxLastProducible> { cartonWithNullTriggerTime };
//            eventAggregator.Publish(new Message<IEnumerable<IProducible>> { MessageType = CartonMessages.RetractedFromMachine, Data = requests });

//            // Assert
//            Specify.That(boxLastSelector.TriggeredRequests.Count).Should.BeEqualTo(4);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(0)).Should.BeLogicallyEqualTo(cartonWithNullTriggerTime);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(1)).Should.BeLogicallyEqualTo(carton1);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(2)).Should.BeLogicallyEqualTo(carton2);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(3)).Should.BeLogicallyEqualTo(carton3);

//            // Act
//            // Reproduce request with Null Trigger Time
//            var cartonWithNullTriggerTime2 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "5", ProductionGroupAlias = pg, TimeTriggered = null };
//            requests = new List<BoxLastProducible> { cartonWithNullTriggerTime2 };
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.FlaggedForReproduction, Data = requests });

//            // Assert
//            Specify.That(boxLastSelector.TriggeredRequests.Count).Should.BeEqualTo(5);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(0)).Should.BeLogicallyEqualTo(cartonWithNullTriggerTime2);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(1)).Should.BeLogicallyEqualTo(cartonWithNullTriggerTime);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(2)).Should.BeLogicallyEqualTo(carton1);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(3)).Should.BeLogicallyEqualTo(carton2);
//            Specify.That(boxLastSelector.TriggeredRequests.ElementAt(4)).Should.BeLogicallyEqualTo(carton3);
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldAssignTimeTriggered_WhenReproducedOrRetractedRequests_HaveNullTriggerTime()
//        {
//            // Arrange
//            const string pg = "BoxLast";
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = pg, SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast}
//            };
//            productionGroupService.SetupGet(p => p.ProductionGroups).Returns(productionGroups);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var carton1 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "1", ProductionGroupAlias = pg };
//            var carton2 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "2", ProductionGroupAlias = pg };
//            var carton3 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "3", ProductionGroupAlias = pg };

//            // Add cartons to producibles
//            requests = new List<BoxLastProducible> { carton1, carton2, carton3 };
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });

//            // Trigger carton1
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger { ProductionGroupAlias = pg, TriggeredOn = new DateTime(2014, 7, 22, 9, 9, 9, 101), BarcodeData = carton1.CustomerUniqueId }
//            });
//            Thread.Sleep(100);

//            // Trigger carton2
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger { ProductionGroupAlias = pg, TriggeredOn = new DateTime(2014, 7, 22, 9, 9, 9, 103), BarcodeData = carton2.CustomerUniqueId }
//            });
//            Thread.Sleep(100);

//            // Trigger carton3
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger { ProductionGroupAlias = pg, TriggeredOn = new DateTime(2014, 7, 22, 9, 9, 9, 105), BarcodeData = carton3.CustomerUniqueId }
//            });
//            Thread.Sleep(100);

//            // Act
//            // Retract request with Null Trigger Time
//            var cartonWithNullTriggerTime = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "4", ProductionGroupAlias = pg, TimeTriggered = null };
//            requests = new List<BoxLastProducible> { cartonWithNullTriggerTime };
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.RetractedFromMachine, Data = requests });

//            // Act
//            // Reproduce request with Null Trigger Time
//            var cartonWithNullTriggerTime2 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "5", ProductionGroupAlias = pg, TimeTriggered = null };
//            requests = new List<BoxLastProducible> { cartonWithNullTriggerTime2 };
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.FlaggedForReproduction, Data = requests });

//            // Assert: Both requests with null trigger time should be assigned the trigger time of the first request that has a trigger time
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.CustomerUniqueId == cartonWithNullTriggerTime.CustomerUniqueId)
//                    .TimeTriggered).Should.BeEqualTo(carton1.TimeTriggered);
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.CustomerUniqueId == cartonWithNullTriggerTime2.CustomerUniqueId)
//                    .TimeTriggered).Should.BeEqualTo(carton1.TimeTriggered);
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.CustomerUniqueId == carton1.CustomerUniqueId).TimeTriggered).Should.BeEqualTo(carton1.TimeTriggered);
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.CustomerUniqueId == carton2.CustomerUniqueId).TimeTriggered).Should.BeEqualTo(carton2.TimeTriggered);
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.CustomerUniqueId == carton3.CustomerUniqueId).TimeTriggered).Should.BeEqualTo(carton3.TimeTriggered);
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenReproducedOrRetractedRequests_HaveNullTriggerTime_AndNoRequestsWithTriggeredTime_AssignsNullTimeTriggered()
//        {
//            // Arrange
//            const string pg = "BoxLast";
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup {Alias = pg, SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast}
//            };
//            productionGroupService.SetupGet(p => p.ProductionGroups).Returns(productionGroups);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            // Act
//            // Retract request with Null Trigger Time
//            var cartonWithNullTriggerTime = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "4", ProductionGroupAlias = pg, TimeTriggered = null };
//            requests = new List<BoxLastProducible> { cartonWithNullTriggerTime };
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.RetractedFromMachine, Data = requests });

//            // Act
//            // Reproduce request with Null Trigger Time
//            var cartonWithNullTriggerTime2 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "5", ProductionGroupAlias = pg, TimeTriggered = null };
//            requests = new List<BoxLastProducible> { cartonWithNullTriggerTime2 };
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.FlaggedForReproduction, Data = requests });

//            // Assert: Both requests should be assigned a trigger time of Now. The assertion on trigger time should only fail if this test takes longer than five seconds
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.CustomerUniqueId == cartonWithNullTriggerTime.CustomerUniqueId)
//                    .TimeTriggered.HasValue).Should.BeTrue();
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.CustomerUniqueId == cartonWithNullTriggerTime.CustomerUniqueId)
//                    .TimeTriggered > DateTime.Now.AddSeconds(5));

//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.CustomerUniqueId == cartonWithNullTriggerTime2.CustomerUniqueId)
//                    .TimeTriggered.HasValue).Should.BeTrue();
//            Specify.That(boxLastSelector.TriggeredRequests.First(r => r.CustomerUniqueId == cartonWithNullTriggerTime2.CustomerUniqueId)
//                    .TimeTriggered > DateTime.Now.AddSeconds(5));
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void BoxLastSelectorStoresFailedCartonsForLaterUse()
//        {
//            var id = Guid.NewGuid();
//            var producible = new Dictionary<Guid, string>(); // Machine can not produce this carton

//            //PG optimization
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 3),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false))
//                .Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[2] }, ProducibleMachines = producible });

//            //Machine optimization
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 1),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[0] } });

//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.IsAny<IEnumerable<ICarton>>(),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.Is<IPacksizeCutCreaseMachine>(m => m.Id == id),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton {  });

//            // mock corrugate calculator to return yields
//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(Guid.Empty))
//                .Returns(productionGroups.First());

//            // ask for next job for machine.
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            //do the test
//            var job = boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });
//            Specify.That(job).Should.Not.BeNull();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonIsNotValid_ItIsNotSent()
//        {
//            const string carton1SerialNo = "carton1";
//            const string carton2SerialNo = "carton2";
//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible { CustomerUniqueId = carton1SerialNo, ProductionGroupAlias = "BoxLast", IsValid = false, TimeTriggered = DateTime.UtcNow },
//                new BoxLastProducible { CustomerUniqueId = carton2SerialNo, ProductionGroupAlias = "BoxLast", , TimeTriggered = DateTime.UtcNow.AddSeconds(2) }
//            };

//            //setup our single carton
//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.Is<IEnumerable<ICarton>>(cr => cr.First().CustomerUniqueId == carton2SerialNo),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton { Requests = new[] { requests.Last() } });

//            cartonService.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), machine.Id)).Returns(true);

//            //optimal for the pg
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 3),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[2] } });

//            //setup optimal corrugate return
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 1),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[0] } });

//            //ask for next job for machine.
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.Last().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            cartonRequestValidator.Setup(c => c.IsValid(It.Is<ICarton>(x => x.CustomerUniqueId == carton1SerialNo))).Returns(false);
//            cartonRequestValidator.Setup(c => c.IsValid(It.Is<ICarton>(x => x.CustomerUniqueId == carton2SerialNo))).Returns(true);

//            //send the carton to machine 1
//            var job = (ICartonRequestOrder)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });

//            cartonRequestValidator.Verify(c => c.IsValid(requests.First()), Times.Once());
//            cartonRequestValidator.Verify(c => c.IsValid(requests.Last()), Times.Once());
//            Specify.That(job.Requests.First().CustomerUniqueId).Should.BeEqualTo(carton2SerialNo);
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonIsMoreOptimalToProduceOnAnotherMachine_ItIsNotSent()
//        {
//            var machine1 = new FusionMachine
//            {
//                Id = Guid.NewGuid(),
//                Tracks = new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = new Corrugate { Alias = "c1", Id = corrugate1Id, Quality = 1, Thickness = 1, Width = 1 } } }
//            };

//            var machine2 = new FusionMachine
//            {
//                Id = Guid.NewGuid(),
//                Tracks = new List<Track> { new Track { TrackNumber = 1, LoadedCorrugate = new Corrugate { Alias = "c2", Id = corrugate2Id, Quality = 1, Thickness = 1, Width = 1 } } }
//            };

//            configuredCorrugates = new List<Guid>
//            {
//                corrugate1Id, corrugate2Id, corrugate3Id
//            };
//            productionGroups = new List<ProductionGroup>
//            {
//                new ProductionGroup
//                {
//                    Alias = "BoxLast",
//                    SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast,
//                    ConfiguredCorrugates = configuredCorrugates,
//                    ConfiguredMachineGroups = new List<Guid> { Guid.NewGuid() }
//                }
//            };
//            productionGroupService
//               .SetupGet(p => p.ProductionGroups)
//               .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(It.IsAny<Guid>()))
//                .Returns(productionGroups.First());

//            //setup our single carton
//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.IsAny<IEnumerable<ICarton>>(),
//                        It.IsAny<CartonOnCorrugate>(),
//                        It.IsAny<IPacksizeCutCreaseMachine>(),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton { Requests = new[] { requests.First() } });

//            cartonService.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid>())).Returns(true);

//            //optimal for the pg
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 3),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false))
//                .Returns(new CartonOnCorrugate
//                {
//                    Corrugate = new Corrugate { Id = configuredCorrugates[1] },
//                    ProducibleMachines = new Dictionary<Guid, string> { { machine2.Id, string.Empty } }
//                });

//            //setup optimal corrugate return
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 1),
//                        It.Is<IEnumerable<IPacksizeCutCreaseMachine>>(m => m.Any(x => x.Id == machine1.Id)),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[0] } });

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 1),
//                        It.Is<IEnumerable<IPacksizeCutCreaseMachine>>(m => m.Any(x => x.Id == machine2.Id)),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[1] } });

//            //ask for next job for machine.
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            // try to send the carton to machine1, should return null as it is more optimal to make on machine 2
//            var job = (ICartonRequestOrder)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine1.Id } });
//            Specify.That(job).Should.BeNull();

//            // send the carton to machine2, should return as it is most optimal on this machine
//            job = (ICartonRequestOrder)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine2.Id } });
//            Specify.That(job.Requests.First()).Should.BeLogicallyEqualTo(requests.First());

//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenMachineProductionGroupNotInPgQueues_GetNextJobReturnsNull()
//        {
//            var expectedGuid = Guid.NewGuid();
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(expectedGuid))
//                .Returns(new ProductionGroup { Alias = "PgNotBoxLast" });

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            // Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });
//            Thread.Sleep(100);

//            // Trigger the carton
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            //do the test
//            var job = boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });
//            Specify.That(job).Should.BeNull();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenRequestsOkToSendToMachineAndRegister_ReturnsFalse_ItIsNotSent()
//        {
//            var id = Guid.NewGuid();
//            var expectedGuid = Guid.NewGuid();
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate() });

//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.IsAny<IEnumerable<ICarton>>(),
//                        It.IsAny<CartonOnCorrugate>(),
//                        It.Is<IPacksizeCutCreaseMachine>(m => m.Id == id),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton {  });

//            productionGroupService
//                .SetupGet(p => p.ProductionGroups)
//                .Returns(productionGroups);
//            productionGroupService
//                .Setup(p => p.FindByMachineGroupId(expectedGuid))
//                .Returns(productionGroups.First());

//            var triggered = false;
//            eventAggregator.GetEvent<IMessage<BoxLastTrigger>>().Subscribe(_ => triggered = true);

//            // Set request as not OK to send to machine, this should result in returning null when GetNextJobForMachineGroup
//            cartonService.Setup(c => c.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), It.IsAny<Guid>())).Returns(false);

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });

//            //When event is triggered Store the request in the corresponding PG queue
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });

//            Retry.For(() => triggered, TimeSpan.FromSeconds(2));

//            var nextJobForMachine = boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });

//            //Make sure that the job gets returned
//            Specify.That(nextJobForMachine).Should.BeNull();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void BoxLastSelectorCreateNextCartonOnCorrugateLoadedOnMachine()
//        {
//            var producibleMachines = new Dictionary<Guid, string> { { machine2Guid, "" }, { machine3Guid, "" } };
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 3),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false))
//                .Returns(new CartonOnCorrugate
//                {
//                    Corrugate = new Corrugate { Id = configuredCorrugates[2] },
//                    ProducibleMachines = producibleMachines
//                });

//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 1),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[0] } });

//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.IsAny<IEnumerable<ICarton>>(),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.Is<IPacksizeCutCreaseMachine>(m => m.Id == machine1Guid),
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton {  });

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            var job = boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });
//            Specify.That(job).Should.Not.BeNull();
//            logger.Verify(
//                l =>
//                    l.Log(LogLevel.Info,
//                        "MachineId:1 dispatched SerialNo:" + requests.First().CustomerUniqueId +
//                        " on corrugate c1 but optimal corrugate is c3. Machines that can produces this corrugate are MachineIds:[2,3]"),
//                Times.Once());
//            cartonService.Verify(
//                m =>
//                    m.RequestsOkToSendToMachineAndRegister(
//                        It.Is<List<ICarton>>(l => l.Any(c => c.CustomerUniqueId == requests.First().CustomerUniqueId)),
//                        machine.Id), Times.Once);
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void CartonRequestRemoved_RemovesCartonRequestFromCurrentCartonRequestsAndProductionGroupQueues()
//        {
//            // Arrange
//            var cartonRequest1 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "CustomerUniqueId1", ProductionGroupAlias = "BoxLast",  };
//            var cartonRequest2 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "CustomerUniqueId2", ProductionGroupAlias = "BoxLast",  };
//            var cartonRequest3 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "CustomerUniqueId3", ProductionGroupAlias = "BoxLast",  };
//            var cartonRequest4 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "CustomerUniqueId4", ProductionGroupAlias = "BoxLast",  };
//            var cartonRequest5 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "CustomerUniqueId5", ProductionGroupAlias = "BoxLast",  };
//            var cartonRequest6 = new BoxLastProducible { Producible = new Carton(){CustomerUniqueId =  "CustomerUniqueId6", ProductionGroupAlias = "BoxLast",  };

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            // Add some requests to available
//            requests = new List<BoxLastProducible> { cartonRequest1, cartonRequest2, cartonRequest3, cartonRequest4, cartonRequest5, cartonRequest6 };
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });

//            // Add some requests to triggered
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = cartonRequest4.ProductionGroupAlias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = cartonRequest4.CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = cartonRequest5.ProductionGroupAlias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = cartonRequest5.CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = cartonRequest6.ProductionGroupAlias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = cartonRequest6.CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            // Act
//            var deletedRequests = new List<BoxLastProducible> { cartonRequest1, cartonRequest4 };
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.CartonRemoved,
//                Data = deletedRequests
//            });

//            //Assert

//            // Validate deleted items removed
//            Specify.That(boxLastSelector.Producibles.Any(r => r.CustomerUniqueId == deletedRequests.First().CustomerUniqueId))
//                .Should.BeFalse("Carton request 1 should not be in current requests");
//            Specify.That(boxLastSelector.TriggeredRequests.Any(r => r.CustomerUniqueId == deletedRequests.Last().CustomerUniqueId))
//                .Should.BeFalse("Carton request 2 should not be in production queue");
//            Specify.That(deletedRequests.All(r => r.State == ProducibleState.RemovedFromProduction));

//            // Validate other items unchanged
//            Specify.That(boxLastSelector.Producibles.Any(r => r.CustomerUniqueId == cartonRequest2.CustomerUniqueId))
//                .Should.BeTrue("Carton request 2 should be unchanged");
//            Specify.That(boxLastSelector.Producibles.Any(r => r.CustomerUniqueId == cartonRequest3.CustomerUniqueId))
//                .Should.BeTrue("Carton request 3 should be unchanged");
//            Specify.That(boxLastSelector.TriggeredRequests.Any(r => r.CustomerUniqueId == cartonRequest5.CustomerUniqueId))
//                .Should.BeTrue("Carton request 5 should be unchanged");
//            Specify.That(boxLastSelector.TriggeredRequests.Any(r => r.CustomerUniqueId == cartonRequest6.CustomerUniqueId))
//                .Should.BeTrue("Carton request 6 should be unchanged");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void TriggeredCartonMultipleTimesOnlySentOnce()
//        {
//            // simulate the triggering of the same item multiple times, the system should not send the carton again.
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonIsRetractedFromMachine_ItCanBeSentBackToTheSameMachine_AndPlacedInHigherPriorityThanOthersThatWereAlreadyInTheQueue()
//        {
//            var carton1SerialNo = Guid.NewGuid().ToString();
//            var carton2SerialNo = Guid.NewGuid().ToString();
//            requests.Clear();
//            requests.Add(new BoxLastProducible { CustomerUniqueId = carton1SerialNo, ProductionGroupAlias = "BoxLast",  });
//            requests.Add(new BoxLastProducible { CustomerUniqueId = carton2SerialNo, ProductionGroupAlias = "BoxLast",  });

//            //setup our second machine
//            var machine2 = new FusionMachine()
//            {
//                Tracks = new List<Track>{new Track
//                {
//                    TrackNumber = 1,
//                    LoadedCorrugate = new Corrugate { Alias = "c1", Id = corrugate1Id, Quality = 1, Thickness = 1, Width = 1 }
//                }}
//            };
//            productionGroupService.Setup(p => p.FindByMachineGroupId(Guid.Empty)).Returns(productionGroups.First());

//            //setup our crm return types
//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.Is<IEnumerable<ICarton>>(cl => cl.Any(cr => cr.CustomerUniqueId == carton1SerialNo)),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), // any machine because both are the same
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton { Requests = new List<BoxLastProducible> { requests[0] } });

//            //setup our crm return types
//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.Is<IEnumerable<ICarton>>(cl => cl.Any(cr => cr.CustomerUniqueId == carton2SerialNo)),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), // any machine because both are the same
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton { Requests = new List<BoxLastProducible> { requests[1] } });

//            cartonService.Setup(
//                m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), machine.Id))
//                .Returns(true);

//            //optimal for the pg
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 3),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[2] } });

//            //setup optimal corrugate return
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 1),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[0] } });

//            //ask for next job for machine.
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = carton1SerialNo
//                }
//            });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = carton2SerialNo
//                }
//            });
//            Thread.Sleep(100);

//            //send the carton to machine 1
//            var job = (ICartonRequestOrder)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });
//            Specify.That(job.Requests.First().CustomerUniqueId).Should.BeEqualTo(carton1SerialNo);

//            //fail this job as retracted
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.RetractedFromMachine, Data = job.Requests });

//            //do the test again and this job should go to that machine again.
//            job = (ICartonRequestOrder)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });
//            Specify.That(job.Requests.First().CustomerUniqueId).Should.BeEqualTo(carton1SerialNo);
//            job = (ICartonRequestOrder)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });
//            Specify.That(job.Requests.First().CustomerUniqueId).Should.BeEqualTo(carton2SerialNo);
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonsAreFlaggedForReproduction_ItIsRoutedAgain()
//        {
//            const string carton1SerialNo = "carton1";
//            const string carton2SerialNo = "carton2";
//            requests = new List<BoxLastProducible>
//            {
//                new BoxLastProducible { CustomerUniqueId = carton1SerialNo, ProductionGroupAlias = "BoxLast", , TimeTriggered = DateTime.UtcNow },
//                new BoxLastProducible { CustomerUniqueId = carton2SerialNo, ProductionGroupAlias = "BoxLast", , TimeTriggered = DateTime.UtcNow.AddSeconds(2) }
//            };

//            //setup our single carton
//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.Is<IEnumerable<ICarton>>(cr => cr.First().CustomerUniqueId == carton1SerialNo),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), // any machine because both are the same
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton { Requests = new[] { requests.First() } });
//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.Is<IEnumerable<ICarton>>(cr => cr.First().CustomerUniqueId == carton2SerialNo),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), // any machine because both are the same
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton { Requests = new[] { requests.Last() } });
//            cartonService.Setup(m => m.RequestsOkToSendToMachineAndRegister(It.IsAny<IEnumerable<ICarton>>(), machine.Id)).Returns(true);

//            //optimal for the pg
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 3),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[2] } });

//            //setup optimal corrugate return
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.Is<IEnumerable<Corrugate>>(corList => corList.Count() == 1),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.IsAny<ICarton>(),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[0] } });

//            //ask for next job for machine.
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.Last().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            //send the carton to machine 1
//            var job = (ICartonRequestOrder)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });
//            Specify.That(job.Requests.First().CustomerUniqueId).Should.BeEqualTo(carton1SerialNo);
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.CartonCompleted, Data = new[] { requests.First() } });
//            Thread.Sleep(100);

//            job = (ICartonRequestOrder)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });
//            Specify.That(job.Requests.First().CustomerUniqueId).Should.BeEqualTo(carton2SerialNo);

//            eventAggregator.Publish(new Message<IEnumerable<ICarton>> { MessageType = CartonMessages.CartonCompleted, Data = new[] { requests.Last() } });
//            Thread.Sleep(100);

//            //Reproduce both cartons
//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.FlaggedForReproduction,
//                Data = requests.Where(c => c.CustomerUniqueId == carton1SerialNo)
//            });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<IEnumerable<ICarton>>
//            {
//                MessageType = CartonMessages.FlaggedForReproduction,
//                Data = requests.Where(c => c.CustomerUniqueId == carton2SerialNo)
//            });
//            Thread.Sleep(100);

//            //Confirm both reproduced requests are added back to queue
//            Specify.That(boxLastSelector.TriggeredRequests.Count(c => c.CustomerUniqueId == carton1SerialNo)).Should.BeEqualTo(1);
//            Specify.That(boxLastSelector.TriggeredRequests.Count(c => c.CustomerUniqueId == carton2SerialNo)).Should.BeEqualTo(1);
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenMachineCannotProduceFirstJob_ItLooksForNextProducibleJob()
//        {
//            var carton1SerialNo = Guid.NewGuid().ToString();
//            var carton2SerialNo = Guid.NewGuid().ToString();
//            requests.Clear();
//            requests.Add(new BoxLastProducible { CustomerUniqueId = carton1SerialNo, ProductionGroupAlias = "BoxLast",  });
//            requests.Add(new BoxLastProducible { CustomerUniqueId = carton2SerialNo, ProductionGroupAlias = "BoxLast",  });

//            //setup our single carton
//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.Is<IEnumerable<ICarton>>(cr => cr.First().CustomerUniqueId == carton1SerialNo),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), // any machine because both are the same
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton { Requests = new[] { requests.First() } });
//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.Is<IEnumerable<ICarton>>(cr => cr.First().CustomerUniqueId == carton2SerialNo),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), // any machine because both are the same
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton { Requests = new[] { requests.Last() } });

//            //optimal for the pg
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.Is<ICarton>(icr => icr.CustomerUniqueId == carton1SerialNo),
//                        It.IsAny<int>(),
//                        false)).Returns((CartonOnCorrugate)null);


//            //optimal for the pg
//            corrugateCalculator.Setup(
//                c =>
//                    c.GetOptimalCorrugate(
//                        It.IsAny<IEnumerable<Corrugate>>(),
//                        It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
//                        It.Is<ICarton>(icr => icr.CustomerUniqueId == carton2SerialNo),
//                        It.IsAny<int>(),
//                        false)).Returns(new CartonOnCorrugate { Corrugate = new Corrugate { Id = configuredCorrugates[0] } });

//            //ask for next job for machine.
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.Last().CustomerUniqueId
//                }
//            });
//            Thread.Sleep(100);

//            //send the carton to machine 1
//            var job = (ICartonRequestOrder)boxLastSelector.GetNextJobForMachineGroup(new MachineGroup { ConfiguredMachines = new List<Guid> { machine.Id } });
//            Specify.That(job.Requests.First().CustomerUniqueId).Should.BeEqualTo(carton2SerialNo);
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("2941")]
//        [TestCategory("Unit")]
//        public void BoxLastHandlesCartonsWithoutProductionGroupAlias()
//        {
//            var carton1SerialNo = Guid.NewGuid().ToString();
//            requests.Clear();
//            requests.Add(new BoxLastProducible { CustomerUniqueId = carton1SerialNo, ProductionGroupAlias = null,  });

//            //setup our single carton
//            cartonService.Setup(
//                c =>
//                    c.CreateCartonRequestOrder(
//                        It.Is<IEnumerable<ICarton>>(cr => cr.First().CustomerUniqueId == carton1SerialNo),
//                        It.Is<CartonOnCorrugate>(cor => cor.Corrugate.Alias == "c1"),
//                        It.IsAny<IPacksizeCutCreaseMachine>(), // any machine because both are the same
//                        It.IsAny<Classification>(),
//                        false,
//                        false,
//                        It.IsAny<Func<DesignQueryParam, IPackagingDesign>>(),
//                        null,
//                        false))
//                .Returns(new Carton { Requests = new[] { requests.First() } });

//            //ask for next job for machine.
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Add our carton to the dictionary
//            Specify.ThatAction(
//                () =>
//                    eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//                    {
//                        MessageType = SelectionAlgorithmMessages.StagingComplete,
//                        Data = ProducibleTypes.BoxLastProducible
//                    })).Should.Not.HaveThrown("Box Last should have handled carton with null production group");
//            Specify.That(boxLastSelector.Producibles.Any(r => r.CustomerUniqueId == requests.First().CustomerUniqueId)).Should.BeFalse();
//            Specify.That(boxLastSelector.TriggeredRequests.Any(r => r.CustomerUniqueId == requests.First().CustomerUniqueId)).Should.BeFalse();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void FillMachineQueueEventIsTriggeredWhenJobIsTriggered()
//        {
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.Publish(new Message<ProducibleTypes, Guid> { MessageType = SelectionAlgorithmMessages.StagingComplete, Data = ProducibleTypes.BoxLastProducible });

//            var eventCalled = false;
//            //eventAggregator.GetEvent<IMessage>().Where(m => m.MessageType == MessageTypes.FillMachineQueues).Subscribe(e => { eventCalled = true; });

//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = productionGroups.First().Alias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = requests.First().CustomerUniqueId
//                }
//            });

//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCtorHappens_JobsLoadedFromRepo_ThatAreInAppropriateStates()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();

//            requests[0].ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStatusUnknown;
//            requests[1].ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported;
//            requests[2].State = ProducibleState.QueuedForProduction;
//            requests[3].State = ProducibleState.QueuedForReproduction;
//            requests[4].State = ProducibleState.ProductionCompleted;
//            requests[5].State = ProducibleState.ProductionInterrupted;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            //Not triggered items in selection algorithm should have all requests in the box last selection algorithms
//            Specify.That(boxLastSelector.Producibles.Count).Should.BeEqualTo(4);
//            Specify.That(boxLastSelector.Producibles.Any(r => r == requests[4])).Should.BeFalse();
//            Specify.That(boxLastSelector.Producibles.Any(r => r == requests[5])).Should.BeFalse();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldRespondToGetProduciblesWithAllJobs()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "yoMomma";

//            var eventCalled = false;
//            //setup is assuming that 
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(e =>
//                {
//                    Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                    Specify.That(e.Data.Count()).Should.BeEqualTo(6);
//                    eventCalled = true;
//                });

//            //set one of our cartons as triggered
//            var request = requests.First(c => c.ProductionGroupAlias == "RobPg1");
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = request.ProductionGroupAlias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = request.CustomerUniqueId
//                }
//            });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", string.Empty},
//                {"Value", string.Empty},
//                {"ProductionGroup", string.Empty}
//            };
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenNoRequests_GetProduciblesResponsdsWithNoRequests()
//        {
//            var pg = new ProductionGroup { Alias = "RobPg1", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast };
//            productionGroups.Clear();
//            productionGroups.Add(pg);

//            const string replyTo = "yoMomma";

//            var eventCalled = false;
//            //setup is assuming that 
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(e =>
//                {
//                    Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                    Specify.That(e.Data.Any()).Should.BeFalse("No requests should have been returned");
//                    eventCalled = true;
//                });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", string.Empty},
//                {"Value", string.Empty},
//                {"ProductionGroup", string.Empty}
//            };
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldRespondToGetTriggeredWithAllTriggeredJobs()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "yoMomma";

//            var eventCalled = false;
//            var expectedSearchCount = 1;
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(e =>
//                {
//                    Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                    Specify.That(e.Data.Count()).Should.BeEqualTo(expectedSearchCount);
//                    eventCalled = true;
//                });

//            //set one of our cartons as triggered
//            var request = requests.First(c => c.ProductionGroupAlias == "RobPg1");
//            eventAggregator.Publish(new Message<BoxLastTrigger>
//            {
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = request.ProductionGroupAlias,
//                    TriggeredOn = DateTime.UtcNow,
//                    BarcodeData = request.CustomerUniqueId
//                }
//            });

//            // "RequestSet=triggered&SearchType=&Value=&ProductionGroup=RobPg1"));
//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "triggered"},
//                {"SearchType", string.Empty},
//                {"Value", string.Empty},
//                {"ProductionGroup", "RobPg1"}
//            };

//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(3));

//            Specify.That(eventCalled).Should.BeTrue();

//            //Do another test...
//            eventCalled = false;
//            expectedSearchCount = 0;

//            dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "triggered"},
//                {"SearchType", string.Empty},
//                {"Value", string.Empty},
//                {"ProductionGroup", "RobPg2"}
//            };

//            // this RobPg2 has never had an item triggered
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(2));
//            Specify.That(eventCalled).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldChunkResponseData()
//        {
//            ISearchResponse<IEnumerable<BoxLastProducible>> response = null;
//            var pg = new ProductionGroup { Alias = "TheCheeseStandsAlone", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast };
//            const string replyTo = "noone";
//            productionGroups.Clear();
//            productionGroups.Add(pg);

//            var pgCartons = CreateNumberOfRequests(100, pg.Alias);
//            requests.Clear();
//            requests.AddRange(pgCartons);
//            BoxLastProducibleRepository.Setup(r => r.All()).Returns(requests);

//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(e =>
//                    response = e
//                );

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", "CustomerUniqueId"},
//                {"Value", ""},
//                {"ProductionGroup", "TheCheeseStandsAlone"}
//            };
//            Retry.For(() => response != null, TimeSpan.FromSeconds(1));

//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 3, 25)
//            });
//            Retry.For(() => response != null, TimeSpan.FromSeconds(1));

//            Specify.That(response).Should.Not.BeNull();
//            Specify.That(response.TotalNumberOfItems).Should.BeEqualTo(100);
//            Specify.That(response.Data.Count()).Should.BeEqualTo(25);

//            response = null;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 4, 25)
//            });
//            Retry.For(() => response != null, TimeSpan.FromSeconds(1));
//            Assert.IsNotNull(response);
//            Specify.That(response.TotalNumberOfItems).Should.BeEqualTo(100);
//            Specify.That(response.Data.Count()).Should.BeEqualTo(0);

//            response = null;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 3, 30)
//            });
//            Retry.For(() => response != null, TimeSpan.FromSeconds(1));
//            Assert.IsNotNull(response);
//            Specify.That(response.TotalNumberOfItems).Should.BeEqualTo(100);
//            Specify.That(response.Data.Count()).Should.BeEqualTo(10);
//            boxLastSelector.Dispose();
//        }

//        private IEnumerable<BoxLastProducible> CreateNumberOfRequests(int numberOfRequestsToCreate, string pgAlias)
//        {
//            var ret = new List<BoxLastProducible>();

//            for (var i = 0; i < numberOfRequestsToCreate; i++)
//            {
//                ret.Add(new BoxLastProducible
//                {
//                    Producible = new Carton(),
//                    ProductionGroupAlias = pgAlias,
                    
//                });
//            }

//            return ret;
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonIsTriggeredThenTheTriggerTimeIsSetInTheCartonRequest()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var timeTriggered = DateTime.UtcNow;

//            var request = BoxLastProducibleRepository.Object.All().First(c => c.ProductionGroupAlias == "RobPg1");

//            eventAggregator.Publish(
//                new Message<BoxLastTrigger>
//                {
//                    Data = new BoxLastTrigger
//                    {
//                        ProductionGroupAlias = request.ProductionGroupAlias,
//                        TriggeredOn = timeTriggered,
//                        BarcodeData = request.CustomerUniqueId
//                    }
//                });
//            Thread.Sleep(100);

//            Specify.That(boxLastSelector.TriggeredRequests.Any(r => r.TimeTriggered == timeTriggered)).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonIsTriggered_ThenThePgIsSetToTriggeredPg_AndStateIsSetToQueuedForProduction_AndCartonIsUpdated()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var timeTriggered = DateTime.UtcNow;
//            var request = BoxLastProducibleRepository.Object.All().First(c => c.ProductionGroupAlias == "RobPg1");
//            BoxLastProducibleRepository.Setup(r => r.Find(request.CustomerUniqueId)).Returns(request);

//            eventAggregator.Publish(
//                new Message<BoxLastTrigger>
//                {
//                    Data = new BoxLastTrigger
//                    {
//                        ProductionGroupAlias = "RobPg2",
//                        TriggeredOn = timeTriggered,
//                        BarcodeData = request.CustomerUniqueId
//                    }
//                });
//            Thread.Sleep(100);

//            Specify.That(request.State).Should.BeEqualTo(ProducibleState.QueuedForProduction);
//            Specify.That(request.ProductionGroupAlias).Should.BeEqualTo("RobPg2");
//            BoxLastProducibleRepository.Verify(r => r.Update(It.Is<BoxLastProducible>(c => c.CustomerUniqueId == request.CustomerUniqueId && c.ProductionGroupAlias == "RobPg2")));
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonIsRetriggeredThenThePgIsSetAndCartonIsUpdatedAndRevalidated()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            bool eventTriggered = false;
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var timeTriggered = DateTime.UtcNow;
//            var request = BoxLastProducibleRepository.Object.All().First(c => c.ProductionGroupAlias == "RobPg1");
//            BoxLastProducibleRepository.Setup(r => r.Find(request.CustomerUniqueId)).Returns(request);
//            BoxLastProducibleRepository.Setup(r => r.Update(It.IsAny<BoxLastProducible>())).Callback<BoxLastProducible>(c => eventTriggered = true);

//            // Trigger to same PG
//            eventAggregator.Publish(
//                new Message<BoxLastTrigger>
//                {
//                    Data = new BoxLastTrigger
//                    {
//                        ProductionGroupAlias = "RobPg1",
//                        TriggeredOn = timeTriggered,
//                        BarcodeData = request.CustomerUniqueId
//                    }
//                });
//            Thread.Sleep(100);

//            // Then retrigger to a different PG
//            eventAggregator.Publish(
//                new Message<BoxLastTrigger>
//                {
//                    Data = new BoxLastTrigger
//                    {
//                        ProductionGroupAlias = "RobPg2",
//                        TriggeredOn = timeTriggered,
//                        BarcodeData = request.CustomerUniqueId
//                    }
//                });
//            Thread.Sleep(100);

//            Retry.For(() => eventTriggered, TimeSpan.FromSeconds(5));
//            BoxLastProducibleRepository.Verify(r => r.Update(It.Is<BoxLastProducible>(c => c.CustomerUniqueId == request.CustomerUniqueId && c.ProductionGroupAlias == "RobPg2")));
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnCustomerUniqueId()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            var eventCalled = false;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(e.Data.Any(r => r.CustomerUniqueId.Contains("a"))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(3);
//                        eventCalled = true;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", "CustomerUniqueId"},
//                {"Value", "a"},
//                {"ProductionGroup", "RobPg2"}
//            };

//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue("Event wasn't triggered");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnName()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "Name";
//            var eventCalled = false;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r => r.GetType().GetProperty(property).GetValue(r, null).ToString().Contains("BoxLastN")))
//                            .Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(6);
//                        eventCalled = true;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", property},
//                {"Value", "BoxLastN"},
//                {"ProductionGroup", ""}
//            };

//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue("Event wasn't triggered");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnLength()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "Length";
//            var eventCalled = 0;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var currentValue = 11;
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(2);
//                        eventCalled++;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", property},
//                {"Value", currentValue.ToString(CultureInfo.InvariantCulture)},
//                {"ProductionGroup", ""}
//            };
//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 0, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(1, "Event wasn't triggered (11)");

//            currentValue = 12;
//            dictFilters["Value"] = currentValue.ToString(CultureInfo.InvariantCulture);
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 1, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(2, "Event wasn't triggered (12)");

//            currentValue = 13;
//            dictFilters["Value"] = currentValue.ToString(CultureInfo.InvariantCulture);
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 2, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(3, "Event wasn't triggered (13)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnWidth()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "Width";

//            var eventCalled = false;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var currentValue = 21;
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(2);
//                        eventCalled = true;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", property},
//                {"Value", currentValue.ToString(CultureInfo.InvariantCulture)},
//                {"ProductionGroup", ""}
//            };
//            Thread.Sleep(200);
//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue("Event wasn't triggered (21)");
//            eventCalled = false;

//            currentValue = 22;
//            dictFilters["Value"] = currentValue.ToString(CultureInfo.InvariantCulture);
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue("Event wasn't triggered (22)");
//            eventCalled = false;

//            currentValue = 23;
//            dictFilters["Value"] = currentValue.ToString(CultureInfo.InvariantCulture);
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue("Event wasn't triggered (23)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnHeight()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "Height";

//            var eventCalled = 0;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var currentValue = 31;
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(2);
//                        eventCalled++;
//                    });
//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", property},
//                {"Value", currentValue.ToString(CultureInfo.InvariantCulture)},
//                {"ProductionGroup", ""}
//            };
//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 0, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(1, "Event wasn't triggered (31)");

//            currentValue = 32;
//            dictFilters["Value"] = currentValue.ToString(CultureInfo.InvariantCulture);
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 1, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(2, "Event wasn't triggered (32)");

//            currentValue = 33;
//            dictFilters["Value"] = currentValue.ToString(CultureInfo.InvariantCulture);
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 2, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(3, "Event wasn't triggered (33)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnClassificationNumber()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "ClassificationNumber";

//            var eventCalled = 0;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var currentValue = "1";
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(2);
//                        eventCalled++;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", property},
//                {"Value", currentValue},
//                {"ProductionGroup", ""}
//            };

//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 0, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(1, "Event wasn't triggered (1)");

//            currentValue = "2";
//            dictFilters["Value"] = currentValue;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 1, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(2, "Event wasn't triggered (2)");

//            currentValue = "3";
//            dictFilters["Value"] = currentValue;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 2, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(3, "Event wasn't triggered (3)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnCorrugateQuality()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "CorrugateQuality";

//            var eventCalled = 0;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var currentValue = 1;
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(2);
//                        eventCalled++;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", property},
//                {"Value", currentValue.ToString(CultureInfo.InvariantCulture)},
//                {"ProductionGroup", ""}
//            };

//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });

//            currentValue = 2;
//            dictFilters["Value"] = currentValue.ToString(CultureInfo.InvariantCulture);
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 1, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(2, "Event wasn't triggered (2)");

//            currentValue = 3;
//            dictFilters["Value"] = currentValue.ToString(CultureInfo.InvariantCulture);
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 2, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(3, "Event wasn't triggered (3)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnPgAlias()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "ProductionGroupAlias";

//            var eventCalled = 0;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var currentValue = "RobPg1";
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(3);
//                        eventCalled++;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                { "RequestSet", "all" },
//                { "SearchType", property },
//                { "Value", currentValue },
//                { "ProductionGroup", "" }
//            };

//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 0, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(1, "Event wasn't triggered (1)");

//            currentValue = "RobPg2";
//            dictFilters["Value"] = currentValue;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 1, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(2, "Event wasn't triggered (2)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnCartonPropertyGroupAlias()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "CartonPropertyGroupAlias";

//            var eventCalled = 0;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var currentValue = "G1";
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(2);
//                        eventCalled++;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", property},
//                {"Value", currentValue},
//                {"ProductionGroup", ""}
//            };


//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 0, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(1, "Event wasn't triggered (1)");

//            currentValue = "G2";
//            dictFilters["Value"] = currentValue;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 1, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(2, "Event wasn't triggered (2)");

//            currentValue = "G3";
//            dictFilters["Value"] = currentValue;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 2, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(3, "Event wasn't triggered (3)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnPickZone()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "PickZone";

//            var eventCalled = false;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var currentValue = "AZ1";
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(2);
//                        eventCalled = true;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", property},
//                {"Value", currentValue},
//                {"ProductionGroup", ""}
//            };

//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue("Event wasn't triggered (1)");
//            eventCalled = false;

//            currentValue = "AZ2";
//            dictFilters["Value"] = currentValue;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue("Event wasn't triggered (2)");
//            eventCalled = false;

//            currentValue = "AZ3";
//            dictFilters["Value"] = currentValue;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeTrue("Event wasn't triggered (3)");
//            eventCalled = false;
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void JobsShouldBeFilterableOnDesignId()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "DesignId";

//            var eventCalled = 0;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            var currentValue = "1337";
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(2);
//                        eventCalled++;
//                    });
//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", property},
//                {"Value", currentValue},
//                {"ProductionGroup", ""}
//            };

//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 0, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(1, "Event wasn't triggered (1)");

//            currentValue = "1338";
//            dictFilters["Value"] = currentValue;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 1, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(2, "Event wasn't triggered (2)");

//            currentValue = "1339";
//            dictFilters["Value"] = currentValue;
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 2, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(3, "Event wasn't triggered (3)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void TriggeredJobsShouldBeFilterable()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            const string property = "Length";

//            var eventCalled = 0;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            const int currentValue = 11;
//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(
//                            e.Data.Any(
//                                r =>
//                                    r.GetType()
//                                        .GetProperty(property)
//                                        .GetValue(r, null)
//                                        .ToString()
//                                        .Contains(currentValue.ToString(CultureInfo.InvariantCulture)))).Should.BeTrue();
//                        Specify.That(e.Data.Count()).Should.BeEqualTo(1, property + "=" + currentValue);
//                        eventCalled++;
//                    });


//            var timeTriggered = DateTime.UtcNow;

//            var request = BoxLastProducibleRepository.Object.All().First(c => c.ProductionGroupAlias == "RobPg1");
//            eventAggregator.Publish(
//                new Message<BoxLastTrigger>
//                {
//                    Data = new BoxLastTrigger
//                    {
//                        ProductionGroupAlias = request.ProductionGroupAlias,
//                        TriggeredOn = timeTriggered,
//                        BarcodeData = request.CustomerUniqueId
//                    }
//                });

//            Thread.Sleep(100);

//            request = BoxLastProducibleRepository.Object.All().First(c => c.ProductionGroupAlias == "RobPg2");
//            eventAggregator.Publish(
//                new Message<BoxLastTrigger>
//                {
//                    Data = new BoxLastTrigger
//                    {
//                        ProductionGroupAlias = request.ProductionGroupAlias,
//                        TriggeredOn = timeTriggered,
//                        BarcodeData = request.CustomerUniqueId
//                    }
//                });

//            Thread.Sleep(100);

//            var dictFilters = new Dictionary<string, string>
//            {
//                { "RequestSet", "triggered" },
//                { "SearchType", property },
//                { "Value", currentValue.ToString(CultureInfo.InvariantCulture) },
//                { "ProductionGroup", "RobPg2" }
//            };
//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });

//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(1, "Event wasn't triggered (1)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void AllAvaliableJobsShouldBeSortedByCustomerUniqueId()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";

//            var eventCalled = 0;

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        Specify.That(e.Data.Any()).Should.BeLogicallyEqualTo(true);

//                        var data = e.Data.ToArray();
//                        for (var i = 0; i < e.Data.Count() - 1; i++)
//                        {
//                            Specify.That(String.CompareOrdinal(data[i].CustomerUniqueId, data[i + 1].CustomerUniqueId) < 0)
//                                .Should.BeEqualTo(true);
//                        }

//                        eventCalled++;
//                    });

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "all"},
//                {"SearchType", string.Empty},
//                {"Value", string.Empty},
//                {"ProductionGroup", string.Empty}
//            };
//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Retry.For(() => eventCalled > 0, TimeSpan.FromSeconds(1));
//            Specify.That(eventCalled).Should.BeEqualTo(1, "Event wasn't triggered (1)");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void TriggeredJobsShouldBeSortedByTriggerTime()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            const string replyTo = "theVoid";
//            var eventCalled = 0;
//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.GetEvent<ISearchResponse<IEnumerable<BoxLastProducible>>>()
//                .Where(m => m.MessageType == ResponseMessages.SearchedProducibles)
//                .Subscribe(
//                    e =>
//                    {
//                        Specify.That(e.ReplyTo).Should.BeEqualTo(replyTo);
//                        var data = e.Data.ToArray();
//                        for (var i = 0; i < e.Data.Count() - 1; i++)
//                        {
//                            Specify.That(data[i].TimeTriggered <= data[i + 1].TimeTriggered)
//                                .Should.BeLogicallyEqualTo(true);
//                        }
//                        Specify.That(e.Data.First().CustomerUniqueId).Should.BeLogicallyEqualTo("a2");
//                    });

//            eventAggregator.GetEvent<BoxLastTrigger>().Subscribe(e => eventCalled++);

//            var timeTriggered = DateTime.UtcNow;
//            var request = BoxLastProducibleRepository.Object.All().First(c => c.ProductionGroupAlias == "RobPg1");
//            eventAggregator.Publish(
//                new BoxLastTrigger
//                {
//                    ProductionGroupAlias = request.ProductionGroupAlias,
//                    TriggeredOn = timeTriggered,
//                    BarcodeData = request.CustomerUniqueId
//                });

//            Retry.For(() => eventCalled == 1, TimeSpan.FromSeconds(2));
//            Specify.That(eventCalled).Should.BeEqualTo(1);

//            request =
//                BoxLastProducibleRepository.Object.All().First(
//                    c => c.ProductionGroupAlias == "RobPg1" && c.TimeTriggered == null);
//            timeTriggered = timeTriggered.AddDays(-1);
//            eventAggregator.Publish(
//                new BoxLastTrigger
//                {
//                    ProductionGroupAlias = request.ProductionGroupAlias,
//                    TriggeredOn = timeTriggered,
//                    BarcodeData = request.CustomerUniqueId
//                });

//            Retry.For(() => eventCalled == 2, TimeSpan.FromSeconds(2));
//            Specify.That(eventCalled).Should.BeEqualTo(2);

//            var dictFilters = new Dictionary<string, string>
//            {
//                {"RequestSet", "triggered"},
//                {"SearchType", string.Empty},
//                {"Value", string.Empty},
//                {"ProductionGroup", "RobPg1"}
//            };

//            //Do test
//            eventAggregator.Publish(new Message<DataRequest>
//            {
//                MessageType = MessageTypes.GetProducibles,
//                ReplyTo = replyTo,
//                Data = new DataRequest("BoxLast", dictFilters, 0, 25)
//            });
//            Specify.That(eventCalled).Should.BeEqualTo(2, "Event wasn't triggered twice");
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldPublishSelectionAlgorithmIfPGIsPresent()
//        {
//            var eventCalled = false;
//            eventAggregator.GetEvent<IMessage<Tuple<SelectionAlgorithmTypes, IScanTrigger>>>().Subscribe(data =>
//            {
//                eventCalled = true;
//                Specify.That(data.Data.Item1).Should.BeEqualTo(SelectionAlgorithmTypes.BoxLast);
//            });

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.Publish(new Message<IScanTrigger>
//            {
//                MessageType = MessageTypes.GetSelectionAlgorithmForProductionGroup,
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = "BoxLast"
//                }
//            });

//            Retry.For(() => eventCalled, TimeSpan.FromSeconds(2));
//            Specify.That(eventCalled).Should.BeTrue();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotPublishSelectionAlgorithmIfPGIsNotPresent()
//        {
//            var eventCalled = false;
//            eventAggregator.GetEvent<IMessage<Tuple<SelectionAlgorithmTypes, IScanTrigger>>>().Subscribe(data =>
//            {
//                eventCalled = true;
//                Specify.That(data.Data.Item1).Should.BeEqualTo(SelectionAlgorithmTypes.BoxLast);
//            });

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.Publish(new Message<IScanTrigger>
//            {
//                MessageType = MessageTypes.GetSelectionAlgorithmForProductionGroup,
//                Data = new BoxLastTrigger
//                {
//                    ProductionGroupAlias = "NOTAVALIDBOXLASTPG"
//                }
//            });

//            Specify.That(eventCalled).Should.BeFalse();
//            boxLastSelector.Dispose();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenCartonImported_ShouldSetPriorityLabel_BasedOnClassification()
//        {
//            SetupThreePgWithJobsOnePgNotBoxLast();
//            classificationsManager.SetupGet(c => c.Classifications)
//                .Returns(new List<Classification>
//                {
//                    new Classification("1") { IsPriorityLabel = false },
//                    new Classification("2") { IsPriorityLabel = true }
//                });

//            var boxLastSelector = new BoxLastSelectionAlgorithmService(resolverService.Object, corrugateService.Object,
//                eventAggregator,
//                eventAggregator,

//                productionGroupService.Object,
//                BoxLastProducibleRepository.Object,
//                classificationsManager.Object,
//                corrugateCalculator.Object,
//                designManager.Object,
//                machineGroupService.Object,
//                machineService.Object,
//                serviceLocator.Object,
//                uiCommunicationService.Object,
//                logger.Object);

//            eventAggregator.Publish(new Message<ProducibleTypes, Guid>
//            {
//                MessageType = SelectionAlgorithmMessages.StagingComplete,
//                Data = ProducibleTypes.BoxLastProducible
//            });

//            Thread.Sleep(100);

//            Specify.That(boxLastSelector.Producibles.Where(r => r.ClassificationNumber == "1").All(x => x.StoredIsPriorityLabel == false)).Should.BeTrue();
//            Specify.That(boxLastSelector.Producibles.Where(r => r.ClassificationNumber == "2").All(x => x.StoredIsPriorityLabel)).Should.BeTrue();
//        }

//        private void SetupThreePgWithJobsOnePgNotBoxLast()
//        {
//            var pg = new ProductionGroup { Alias = "RobPg1", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast };
//            var pg2 = new ProductionGroup { Alias = "RobPg2", SelectionAlgorithm = SelectionAlgorithmTypes.BoxLast };
//            var notBoxLastPg = new ProductionGroup { Alias = "NotBoxLast", SelectionAlgorithm = SelectionAlgorithmTypes.BoxFirst };
//            productionGroups.Clear();
//            productionGroups.Add(pg);
//            productionGroups.Add(pg2);
//            productionGroups.Add(notBoxLastPg);

//            #region Box last PG 1 Cartons

//            var pgCartons = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
//                    Producible = new Carton(){CustomerUniqueId =  "a1",
//                    ProductionGroupAlias = pg.Alias,
//                    Length = 11,
//                    Width = 21,
//                    Height = 31,
//                    ClassificationNumber = "1",
//                    CorrugateQuality = 1,
//                    CartonPropertyGroupAlias = "G1",
//                    PickZone = "AZ1",
//                    DesignId = 1337,
//                    Name = "BoxLastN1",
//                    ,
//                    StoredIsPriorityLabel = true
//                },
//                new BoxLastProducible
//                {
//                    Producible = new Carton(){CustomerUniqueId =  "a2",
//                    ProductionGroupAlias = pg.Alias,
//                    Length = 12,
//                    Width = 22,
//                    Height = 32,
//                    ClassificationNumber = "2",
//                    CorrugateQuality = 2,
//                    CartonPropertyGroupAlias = "G2",
//                    PickZone = "AZ2",
//                    DesignId = 1338,
//                    Name = "BoxLastN2",
//                    ,
//                    StoredIsPriorityLabel = true
//                },
//                new BoxLastProducible
//                {
//                    Producible = new Carton(){CustomerUniqueId =  "a3",
//                    ProductionGroupAlias = pg.Alias,
//                    Length = 13,
//                    Width = 23,
//                    Height = 33,
//                    ClassificationNumber = "3",
//                    CorrugateQuality = 3,
//                    CartonPropertyGroupAlias = "G3",
//                    PickZone = "AZ3",
//                    DesignId = 1339,
//                    Name = "BoxLastN3",
//                    ,
//                    StoredIsPriorityLabel = true
//                }
//            };

//            #endregion

//            #region Box last PG 2 Cartons

//            var pg2Cartons = new List<BoxLastProducible>
//            {
//                new BoxLastProducible
//                {
//                    Producible = new Carton(){CustomerUniqueId =  "b1",
//                    ProductionGroupAlias = pg2.Alias,
//                    Length = 11,
//                    Width = 21,
//                    Height = 31,
//                    ClassificationNumber = "1",
//                    CorrugateQuality = 1,
//                    CartonPropertyGroupAlias = "G1",
//                    PickZone = "AZ1",
//                    DesignId = 1337,
//                    Name = "BoxLastN1",
//                    ,
//                    StoredIsPriorityLabel = false
//                },
//                new BoxLastProducible
//                {
//                    Producible = new Carton(){CustomerUniqueId =  "b2",
//                    ProductionGroupAlias = pg2.Alias,
//                    Length = 12,
//                    Width = 22,
//                    Height = 32,
//                    ClassificationNumber = "2",
//                    CorrugateQuality = 2,
//                    CartonPropertyGroupAlias = "G2",
//                    PickZone = "AZ2",
//                    DesignId = 1338,
//                    Name = "BoxLastN2",
//                    ,
//                    StoredIsPriorityLabel = false
//                },
//                new BoxLastProducible
//                {
//                    Producible = new Carton(){CustomerUniqueId =  "b3",
//                    ProductionGroupAlias = pg2.Alias,
//                    Length = 13,
//                    Width = 23,
//                    Height = 33,
//                    ClassificationNumber = "3",
//                    CorrugateQuality = 3,
//                    CartonPropertyGroupAlias = "G3",
//                    PickZone = "AZ3",
//                    DesignId = 1339,
//                    Name = "BoxLastN3",
//                    ,
//                    StoredIsPriorityLabel = false
//                }
//            };

//            #endregion

//            requests.Clear();
//            requests.AddRange(pgCartons);
//            requests.AddRange(pg2Cartons);
//            BoxLastProducibleRepository.Setup(r => r.All()).Returns(requests);
//        }
//    }
//}