﻿using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.DTO.ScanToCreate;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Data.ScanToCreateProducible;
using PackNet.Services;
using PackNet.Services.SelectionAlgorithm.ScanToCreate;

using Testing.Specificity;

namespace ServicesTests.SelectionAlgorithm.ScanToCreate
{
    using System.Reactive.Linq;

    using PackNet.Common.Interfaces.DTO.Messaging;
    using PackNet.Common.Interfaces.ExtensionMethods;

    [TestClass]
    public class ScanToCreateSelectionAlgorithmServiceTests
    {
        ScanToCreateSelectionAlgorithmService service;
        IRestrictionResolverService restrictionResolver;

        Mock<IRestrictionResolverService> restrictionResolverMock;
        Mock<IPackagingDesignService> packagagingDesignMock;
        Mock<IScanToCreateRepository> repoMock;
        Mock<ICorrugateService> corrugateServiceMock;
        Mock<IProductionGroupService> productionGroupServiceMock;
        Mock<IMachineGroupService> machineGroupServiceMock;
        Mock<IUserNotificationService> userNotificationServiceMock;
        Mock<IServiceLocator> serviceLocatorMock;
        Mock<IUICommunicationService> uiCommunicationServiceMock;
        Mock<ILogger> loggerMock;

        readonly EventAggregator aggregator = new EventAggregator();

        private static ProductionGroup pg1;
        private static Guid pgId = new Guid("4e3f23c4-f326-44a5-bf1b-8af275122a47");
        private static MachineGroup mg1;
        private static Guid mgId = new Guid("4e3f23c4-f326-44a5-bf1b-8af275122a48");

        [TestInitialize]
        public void Setup()
        {
            restrictionResolver = GetRestrioctionResolverService(); //new RestrictionResolverService();
            restrictionResolverMock = new Mock<IRestrictionResolverService>();
            packagagingDesignMock = new Mock<IPackagingDesignService>();
            repoMock = new Mock<IScanToCreateRepository>();
            corrugateServiceMock = new Mock<ICorrugateService>();
            productionGroupServiceMock = GetProductionGroupMock();
            machineGroupServiceMock = new Mock<IMachineGroupService>();
            userNotificationServiceMock = new Mock<IUserNotificationService>();
            serviceLocatorMock = new Mock<IServiceLocator>();
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();
            loggerMock = new Mock<ILogger>();

            service = new ScanToCreateSelectionAlgorithmService(restrictionResolver, packagagingDesignMock.Object,
                repoMock.Object, aggregator, aggregator, corrugateServiceMock.Object, productionGroupServiceMock.Object,
                machineGroupServiceMock.Object, userNotificationServiceMock.Object, serviceLocatorMock.Object,
                uiCommunicationServiceMock.Object, loggerMock.Object);
        }

        private static IRestrictionResolverService GetRestrioctionResolverService()
        {
            var restrictionResolverService = new RestrictionResolverService();

            return restrictionResolverService;
        }

        private static Mock<IProductionGroupService> GetProductionGroupMock()
        {
            var mock = new Mock<IProductionGroupService>();

            mg1 = new MachineGroup
            {
                Id = Guid.NewGuid(),
            };

            mg1.AddOrUpdateCapabilities(new List<ICapability> { new ProductionGroupSpecificCapability(pgId) });

            pg1 = new ProductionGroup()
            {
                Id = pgId,
                SelectionAlgorithm = SelectionAlgorithmTypes.ScanToCreate,
                ConfiguredMachineGroups = new ConcurrentList<Guid>() { mg1.Id }
            };

            mock.Setup(m => m.GetProductionGroupForMachineGroup(It.IsAny<Guid>())).Returns(pg1);
            mock.Setup(m => m.ProductionGroups).Returns(new List<ProductionGroup>() { pg1 });

            return mock;
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldBeAbleToRemoveScanToCreateProducibles()
        {
            var producibles = new List<ScanToCreateProducible>()
            {
                new ScanToCreateProducible
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = new Carton
                    {
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    }}};

            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));

            producibles.ForEach(service.AddToProductionGroupQueue);

            var foundJob = service.GetNextJobForMachineGroup(mg1);

            Specify.That(foundJob).Should.Not.BeNull();

            repoMock.Setup(x => x.FindByIdOrSubId(producibles[0].Id))
                .Returns(new ScanToCreateProducible { CustomerUniqueId = "111", Id = producibles[0].Id });

            service.RemoveProducibles(producibles, "mango");

            foundJob = service.GetNextJobForMachineGroup(mg1);

            Specify.That(foundJob).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldBeAbleToRemoveScanToCreateProducibles_OfTypeKit()
        {
            var kit = new Kit { CustomerUniqueId = "111" };
            kit.AddProducible(new Order(new Carton
            {
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                CartonOnCorrugate = new CartonOnCorrugate()
            }, 10));
            var stcProducible = new ScanToCreateProducible
                                {
                                    CustomerUniqueId = "111",
                                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                                    Producible = kit
                                };
            var producibles = new List<ScanToCreateProducible>() { stcProducible };

            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));

            producibles.ForEach(service.AddToProductionGroupQueue);

            var foundJob = service.GetNextJobForMachineGroup(mg1);

            Specify.That(foundJob).Should.Not.BeNull();

            repoMock.Setup(x => x.FindByIdOrSubId(producibles[0].Id))
                .Returns(new ScanToCreateProducible { CustomerUniqueId = "111", Id = producibles[0].Id });

            service.RemoveProducibles(producibles, "mango");

            foundJob = service.GetNextJobForMachineGroup(mg1);

            Specify.That(foundJob).Should.BeNull();
            Specify.That(stcProducible.ProducibleStatus).Should.BeLogicallyEqualTo(ProducibleStatuses.ProducibleRemoved);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldTriggerRestageWhenUpdatinngProductionGroup()
        {
            bool called = false;
            IDisposable subs = null;
            subs = aggregator.GetEvent<ImportMessage<IEnumerable<IProducible>>>().Where(msg=> msg.MessageType == MessageTypes.Restaged).DurableSubscribe(
                message =>
                {
                    called = true;
                    subs.Dispose();
                });
            var producibles = new List<ScanToCreateProducible>()
            {
                new ScanToCreateProducible
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = new Carton
                    {
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    }},
                new ScanToCreateProducible
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = new Carton
                    {
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    }}
            };
            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(service.AddToProductionGroupQueue);

            aggregator.Publish(new Message<ProductionGroup> { MessageType = ProductionGroupMessages.ProductionGroupChanged, Data = pg1});

            Retry.For(() => called, TimeSpan.FromSeconds(5));
            Specify.That(called).Should.BeTrue("Expected restage after pg update");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldProduceKitWithScanToCreate()
        {
            var carton = new Carton { ArticleId = "a carton", DesignId = 201, Length = 5, Width = 5, Height = 5, CartonOnCorrugate = new CartonOnCorrugate { TileCount = 1 } };
            var order = new Order(carton, 2);
            var kit = new Kit();
            kit.AddProducible(order);
            var producible = new ScanToCreateProducible { Producible = kit };
            var producibles = new List<ScanToCreateProducible> { producible };

            producibles.ForEach(p => p.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged);
            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(service.AddToProductionGroupQueue);

            for (int i = 0; i < 2; i++)
            {
                var foundJob = service.GetNextJobForMachineGroup(mg1);
                Specify.That(foundJob).Should.Not.BeNull();
                foundJob.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            }

            var noJob = service.GetNextJobForMachineGroup(mg1);
            Specify.That(noJob).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotDistrubuteOrders_BetweenMachineGroups_WhenUsingSTC()
        {
            var carton = new Carton { ArticleId = "a carton", DesignId = 201, Length = 5, Width = 5, Height = 5, CartonOnCorrugate = new CartonOnCorrugate { TileCount = 1 } };
            var order = new Order(carton, 1);
            var order2 = new Order(carton, 2);
            var kit = new Kit();
            kit.AddProducible(order);
            kit.AddProducible(order2);
            var producible = new ScanToCreateProducible { Producible = kit };
            var producibles = new List<ScanToCreateProducible> { producible };

            producibles.ForEach(p => p.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged);
            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(service.AddToProductionGroupQueue);
            var mg2 = new MachineGroup(){Id = Guid.NewGuid()};
            mg2.AddOrUpdateCapabilities(new List<ICapability> { new ProductionGroupSpecificCapability(pgId) });
            pg1.ConfiguredMachineGroups.Add(mg2.Id);

            var foundJob = service.GetNextJobForMachineGroup(mg1);
            Specify.That(foundJob).Should.Not.BeNull("Should get first job in the order");

            var noJob = service.GetNextJobForMachineGroup(mg2);
            Specify.That(noJob).Should.BeNull("Should not get the second job from the order for mg2");

            foundJob = service.GetNextJobForMachineGroup(mg1);
            Specify.That(foundJob).Should.Not.BeNull("Should get the second job from the order for mg1");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotDistrubuteOrders_BetweenMachineGroups_WhenUsingSTC_ButOtherMGsShouldStill_GetAvailableOrders()
        {
            var carton = new Carton { CustomerUniqueId = "a carton", DesignId = 201, Length = 5, Width = 5, Height = 5, CartonOnCorrugate = new CartonOnCorrugate { TileCount = 1 } };
            
            var order = new Order(carton, 1);
            var kit = new Kit();
            kit.AddProducible(order);
            var producible1 = new ScanToCreateProducible { Producible = kit };



            var carton2 = new Carton { CustomerUniqueId = "another carton", DesignId = 201, Length = 5, Width = 5, Height = 5, CartonOnCorrugate = new CartonOnCorrugate { TileCount = 1 } };
            var kit2 = new Kit();
            var order2 = new Order(carton2, 2);
            kit2.AddProducible(order2);
            var producible2 = new ScanToCreateProducible { Producible = kit2 };

            var producibles = new List<ScanToCreateProducible> { producible1, producible2 };

            producibles.ForEach(p => p.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged);
            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(service.AddToProductionGroupQueue);
            var mg2 = new MachineGroup() { Id = Guid.NewGuid() };
            mg2.AddOrUpdateCapabilities(new List<ICapability> { new ProductionGroupSpecificCapability(pgId) });
            pg1.ConfiguredMachineGroups.Add(mg2.Id);

            var foundJob = service.GetNextJobForMachineGroup(mg1);
            Specify.That(foundJob).Should.Not.BeNull("Should get an order for mg1");
            Specify.That(foundJob.CustomerUniqueId.Contains(carton.CustomerUniqueId) 
                || foundJob.CustomerUniqueId.Contains(carton2.CustomerUniqueId)).Should.BeTrue(foundJob.CustomerUniqueId,"Should get one of the orders we have. It doesn't matter which one." );
            
            var jobForMg2 = service.GetNextJobForMachineGroup(mg2);
            Specify.That(jobForMg2).Should.Not.BeNull("Should get the second order for mg2");
            Specify.That(jobForMg2.CustomerUniqueId.Contains(carton.CustomerUniqueId) || jobForMg2.CustomerUniqueId.Contains(carton2.CustomerUniqueId))
                .Should.BeTrue("Should get one of the orders we have. But not the one we got for mg1");
            Specify.That(jobForMg2.CustomerUniqueId).Should.Not.Contain(foundJob.CustomerUniqueId, "CUID should not be the same as the previous order");
            
            foundJob.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            foundJob = service.GetNextJobForMachineGroup(mg1);
            Specify.That(foundJob).Should.BeNull("Should not get another order for mg1");
        }
        
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateRemainingQuantityForScanToCreateProducible_WhenAllProduciblesAreComplete()
        {
            var carton = new Carton { ArticleId = "a carton", DesignId = 201, Length = 5, Width = 5, Height = 5, CartonOnCorrugate = new CartonOnCorrugate { TileCount = 1 } };
            var order = new Order(carton, 2);
            var kit = new Kit();
            kit.AddProducible(order);
            var producible = new ScanToCreateProducible { Producible = kit };
            var producibles = new List<ScanToCreateProducible> { producible };

            producibles.ForEach(p => p.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged);
            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(service.AddToProductionGroupQueue);

            for (int i = 0; i < 2; i++)
            {
                var foundJob = service.GetNextJobForMachineGroup(mg1);
                Specify.That(foundJob).Should.Not.BeNull();
                Specify.That(producible.RemainingQuantity).Should.BeEqualTo(1);
                foundJob.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            }

            var noJob = service.GetNextJobForMachineGroup(mg1);
            Specify.That(producible.RemainingQuantity).Should.BeEqualTo(0);
            Specify.That(noJob).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldBeAbleToRemoveMultipleScanToCreateProducibles()
        {
            var producibles = new List<ScanToCreateProducible>()
            {
                new ScanToCreateProducible
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = new Carton
                    {
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    }},
                new ScanToCreateProducible
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = new Carton
                    {
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    }}
            };

            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));

            producibles.ForEach(service.AddToProductionGroupQueue);

            var foundJob = service.GetNextJobForMachineGroup(mg1);

            Specify.That(foundJob).Should.Not.BeNull();

            repoMock.Setup(x => x.FindByIdOrSubId(producibles[0].Id))
                .Returns(new ScanToCreateProducible { CustomerUniqueId = "111", Id = producibles[0].Id });
            repoMock.Setup(x => x.FindByIdOrSubId(producibles[1].Id))
                .Returns(new ScanToCreateProducible { CustomerUniqueId = "111", Id = producibles[1].Id });

            service.RemoveProducibles(producibles, "mango");

            foundJob = service.GetNextJobForMachineGroup(mg1);

            Specify.That(foundJob).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishOrderListWhenProducibleIsAdded()
        {
            var lst = new List<ScanToCreateProducible>();
            
            uiCommunicationServiceMock.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<ScanToCreateProducible>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<ScanToCreateProducible>>, bool, bool>((msg, pubInternal, b) => 
                    lst = msg.Data.ToList()
                );

            var p1 = new ScanToCreateProducible(new Carton(), 1) {OrderId = "1"};
            p1.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id));
            var p2 = new ScanToCreateProducible(new Carton(), 1) { OrderId = "2" };
            p2.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id));
            var p3 = new ScanToCreateProducible(new Carton(), 1) { OrderId = "3" };
            p3.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id));

            service.AddToProductionGroupQueue(p1);

            Specify.That(lst.Count).Should.BeEqualTo(1, "Should have sent message with list containing first order");
            Specify.That(lst.ElementAt(0).OrderId).Should.BeEqualTo("1", "First order should the first added, since nothing is produced yet");

            service.AddToProductionGroupQueue(p2);

            Specify.That(lst.Count).Should.BeEqualTo(2, "Should have sent message with list containing first and second order");
            Specify.That(lst.ElementAt(1).OrderId).Should.BeEqualTo("2", "Second order should the second added, since nothing is produced yet");

            p1.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            service.AddToProductionGroupQueue(p3);

            Specify.That(lst.Count).Should.BeEqualTo(2, "Should have sent message with list containing second and third order");
            Specify.That(lst.ElementAt(1).OrderId).Should.BeEqualTo("3", "Second order should be the third added, since the first added is completed");
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 10865")]
        public void ShouldPublishNotificationToUiWhenAllProduciblesAreSkippedDueToMissingCapabilities()
        {

            service = new ScanToCreateSelectionAlgorithmService(restrictionResolverMock.Object, packagagingDesignMock.Object,
                repoMock.Object, aggregator, aggregator, corrugateServiceMock.Object, productionGroupServiceMock.Object,
                machineGroupServiceMock.Object, userNotificationServiceMock.Object, serviceLocatorMock.Object,
                uiCommunicationServiceMock.Object, loggerMock.Object);

            restrictionResolverMock.Setup(m => m.Resolve(It.IsAny<IRestriction>(), It.IsAny<IEnumerable<ICapability>>()))
                .Returns(false);

            var producibles = new List<ScanToCreateProducible>()
            {
                new ScanToCreateProducible()
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = new Carton()
                    {
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    }
                },
                new ScanToCreateProducible()
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                    Producible = new Carton()
                    {
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    }
                }
            };

            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));

            producibles.ForEach(service.AddToProductionGroupQueue);

            var producible = service.GetNextJobForMachineGroup(mg1);

            Specify.That(producible).Should.BeNull("A producible was returned even though that the Machine Group does not satisfy the restrictions");
            userNotificationServiceMock.Verify(m => m.SendNotificationToMachineGroup(NotificationSeverity.Warning,
                It.Is<string>(s => s.Contains("Failed to produce 2 producibles because of failed restrictions:")), string.Empty, mg1.Id));
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 15657")]
        public void ShouldNotFailProducibleWhenItIsRemoved()
        {

            var carton = new Carton { ArticleId = "a carton", DesignId = 201, Length = 5, Width = 5, Height = 5, CartonOnCorrugate = new CartonOnCorrugate { TileCount = 1 } };
            var order = new Order(carton, 2);
            var kit = new Kit();
            kit.AddProducible(order);
            var producible = new ScanToCreateProducible { Producible = kit };
            var producibles = new List<ScanToCreateProducible> { producible };

            producibles.ForEach(p => p.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged);
            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(p => p.Producible.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));
            producibles.ForEach(service.AddToProductionGroupQueue);


            var foundJob = service.GetNextJobForMachineGroup(mg1);

            Specify.That(foundJob).Should.Not.BeNull("A producible should be returned");
            Specify.That(foundJob.ProducibleStatus).Should.BeLogicallyEqualTo(NotInProductionProducibleStatuses.AddedToMachineGroupQueue, "The job should be added to the queue");
            foundJob.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;
            Specify.That(kit.ProducibleStatus).Should.Not.BeEqualTo(ErrorProducibleStatuses.ProducibleFailed, "The kit should not fail because one producible is removed from the queue, like when the machine pauses");
            Retry.For(() => foundJob.ProducibleStatus == ProducibleStatuses.ProducibleRemoved, TimeSpan.FromSeconds(5));
            Specify.That(foundJob.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleRemoved, "When setting the status to removed we expect to be set to removed.");
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 10880")]
        public void ScanToCreateItemsWithoutCartonOnCorrugateShouldBeSetToNotProducible()
        {
            Assert.Inconclusive("todo");
            //todo: this should call the staging algorithm to test.
            var producibles = new List<ScanToCreateProducible>()
            {
                new ScanToCreateProducible()
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported,
                    Producible = new Carton()
                    {
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported
                    }
                },
                new ScanToCreateProducible()
                {
                    ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported,
                    Producible = new Carton()
                    {
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleImported,
                        CartonOnCorrugate = new CartonOnCorrugate()
                    }
                }
            };

            producibles.ForEach(p => p.Restrictions.Add(new ProductionGroupSpecificRestriction(pg1.Id)));


            producibles.ForEach(service.AddToProductionGroupQueue);

            Retry.For(() => producibles.Any(p => p.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleImported) == false, TimeSpan.FromSeconds(1));
            Specify.That(producibles.ElementAt(0).ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible);
            Specify.That(producibles.ElementAt(0).Producible.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible);
            Specify.That(producibles.ElementAt(1).ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(producibles.ElementAt(1).Producible.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
        }
    }
}
