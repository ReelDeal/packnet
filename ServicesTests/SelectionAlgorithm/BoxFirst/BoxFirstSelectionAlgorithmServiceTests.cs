﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;

using Microsoft.Reactive.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MongoDB.Bson;
using MongoDB.Driver;

using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.CartonPropertyGroups;
using PackNet.Common.Interfaces.DTO.Classifications;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.CanProduce;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Data.BoxFirstProducible;
using PackNet.Services.SelectionAlgorithm.BoxFirst;

using Testing.Specificity;

using TestUtils;

using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

namespace ServicesTests.SelectionAlgorithm.BoxFirst
{
    using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;

    [TestClass]
    public class BoxFirstSelectionAlgorithmServiceTests
    {
        private List<IMachine> machines;
        private List<MachineGroup> machineGroups; 
        private List<Corrugate> corrugates;
        private Dictionary<Guid, string> machineIdsAndAliases = new Dictionary<Guid, string>();

        [TestInitialize]
        public void Setup()
        {
            machines = new List<IMachine>();
            corrugates = new List<Corrugate>();
            machineGroups = new List<MachineGroup>();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldResetJobSentToMachineAndMachineGroup_OnStartup()
        {
            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var productionGroupService = GetProductionGroupServiceMock().Object;
            var corrugateService = GetCorrugateServiceMock().Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, productionGroupService, corrugateService);
            var aggregator = new EventAggregator();

            var invalidStartupStatuses = new List<ProducibleStatuses>
                                         {
                                             NotInProductionProducibleStatuses.ProducibleSelected,
                                             InProductionProducibleStatuses.ProducibleSentToMachineGroup,
                                             InProductionProducibleStatuses.ProducibleSentToMachine,
                                             InProductionProducibleStatuses.ProducibleProductionStarted,
                                         };

            var p1 = new BoxFirstProducible() { ProducibleStatus = NotInProductionProducibleStatuses.ProducibleSelected, Producible = new Producible()};
            var p2 = new BoxFirstProducible() { ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachineGroup, Producible = new Producible() };
            var p3 = new BoxFirstProducible() { ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine, Producible = new Producible() };
            var p4 = new BoxFirstProducible() { ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted, Producible = new Producible() };

            SetAggregator(aggregator, serviceLocator);
            var repo = GetBoxFirstRepositoryMock(new List<BoxFirstProducible>(){p1, p2, p3, p4}).Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator.Object, repo);

            Thread.Sleep(TimeSpan.FromSeconds(8));
            var invalidProducibles = service.GetAllProduciblesWithStatuses(invalidStartupStatuses);
            var validProducibles = service.GetAllProduciblesWithStatuses(new List<ProducibleStatuses>() { NotInProductionProducibleStatuses.ProducibleStaged });

            Specify.That(invalidProducibles.Count()).Should.BeLogicallyEqualTo(0, "Statuses were not reset at startup");
            Specify.That(validProducibles.Count()).Should.BeLogicallyEqualTo(4, "Missing some producibles");
            Specify.That(validProducibles.All(p => !invalidStartupStatuses.Contains(p.ProducibleStatus))).Should.BeTrue("Valid producibles still contains invalid statuses");
            Specify.That(validProducibles.All(p => p.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleStaged)).Should.BeTrue("All producibles should be staged");
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 10595")]
        public void ShouldBeAbleToRemoveProduciblesBasedOfOfCustomerUniqueId()
        {
            var deleteCalled = false;
            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var productionGroupService = GetProductionGroupServiceMock().Object;
            var corrugateService = GetCorrugateServiceMock().Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, productionGroupService, corrugateService);
            var aggregator = new EventAggregator();

            var p1 = new BoxFirstProducible()
            {
                Id = Guid.NewGuid(),
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleSelected,
                Producible = new Producible() { CustomerUniqueId = "1" }
            };
            var p2 = new BoxFirstProducible()
            {
                Id = Guid.NewGuid(),
                CustomerUniqueId = "2",
                ProducibleStatus = ProducibleStatuses.ProducibleCompleted,
                Producible = new Producible() { CustomerUniqueId = "2" }
            };
            var p3 = new BoxFirstProducible()
            {
                Id = Guid.NewGuid(),
                CustomerUniqueId = "3",
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                Producible = new Producible() { CustomerUniqueId = "3" }
            };
            var p4 = new BoxFirstProducible()
            {
                Id = Guid.NewGuid(),
                CustomerUniqueId = "4",
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                Producible = new Producible() { CustomerUniqueId = "4" }
            };

            SetAggregator(aggregator, serviceLocator);
            var repo = GetBoxFirstRepositoryMock(new List<BoxFirstProducible>() { p1, p2, p3, p4 });

            repo.Setup(m => m.Delete(It.IsAny<IEnumerable<Guid>>())).Callback<IEnumerable<Guid>>(
                bfp =>
                {
                    Specify.That(bfp.Count()).Should.BeEqualTo(2);
                    Specify.That(bfp.Count(b => b == p3.Id)).Should.BeEqualTo(1);
                    Specify.That(bfp.Count(b => b == p4.Id)).Should.BeEqualTo(1);
                    deleteCalled = true;
                });

            var service = new BoxFirstSelectionAlgorithmService(serviceLocator.Object, repo.Object);

            aggregator.Publish(new Message<IEnumerable<string>>()
            {
                MessageType = CartonMessages.RemoveBoxFirstCartons,
                Data = new List<string>() { "1", "3", "4" }
            });

            Retry.For(() => deleteCalled, TimeSpan.FromSeconds(1));
            Specify.That(deleteCalled).Should.BeTrue();

            Specify.That(service.GetProducibles().Any(p => p.CustomerUniqueId == "3")).Should.BeFalse("The correct element was not removed");
            Specify.That(service.GetProducibles().Any(p => p.CustomerUniqueId == "4")).Should.BeFalse("The correct element was not removed");
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 10595")]
        [TestCategory("Bug 10568")]
        public void ShouldNotAddNewItemsToRepositoryWhenUpdatingStatusOfProducibles()
        {
            var createCalled = false;
            var updateCalled = false;

            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var productionGroupService = GetProductionGroupServiceMock().Object;
            var corrugateService = GetCorrugateServiceMock().Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, productionGroupService, corrugateService);
            var aggregator = new EventAggregator();

            var p1 = new BoxFirstProducible()
            {
                Id = Guid.NewGuid(),
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleSelected,
                Producible = new Producible() { CustomerUniqueId = "1" }
            };
            var p2 = new BoxFirstProducible()
            {
                Id = Guid.NewGuid(),
                CustomerUniqueId = "2",
                ProducibleStatus = ProducibleStatuses.ProducibleCompleted,
                Producible = new Producible() { CustomerUniqueId = "2" }
            };
            var p3 = new BoxFirstProducible()
            {
                Id = Guid.NewGuid(),
                CustomerUniqueId = "3",
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                Producible = new Producible() { CustomerUniqueId = "3" }
            };
            var p4 = new BoxFirstProducible()
            {
                Id = Guid.NewGuid(),
                CustomerUniqueId = "4",
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                Producible = new Producible() { CustomerUniqueId = "4" }
            };

            SetAggregator(aggregator, serviceLocator);
            var repo = GetBoxFirstRepositoryMock(new List<BoxFirstProducible>() { p1, p2, p3, p4 });

            repo.Setup(m => m.Create(It.IsAny<BoxFirstProducible>())).Callback<BoxFirstProducible>(
                bfp =>
                {
                    createCalled = true;
                });

            repo.Setup(m => m.Create(It.IsAny<IEnumerable<BoxFirstProducible>>())).Callback<IEnumerable<BoxFirstProducible>>(
                bfp =>
                {
                    createCalled = true;
                });

            repo.Setup(m => m.Update(It.IsAny<IEnumerable<BoxFirstProducible>>())).Callback<IEnumerable<BoxFirstProducible>>(
                bfps =>
                {
                    Specify.That(bfps.Count()).Should.BeEqualTo(1);

                    var p = bfps.First();
                    Specify.That(p.Id == p4.Id).Should.BeTrue();
                    Specify.That(p.ProducibleStatus == ProducibleStatuses.ProducibleCompleted).Should.BeTrue();

                    updateCalled = true;
                });

            var service = new BoxFirstSelectionAlgorithmService(serviceLocator.Object, repo.Object);

            service.UpdateProducibleStatus(new BoxFirstProducible() { Id = Guid.NewGuid() }, ProducibleStatuses.ProducibleCompleted);

            Specify.That(createCalled).Should.BeFalse();

            createCalled = false;
            updateCalled = false;

            service.UpdateProducibleStatuses(new List<BoxFirstProducible>() { new BoxFirstProducible() { Id = Guid.NewGuid(), CustomerUniqueId = "5" }, p4 }, ProducibleStatuses.ProducibleCompleted);
            Specify.That(createCalled).Should.BeFalse();
            Specify.That(updateCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRequestStaging_OnStartup()
        {
            var callCount = 0;

            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var productionGroupService = GetProductionGroupServiceMock().Object;
            var corrugateService = GetCorrugateServiceMock().Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, productionGroupService, corrugateService);
            var aggregator = new EventAggregator();

            var testScheduler = new TestScheduler();
            aggregator.Scheduler = testScheduler;

            SetAggregator(aggregator, serviceLocator);

            aggregator.GetEvent<ImportMessage<IEnumerable<IProducible>>>().Subscribe(msg
               =>
            {
                Specify.That(msg.MessageType).Should.BeEqualTo(MessageTypes.Restaged);
                Specify.That(msg.Data).Should.BeEmpty();;
                callCount++;
            });

            var repo = GetBoxFirstRepositoryMock().Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator.Object, repo);
            Thread.Sleep(TimeSpan.FromSeconds(8));

            testScheduler.AdvanceBy(TimeSpan.FromMilliseconds(100).Ticks);
            Specify.That(callCount).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [Ignore]//"This test is non-deterministic"
        public void ShouldBufferOnMachineProductionCompletedEvents_AndPublishProducibleCountToCPGService_AtSetTimeInterval()
        {
            var callCount = 0;

            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = Guid.NewGuid() };
            var cpg2 = new CartonPropertyGroup() { Alias = "Az2", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() {cpg, cpg2};
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object);
            var aggregator = new EventAggregator();

            var testScheduler = new TestScheduler();
            aggregator.Scheduler = testScheduler;

            SetAggregator(aggregator, serviceLocator);


            var p1 = GetBoxFirstProducibleCarton("1", new List<int>() { 1 }, cpg, machineIdsAndAliases);
            var p2 = GetBoxFirstProducibleCarton("1", new List<int>() { 1 }, cpg, machineIdsAndAliases);
            var p3 = GetBoxFirstProducibleCarton("1", new List<int>() { 1 }, cpg2, machineIdsAndAliases);
            var p4 = GetBoxFirstProducibleCarton("1", new List<int>() { 1 }, cpg2, machineIdsAndAliases, ProducibleStatuses.ProducibleCompleted);
            var p5 = GetBoxFirstProducibleCarton("1", new List<int>() { 1 }, cpg2, machineIdsAndAliases, ProducibleStatuses.ProducibleRemoved);
            var p6 = GetBoxFirstProducibleCarton("1", new List<int>() { 1 }, cpg2, machineIdsAndAliases, ErrorProducibleStatuses.ProducibleFailed);
            var producibles = new List<BoxFirstProducible>() { p1, p2, p3, p4, p5, p6 };
            var repo = GetBoxFirstRepositoryMock(producibles).Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator.Object, repo);
            
            aggregator.GetEvent<IMessage<IEnumerable<BoxFirstProducible>>>().Subscribe(msg
                =>
            {
                Specify.That(msg.MessageType).Should.BeEqualTo(StagingMessages.ProduciblesStaged);
                Specify.That(msg.Data.Count()).Should.BeEqualTo(3);
                Specify.That(msg.Data.ElementAt(0).Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>().Id).Should.BeEqualTo(cpg.Id);
                Specify.That(msg.Data.ElementAt(1).Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>().Id).Should.BeEqualTo(cpg.Id);
                Specify.That(msg.Data.ElementAt(2).Restrictions.GetRestrictionOfTypeInBasicRestriction<CartonPropertyGroup>().Id).Should.BeEqualTo(cpg2.Id);
                callCount++;
            });

            for (int i = 0; i < 4999; i++)
            {
                aggregator.Publish(new Message<IProducible>()
                {
                    MessageType = MessageTypes.ProducibleStatusChanged,
                });

                testScheduler.AdvanceTo(TimeSpan.FromMilliseconds(i).Ticks);
                Specify.That(callCount).Should.BeEqualTo(0);
            }

            testScheduler.AdvanceTo(TimeSpan.FromMilliseconds(5001).Ticks);
            Specify.That(callCount).Should.BeEqualTo(1);
            testScheduler.AdvanceTo(TimeSpan.FromMilliseconds(100001).Ticks);
            Specify.That(callCount).Should.BeEqualTo(1);
            
            aggregator.Publish(new Message<IProducible>()
            {
                MessageType = MessageTypes.ProducibleStatusChanged,
            });
            testScheduler.AdvanceTo(TimeSpan.FromMilliseconds(150001).Ticks);
            Specify.That(callCount).Should.BeEqualTo(2);
            testScheduler.AdvanceTo(TimeSpan.FromMilliseconds(200001).Ticks);
            Specify.That(callCount).Should.BeEqualTo(2);

            aggregator.Publish(new Message<IProducible>()
            {
                MessageType = MessageTypes.ProducibleStatusChanged,
            });
            testScheduler.AdvanceTo(TimeSpan.FromMilliseconds(250001).Ticks);
            Specify.That(callCount).Should.BeEqualTo(3);
        }

        private void SetAggregator(EventAggregator aggregator, Mock<IServiceLocator> serviceLocator)
        {
            serviceLocator.Setup(m => m.Locate<IEventAggregatorPublisher>()).Returns(aggregator);
            serviceLocator.Setup(m => m.Locate<IEventAggregatorSubscriber>()).Returns(aggregator);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSelectTiledJobIfPossible()
        {
            CreateCorugateWithId(1, 300d);
            CreateCorugateWithId(2, 600d);
            CreateCorugateWithId(3, 900d);

            var m1 = CreateEmMachineWithId(1, GetCorrugateWithId(1), GetCorrugateWithId(2));
            var m2 = CreateEmMachineWithId(2, GetCorrugateWithId(3));
            var machines = new List<IMachine>() { m1, m2 };

            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = new Guid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object).Object;

            var p1 = GetBoxFirstProducibleCarton("1", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            var p2 = GetBoxFirstProducibleCarton("2", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            var pList = new[] { p1, p2 };

            var repo = GetBoxFirstRepositoryMock(pList).Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            service.UpdateProducibleStatus(p1, NotInProductionProducibleStatuses.ProducibleStaged);
            service.UpdateProducibleStatus(p2, NotInProductionProducibleStatuses.ProducibleStaged);

            var mg = GetMachineGroup(Guid.Empty, machines.Select(m => m.Id));
            var first = service.PrepareNextJobForMachineGroup(mg, pList);

            Specify.That(first).Should.Not.BeNull();
            var firstBox = first as Kit;
            Specify.That((firstBox.ItemsToProduce.First() as ICarton).CartonOnCorrugate.TileCount).Should.BeEqualTo(2);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleProducibleStatusUpdatesProperty_TiledCartonFirstLabelOnPrinterThenLabelStatusSetToRemoved()
        {
            CreateCorugateWithId(1, 300d);
            CreateCorugateWithId(2, 600d);
            CreateCorugateWithId(3, 900d);

            var m1 = CreateEmMachineWithId(1, GetCorrugateWithId(1), GetCorrugateWithId(2));
            var m2 = CreateEmMachineWithId(2, GetCorrugateWithId(3));
            var machines = new List<IMachine>() { m1, m2 };

            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = new Guid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object).Object;

            var p1 = GetBoxFirstProducibleCartonWithLabel("1", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            var p2 = GetBoxFirstProducibleCartonWithLabel("2", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            p1.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachineGroup;//set this status so we know when the load from the db is finished after the service ctor.
            var pList = new[] { p1, p2 };

            var repo = GetBoxFirstRepositoryMock(pList);
            //repo.Setup(r => r.Create(It.IsAny<BoxFirstProducible>())).Callback<BoxFirstProducible>(bf =>
            //{

            //});
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo.Object);

            //wait until jobs from database set to imported
            //Retry.UntilTrue(() => p1.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleImported, TimeSpan.FromSeconds(30));

            service.UpdateProducibleStatus(p1, NotInProductionProducibleStatuses.ProducibleStaged);
            service.UpdateProducibleStatus(p2, NotInProductionProducibleStatuses.ProducibleStaged);

            var mg = GetMachineGroup(Guid.Empty, machines.Select(m => m.Id));
            var first = service.PrepareNextJobForMachineGroup(mg, pList);

            Specify.That(first).Should.Not.BeNull();
            var firstkit = first as Kit;
            var tiledCarton = firstkit.ItemsToProduce[0] as ICarton;
            var label1 = firstkit.ItemsToProduce[1] as Label;
            var label2 = firstkit.ItemsToProduce[2] as Label;
            Specify.That(tiledCarton.CartonOnCorrugate.TileCount).Should.BeEqualTo(2);

            //do the test:
            label1.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            label1.ProducibleStatus = ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;
            //wait until first job is sent to machine.
            Retry.UntilTrue(() => firstkit.ProducibleStatus is InProductionProducibleStatuses, TimeSpan.FromSeconds(10));

            //tiledCarton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            label1.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;// if the machine went offline we would remove it from the queue.

            Retry.UntilTrue(() => label1.ProducibleStatus == ProducibleStatuses.ProducibleCompleted, TimeSpan.FromSeconds(10));
            Specify.That(p1.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
            Specify.That(p2.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(label1.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
            Specify.That(label2.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(label2.History.Any(h => h.PreviousStatus == ProducibleStatuses.ProducibleRemoved)).Should.BeTrue();

        }
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleProducibleStatusUpdatesProperty_TiledCartonFirstLabelOnPrinterSecondLabelRemoved()
        {
            CreateCorugateWithId(1, 300d);
            CreateCorugateWithId(2, 600d);
            CreateCorugateWithId(3, 900d);

            var m1 = CreateEmMachineWithId(1, GetCorrugateWithId(1), GetCorrugateWithId(2));
            var m2 = CreateEmMachineWithId(2, GetCorrugateWithId(3));
            var machines = new List<IMachine>() { m1, m2 };

            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = new Guid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object).Object;

            var p1 = GetBoxFirstProducibleCartonWithLabel("1", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            var p2 = GetBoxFirstProducibleCartonWithLabel("2", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            p1.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachineGroup;//set this status so we know when the load from the db is finished after the service ctor.
            var pList = new[] { p1, p2 };

            var repo = GetBoxFirstRepositoryMock(pList);
            //repo.Setup(r => r.Create(It.IsAny<BoxFirstProducible>())).Callback<BoxFirstProducible>(bf =>
            //{

            //});
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo.Object);

            //wait until jobs from database set to imported
            //Retry.UntilTrue(() => p1.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleImported, TimeSpan.FromSeconds(30));

            service.UpdateProducibleStatus(p1, NotInProductionProducibleStatuses.ProducibleStaged);
            service.UpdateProducibleStatus(p2, NotInProductionProducibleStatuses.ProducibleStaged);

            var mg = GetMachineGroup(Guid.Empty, machines.Select(m => m.Id));
            var first = service.PrepareNextJobForMachineGroup(mg, pList);

            Specify.That(first).Should.Not.BeNull();
            var firstkit = first as Kit;
            var tiledCarton = firstkit.ItemsToProduce[0] as ICarton;
            var label1 = firstkit.ItemsToProduce[1] as Label;
            var label2 = firstkit.ItemsToProduce[2] as Label;
            Specify.That(tiledCarton.CartonOnCorrugate.TileCount).Should.BeEqualTo(2);

            //do the test:
            tiledCarton.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            label1.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            label1.ProducibleStatus = ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;
            //wait until first job is sent to machine.
            Retry.UntilTrue(() => firstkit.ProducibleStatus == InProductionProducibleStatuses.ProducibleSentToMachine, TimeSpan.FromSeconds(10));

            //tiledCarton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            label2.ProducibleStatus = ProducibleStatuses.ProducibleRemoved;// if the machine went offline we would remove it from the queue.

            Retry.UntilTrue(() => label1.ProducibleStatus == ProducibleStatuses.ProducibleCompleted, TimeSpan.FromSeconds(10));
            Specify.That(p1.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
            Specify.That(p2.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(label1.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
            Specify.That(label2.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(label2.History.Any(h => h.PreviousStatus == ProducibleStatuses.ProducibleRemoved)).Should.BeTrue();

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleProducibleStatusUpdatesProperty_TiledCartonFirstLabelOnPrinterSecondLabelFailed()
        {
            CreateCorugateWithId(1, 300d);
            CreateCorugateWithId(2, 600d);
            CreateCorugateWithId(3, 900d);

            var m1 = CreateEmMachineWithId(1, GetCorrugateWithId(1), GetCorrugateWithId(2));
            var m2 = CreateEmMachineWithId(2, GetCorrugateWithId(3));
            var machines = new List<IMachine>() { m1, m2 };

            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = new Guid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object).Object;

            var p1 = GetBoxFirstProducibleCartonWithLabel("1", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            var p2 = GetBoxFirstProducibleCartonWithLabel("2", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            p1.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachineGroup;//set this status so we know when the load from the db is finished after the service ctor.
            var pList = new[] { p1, p2 };

            var repo = GetBoxFirstRepositoryMock(pList);
            //repo.Setup(r => r.Create(It.IsAny<BoxFirstProducible>())).Callback<BoxFirstProducible>(bf =>
            //{

            //});
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo.Object);

            //wait until jobs from database set to imported
            //Retry.UntilTrue(() => p1.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleImported, TimeSpan.FromSeconds(30));

            service.UpdateProducibleStatus(p1, NotInProductionProducibleStatuses.ProducibleStaged);
            service.UpdateProducibleStatus(p2, NotInProductionProducibleStatuses.ProducibleStaged);

            var mg = GetMachineGroup(Guid.Empty, machines.Select(m => m.Id));
            var first = service.PrepareNextJobForMachineGroup(mg, pList);

            Specify.That(first).Should.Not.BeNull();
            var firstkit = first as Kit;
            var tiledCarton = firstkit.ItemsToProduce[0] as ICarton;
            var label1 = firstkit.ItemsToProduce[1] as Label;
            var label2 = firstkit.ItemsToProduce[2] as Label;
            Specify.That(tiledCarton.CartonOnCorrugate.TileCount).Should.BeEqualTo(2);

            //do the test:
            tiledCarton.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            label1.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            label1.ProducibleStatus = ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff;
            //wait until first job is sent to machine.
            Retry.UntilTrue(() => firstkit.ProducibleStatus == InProductionProducibleStatuses.ProducibleSentToMachine, TimeSpan.FromSeconds(10));

            tiledCarton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            label2.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;// if the machine went offline we would remove it from the queue.

            Retry.UntilTrue(() => label1.ProducibleStatus == ProducibleStatuses.ProducibleCompleted, TimeSpan.FromSeconds(10));
            Specify.That(p1.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
            Specify.That(p2.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(label1.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
            Specify.That(label2.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(label2.History.Any(h => h.PreviousStatus == ErrorProducibleStatuses.ProducibleFailed)).Should.BeTrue();

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleProducibleStatusUpdatesProperty_TiledCartonFirstLabelOnPrinterFirstLabelFailed()
        {
            CreateCorugateWithId(1, 300d);
            CreateCorugateWithId(2, 600d);
            CreateCorugateWithId(3, 900d);

            var m1 = CreateEmMachineWithId(1, GetCorrugateWithId(1), GetCorrugateWithId(2));
            var m2 = CreateEmMachineWithId(2, GetCorrugateWithId(3));
            var machines = new List<IMachine>() { m1, m2 };

            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = new Guid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object).Object;

            var p1 = GetBoxFirstProducibleCartonWithLabel("1", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            var p2 = GetBoxFirstProducibleCartonWithLabel("2", new List<int>() { 2, 1 }, cpg, machineIdsAndAliases);
            p1.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachineGroup;//set this status so we know when the load from the db is finished after the service ctor.
            var pList = new[] { p1, p2 };

            var repo = GetBoxFirstRepositoryMock(pList);
            //repo.Setup(r => r.Create(It.IsAny<BoxFirstProducible>())).Callback<BoxFirstProducible>(bf =>
            //{

            //});
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo.Object);

            //wait until jobs from database set to imported
            Retry.UntilTrue(() => p1.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleStaged, TimeSpan.FromSeconds(30));

            service.UpdateProducibleStatus(p1, NotInProductionProducibleStatuses.ProducibleStaged);
            service.UpdateProducibleStatus(p2, NotInProductionProducibleStatuses.ProducibleStaged);

            var mg = GetMachineGroup(Guid.Empty, machines.Select(m => m.Id));
            var first = service.PrepareNextJobForMachineGroup(mg, pList);

            Specify.That(first).Should.Not.BeNull();
            var firstkit = first as Kit;
            var tiledCarton = firstkit.ItemsToProduce[0] as ICarton;
            var label1 = firstkit.ItemsToProduce[1] as Label;
            var label2 = firstkit.ItemsToProduce[2] as Label;
            Specify.That(tiledCarton.CartonOnCorrugate.TileCount).Should.BeEqualTo(2);

            //do the test:
           // tiledCarton.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            label1.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            //wait until first job is sent to machine.
            //Retry.UntilTrue(() => firstkit.ProducibleStatus == InProductionProducibleStatuses.ProducibleSentToMachine, TimeSpan.FromSeconds(10));

           // tiledCarton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            label1.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;// if the machine went offline we would remove it from the queue.

            //Retry.UntilTrue(() => label1.ProducibleStatus == ProducibleStatuses.ProducibleCompleted, TimeSpan.FromSeconds(10));
            Specify.That(p1.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(p2.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(label1.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(label1.History.Any(h => h.PreviousStatus == ErrorProducibleStatuses.ProducibleFailed)).Should.BeTrue();
            Specify.That(label2.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);
            Specify.That(label2.History.Any(h => h.PreviousStatus == ErrorProducibleStatuses.ProducibleFailed)).Should.BeTrue();

        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetProdcblesWithPriorityAndNonPriorityClassification_WhenMachineGroupContainsPriorityAndNonPriorityPrinters()
        {
            CreateCorugateWithId(1, 300d);
            var em = CreateEmMachineWithId(1, GetCorrugateWithId(1));
            var priorityPrinter = CreatePrinterWithId(2, true);
            var nonPriorityPrinter = CreatePrinterWithId(3, false);
            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };

            var cls1 = new Classification() { Id = Guid.NewGuid(), Alias = "cls1", IsPriorityLabel = true };
            var cls2 = new Classification() { Id = Guid.NewGuid(), Alias = "cls2", IsPriorityLabel = false };

            var p1 = GetBoxFirstCartonWithLabel("1", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p2 = GetBoxFirstCartonWithLabel("2", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p3 = GetBoxFirstCartonWithLabel("3", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p4 = GetBoxFirstCartonWithLabel("4", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var p5 = GetBoxFirstCartonWithLabel("5", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var p6 = GetBoxFirstCartonWithLabel("6", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var producibles = new List<BoxFirstProducible>() { p1, p2, p3, p4, p5, p6 };

            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var classificationService = GetClassificationServiceMock(new List<Classification>() { cls1, cls2 }).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object, classificationService).Object;

            var repo = GetBoxFirstRepositoryMock().Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            var machines = new List<IMachine> { em, priorityPrinter, nonPriorityPrinter };
            var mg = GetMachineGroup(Guid.NewGuid(), machines.Select(m => m.Id));
            var filteredProducibles = service.FilterOutProduciblesWhereLabelCannotBeProducedOnMachineGroup(mg, producibles);

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(6);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetProdcblesWithPriorityClassification_WhenMachineGroupContainsPriorityPrintersOnly()
        {
            CreateCorugateWithId(1, 300d);
            var em = CreateEmMachineWithId(1, GetCorrugateWithId(1));
            var priorityPrinter = CreatePrinterWithId(2, true);
            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };

            var cls1 = new Classification() { Id = Guid.NewGuid(), Alias = "cls1", IsPriorityLabel = true };
            var cls2 = new Classification() { Id = Guid.NewGuid(), Alias = "cls2", IsPriorityLabel = false };

            var p1 = GetBoxFirstCartonWithLabel("1", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p2 = GetBoxFirstCartonWithLabel("2", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p3 = GetBoxFirstCartonWithLabel("3", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p4 = GetBoxFirstCartonWithLabel("4", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var p5 = GetBoxFirstCartonWithLabel("5", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var p6 = GetBoxFirstCartonWithLabel("6", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var producibles = new List<BoxFirstProducible>() { p1, p2, p3, p4, p5, p6 };

            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var classificationService = GetClassificationServiceMock(new List<Classification>() { cls1, cls2 }).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object, classificationService).Object;
 
            var repo = GetBoxFirstRepositoryMock().Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            var machines = new List<IMachine> { em, priorityPrinter };
            var mg = GetMachineGroup(Guid.NewGuid(), machines.Select(m => m.Id));
            var filteredProducibles = service.FilterOutProduciblesWhereLabelCannotBeProducedOnMachineGroup(mg, producibles);

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(3);
            var expectedIds = new List<string>() { "1", "2", "3" };
            Specify.That(filteredProducibles.All(p => expectedIds.Contains(p.CustomerUniqueId))).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetProduciblesWithNonPriorityClassification_WhenMachineGroupContainsNonPriorityPrintersOnly()
        {
            CreateCorugateWithId(1, 300d);
            var em = CreateEmMachineWithId(1, GetCorrugateWithId(1));
            var nonPriorityPrinter = CreatePrinterWithId(2, false);
            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };

            var cls1 = new Classification() { Id = Guid.NewGuid(), Alias = "cls1", IsPriorityLabel = true };
            var cls2 = new Classification() { Id = Guid.NewGuid(), Alias = "cls2", IsPriorityLabel = false };

            var p1 = GetBoxFirstCartonWithLabel("1", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p2 = GetBoxFirstCartonWithLabel("2", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p3 = GetBoxFirstCartonWithLabel("3", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p4 = GetBoxFirstCartonWithLabel("4", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var p5 = GetBoxFirstCartonWithLabel("5", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var p6 = GetBoxFirstCartonWithLabel("6", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var producibles = new List<BoxFirstProducible>() { p1, p2, p3, p4, p5, p6 };

            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var classificationService = GetClassificationServiceMock(new List<Classification>() { cls1, cls2 }).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object, classificationService).Object;

            var repo = GetBoxFirstRepositoryMock().Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            var machines = new List<IMachine> { em, nonPriorityPrinter };
            var mg = GetMachineGroup(Guid.NewGuid(), machines.Select(m => m.Id));
            var filteredProducibles = service.FilterOutProduciblesWhereLabelCannotBeProducedOnMachineGroup(mg, producibles);

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(3);
            var expectedIds = new List<string>() { "4", "5", "6" };
            Specify.That(filteredProducibles.All(p => expectedIds.Contains(p.CustomerUniqueId))).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGetProduciblesOfAllClassifications_IfNoLabelsExists()
        {
            CreateCorugateWithId(1, 300d);
            var em = CreateEmMachineWithId(1, GetCorrugateWithId(1));
            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = Guid.NewGuid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };

            var cls1 = new Classification() { Id = Guid.NewGuid(), Alias = "cls1", IsPriorityLabel = true };
            var cls2 = new Classification() { Id = Guid.NewGuid(), Alias = "cls2", IsPriorityLabel = false };

            var p1 = GetBoxFirstProducibleCarton("1", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p2 = GetBoxFirstProducibleCarton("2", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p3 = GetBoxFirstProducibleCarton("3", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls1);
            var p4 = GetBoxFirstProducibleCarton("4", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var p5 = GetBoxFirstProducibleCarton("5", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var p6 = GetBoxFirstProducibleCarton("6", new List<int>() { 1 }, cpg, machineIdsAndAliases, null, cls2);
            var producibles = new List<BoxFirstProducible>() { p1, p2, p3, p4, p5, p6 };

            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var classificationService = GetClassificationServiceMock(new List<Classification>() { cls1, cls2 }).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object, classificationService).Object;

            var repo = GetBoxFirstRepositoryMock().Object;
            
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            var machines = new List<IMachine> { em };
            var mg = GetMachineGroup(Guid.NewGuid(), machines.Select(m => m.Id));
            var filteredProducibles = service.FilterOutProduciblesWhereLabelCannotBeProducedOnMachineGroup(mg, producibles);

            Specify.That(filteredProducibles).Should.Not.BeNull();
            Specify.That(filteredProducibles.Count()).Should.BeEqualTo(6);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void FilterShouldOnlyReturnJobsThatCanBeProducedOnTheMostOptimalCorrugate()
        {
            CreateCorugateWithId(1, 300d);
            CreateCorugateWithId(2, 600d);
            CreateCorugateWithId(3, 900d);

            var m1 = CreateEmMachineWithId(1, GetCorrugateWithId(1), GetCorrugateWithId(2));
            var m2 = CreateEmMachineWithId(2, GetCorrugateWithId(3));
            var machines = new List<IMachine>() { m1, m2 };

            var cpg = new CartonPropertyGroup() { Alias = "Az1", Id = new Guid() };
            var cpgs = new List<CartonPropertyGroup>() { cpg };
            var cartonPropertyGroupService = GetCartonPropertyGroupServiceMock(cpgs).Object;
            var serviceLocator = GetServiceLocatorMock(cartonPropertyGroupService, GetProductionGroupServiceMock().Object, GetCorrugateServiceMock().Object).Object;
            var repo = GetBoxFirstRepositoryMock().Object; 
            
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            
            var coc1 = new CartonOnCorrugate();
            coc1.TileCount = 3;
            //coc1.ProducibleMachines.Add(m1.Id, ); = machines.Select(m => m.Id);


            var producibles = new List<BoxFirstProducible>();
            var c1 = new BoxFirstProducible();
            c1.Id = Guid.NewGuid();
            


            Assert.Inconclusive("not done yet");
        }

        [TestMethod]
        [TestCategory("Unit")]
        [Ignore]
        public void ShouldPrepareWorkflowSelectedJobsForMachineGroup()
        {
            CreateCorugateWithId(1, 300d);
            CreateCorugateWithId(2, 600d);
            CreateCorugateWithId(3, 900d);

            var m1 = CreateEmMachineWithId(1, GetCorrugateWithId(1), GetCorrugateWithId(2));
            var m2 = CreateEmMachineWithId(2, GetCorrugateWithId(3));
            var machines = new List<IMachine>() { m1, m2 };
            
            var mg = GetMachineGroup(Guid.NewGuid(), machines.Select(m => m.Id));
            var cpg1 = new CartonPropertyGroup() { Alias = "Az1", Id = new Guid("00000000-0000-0000-1000-000000000000") };
            var cpg2 = new CartonPropertyGroup() { Alias = "Az2", Id = new Guid("00000000-0000-0000-2000-000000000000") };
            var cpgs = new List<CartonPropertyGroup>() { cpg1, cpg1, cpg2 };
            var cpgService = GetCartonPropertyGroupServiceMock(cpgs).Object;

            CreateCorugateWithId(1, 100);
            CreateCorugateWithId(2, 200);
            CreateCorugateWithId(3, 300);
            CreateCorugateWithId(4, 100000);

            var serviceLocator = GetServiceLocatorMock(cpgService, GetProductionGroupServiceMock(null).Object, GetCorrugateServiceMock().Object).Object;
            var repo = GetBoxFirstRepositoryMock().Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            var producibles = new List<BoxFirstProducible>()
            {
                GetBoxFirstProducibleCarton("1", new List<int>() { 3, 2, 1, 4 }, cpg1, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("2", new List<int>() { 3, 2, 1, 4 }, cpg2, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("3", new List<int>() { 3, 2, 1, 4 }, cpg2, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("4", new List<int>() { 3, 2, 1, 4 }, cpg1, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("5", new List<int>() { 3, 2, 1, 4 }, cpg2, machineIdsAndAliases)
            };

            var output = service.PrepareNextJobForMachineGroup(mg, producibles);

            var kit = output as Kit;
            Specify.That(kit).Should.Not.BeNull();
            Specify.That(kit.ItemsToProduce.Count).Should.BeEqualTo(1);
            var tilingRestriction =
                kit.ItemsToProduce[0].Restrictions.Single(r => r is CanProduceWithTileCountRestriction) as
                    CanProduceWithTileCountRestriction;
            Specify.That(tilingRestriction.Value).Should.BeEqualTo(3);
            Specify.That(kit.ItemsToProduce.ElementAt(0).Id).Should.BeEqualTo(producibles.ElementAt(0).Id);
            Specify.That(kit.CustomerUniqueId).Should.BeEqualTo("1 + 2 + 3");

            var firstKitCarton = kit.ItemsToProduce.First() as ICarton;
            Specify.That(firstKitCarton.CartonOnCorrugate.Corrugate.Id).Should.BeEqualTo(new Guid("30000000-0000-0000-0000-000000000000"));
            Specify.That(firstKitCarton.CartonOnCorrugate.TileCount).Should.BeEqualTo(3);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSortCartonOnCorrugatesByYield()
        {
            this.corrugates.Clear();
            var mg = GetMachineGroup(Guid.NewGuid());
            machineGroups.Add(mg);

            var cpg1 = new CartonPropertyGroup() { Alias = "Az1", Id = new Guid("00000000-0000-0000-1000-000000000000") };
            var cpg2 = new CartonPropertyGroup() { Alias = "Az2", Id = new Guid("00000000-0000-0000-2000-000000000000") };
            var cpgs = new List<CartonPropertyGroup>() { cpg1, cpg1, cpg2 };
            var cpgService = GetCartonPropertyGroupServiceMock(cpgs).Object;

            CreateCorugateWithId(1, 100);
            CreateCorugateWithId(2, 200);
            CreateCorugateWithId(3, 300);
            CreateCorugateWithId(4, 100000);


            var pg1 = new ProductionGroup()
            {
                ConfiguredCorrugates = new ConcurrentList<Guid>(corrugates.Select(c => c.Id).ToList()),
                ConfiguredMachineGroups = new ConcurrentList<Guid>() { mg.Id },

            };
            var pgService = GetProductionGroupServiceMock(pg1).Object;

            var serviceLocator = GetServiceLocatorMock(cpgService, pgService, GetCorrugateServiceMock().Object).Object;
            var repo = GetBoxFirstRepositoryMock().Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            var producibles = new List<BoxFirstProducible>()
            {
                GetBoxFirstProducibleCarton("1", new List<int>() { 3, 2, 1, 4 }, cpg1, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("2", new List<int>() { 3, 2, 1, 4 }, cpg2, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("3", new List<int>() { 3, 2, 1, 4 }, cpg2, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("4", new List<int>() { 3, 2, 1, 4 }, cpg1, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("5", new List<int>() { 3, 2, 1, 4 }, cpg2, machineIdsAndAliases)
            };

            var output = service.AssignOptimalCorrugateAndTilingPartnersForProducibles(producibles);
            Specify.That(output.Count()).Should.BeEqualTo(5);

            var bfProducibles = output.Cast<BoxFirstProducible>();

            Specify.That(bfProducibles.ElementAt(0).CartonOnCorrugates.Count()).Should.BeEqualTo(4);
            for (int i = 0; i < 5; i++)
            {
                Specify.That(bfProducibles.ElementAt(i).CartonOnCorrugates.ElementAt(0).TileCount).Should.BeEqualTo(3);
                Specify.That(bfProducibles.ElementAt(i).CartonOnCorrugates.ElementAt(1).TileCount).Should.BeEqualTo(2);
                Specify.That(bfProducibles.ElementAt(i).CartonOnCorrugates.ElementAt(2).TileCount).Should.BeEqualTo(1);
                Specify.That(bfProducibles.ElementAt(i).CartonOnCorrugates.ElementAt(3).TileCount).Should.BeEqualTo(4);
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetCocToNullIfNoProductionGroupCanBeFoundForCpgOfProducible()
        {
            this.corrugates.Clear();
            var mg = GetMachineGroup(Guid.NewGuid());
            machineGroups.Add(mg);

            var cpg1 = new CartonPropertyGroup() { Alias = "Az1", Id = new Guid("00000000-0000-0000-1000-000000000000") };
            var cpg2 = new CartonPropertyGroup() { Alias = "Az2", Id = new Guid("00000000-0000-0000-2000-000000000000") };
            var cpgs = new List<CartonPropertyGroup>() { cpg1, cpg1, cpg2 };
            var cpgService = GetCartonPropertyGroupServiceMock(cpgs).Object;

            CreateCorugateWithId(1, 100);
            CreateCorugateWithId(2, 200);
            CreateCorugateWithId(3, 300);
            CreateCorugateWithId(4, 100000);


            var pg1 = new ProductionGroup()
            {
                ConfiguredCorrugates = new ConcurrentList<Guid>(corrugates.Select(c => c.Id).ToList()),
                ConfiguredMachineGroups = new ConcurrentList<Guid>() { mg.Id },
            };
            var pgService = GetProductionGroupServiceMock(new List<CartonPropertyGroup>{cpg1}, pg1);
            
            var serviceLocator = GetServiceLocatorMock(cpgService, pgService.Object, GetCorrugateServiceMock().Object).Object;
            var repo = GetBoxFirstRepositoryMock().Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            var producibles = new List<BoxFirstProducible>()
            {
                GetBoxFirstProducibleCarton("1", new List<int>() { 3, 2, 1, 4 }, cpg2, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("2", new List<int>() { 3, 2, 1, 4 }, cpg1, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("3", new List<int>() { 3, 2, 1, 4 }, cpg2, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("4", new List<int>() { 3, 2, 1, 4 }, cpg1, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("5", new List<int>() { 3, 2, 1, 4 }, cpg2, machineIdsAndAliases)
            };

            var output = service.AssignOptimalCorrugateAndTilingPartnersForProducibles(producibles).Cast<BoxFirstProducible>();
            Specify.That(output.Count()).Should.BeEqualTo(5);


            Specify.That(output.ElementAt(0).CartonOnCorrugates).Should.BeNull();
            Specify.That(output.ElementAt(1).CartonOnCorrugates).Should.Not.BeNull();
            Specify.That(output.ElementAt(2).CartonOnCorrugates).Should.BeNull();
            Specify.That(output.ElementAt(3).CartonOnCorrugates).Should.Not.BeNull();
            Specify.That(output.ElementAt(4).CartonOnCorrugates).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReproduceAProducibleOnMessage()
        {
            var eventCalled = false;
            var serviceLocator =
                GetServiceLocatorMock(GetCartonPropertyGroupServiceMock(new List<CartonPropertyGroup>()).Object,
                    GetProductionGroupServiceMock(new ProductionGroup()).Object, GetCorrugateServiceMock().Object);
            var aggregator = new EventAggregator();
            SetAggregator(aggregator, serviceLocator);

            var boxFirstProducible =
                GetBoxFirstProducibleCarton("1", new List<int>() { 3, 2, 1, 4 }, null, machineIdsAndAliases,
                    ProducibleStatuses.ProducibleCompleted);
            
            var repo = GetBoxFirstRepositoryMock(new List<BoxFirstProducible>()
            {
                boxFirstProducible
            });

            repo.Setup(r => r.Create(boxFirstProducible)).Throws(new MongoDuplicateKeyException("test", new WriteConcernResult(new BsonDocument())));
            repo.Setup(m => m.Update(It.IsAny<BoxFirstProducible>())).Callback<BoxFirstProducible>(p =>
            {
                if (eventCalled)
                    return;

                Specify.That(p.Id).Should.BeEqualTo(boxFirstProducible.Id);
                Specify.That(p.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction);
                eventCalled = true;
            });

            repo.Setup(m => m.Find(It.IsAny<Guid>())).Returns(new BoxFirstProducible());

            repo.Setup(m => m.FindByCustomerUniqueId(It.IsAny<string>())).Returns(boxFirstProducible);

            var service = new BoxFirstSelectionAlgorithmService(serviceLocator.Object, repo.Object);


            var producible = new Producible()
                             {
                                 Id = boxFirstProducible.Id,
                                 CustomerUniqueId = boxFirstProducible.CustomerUniqueId
                             };
            aggregator.Publish(new Message<Producible>()
            {
                MessageType = CartonMessages.ReproduceCarton,
                Data = producible
            });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReproduceARangeOfProduciblesOnMessage()
        {
            var reproductionCallCount = 0;
            var serviceLocator =
                GetServiceLocatorMock(GetCartonPropertyGroupServiceMock(new List<CartonPropertyGroup>()).Object,
                    GetProductionGroupServiceMock(new ProductionGroup()).Object, GetCorrugateServiceMock().Object);
            var aggregator = new EventAggregator();
            SetAggregator(aggregator, serviceLocator);

            var boxFirstProducibles = new List<BoxFirstProducible>()
            {
                GetBoxFirstProducibleCarton("1", new List<int>() { 3, 2, 1, 4 }, null, machineIdsAndAliases,
                    ProducibleStatuses.ProducibleCompleted),
                GetBoxFirstProducibleCarton("2", new List<int>() { 3, 2, 1, 4 }, null, machineIdsAndAliases,
                    ProducibleStatuses.ProducibleCompleted),
                GetBoxFirstProducibleCarton("3", new List<int>() { 3, 2, 1, 4 }, null, machineIdsAndAliases,
                    ProducibleStatuses.ProducibleCompleted),
                GetBoxFirstProducibleCarton("4", new List<int>() { 3, 2, 1, 4 }, null, machineIdsAndAliases,
                    ProducibleStatuses.ProducibleCompleted),
                GetBoxFirstProducibleCarton("5", new List<int>() { 3, 2, 1, 4 }, null, machineIdsAndAliases,
                    ProducibleStatuses.ProducibleCompleted),
            };

            var repo = GetBoxFirstRepositoryMock(boxFirstProducibles);

            repo.Setup(m => m.FindByCustomerUniqueId("1")).Returns(boxFirstProducibles.First());
            repo.Setup(m => m.FindByCustomerUniqueId("2")).Returns(boxFirstProducibles.Skip(1).First());
            repo.Setup(m => m.FindByCustomerUniqueId("3")).Returns(boxFirstProducibles.Skip(2).First());
            repo.Setup(m => m.FindByCustomerUniqueId("4")).Returns(boxFirstProducibles.Skip(3).First());
            repo.Setup(m => m.FindByCustomerUniqueId("5")).Returns(boxFirstProducibles.Skip(4).First());

            repo.Setup(r => r.Create(boxFirstProducibles.First())).Throws(new MongoDuplicateKeyException("test", new WriteConcernResult(new BsonDocument())));
            repo.Setup(r => r.Create(boxFirstProducibles.Skip(1).First())).Throws(new MongoDuplicateKeyException("test", new WriteConcernResult(new BsonDocument())));
            repo.Setup(r => r.Create(boxFirstProducibles.Skip(2).First())).Throws(new MongoDuplicateKeyException("test", new WriteConcernResult(new BsonDocument())));
            repo.Setup(r => r.Create(boxFirstProducibles.Skip(3).First())).Throws(new MongoDuplicateKeyException("test", new WriteConcernResult(new BsonDocument())));

            repo.Setup(m => m.Find(It.Is<Guid>(g => boxFirstProducibles.First().Id == g))).Returns(boxFirstProducibles.First());
            repo.Setup(m => m.Find(It.Is<Guid>(g => boxFirstProducibles.Skip(1).First().Id == g))).Returns(boxFirstProducibles.Skip(1).First());
            repo.Setup(m => m.Find(It.Is<Guid>(g => boxFirstProducibles.Skip(2).First().Id == g))).Returns(boxFirstProducibles.Skip(2).First());
            repo.Setup(m => m.Find(It.Is<Guid>(g => boxFirstProducibles.Skip(3).First().Id == g))).Returns(boxFirstProducibles.Skip(3).First());
            repo.Setup(m => m.Find(It.Is<Guid>(g => boxFirstProducibles.Skip(4).First().Id == g))).Returns(boxFirstProducibles.Skip(4).First());

            repo.Setup(m => m.Find(It.IsAny<IEnumerable<Guid>>())).Returns(boxFirstProducibles);

            repo.Setup(m => m.Update(It.IsAny<BoxFirstProducible>())).Callback<BoxFirstProducible>(p =>
            {
                if (p.ProducibleStatus == NotInProductionProducibleStatuses.ProducibleFlaggedForReproduction)
                    reproductionCallCount++;
            });

            var service = new BoxFirstSelectionAlgorithmService(serviceLocator.Object, repo.Object);

            var producibles = boxFirstProducibles.GetRange(1,3).Select(b => new Producible() { Id = b.Id, CustomerUniqueId = b.CustomerUniqueId});

            aggregator.Publish(new Message<IEnumerable<Producible>>()
            {
                MessageType = CartonMessages.ReproduceCartons,
                Data = producibles
            });

            Retry.For(() => reproductionCallCount == 3, TimeSpan.FromSeconds(1));
            Specify.That(reproductionCallCount).Should.BeEqualTo(3);
        }

        [TestMethod]
        public void ShouldOnlyAddItemsIfTheyDontExist()
        {
            var updateCallCount = 0;
            var createCallCount = 0;
            var serviceLocator =
                GetServiceLocatorMock(GetCartonPropertyGroupServiceMock(new List<CartonPropertyGroup>()).Object,
                    GetProductionGroupServiceMock(new ProductionGroup()).Object, GetCorrugateServiceMock().Object);
            var aggregator = new EventAggregator();
            SetAggregator(aggregator, serviceLocator);


            var produciblesInDB = new List<BoxFirstProducible>()
            {
                GetBoxFirstProducibleCarton("1", new List<int>() { 3, 2, 1, 4 }, null, machineIdsAndAliases,
                    ProducibleStatuses.ProducibleCompleted),
                GetBoxFirstProducibleCarton("2", new List<int>() { 3, 2, 1, 4 }, null, machineIdsAndAliases,
                    NotInProductionProducibleStatuses.ProducibleStaged),
            };

            var repo = GetBoxFirstRepositoryMock(produciblesInDB);

            repo.Setup(m => m.Find(It.IsAny<Guid>())).Returns(new BoxFirstProducible());

            repo.Setup(m => m.Find(It.IsAny<IEnumerable<Guid>>())).Returns(new List<BoxFirstProducible>());
            repo.Setup(m => m.FindByCustomerUniqueId(It.IsAny<IEnumerable<string>>())).Returns(produciblesInDB);
            repo.Setup(m => m.Create(It.IsAny<IEnumerable<BoxFirstProducible>>())).Callback<IEnumerable<BoxFirstProducible>>(p =>
            {
                createCallCount += p.Count();
            });

            repo.Setup(m => m.Update(It.IsAny<IEnumerable<BoxFirstProducible>>())).Callback<IEnumerable<BoxFirstProducible>>(p =>
            {
                updateCallCount += p.Count();
            });

            repo.Setup(m => m.FindByCustomerUniqueId(It.IsAny<string>())).Returns(new BoxFirstProducible());

            var service = new BoxFirstSelectionAlgorithmService(serviceLocator.Object, repo.Object);

            //Simulate dropping file twice
            var producibles = produciblesInDB.Select(b => new BoxFirstProducible() { Id = Guid.NewGuid(), CustomerUniqueId = b.CustomerUniqueId });
            createCallCount = 0;
            updateCallCount = 0;
            service.AddProduciblesButEnforceUniqueness(producibles);

            Retry.For(() => updateCallCount == 1, TimeSpan.FromSeconds(1));
            Specify.That(updateCallCount).Should.BeEqualTo(1);
            Specify.That(createCallCount).Should.BeEqualTo(0);
            Specify.That(service.GetProducibles().Count).Should.BeEqualTo(1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug 10424")]
        [Description("When a production group has changed we cannot use the cached corrugate calculation any longer since assigned machine groups and/or corrugates could have changed")]
        public void ShouldRecalculateCacheForFirstIdenticalBoxWhenStaging()
        {
            corrugates.Clear();
            var mg = GetMachineGroup(Guid.NewGuid());
            machineGroups.Add(mg);

            var cpg1 = new CartonPropertyGroup() { Alias = "AZ1", Id = new Guid("00000000-0000-0000-1000-000000000000") };
            var cpgs = new List<CartonPropertyGroup>() { cpg1 };
            var cpgService = GetCartonPropertyGroupServiceMock(cpgs).Object;

            CreateCorugateWithId(1, 700);

            var pg1 = new ProductionGroup
            {
                ConfiguredCorrugates = new ConcurrentList<Guid>(corrugates.Select(c => c.Id).ToList()),
                ConfiguredMachineGroups = new ConcurrentList<Guid>() { mg.Id },
            };
            var pgService = GetProductionGroupServiceMock(new List<CartonPropertyGroup> { cpg1 }, pg1);
            var corrugateMock = GetCorrugateServiceMock();
            var serviceLocator = GetServiceLocatorMock(cpgService, pgService.Object, corrugateMock.Object).Object;
            var repo = GetBoxFirstRepositoryMock().Object;
            var service = new BoxFirstSelectionAlgorithmService(serviceLocator, repo);

            var producibles = new List<BoxFirstProducible>()
            {
                GetBoxFirstProducibleCarton("1", new List<int>() { 1 }, cpg1, machineIdsAndAliases),
                GetBoxFirstProducibleCarton("2", new List<int>() {1 }, cpg1, machineIdsAndAliases)
            };
            var carton1 = ((Kit)producibles[0].Producible).ItemsToProduce[0] as ICarton;
            var carton2 = ((Kit)producibles[1].Producible).ItemsToProduce[0] as ICarton;
            var output = service.AssignOptimalCorrugateAndTilingPartnersForProducibles(producibles).Cast<BoxFirstProducible>();
            corrugateMock.Verify(
                cs =>
                    cs.GetOptimalCorrugates(corrugates, new List<IPacksizeCutCreaseMachine>(),
                        carton1, true, Int32.MaxValue), Times.Once);
            corrugateMock.Verify(
                cs =>
                    cs.GetOptimalCorrugates(It.IsAny<IEnumerable<Corrugate>>(), new List<IPacksizeCutCreaseMachine>(),
                        carton2, false, Int32.MaxValue), Times.Once);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug 12686")]
        public void ShouldCompleteRemovedJobIfLabelProductionHasStarted()
        {
            var carton = new Carton() { ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged };
            var label = new Label() { ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged };
            var kit = new Kit();

            kit.AddProducible(carton);
            kit.AddProducible(label);
            
            var mgId = Guid.NewGuid();
            var bf = new BoxFirstProducible(kit);
            var itemToProduce = GetSubscribedProducible(bf, mgId);
            Specify.That(itemToProduce.ProducedOnMachineGroupId).Should.BeEqualTo(mgId);
            Specify.That(itemToProduce.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);

            carton.ProducibleStatus = InProductionProducibleStatuses.ProducibleCompleted;
            label.ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;
            label.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleRemoved;

            Specify.That(itemToProduce.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleCompleted);
        }

        private IProducible GetSubscribedProducible(BoxFirstProducible bf, Guid mgId)
        {
            
            var cpgService = GetCartonPropertyGroupServiceMock(new List<CartonPropertyGroup>()).Object;
            var serviceLocator = GetServiceLocatorMock(cpgService, new Mock<IProductionGroupService>().Object, new Mock<ICorrugateService>().Object, new Mock<IClassificationService>().Object).Object;
            var repo = new Mock<IBoxFirstRepository>();
            repo.Setup(r => r.FindByCustomerUniqueId(It.IsAny<String>())).Returns(bf);
            var tester = new BoxFirstSelectionTester(serviceLocator, repo.Object);
            var itemToProduce = tester.AddProduciblesAndSubscribe(mgId, new List<BoxFirstProducible>() { bf });
            return itemToProduce;
        }

        private BoxFirstProducible GetBoxFirstProducibleCarton(string customerId, List<int> corrugateIds, CartonPropertyGroup cpg, Dictionary<Guid, string> machineIdsAndAliases,
                                    ProducibleStatuses productionStatus = null, Classification classification = null)
        {
            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg));
            p1.CustomerUniqueId = customerId;
            var box = new Carton()
            {
                DesignId = 1,
                Length = 2,
                Width = 3,
                Height = 4,
                CorrugateQuality = 5,
                CustomerUniqueId = customerId
            };

            if (productionStatus != null)
            {
                p1.ProducibleStatus = productionStatus;
                box.ProducibleStatus = productionStatus;
            }
            
            var kit = new Kit();
            kit.AddProducible(box);
            kit.CustomerUniqueId = box.CustomerUniqueId;
            p1.Producible = kit;


            var cartonOnCorrugates = new List<CartonOnCorrugate>();
            for (int i = 1; i <= corrugateIds.Count(); i++)
            {
                cartonOnCorrugates.Add(new CartonOnCorrugate() { Corrugate = GetCorrugateWithId(corrugateIds.ElementAt(i - 1)), TileCount = corrugateIds.ElementAt(i - 1), ProducibleMachines = machineIdsAndAliases });
            }

            p1.CartonOnCorrugates = cartonOnCorrugates;

            if (classification != null)
                p1.Restrictions.Add(new BasicRestriction<Classification>(classification));

            return p1;
        }


        private BoxFirstProducible GetBoxFirstProducibleCartonWithLabel(string customerId, List<int> corrugateIds, CartonPropertyGroup cpg, Dictionary<Guid, string> machineIdsAndAliases,
                                    ProducibleStatuses productionStatus = null, Classification classification = null)
        {
            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg));
            p1.CustomerUniqueId = customerId;
            var box = new Carton()
            {
                DesignId = 1,
                Length = 2,
                Width = 3,
                Height = 4,
                CorrugateQuality = 5,
                CustomerUniqueId = customerId
            };

            if (productionStatus != null)
            {
                p1.ProducibleStatus = productionStatus;
                box.ProducibleStatus = productionStatus;
            }
            var label = new Label
            {
                CustomerUniqueId = box.CustomerUniqueId,
                PrintData = "print data"
            };

            var kit = new Kit();
            kit.AddProducible(box);
            kit.AddProducible(label);
            kit.CustomerUniqueId = box.CustomerUniqueId;
            p1.Producible = kit;


            var cartonOnCorrugates = new List<CartonOnCorrugate>();
            for (int i = 1; i <= corrugateIds.Count(); i++)
            {
                cartonOnCorrugates.Add(new CartonOnCorrugate() { Corrugate = GetCorrugateWithId(corrugateIds.ElementAt(i - 1)), TileCount = corrugateIds.ElementAt(i - 1), ProducibleMachines = machineIdsAndAliases });
            }

            p1.CartonOnCorrugates = cartonOnCorrugates;

            if (classification != null)
                p1.Restrictions.Add(new BasicRestriction<Classification>(classification));

            return p1;
        }

        private BoxFirstProducible GetBoxFirstCartonWithLabel(string customerId, List<int> corrugateIds, CartonPropertyGroup cpg, Dictionary<Guid, string> machineIdsAndAliases,
            ProducibleStatuses productionStatus = null, Classification classification = null)
        {
            var prod = new BoxFirstProducible();
            prod.Restrictions.Add(new BasicRestriction<CartonPropertyGroup>(cpg));
            prod.CustomerUniqueId = customerId;
            if(classification != null)
                prod.Restrictions.Add(new BasicRestriction<Classification>(classification));
            
            var kit = new Kit();
            kit.CustomerUniqueId = customerId;
            var label = new Label();
            var carton = new Carton() { DesignId = 1, Length = 2, Width = 3, Height = 4, CorrugateQuality = 5,  CustomerUniqueId = customerId };

            if (productionStatus != null)
            {
                kit.ProducibleStatus = productionStatus;
                prod.ProducibleStatus = productionStatus;
                carton.ProducibleStatus = productionStatus;
                label.ProducibleStatus = productionStatus;
            }

            var cartonOnCorrugates = new List<CartonOnCorrugate>();
            for (int i = 1; i <= corrugateIds.Count(); i++)
            {
                cartonOnCorrugates.Add(new CartonOnCorrugate() { Corrugate = GetCorrugateWithId(corrugateIds.ElementAt(i - 1)), TileCount = corrugateIds.ElementAt(i - 1), ProducibleMachines = machineIdsAndAliases });
            }

            prod.CartonOnCorrugates = cartonOnCorrugates;

            kit.ProducibleStatus = carton.ProducibleStatus;
            kit.AddProducible(carton);
            kit.AddProducible(label);
            prod.Producible = kit;

            return prod;
        }

        private Mock<IServiceLocator> GetServiceLocatorMock(ICartonPropertyGroupService cartonPropertyGroupService, IProductionGroupService pgService, ICorrugateService corrugateService, IClassificationService classificationService = null)
        {
            var service = new Mock<IServiceLocator>();
            var aggregator = new EventAggregator();
            if (classificationService == null)
                classificationService = new Mock<IClassificationService>().Object;
            service.Setup(s => s.Locate<IProductionGroupService>()).Returns(pgService);
            service.Setup(s => s.Locate<ICartonPropertyGroupService>()).Returns(cartonPropertyGroupService);
            service.Setup(s => s.Locate<ICorrugateService>()).Returns(corrugateService);
            service.Setup(s => s.Locate<IClassificationService>()).Returns(classificationService);
            service.Setup(s => s.Locate<IAggregateMachineService>()).Returns(GetMachineServiceMock().Object);
            service.Setup(s => s.Locate<IMachineGroupService>()).Returns(GetMachineGroupServiceMock().Object);
            service.Setup(s => s.Locate<IUICommunicationService>()).Returns(new Mock<IUICommunicationService>().Object);
            service.Setup(s => s.Locate<IEventAggregatorSubscriber>()).Returns(aggregator);//GetSubscriberMock().Object);
            service.Setup(s => s.Locate<IEventAggregatorPublisher>()).Returns(aggregator);//GetPublisherMock().Object);
            service.Setup(s => s.Locate<ILogger>()).Returns(new ConsoleLogger());
            
            return service;
        }

        private Mock<IClassificationService> GetClassificationServiceMock(IEnumerable<Classification> classifications)
        {
            var mock = new Mock<IClassificationService>();
            mock.Setup(m => m.Classifications).Returns(classifications);
            return mock;
        }

        private Mock<IMachineGroupService> GetMachineGroupServiceMock()
        {
            var service = new Mock<IMachineGroupService>();

            service.Setup(s => s.MachineGroups).Returns(this.machineGroups);

            return service;
        }

        private Mock<ICorrugateService> GetCorrugateServiceMock()
        {
            var service = new Mock<ICorrugateService>();

            service.Setup(s => s.Corrugates).Returns(this.corrugates);
            service.Setup(
                s =>
                    s.GetOptimalCorrugates(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
                        It.IsAny<ICarton>(), It.IsAny<bool>(), It.IsAny<int>()))
                .Returns<IEnumerable<Corrugate>, IEnumerable<IPacksizeCutCreaseMachine>, ICarton, bool, int>(
                    (passedCorrugates, passedMachines, carton, recalculateCached, maxTilCount) =>
                    {
                        var cartonOnCorrugates = new List<CartonOnCorrugate>();
                        this.corrugates.ForEach(c =>
                        {
                            var CoC = new CartonOnCorrugate(new Carton(), c, c.Id.ToString(), new PackagingDesignCalculation()
                            {
                                Width = 100,
                                Length = 20
                            }, OrientationEnum.Degree0);
                            CoC.TileCount = c.Width / 100;
                            CoC.TileCount = c.Width > 1000 ? 4 : CoC.TileCount;
                            cartonOnCorrugates.Add(CoC);
                        });

                        return cartonOnCorrugates;
                    });

            return service;
        }

        private Mock<IAggregateMachineService> GetMachineServiceMock()
        {
            var service = new Mock<IAggregateMachineService>();
            service.Setup(s => s.FindById(It.IsAny<Guid>())).Returns<Guid>((id) => machines.FirstOrDefault(m => m.Id == id));
            service.Setup(s => s.Machines).Returns(this.machines);
            
            return service;
        }

        private Mock<IEventAggregatorSubscriber> GetSubscriberMock()
        {
            var subscriber = new Mock<IEventAggregatorSubscriber>();
            subscriber.Setup(s => s.GetEvent<IMessage>()).Returns(new Subject<object>().AsObservable().OfType<IMessage>);
            subscriber.Setup(s => s.GetEvent<IMessage<IProducible>>()).Returns(new Subject<object>().AsObservable().OfType<IMessage<IProducible>>);
            subscriber.Setup(s => s.GetEvent<IMessage<Producible>>()).Returns(new Subject<object>().AsObservable().OfType<IMessage<Producible>>);
            subscriber.Setup(s => s.GetEvent<IMessage<IEnumerable<Producible>>>()).Returns(new Subject<object>().AsObservable().OfType<IMessage<IEnumerable<Producible>>>);
            subscriber.Setup(s => s.GetEvent<IMessage<SearchProduciblesWithRestrictions>>())
                .Returns(new Subject<IMessage<SearchProduciblesWithRestrictions>>());
            subscriber.Setup(s => s.Scheduler).Returns(Scheduler.Default);

            return subscriber;
        }

        private Mock<IEventAggregatorPublisher> GetPublisherMock()
        {
            var publisher = new Mock<IEventAggregatorPublisher>();

            return publisher;
        }

        private Mock<ICartonPropertyGroupService> GetCartonPropertyGroupServiceMock(IEnumerable<CartonPropertyGroup> cpgs)
        {
            var service = new Mock<ICartonPropertyGroupService>();
            return service;
        }

        private Mock<IProductionGroupService> GetProductionGroupServiceMock(ProductionGroup pg = null)
        {
            var service = new Mock<IProductionGroupService>();

            if (pg != null)
            {
                service.Setup(s => s.FindByCartonPropertyGroupAlias(It.IsAny<string>())).Returns(pg);
                service.Setup(s => s.FindByCartonPropertyGroupAlias(It.IsAny<IEnumerable<ProductionGroup>>(), It.IsAny<string>())).Returns(pg);
                service.Setup(s => s.GetProductionGroupForMachineGroup(It.IsAny<Guid>())).Returns(pg);
            }
            else
            {
                service.Setup(s => s.GetProductionGroupForMachineGroup(It.IsAny<Guid>())).Returns(new ProductionGroup());
            }

            return service;
        }

        private Mock<IProductionGroupService> GetProductionGroupServiceMock(IEnumerable<CartonPropertyGroup> cpgs, ProductionGroup pg)
        {
            var service = new Mock<IProductionGroupService>();

            service.Setup(s => s.FindByCartonPropertyGroupAlias(It.IsAny<string>())).Returns<string>(alias => cpgs.Any(c => c.Alias == alias) ? pg : null);
            service.Setup(s => s.FindByCartonPropertyGroupAlias(It.IsAny<IEnumerable<ProductionGroup>>(), It.IsAny<string>())).Returns<IEnumerable<ProductionGroup>, string>((pgs, alias) => cpgs.Any(c => c.Alias == alias) ? pg : null);
            service.Setup(s => s.GetProductionGroupForMachineGroup(It.IsAny<Guid>())).Returns(pg);
            
            return service;
        }

        private Mock<IBoxFirstRepository> GetBoxFirstRepositoryMock(IEnumerable<BoxFirstProducible> producibles = null)
        {
            var repo = new Mock<IBoxFirstRepository>();
            if (producibles != null)
            {
                repo.Setup(r => r.All()).Returns(producibles);
                repo.Setup(r => r.GetNonCompletedCartons()).Returns(producibles.Where(p=>p.ProducibleStatus != ProducibleStatuses.ProducibleCompleted));
                repo.Setup(m => m.Find(It.IsAny<Guid>())).Returns<Guid>(id => producibles.FirstOrDefault(bfp => bfp.Id == id));
                repo.Setup(m => m.Find(It.IsAny<IEnumerable<Guid>>()))
                    .Returns<IEnumerable<Guid>>(ids => producibles.Where(p => ids.Any(id => p.Id == id)).ToList());
            }
            return repo;
        }

        private MachineGroup GetMachineGroup(Guid id, IEnumerable<Guid> configuredMachines = null){
            var mg = new MachineGroup();
            mg.Id = id;
            if(configuredMachines != null)
                mg.ConfiguredMachines = new ConcurrentList<Guid>(configuredMachines.ToList());
            return mg;
        }

        private Corrugate GetCorrugateWithId(int id)
        {
            var guidId = String.Format("{0}-0000-0000-0000-000000000000", id.ToString().PadRight(8, '0'));
            return corrugates.FirstOrDefault(c => c.Id == new Guid(guidId));
        }

        private void CreateCorugateWithId(int id, MicroMeter width)
        {
            var guidId = String.Format("{0}-0000-0000-0000-000000000000", id.ToString().PadRight(8, '0'));
            var c = new Corrugate();
            c.Alias = String.Format("Corrugate with id {0}", guidId);
            c.Id = new Guid(guidId);
            c.Width = width;
            c.Thickness = 3;
            c.Quality = 1;

            corrugates.Add(c);
        }

        private IMachine CreateEmMachineWithId(int id, Corrugate corrugateOnTrack1 = null, Corrugate corrugateOnTrack2 = null)
        {
            var guidId = String.Format("00000000-0000-0000-0000-{0}", id.ToString().PadLeft(12, '0'));
            var m = new EmMachine();
            m.Id = new Guid(guidId);
            m.Alias = String.Format("Machine with id {0}", guidId);
            m.Tracks = new ConcurrentList<Track>();

            if(corrugateOnTrack1 != null)
                m.Tracks.Add(new Track() {LoadedCorrugate = corrugateOnTrack1, TrackNumber = 1});
            if(corrugateOnTrack2 != null)
                m.Tracks.Add(new Track() {LoadedCorrugate = corrugateOnTrack2, TrackNumber = 2});

            machineIdsAndAliases.Add(m.Id, m.Alias);
            machines.Add(m);

            return m;
        }

        private IMachine CreatePrinterWithId(int id, bool priority)
        {
            var guidId = String.Format("00000000-0000-{0}-0000-000000000000", id.ToString().PadLeft(4, '0'));
            var m = new ZebraPrinter();
            m.Id = new Guid(guidId);
            m.Alias = String.Format("Printer with id {0}", guidId);
            m.IsPriorityPrinter = priority;

            machineIdsAndAliases.Add(m.Id, m.Alias);
            machines.Add(m);

            return m;
        }

        private IMachine GetMachineWithId(int id)
        {
            var guidId = String.Format("00000000-0000-0000-0000-{0}", id.ToString().PadLeft(12, '0'));
            return machines.Single(m => m.Id == new Guid(guidId));
        }

        private class BoxFirstSelectionTester : BoxFirstSelectionAlgorithmService
        {
            public BoxFirstSelectionTester(IServiceLocator serviceLocator, IBoxFirstRepository repository) 
                : base(serviceLocator, repository)
            {
            }

            public IProducible AddProduciblesAndSubscribe(Guid machineGroupId, IEnumerable<BoxFirstProducible> producibles)
            {
                return base.AddProducibleToMachineQueueAndSubscribe(machineGroupId, producibles);
            }
        }
    }
}
