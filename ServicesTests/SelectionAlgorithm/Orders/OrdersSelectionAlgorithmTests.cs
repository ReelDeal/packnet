﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Business.Corrugates;
using PackNet.Business.Orders;
using PackNet.Business.PackagingDesigns;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.Orders;
using PackNet.Common.Interfaces.DTO.PackagingDesigns;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.DTO.ProductionGroups;
using PackNet.Common.Interfaces.DTO.SelectionAlgorithm;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.MachineGroupSpecific;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ProductionGroupSpecific;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Data.Repositories.MongoDB;
using PackNet.Data.Serializers;
using PackNet.Services;
using PackNet.Services.SelectionAlgorithm.Orders;
using Testing.Specificity;
using TestUtils;

namespace ServicesTests.SelectionAlgorithm.Orders
{
    using System.Runtime.InteropServices;

    using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
    using PackNet.Data.Orders;
    using PackNet.Services.MachineServices;
    using PackNet.Services.ProductionGroup;

    using Orders = PackNet.Business.Orders.Orders;

    [TestClass]
    public class OrdersSelectionAlgorithmTests
    {
        private Guid cartonId = Guid.NewGuid();
        private Guid machineGroupId = Guid.NewGuid();
        private Guid machineGroup2Id = Guid.NewGuid();
        private Guid productionGroupId = Guid.NewGuid();
        private Mock<IOrders> orders = new Mock<IOrders>();
        private Mock<IProductionGroupService> productionGroupService = new Mock<IProductionGroupService>();
        private Mock<IMachineGroupService> machineGroupService = new Mock<IMachineGroupService>();
        private Mock<IServiceLocator> serviceLocator = new Mock<IServiceLocator>();
        private Mock<IOptimalCorrugateCalculator> optimalCorrugateCalculator = new Mock<IOptimalCorrugateCalculator>();
        private Mock<IPackagingDesignService> packagingDesignServiceMock = new Mock<IPackagingDesignService>();
        private Mock<IUICommunicationService> uiCommunicationService = new Mock<IUICommunicationService>();
        private Mock<IUserNotificationService> userNotificationService = new Mock<IUserNotificationService>();
        private Mock<ICorrugateService> corrugateServiceMock = new Mock<ICorrugateService>();
        private ILogger logger = new ConsoleLogger();
        private EventAggregator eventAggregator = new EventAggregator();
        private Carton producible;
        private ProductionGroup productionGroup = null;
        private Guid machineId = Guid.NewGuid();

        [TestInitialize]
        public void TestInitialize()
        {

            MongoDbHelpers.RegisterIgnoreIfNullConvention();

            MongoDbHelpers.TryRegisterSerializer<ProducibleStatuses>(new EnumerationSerializer());
            MongoDbHelpers.TryRegisterSerializer<ErrorProducibleStatuses>(new EnumerationSerializer());
            MongoDbHelpers.TryRegisterSerializer<InProductionProducibleStatuses>(new EnumerationSerializer());
            MongoDbHelpers.TryRegisterSerializer<NotInProductionProducibleStatuses>(new EnumerationSerializer());

            productionGroup = new ProductionGroup { Alias = "PG", Id = productionGroupId, SelectionAlgorithm = SelectionAlgorithmTypes.Order };

            producible = new Carton
            {
                Id = cartonId,
                Length = 5,
                Width = 5,
                Height = 5,
                DesignId = 201,
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged
            };
            productionGroupService.Setup(m => m.ProductionGroups).Returns(new List<ProductionGroup> { productionGroup });

        }

        [ClassCleanup]
        public static void TestCleanup()
        {
            var orderRepo = new MongoOrderTestRepo();
            orderRepo.DropDatabase();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OrderShouldBeFlattenedWhenAddingToProductionGroupQueue()
        {
            /*
             *  When adding an order to the queue in the SA we change the structure to prepare for production:
             *  
             *  example:
             *  From:
             *  Order x 1
             *      |
             *      |-Kit
             *          |-ItemsToProduce
             *                 |-Order x 3             
             *                 |     |-Carton
             *                 |
             *                 |-Order x 2
             *                       |-Label
             * 
             *  To:
             *  Order x 1
             *      |
             *      |-Kit
             *          |-ItemsToProduce
             *                 |-Carton
             *                 |-Carton
             *                 |-Carton
             *                 |-Label
             *                 |-Label
             *                 
             *  If the cartons can be tiled we will do that before sending the kit to production:
             *  
             *  To:
             *  Order x 1             
             *      |-Kit
             *          |-ItemsToProduce
             *                 |-TiledCarton
             *                 |-Carton
             *                 |-Label
             *                 |-Label
             * 
             */

            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 20, Quality = 1, Thickness = 0.11 };

            var carton1 = new Carton
            {
                Id = cartonId,
                Length = 5,
                Width = 5,
                Height = 5,
                DesignId = 201,
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                CartonOnCorrugate =
                    new CartonOnCorrugate(new Carton(), cor1, "huh", new PackagingDesignCalculation { Length = 12, Width = 12 },
                        OrientationEnum.Degree0)
            };

            var label = new Label
            {
                Id = Guid.NewGuid(),
                PrintData = "printdata",
            };

            var kit = new Kit();

            var cartonOrder = new Order(carton1, 5);
            var labelOrder = new Order(label, 3);

            kit.AddProducible(cartonOrder);
            kit.AddProducible(labelOrder);

            var ordersBl = new Mock<IOrders>();

            Order flatOrder = null;

            ordersBl.Setup(o => o.Create(It.IsAny<Order>())).Callback<Order>(ord => flatOrder = ord);

            var ordersSA = new OrdersSelectionAlgorithmService(
                                            ordersBl.Object,
                                            productionGroupService.Object,
                                            machineGroupService.Object,
                                            eventAggregator,
                                            eventAggregator,
                                            serviceLocator.Object,
                                            uiCommunicationService.Object,
                                            userNotificationService.Object,
                                            corrugateServiceMock.Object,
                                            logger);

            var order = new Order(kit, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "production group test order"
            };

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order);

            Specify.That(flatOrder).Should.Not.BeNull();
            Specify.That(flatOrder.CustomerUniqueId).Should.BeEqualTo(order.OrderId);

            var kitInOrder = flatOrder.Producible as Kit;

            Specify.That(kitInOrder).Should.Not.BeNull();

            Specify.That(kitInOrder.ItemsToProduce.Count).Should.BeEqualTo(cartonOrder.OriginalQuantity + labelOrder.OriginalQuantity);
            Specify.That(kitInOrder.ItemsToProduce.OfType<IPrintable>().Count()).Should.BeEqualTo(labelOrder.OriginalQuantity);
            Specify.That(kitInOrder.ItemsToProduce.OfType<ICarton>().Count()).Should.BeEqualTo(cartonOrder.OriginalQuantity);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OrderShouldBeFlattenedWhenAddingToMachineGroupQueue()
        {
            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 20, Quality = 1, Thickness = 0.11 };

            var carton1 = new Carton
            {
                Id = cartonId,
                Length = 5,
                Width = 5,
                Height = 5,
                DesignId = 201,
                ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged,
                CartonOnCorrugate =
                    new CartonOnCorrugate(new Carton(), cor1, "huh", new PackagingDesignCalculation { Length = 12, Width = 12 },
                        OrientationEnum.Degree0)
            };

            var label = new Label
            {
                Id = Guid.NewGuid(),
                PrintData = "printdata",
            };

            var kit = new Kit();

            var cartonOrder = new Order(carton1, 5);
            var labelOrder = new Order(label, 3);

            kit.AddProducible(cartonOrder);
            kit.AddProducible(labelOrder);

            var ordersBl = new Mock<IOrders>();

            Order flatOrder = null;

            ordersBl.Setup(o => o.Create(It.IsAny<Order>())).Callback<Order>(ord => flatOrder = ord);

            var ordersSA = new OrdersSelectionAlgorithmService(
                                            ordersBl.Object,
                                            productionGroupService.Object,
                                            machineGroupService.Object,
                                            eventAggregator,
                                            eventAggregator,
                                            serviceLocator.Object,
                                            uiCommunicationService.Object,
                                            userNotificationService.Object,
                                            corrugateServiceMock.Object,
                                            logger);

            var order = new Order(kit, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "production group test order"
            };

            ordersSA.AddOrderToMachineGroupQueue(productionGroupId, order);

            Specify.That(flatOrder).Should.Not.BeNull();
            Specify.That(flatOrder.CustomerUniqueId).Should.BeEqualTo(order.OrderId);

            var kitInOrder = flatOrder.Producible as Kit;

            Specify.That(kitInOrder).Should.Not.BeNull();

            Specify.That(kitInOrder.ItemsToProduce.Count).Should.BeEqualTo(cartonOrder.OriginalQuantity + labelOrder.OriginalQuantity);
            Specify.That(kitInOrder.ItemsToProduce.OfType<IPrintable>().Count()).Should.BeEqualTo(labelOrder.OriginalQuantity);
            Specify.That(kitInOrder.ItemsToProduce.OfType<ICarton>().Count()).Should.BeEqualTo(cartonOrder.OriginalQuantity);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateOptimalCorrugateWhenPgChanged()
        {
            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 20, Quality = 1, Thickness = 0.11 };
            var cor2 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 30, Quality = 1, Thickness = 0.11 };
            producible.CartonOnCorrugate = new CartonOnCorrugate(new Carton(), cor1, "huh", new PackagingDesignCalculation { Length = 12, Width = 12 }, OrientationEnum.Degree0);
            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>()))
                .Returns(new CartonOnCorrugate(new Carton(), cor2, "updated", new PackagingDesignCalculation { Length = 12, Width = 12 }, OrientationEnum.Degree0));

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order);

            var message = new Message<ProductionGroup>
            {
                Data = productionGroup,
                MessageType = ProductionGroupMessages.ProductionGroupChanged
            };
            eventAggregator.Publish(message);

            Specify.That(producible.CartonOnCorrugate.Corrugate.Id).Should.BeEqualTo(cor2.Id);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldResetFailCounOnNotProducibleOrdersAndSendRestageMessageWhenProductionGroupHasChanged()
        {
            producible.CartonOnCorrugate = null;
            producible.ProducibleStatus = ErrorProducibleStatuses.NotProducible;

            var ordersSA = OrdersSelectionAlgorithm;

            var order = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };
            order.Failed.Add(producible.Id);
            order.ProducibleStatus = ErrorProducibleStatuses.NotProducible;

            orders.Setup(repo => repo.GetAllNotProducibleOrders()).Returns(new List<Order> { order });

            var restagedCalled = false;
            eventAggregator.GetEvent<ImportMessage<IEnumerable<IProducible>>>()
                .Where(m => m.MessageType == MessageTypes.Restaged && m.SelectionAlgorithmType == SelectionAlgorithmTypes.Order)
                .Subscribe(m =>
                {
                    restagedCalled = true;
                    Specify.That(m.Data.Count()).Should.BeEqualTo(1);
                    Specify.That((m.Data.First() as Order).Failed.Count()).Should.BeEqualTo(0);
                });

            orders.ResetCalls();

            var message = new Message<ProductionGroup>
            {
                Data = productionGroup,
                MessageType = ProductionGroupMessages.ProductionGroupChanged
            };
            eventAggregator.Publish(message);

            orders.Verify(o => o.GetAllNotProducibleOrders(), Times.Once);
            Specify.That(restagedCalled).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_Remove_NonProduciblesFromQueueWhenPgCapabilitiesChanges_WHenUsingMGQueue()
        {
            var largeCarton = new Carton { Id = Guid.NewGuid(), Length = 16, Width = 15, Height = 15, DesignId = 201 };
            var smallCarton = new Carton { Id = Guid.NewGuid(), Length = 5, Width = 5, Height = 5, DesignId = 201 };

            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 18.625, Quality = 1, Thickness = 0.16 };
            var cor2 = new Corrugate { Alias = "two", Id = Guid.NewGuid(), Width = 29.250, Quality = 1, Thickness = 0.16 };
            var cor3 = new Corrugate { Alias = "three", Id = Guid.NewGuid(), Width = 55, Quality = 1, Thickness = 0.16 };

            var machine = GetEMMachine(new List<Corrugate> { cor1, cor2, cor3 });
            var mg = GetMachineGroup(machine, new List<Corrugate> { cor1, cor2, cor3 });

            var pg = GetProductionGroup(new List<Corrugate> { cor1, cor2, cor3 }, mg);

            // Set up mocks
            var machineGroupServiceMock = GetMachineGroupServiceMock(mg);
            var machineServiceMock = GetMachineServiceMock(machine);
            var serviceLocatorMock = GetServiceLocatorMock(machineGroupServiceMock, machineServiceMock);
            var corrugatesManagerMock = GetCorrugatesManagerMock(new List<Corrugate> { cor1, cor2, cor3 });
            var productionGroupServiceMock = GetProductionGroupServiceMock(pg);
            var packagingDesignManagerMock = GetPackagingDesignManagerMock(new List<ICarton> { largeCarton, smallCarton });

            // set up classes under test
            var realOptimalCorrugateCalculator = new OptimalCorrugateCalculator(packagingDesignManagerMock.Object,
                serviceLocatorMock.Object, machineGroupServiceMock.Object);

            var corrugateService = new CorrugateService(realOptimalCorrugateCalculator,
                corrugatesManagerMock.Object, serviceLocatorMock.Object, machineServiceMock.Object,
                new Mock<IUICommunicationService>().Object, eventAggregator,
                eventAggregator, new Mock<ILogger>().Object);

            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var ordersSA = GetTestOrderSA(productionGroupServiceMock, machineGroupServiceMock, corrugateService);

            var order = new Order(largeCarton, 1);
            var order1 = new Order(smallCarton, 1);
            var order2 = new Order(smallCarton, 1);

            ordersSA.AddOrderToMachineGroupQueue(pg.Id, order);
            ordersSA.AddOrderToMachineGroupQueue(pg.Id, order1);
            ordersSA.AddOrderToMachineGroupQueue(pg.Id, order2);

            var queue = ordersSA.GetOrderQueue(pg.Id);

            Specify.That(queue).Should.Not.BeNull("Queue should not be null");
            Specify.That(queue.Count()).Should.BeEqualTo(3, "All 3 orders should be in queue when PG contains the Corrugate that can produce all 3");

            pg.ConfiguredCorrugates.Remove(cor3.Id);
            mg.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(mg.Id, new List<Corrugate> { cor1, cor2 })) });

            var message = new Message<ProductionGroup>
            {
                Data = pg,
                MessageType = ProductionGroupMessages.ProductionGroupChanged
            };
            eventAggregator.Publish(message);

            queue = ordersSA.GetOrderQueue(pg.Id);

            orders.Verify(o => o.Update(It.IsAny<Order>()), Times.Exactly(3), "Order database should be updated once for every order when PG has changed");
            Specify.That(queue.Count()).Should.BeEqualTo(2, "Queue should only contain 2 orders after the large corrugate has been rmoved from PG");
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible, "Large package order should not be producible after large corrugate has been romved from PG");
            Specify.That(order.Failed.Count()).Should.BeEqualTo(1, "Large order should have one failed producible after the corrugate has been removed from PG");
            Specify.That(queue.ElementAt(0).Id).Should.BeEqualTo(order1.Id, "First order in queue should be the first one that is producible");
            Specify.That(queue.ElementAt(1).Id).Should.BeEqualTo(order2.Id, "First order in queue should be the second one that is producible");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_Remove_NonProduciblesFromQueueWHenPgCapabilitiesChanges_WHenUsingPGQueue()
        {
            var largeCarton = new Carton { Id = Guid.NewGuid(), Length = 16, Width = 15, Height = 15, DesignId = 201 };
            var smallCarton = new Carton { Id = Guid.NewGuid(), Length = 5, Width = 5, Height = 5, DesignId = 201 };

            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 18.625, Quality = 1, Thickness = 0.16 };
            var cor2 = new Corrugate { Alias = "two", Id = Guid.NewGuid(), Width = 29.250, Quality = 1, Thickness = 0.16 };
            var cor3 = new Corrugate { Alias = "three", Id = Guid.NewGuid(), Width = 55, Quality = 1, Thickness = 0.16 };

            var machine = GetEMMachine(new List<Corrugate> { cor1, cor2, cor3 });
            var mg = GetMachineGroup(machine, new List<Corrugate> { cor1, cor2, cor3 });

            var pg = GetProductionGroup(new List<Corrugate> { cor1, cor2, cor3 }, mg);

            // Set up mocks
            var machineGroupServiceMock = GetMachineGroupServiceMock(mg);
            var machineServiceMock = GetMachineServiceMock(machine);
            var serviceLocatorMock = GetServiceLocatorMock(machineGroupServiceMock, machineServiceMock);
            var corrugatesManagerMock = GetCorrugatesManagerMock(new List<Corrugate> { cor1, cor2, cor3 });
            var productionGroupServiceMock = GetProductionGroupServiceMock(pg);
            var packagingDesignManagerMock = GetPackagingDesignManagerMock(new List<ICarton> { largeCarton, smallCarton });

            // set up classes under test
            var realOptimalCorrugateCalculator = new OptimalCorrugateCalculator(packagingDesignManagerMock.Object,
                serviceLocatorMock.Object, machineGroupServiceMock.Object);

            var corrugateService = new CorrugateService(realOptimalCorrugateCalculator,
                corrugatesManagerMock.Object, serviceLocatorMock.Object, machineServiceMock.Object,
                new Mock<IUICommunicationService>().Object, eventAggregator,
                eventAggregator, new Mock<ILogger>().Object);

            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var ordersSA = GetTestOrderSA(productionGroupServiceMock, machineGroupServiceMock, corrugateService);

            var order = new Order(largeCarton, 1);
            var order1 = new Order(smallCarton, 1);
            var order2 = new Order(smallCarton, 1);

            ordersSA.AddOrderToProductionGroupQueue(pg.Id, order);
            ordersSA.AddOrderToProductionGroupQueue(pg.Id, order1);
            ordersSA.AddOrderToProductionGroupQueue(pg.Id, order2);

            var queue = ordersSA.GetOrderQueue(pg.Id);

            Specify.That(queue).Should.Not.BeNull("Queue should not be null");
            Specify.That(queue.Count()).Should.BeEqualTo(3, "All 3 orders should be in queue when PG contains the Corrugate that can produce all 3");

            pg.ConfiguredCorrugates.Remove(cor3.Id);
            mg.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(mg.Id, new List<Corrugate> { cor1, cor2 })) });

            var message = new Message<ProductionGroup>
            {
                Data = pg,
                MessageType = ProductionGroupMessages.ProductionGroupChanged
            };
            eventAggregator.Publish(message);

            queue = ordersSA.GetOrderQueue(pg.Id);

            orders.Verify(o => o.Update(It.IsAny<Order>()), Times.Exactly(3), "Order database should be updated once for every order when PG has changed");
            Specify.That(queue.Count()).Should.BeEqualTo(2, "Queue should only contain 2 orders after the large corrugate has been rmoved from PG");
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible, "Large package order should not be producible after large corrugate has been romved from PG");
            Specify.That(order.Failed.Count()).Should.BeEqualTo(1, "Large order should have one failed producible after the corrugate has been removed from PG");
            Specify.That(queue.ElementAt(0).Id).Should.BeEqualTo(order1.Id, "First order in queue should be the first one that is producible");
            Specify.That(queue.ElementAt(1).Id).Should.BeEqualTo(order2.Id, "First order in queue should be the second one that is producible");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldNotAddNonProducibleOrder_Containing_ASingleCarton_ToQueue()
        {
            var largeCarton = new Carton { Id = Guid.NewGuid(), Length = 16, Width = 15, Height = 15, DesignId = 201 };

            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 18.625, Quality = 1, Thickness = 0.16 };
            var cor2 = new Corrugate { Alias = "two", Id = Guid.NewGuid(), Width = 29.250, Quality = 1, Thickness = 0.16 };

            var machine = GetEMMachine(new List<Corrugate> { cor1, cor2 });
            var mg = GetMachineGroup(machine, new List<Corrugate> { cor1, cor2 });

            var pg = GetProductionGroup(new List<Corrugate> { cor1, cor2 }, mg);

            // Set up mocks
            var machineGroupServiceMock = GetMachineGroupServiceMock(mg);
            var machineServiceMock = GetMachineServiceMock(machine);
            var serviceLocatorMock = GetServiceLocatorMock(machineGroupServiceMock, machineServiceMock);
            var corrugatesManagerMock = GetCorrugatesManagerMock(new List<Corrugate> { cor1, cor2 });
            var productionGroupServiceMock = GetProductionGroupServiceMock(pg);
            var packagingDesignManagerMock = GetPackagingDesignManagerMock(new List<ICarton> { largeCarton });

            // set up classes under test
            var realOptimalCorrugateCalculator = new OptimalCorrugateCalculator(packagingDesignManagerMock.Object,
                serviceLocatorMock.Object, machineGroupServiceMock.Object);

            var corrugateService = new CorrugateService(realOptimalCorrugateCalculator,
                corrugatesManagerMock.Object, serviceLocatorMock.Object, machineServiceMock.Object,
                new Mock<IUICommunicationService>().Object, eventAggregator,
                eventAggregator, new Mock<ILogger>().Object);

            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var ordersSA = GetTestOrderSA(productionGroupServiceMock, machineGroupServiceMock, corrugateService);

            var order = new Order(largeCarton, 1);

            ordersSA.AddOrderToProductionGroupQueue(pg.Id, order);

            var queue = ordersSA.GetOrderQueue(pg.Id);

            Specify.That(queue).Should.Not.BeNull("Queue should not be null");
            Specify.That(queue.Count()).Should.BeEqualTo(0, "Order containing a carton that cannot be produced on any of the corrugates in the PG should not be added to queue");
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible, "Order that cannot be produced using the current corrugates in PG should have satatus NotProducible");
            Specify.That(order.Failed.Count()).Should.BeEqualTo(1, "NonProducibleOrder should have failedcount of 1");
            Specify.That(order.Failed.ElementAt(0)).Should.BeEqualTo(largeCarton.Id, "Failed list on order should contain the correct carton");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldNotAddNonProducibleOrder_Containing_A_Kit_With_NonProducible_Cartons_ToQueue()
        {
            var largeCarton1 = new Carton { Id = Guid.NewGuid(), Length = 16, Width = 15, Height = 15, DesignId = 201 };
            var largeCarton2 = new Carton { Id = Guid.NewGuid(), Length = 16, Width = 15, Height = 15, DesignId = 201 };

            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 18.625, Quality = 1, Thickness = 0.16 };
            var cor2 = new Corrugate { Alias = "two", Id = Guid.NewGuid(), Width = 29.250, Quality = 1, Thickness = 0.16 };

            var machine = GetEMMachine(new List<Corrugate> { cor1, cor2 });
            var mg = GetMachineGroup(machine, new List<Corrugate> { cor1, cor2 });

            var pg = GetProductionGroup(new List<Corrugate> { cor1, cor2 }, mg);

            // Set up mocks
            var machineGroupServiceMock = GetMachineGroupServiceMock(mg);
            var machineServiceMock = GetMachineServiceMock(machine);
            var serviceLocatorMock = GetServiceLocatorMock(machineGroupServiceMock, machineServiceMock);
            var corrugatesManagerMock = GetCorrugatesManagerMock(new List<Corrugate> { cor1, cor2 });
            var productionGroupServiceMock = GetProductionGroupServiceMock(pg);
            var packagingDesignManagerMock = GetPackagingDesignManagerMock(new List<ICarton> { largeCarton1, largeCarton2 });

            // set up classes under test
            var realOptimalCorrugateCalculator = new OptimalCorrugateCalculator(packagingDesignManagerMock.Object,
                serviceLocatorMock.Object, machineGroupServiceMock.Object);

            var corrugateService = new CorrugateService(realOptimalCorrugateCalculator,
                corrugatesManagerMock.Object, serviceLocatorMock.Object, machineServiceMock.Object,
                new Mock<IUICommunicationService>().Object, eventAggregator,
                eventAggregator, new Mock<ILogger>().Object);

            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var ordersSA = GetTestOrderSA(productionGroupServiceMock, machineGroupServiceMock, corrugateService);

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible> { largeCarton1, largeCarton2 }
            };


            var order = new Order(kit, 1);

            ordersSA.AddOrderToProductionGroupQueue(pg.Id, order);

            var queue = ordersSA.GetOrderQueue(pg.Id);

            Specify.That(queue).Should.Not.BeNull("Queue should not be null");
            Specify.That(queue.Count()).Should.BeEqualTo(0, "Kit containing cartons that does not fit on any of the corrugates currently in PG should not be added to queue");
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible, "Order containing nonproducible kit should have correct status");
            Specify.That(order.Failed.Count()).Should.BeEqualTo(2, "Order should have 2 failed jobes");
            Specify.That(order.Failed.ElementAt(0)).Should.BeEqualTo(largeCarton1.Id, "first failed job should be the first failed carton");
            Specify.That(order.Failed.ElementAt(1)).Should.BeEqualTo(largeCarton2.Id, "second failed job should be the second failed carton");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldAddOrder_Containing_A_KitWith_ProducibleCartons_ToQueue()
        {
            var smallCarton = new Carton { Id = Guid.NewGuid(), Length = 5, Width = 5, Height = 5, DesignId = 201 };

            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 18.625, Quality = 1, Thickness = 0.16 };
            var cor2 = new Corrugate { Alias = "two", Id = Guid.NewGuid(), Width = 29.250, Quality = 1, Thickness = 0.16 };

            var machine = GetEMMachine(new List<Corrugate> { cor1, cor2 });
            var mg = GetMachineGroup(machine, new List<Corrugate> { cor1, cor2 });

            var pg = GetProductionGroup(new List<Corrugate> { cor1, cor2 }, mg);

            // Set up mocks
            var machineGroupServiceMock = GetMachineGroupServiceMock(mg);
            var machineServiceMock = GetMachineServiceMock(machine);
            var serviceLocatorMock = GetServiceLocatorMock(machineGroupServiceMock, machineServiceMock);
            var corrugatesManagerMock = GetCorrugatesManagerMock(new List<Corrugate> { cor1, cor2 });
            var productionGroupServiceMock = GetProductionGroupServiceMock(pg);
            var packagingDesignManagerMock = GetPackagingDesignManagerMock(new List<ICarton> { smallCarton });

            // set up classes under test
            var realOptimalCorrugateCalculator = new OptimalCorrugateCalculator(packagingDesignManagerMock.Object,
                serviceLocatorMock.Object, machineGroupServiceMock.Object);

            var corrugateService = new CorrugateService(realOptimalCorrugateCalculator,
                corrugatesManagerMock.Object, serviceLocatorMock.Object, machineServiceMock.Object,
                new Mock<IUICommunicationService>().Object, eventAggregator,
                eventAggregator, new Mock<ILogger>().Object);

            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var ordersSA = GetTestOrderSA(productionGroupServiceMock, machineGroupServiceMock, corrugateService);

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible> { smallCarton }
            };

            var order = new Order(kit, 1);

            ordersSA.AddOrderToProductionGroupQueue(pg.Id, order);

            var queue = ordersSA.GetOrderQueue(pg.Id);

            Specify.That(queue).Should.Not.BeNull("Queue should not be null");
            Specify.That(queue.Count()).Should.BeEqualTo(1, "Queue should contain one item");
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged, "Producible order should have been set to staged");
            Specify.That((order.Producible as Kit).ItemsToProduce.First(i => i.Id == smallCarton.Id).ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleImported, "Producible caron should have correct status");    // should perhaps be staged?         
            Specify.That(order.Failed.Count()).Should.BeEqualTo(0, "Producible order shoul have a fail count of 0");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldNotAddOrder_Containing_A_Kit_With_NonProducible_Cartons_AndProducibleCartons_ToQueue()
        {
            var largeCarton = new Carton { Id = Guid.NewGuid(), Length = 16, Width = 15, Height = 15, DesignId = 201 };
            var smallCarton = new Carton { Id = Guid.NewGuid(), Length = 5, Width = 5, Height = 5, DesignId = 201 };

            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 18.625, Quality = 1, Thickness = 0.16 };
            var cor2 = new Corrugate { Alias = "two", Id = Guid.NewGuid(), Width = 29.250, Quality = 1, Thickness = 0.16 };

            var machine = GetEMMachine(new List<Corrugate> { cor1, cor2 });
            var mg = GetMachineGroup(machine, new List<Corrugate> { cor1, cor2 });

            var pg = GetProductionGroup(new List<Corrugate> { cor1, cor2 }, mg);

            // Set up mocks
            var machineGroupServiceMock = GetMachineGroupServiceMock(mg);
            var machineServiceMock = GetMachineServiceMock(machine);
            var serviceLocatorMock = GetServiceLocatorMock(machineGroupServiceMock, machineServiceMock);
            var corrugatesManagerMock = GetCorrugatesManagerMock(new List<Corrugate> { cor1, cor2 });
            var productionGroupServiceMock = GetProductionGroupServiceMock(pg);
            var packagingDesignManagerMock = GetPackagingDesignManagerMock(new List<ICarton> { largeCarton, smallCarton });

            // set up classes under test
            var realOptimalCorrugateCalculator = new OptimalCorrugateCalculator(packagingDesignManagerMock.Object,
                serviceLocatorMock.Object, machineGroupServiceMock.Object);

            var corrugateService = new CorrugateService(realOptimalCorrugateCalculator,
                corrugatesManagerMock.Object, serviceLocatorMock.Object, machineServiceMock.Object,
                new Mock<IUICommunicationService>().Object, eventAggregator,
                eventAggregator, new Mock<ILogger>().Object);

            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var ordersSA = GetTestOrderSA(productionGroupServiceMock, machineGroupServiceMock, corrugateService);

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible> { largeCarton, smallCarton }
            };

            var order = new Order(kit, 1);

            ordersSA.AddOrderToProductionGroupQueue(pg.Id, order);

            var queue = ordersSA.GetOrderQueue(pg.Id);

            Specify.That(queue).Should.Not.BeNull("Queue should not be null");
            Specify.That(queue.Count()).Should.BeEqualTo(0, "No orders should have been added to queue");
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible, "Order containing a mix of producible and nonproducible cartons should have status NonProducible");
            Specify.That((order.Producible as Kit).ItemsToProduce.First(i => i.Id == smallCarton.Id).ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleImported, "Producible carton in kit should have status imported");
            Specify.That((order.Producible as Kit).ItemsToProduce.First(i => i.Id == largeCarton.Id).ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible, "Nonproducible carton in kit should have status NotProducible");            
            Specify.That(order.Failed.Count()).Should.BeEqualTo(1, "Order should have a failed count of 1");
            Specify.That(order.Failed.ElementAt(0)).Should.BeEqualTo(largeCarton.Id, "Failed list in order should only contain the nonproducible carton");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_Add_NonProducible_Orders_When_Pg_Is_UpdatedWithLargerCorrugate()
        {
            var largeCarton = new Carton { Id = Guid.NewGuid(), Length = 16, Width = 15, Height = 15, DesignId = 201 };
            var smallCarton = new Carton { Id = Guid.NewGuid(), Length = 5, Width = 5, Height = 5, DesignId = 201 };

            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 18.625, Quality = 1, Thickness = 0.16 };
            var cor2 = new Corrugate { Alias = "two", Id = Guid.NewGuid(), Width = 29.250, Quality = 1, Thickness = 0.16 };
            var cor3 = new Corrugate { Alias = "three", Id = Guid.NewGuid(), Width = 55, Quality = 1, Thickness = 0.16 };

            var machine = GetEMMachine(new List<Corrugate> { cor1, cor2 });
            var mg = GetMachineGroup(machine, new List<Corrugate> { cor1, cor2 });

            var pg = GetProductionGroup(new List<Corrugate> { cor1, cor2 }, mg);

            // Set up mocks
            var machineGroupServiceMock = GetMachineGroupServiceMock(mg);
            var machineServiceMock = GetMachineServiceMock(machine);
            var serviceLocatorMock = GetServiceLocatorMock(machineGroupServiceMock, machineServiceMock);
            var corrugatesManagerMock = GetCorrugatesManagerMock(new List<Corrugate> { cor1, cor2, cor3 });
            var productionGroupServiceMock = GetProductionGroupServiceMock(pg);
            var packagingDesignManagerMock = GetPackagingDesignManagerMock(new List<ICarton> { largeCarton, smallCarton });

            // set up classes under test
            var realOptimalCorrugateCalculator = new OptimalCorrugateCalculator(packagingDesignManagerMock.Object,
                serviceLocatorMock.Object, machineGroupServiceMock.Object);

            var corrugateService = new CorrugateService(realOptimalCorrugateCalculator,
                corrugatesManagerMock.Object, serviceLocatorMock.Object, machineServiceMock.Object,
                new Mock<IUICommunicationService>().Object, eventAggregator,
                eventAggregator, new Mock<ILogger>().Object);

            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var ordersSA = GetTestOrderSA(productionGroupServiceMock, machineGroupServiceMock, corrugateService);

            var order1 = new Order(largeCarton, 1);

            var kit1 = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1 })
            };

            var kitOrder = new Order(kit1, 1);

            ordersSA.AddOrderToProductionGroupQueue(pg.Id, kitOrder);
            var queue = ordersSA.GetOrderQueue(pg.Id);
            Specify.That(queue.Count()).Should.BeEqualTo(0, "Order should not be added to queue if it cannot be produced");
            Specify.That(kitOrder.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible, "Not producible order should have status NotProiducible");
            Specify.That(kitOrder.Failed.Count()).Should.BeEqualTo(1, "NonProducibleOrder should have failed count of 1");

            pg.ConfiguredCorrugates.Add(cor3.Id);
            mg.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(mg.Id, new List<Corrugate> { cor1, cor2, cor3 })) });

            var kit2 = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1 })
            };

            var largeOrder1 = new Order(kit2, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "large test order 2"
            };

            ordersSA.AddOrderToProductionGroupQueue(pg.Id, largeOrder1);
            queue = ordersSA.GetOrderQueue(pg.Id);
            
            Specify.That(queue.Count()).Should.BeEqualTo(1, "Order should be added to queue when PG contains corrugate that can produce the order");
            Specify.That(queue.First().Id).Should.BeEqualTo(largeOrder1.Id, "Queue should contain th correct order");
            Specify.That(largeOrder1.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged, "Order in queue should have staged status");
            Specify.That((largeOrder1.Producible as Kit).ItemsToProduce.First().ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleImported, "Carton in order should have imported status");
            Specify.That(largeOrder1.Failed.Count()).Should.BeEqualTo(0, "Producibel order should have failcount of 0");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateOptimalCorrugateForManualOrdersWhenMgCapabilitiesChanges()
        {
            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 20, Quality = 1, Thickness = 0.11 };
            var cor2 = new Corrugate { Alias = "two", Id = Guid.NewGuid(), Width = 30, Quality = 1, Thickness = 0.11 };
            producible.CartonOnCorrugate = new CartonOnCorrugate(new Carton(), cor1, "huh", new PackagingDesignCalculation { Length = 12, Width = 12 }, OrientationEnum.Degree0);
            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            var machineGroup = new MachineGroup()
            {
                Alias = "MG",
                Id = machineGroupId,
                ConfiguredMachines = new ConcurrentList<Guid>() { machineId },
            };

            machineGroupService.Setup(mgs => mgs.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>()))
                .Returns(new CartonOnCorrugate(new Carton(), cor2, "updated", new PackagingDesignCalculation { Length = 12, Width = 12 }, OrientationEnum.Degree0));

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var message = new Message
            {
                MessageType = MachineGroupMessages.CapabilitiesChanged,
                MachineGroupId = machineGroupId,
                MachineGroupName = machineGroup.Alias
            };
            eventAggregator.Publish(message);

            Specify.That(producible.CartonOnCorrugate.Corrugate.Id).Should.BeEqualTo(cor2.Id);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateOptimalCorrugateForMachineGroupOrdersWhenPgChangedAndMgInThatProductionGroup()
        {
            var cor1 = new Corrugate { Alias = "one", Id = Guid.NewGuid(), Width = 20, Quality = 1, Thickness = 0.11 };
            var cor2 = new Corrugate { Alias = "two", Id = Guid.NewGuid(), Width = 30, Quality = 1, Thickness = 0.11 };
            producible.CartonOnCorrugate = new CartonOnCorrugate(new Carton(), cor1, "huh", new PackagingDesignCalculation { Length = 12, Width = 12 }, OrientationEnum.Degree0);
            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            var producible2 = new Carton
            {
                Id = cartonId,
                Length = 5,
                Width = 5,
                Height = 5,
                DesignId = 201
            };
            producible2.CartonOnCorrugate = new CartonOnCorrugate(new Carton(), cor1, "huh", new PackagingDesignCalculation { Length = 12, Width = 12 }, OrientationEnum.Degree0);

            var order2 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order2"
            };

            // add machine to machine group
            var mg = new MachineGroup { Id = machineGroupId };
            machineGroupService.Setup(mgs => mgs.FindByMachineGroupId(It.Is<Guid>(g => g == machineGroupId))).Returns(mg);
            productionGroup.ConfiguredMachineGroups.Add(machineGroupId);

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), mg, It.IsAny<int>()))
                    .Returns(new CartonOnCorrugate(new Carton(), cor2, "updated", new PackagingDesignCalculation { Length = 12, Width = 12 }, OrientationEnum.Degree0));

            //add correct restriction to order, mg or pg 
            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);
            var machineGroupId2 = Guid.NewGuid();

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId2, order2);

            var message = new Message<ProductionGroup>
            {
                Data = productionGroup,
                MessageType = ProductionGroupMessages.ProductionGroupChanged
            };
            eventAggregator.Publish(message);

            //should have changed optimal corrugate.
            Specify.That(producible.CartonOnCorrugate).Should.Not.BeNull("CartonOnCorrugate should not be null.");
            Specify.That(producible.CartonOnCorrugate.Corrugate.Id).Should.BeEqualTo(cor2.Id);
            //should not have updated this one.
            Specify.That(producible2.CartonOnCorrugate.Corrugate.Id).Should.BeEqualTo(cor1.Id);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AddingToMachineQueueIsTrackedInOrder()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>())).Returns(coc);

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(1, "The order started with a quantity of one. Remaining quantity should be 1");

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(0, "The order started with a quantity of one, now we have produced one. Remaining quantity should be 0");

            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.ProducedQuantity).Should.BeEqualTo(1, "The order started with a quantity of one, now we have produced one. Produced quantity should be 1");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected item should have been removed to the InProgress list");
            Specify.That(order.Produced.Count).Should.BeEqualTo(1, "The selected item should have been added to the produced list");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotTileCartonsWithSameCustomerUniqueId_AndSameCartonOnCorrugate_If_Different_Producibles_InSameKit()
        {
            Assert.Inconclusive("Since we currently group on CusomerUniqueId + CartonOnCorrugate.Key if 2 cartons in kit have the same CustomerUniqueID and CartonOnCorrugate they will be tiled, thus breaking the kit order");
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "key1", null, OrientationEnum.Degree0) { TileCount = 2 };
            var coc1 = new CartonOnCorrugate(new Carton(), cor1, "key1", null, OrientationEnum.Degree0) { TileCount = 2 };
            coc.ProducibleMachines.Add(machineId, "");
            coc1.ProducibleMachines.Add(machineId, "");

            var carton1 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc };
            var carton2 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc1 };

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton1, machineGroup, It.IsAny<int>())).Returns(coc);
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton2, machineGroup, It.IsAny<int>())).Returns(coc1);

            var order1 = new Order(carton1, 1) { OrderId = "First order in the kit" };
            var order2 = new Order(carton2, 1) { OrderId = "Second order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1, order2 }),
            };

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(kit, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;
            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeLogicallyEqualTo(2);

            Specify.That(selected.ItemsToProduce[0].CustomerUniqueId).Should.BeEqualTo(order.OrderId + ":111:1", "Should contain correct customeruniqueid ({orderid}:{cartonId}:{tilenumber})");
            Specify.That(selected.ItemsToProduce[1].CustomerUniqueId).Should.BeEqualTo(order.OrderId + ":111:1", "Should contain correct customeruniqueid ({orderid}:{cartonId}:{tilenumber})");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeAbleToCancelProducingOrder()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "key1", null, OrientationEnum.Degree0) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");

            var carton1 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc };

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton1, machineGroup, It.IsAny<int>())).Returns(coc);

            var order1 = new Order(carton1, 20) { OrderId = "First order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1 }),
            };

            var ordersSA = OrdersSelectionAlgorithm;

            var order = new Order(kit, 2)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(order.ProducibleInProgress.Count()).Should.BeEqualTo(1);
            Specify.That(order.ProducibleInProgress[0].Id).Should.BeEqualTo(selected.Id);

            var cancelOrderCalled = false;

            OrderMessages.CancelOrders.OnMessage<List<Guid>>(eventAggregator, new Mock<ILogger>().Object, msg =>
            {
                cancelOrderCalled = true;
            });

            var message = new Message<List<Guid>>
            {
                Data = new List<Guid>() { order.Id },
                MessageType = OrderMessages.CancelOrders
            };
            eventAggregator.Publish(message);

            Retry.For(() => cancelOrderCalled, TimeSpan.FromSeconds(10));
            Specify.That(cancelOrderCalled).Should.BeTrue();

            Specify.That(order.ProducibleInProgress.Count()).Should.BeEqualTo(0);
            Specify.That(order.ProducibleStatus).Should.BeLogicallyEqualTo(ProducibleStatuses.ProducibleRemoved);
            Specify.That(order.Producible.ProducibleStatus).Should.BeLogicallyEqualTo(ProducibleStatuses.ProducibleRemoved);
            (order.Producible as Kit).ItemsToProduce.ForEach(item => Specify.That(item.ProducibleStatus).Should.BeLogicallyEqualTo(ProducibleStatuses.ProducibleRemoved));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendFullListOfOpenOrdersWhenMainOrderIsCompleted()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "key1", null, OrientationEnum.Degree0) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");

            var carton1 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc };

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton1, machineGroup, It.IsAny<int>())).Returns(coc);

            var order1 = new Order(carton1, 1) { OrderId = "First order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1 }),
            };

            var ordersSA = OrdersSelectionAlgorithm;

            var mainOrderId = Guid.NewGuid();

            var order = new Order(kit, 1)
            {
                Id = mainOrderId,
                OrderId = "Kit order"
            };

            orders.Setup(o => o.GetOpenOrders(It.IsAny<int>())).Returns(new List<Order> { order });

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            orders.ResetCalls();
            uiCommunicationService.ResetCalls();

            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            orders.Verify(o => o.GetOpenOrders(It.IsAny<int>()), Times.Exactly(1), "Expecting call to GetOpenOrders when order is completed");

            uiCommunicationService.Verify(s => s.SendMessageToUI(
                It.Is<Message<List<Order>>>(m =>
                    m.MessageType == OrderMessages.Orders &&
                    m.Data.Any() &&
                    m.Data.First() == order),
                It.IsAny<bool>(),
                It.IsAny<bool>()), Times.Exactly(1), "Expecting SendmessageToUi to be called containing the latest 20 open orders in the database when order is completed or failed");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendFullListOfOpenOrdersWhenMainOrderIsNotProducible()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "key1", null, OrientationEnum.Degree0) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");

            var carton1 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc };

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton1, machineGroup, It.IsAny<int>())).Returns(coc);

            var order1 = new Order(carton1, 1) { OrderId = "First order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1 }),
            };

            var ordersSA = OrdersSelectionAlgorithm;

            var mainOrderId = Guid.NewGuid();

            var order = new Order(kit, 1)
            {
                Id = mainOrderId,
                OrderId = "Kit order"
            };

            orders.Setup(o => o.GetOpenOrders(It.IsAny<int>())).Returns(new List<Order> { order });

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            orders.ResetCalls();
            uiCommunicationService.ResetCalls();

            selected.ProducibleStatus = ErrorProducibleStatuses.NotProducible;

            orders.Verify(o => o.GetOpenOrders(It.IsAny<int>()), Times.Exactly(1), "Expecting call to GetOpenOrders when order is completed");

            uiCommunicationService.Verify(s => s.SendMessageToUI(
                It.Is<Message<List<Order>>>(m =>
                    m.MessageType == OrderMessages.Orders &&
                    m.Data.Any() &&
                    m.Data.First() == order),
                It.IsAny<bool>(),
                It.IsAny<bool>()), Times.Exactly(1), "Expecting SendmessageToUi to be called containing the latest 20 open orders in the database when order is completed or failed");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleOrders_WhereTheProducibleIs_A_Kit()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "key1", null, OrientationEnum.Degree0) { TileCount = 1 };
            var coc1 = new CartonOnCorrugate(new Carton(), cor1, "key2", null, OrientationEnum.Degree0) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            coc1.ProducibleMachines.Add(machineId, "");

            var carton1 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc, Length = 5, Width = 5, Height = 5 };
            var carton2 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "222", CartonOnCorrugate = coc1, Length = 15, Width = 5, Height = 5 };

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton1, machineGroup, It.IsAny<int>())).Returns(coc);
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton2, machineGroup, It.IsAny<int>())).Returns(coc1);

            var order1 = new Order(carton1, 2) { OrderId = "First order in the kit" };
            var order2 = new Order(carton2, 1) { OrderId = "Second order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1, order2 }),
            };

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(kit, 2)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;
            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeLogicallyEqualTo(3);

            Specify.That(selected.ItemsToProduce[0].CustomerUniqueId).Should.BeEqualTo(order.OrderId + ":111:2:1", "Should contain correct customeruniqueid ({orderid}:{cartonId}:{tilenumber})");
            Specify.That(selected.ItemsToProduce[1].CustomerUniqueId).Should.BeEqualTo(order.OrderId + ":111:2:2", "Should contain correct customeruniqueid ({orderid}:{cartonId}:{tilenumber})");
            Specify.That(selected.ItemsToProduce[2].CustomerUniqueId).Should.BeEqualTo(order.OrderId + ":222:2:1", "Should contain correct customeruniqueid ({orderid}:{cartonId}:{tilenumber})");

            Specify.That(selected.CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId);
            selected.ItemsToProduce.ForEach(item =>
            {
                item.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            });

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;
            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeLogicallyEqualTo(3);

            Specify.That(selected.ItemsToProduce[0].CustomerUniqueId).Should.BeEqualTo(order.OrderId + ":111:1:1", "Should contain correct customeruniqueid ({orderid}:{cartonId}:{tilenumber})");
            Specify.That(selected.ItemsToProduce[1].CustomerUniqueId).Should.BeEqualTo(order.OrderId + ":111:1:2", "Should contain correct customeruniqueid ({orderid}:{cartonId}:{tilenumber})");
            Specify.That(selected.ItemsToProduce[2].CustomerUniqueId).Should.BeEqualTo(order.OrderId + ":222:1:1", "Should contain correct customeruniqueid ({orderid}:{cartonId}:{tilenumber})");

            Specify.That(selected.CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId);
            selected.ItemsToProduce.ForEach(item =>
            {
                item.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            });

            Specify.That(order.ProducibleStatus).Should.BeLogicallyEqualTo(ProducibleStatuses.ProducibleCompleted);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleOrders_WhereTheProducibleIs_A_Kit_AndTheKitContains_TiledCartons()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "coc1", null, OrientationEnum.Degree0) { TileCount = 2 };
            var coc2 = new CartonOnCorrugate(new Carton(), cor1, "coc2", null, OrientationEnum.Degree0) { TileCount = 3 };
            coc.ProducibleMachines.Add(machineId, "");
            coc2.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, 2)).Returns(coc);
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, 3)).Returns(coc2);

            var order1 = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc2, Length = 5, Width = 5, Height = 5 }, 3) { OrderId = "First order in the kit" };
            var order2 = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "222", CartonOnCorrugate = coc, Length = 15, Width = 5, Height = 5 }, 2) { OrderId = "Second order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1, order2 }),
            };

            var order = new Order(kit, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            var ordersSA = OrdersSelectionAlgorithm;

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);
            Kit selected = null;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.First().CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId + ":" + order1.Producible.CustomerUniqueId + ":3+2+1");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            Specify.That(selected.ItemsToProduce.Last().CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId + ":" + order2.Producible.CustomerUniqueId + ":2+1");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            selected.ItemsToProduce.ForEach(item =>
            {
                item.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            });

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;
            Specify.That(selected).Should.BeNull();

            Specify.That(order.ProducibleStatus).Should.BeLogicallyEqualTo(ProducibleStatuses.ProducibleCompleted);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_UpdateTheKitStaus_When_Producing()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree0) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, 2)).Returns(coc);

            var cartonOrder = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc }, 2) { OrderId = "First order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { cartonOrder }),
            };


            var order = new Order(kit, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            var ordersSA = OrdersSelectionAlgorithm;

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);
            Kit selected = null;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(order.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count()).Should.BeEqualTo(2);

            var firstCarton = selected.ItemsToProduce.First();

            firstCarton.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachineGroup;
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);
            Specify.That(selected.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleSentToMachineGroup);
            firstCarton.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);
            Specify.That(selected.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleSentToMachine);
            firstCarton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleProductionStarted);
            Specify.That(selected.ProducibleStatus).Should.BeEqualTo(NotInProductionProducibleStatuses.ProducibleStaged);

            var secondCarton = selected.ItemsToProduce.Last();
            secondCarton.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(selected.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("Bug")]
        [TestCategory("Bug 15709")]
        public void Should_Keep_The_Correct_CustomerUniqueId_WhenProducing_Multiples_Of_The_Same_SingleOut()
        {
            Assert.Inconclusive("Will return the wrong name because of a workaround for 15709. The test is correct and the implementation should be fixed.");
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "coc1", null, OrientationEnum.Degree0) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>()))
                .Returns(coc);

            var cartonOrder = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc }, 1)
            {
                OrderId = "First order in the kit"
            };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { cartonOrder }),
            };

            var order = new Order(kit, 5)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            var ordersSA = OrdersSelectionAlgorithm;

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);
            Kit selected = null;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.First().CustomerUniqueId).Should.BeLogicallyEqualTo("Kit order:111:1");

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.First().CustomerUniqueId).Should.BeLogicallyEqualTo("Kit order:111:1");

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.First().CustomerUniqueId).Should.BeLogicallyEqualTo("Kit order:111:1");

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.First().CustomerUniqueId).Should.BeLogicallyEqualTo("Kit order:111:1");

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.First().CustomerUniqueId).Should.BeLogicallyEqualTo("Kit order:111:1");

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.BeNull();

        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("Bug")]
        [TestCategory("Bug 15709")]
        public void Should_Tile_Between_Orders_When_Order_Contains_One_Carton()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "coc1", null, OrientationEnum.Degree0) { TileCount = 2 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>()))
                .Returns(coc);

            var cartonOrder = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc }, 1)
            {
                OrderId = "First order in the kit"
            };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { cartonOrder }),
            };

            var order = new Order(kit, 5)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            var ordersSA = OrdersSelectionAlgorithm;

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);
            Kit selected = null;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.OfType<TiledCarton>().Count()).Should.BeEqualTo(1);

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.OfType<TiledCarton>().Count()).Should.BeEqualTo(1);

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.OfType<Carton>().Count()).Should.BeEqualTo(1);

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;
            Specify.That(selected).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("Bug")]
        [TestCategory("Bug 15826")]
        public void ShouldProduceWholeKit_WhenTiling()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 72, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "coc1", null, OrientationEnum.Degree0) { TileCount = 2 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>()))
                .Returns(coc);

            var cartonOrder = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc }, 2)
            {
                OrderId = "OrderWith2Cartons"
            };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { cartonOrder }),
            };

            var order = new Order(kit, 2)
            {
                Id = Guid.NewGuid(),
                OrderId = "KitWith2Orders"
            };

            var ordersSA = OrdersSelectionAlgorithm;

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);
            Kit selected = null;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull("We expect to get something back");
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1, "We expect to get one producible back and it should be a tiled carton");
            Specify.That(selected.ItemsToProduce.OfType<TiledCarton>().Count()).Should.BeEqualTo(1, "We expect to get one producible back and it should be a tiled carton");
            var selectedGuid = selected.Id;
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.RemainingQuantity)
                .Should.BeEqualTo(
                    1,
                    "The issue with this bug was that the tiled carton was added to ProducibleInProgress twice when it should have only been added once so RemainingQuantity was reduced to 0");

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull("We expect to get something back");
            Specify.That(selectedGuid).Should.Not.BeEqualTo(selected.Id, "We don't want the same producible back as we got before");
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(1, "We still expect to get one producible back and it should be a tiled carton");
            Specify.That(selected.ItemsToProduce.OfType<TiledCarton>().Count()).Should.BeEqualTo(1, "We still expect to get one producible back and it should be a tiled carton");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(0, "Now all the cartons in the order have been selected and produced so the quantity should be 0");
            Specify.That(order.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted, "The whole kit should now be completed.");

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;
            Specify.That(selected).Should.BeNull("The order is completed and we don't have anything in the queue so this should be null.");
        }

        [TestMethod]
        [TestCategory("Integration")]
        [TestCategory("Bug")]
        [TestCategory("Bug 15709")]
        public void Should_Tile_Between_Orders_When_Order_Contains_One_Carton_And_One_Label()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "coc1", null, OrientationEnum.Degree0) { TileCount = 2 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>()))
                .Returns(coc);

            var cartonOrder = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc }, 1)
            {
                OrderId = "First order in the kit"
            };

            var labelOrder = new Order(new Label() { Id = Guid.NewGuid(), CustomerUniqueId = "222" }, 1);

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { cartonOrder, labelOrder }),
            };

            var order = new Order(kit, 5)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            var ordersSA = OrdersSelectionAlgorithm;

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);
            Kit selected = null;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(3);
            Specify.That(selected.ItemsToProduce.OfType<TiledCarton>().Count()).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.OfType<Label>().Count()).Should.BeEqualTo(2);

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(3);
            Specify.That(selected.ItemsToProduce.OfType<TiledCarton>().Count()).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.OfType<Label>().Count()).Should.BeEqualTo(2);

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count).Should.BeEqualTo(2);
            Specify.That(selected.ItemsToProduce.OfType<Carton>().Count()).Should.BeEqualTo(1);
            Specify.That(selected.ItemsToProduce.OfType<Label>().Count()).Should.BeEqualTo(1);

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;
            Specify.That(selected).Should.BeNull();
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldSet_The_CorrectQuantities_With_Kit_Containing_Cartons()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "coc1", null, OrientationEnum.Degree0) { TileCount = 1 };
            var coc2 = new CartonOnCorrugate(new Carton(), cor1, "coc2", null, OrientationEnum.Degree0) { TileCount = 2 };
            coc.ProducibleMachines.Add(machineId, "");
            coc2.ProducibleMachines.Add(machineId, "");

            var carton1 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc2, Length = 5, Width = 5, Height = 5 };
            var carton2 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "222", CartonOnCorrugate = coc, Length = 15, Width = 15, Height = 15 };
            var carton3 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "333", CartonOnCorrugate = coc2, Length = 5, Width = 5, Height = 5 };

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton2, machineGroup, It.IsAny<int>())).Returns(coc);
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton1, machineGroup, It.IsAny<int>())).Returns(coc2);
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(carton3, machineGroup, It.IsAny<int>())).Returns(coc2);

            var order1 = new Order(carton1, 2) { OrderId = "First order in the kit" };
            var order2 = new Order(carton2, 1) { OrderId = "Second order in the kit" };
            var order3 = new Order(carton3, 4) { OrderId = "Third order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1, order2, order3 }),
            };


            var order = new Order(kit, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            var ordersSA = OrdersSelectionAlgorithm;

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);
            Kit selected = null;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count()).Should.BeEqualTo(4);
            var firstOrder = selected.ItemsToProduce.First();
            Specify.That(firstOrder.CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId + ":" + order1.Producible.CustomerUniqueId + ":2+1");

            var secondOrder = selected.ItemsToProduce.Skip(1).Take(1).FirstOrDefault();
            Specify.That(secondOrder).Should.Not.BeNull();
            Specify.That(secondOrder.CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId + ":" + order2.Producible.CustomerUniqueId + ":1:1");

            var thirdOrder = selected.ItemsToProduce.Skip(2).Take(1).FirstOrDefault();
            Specify.That(thirdOrder as TiledCarton).Should.Not.BeNull();
            Specify.That(thirdOrder).Should.Not.BeNull();
            Specify.That(thirdOrder.CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId + ":" + order3.Producible.CustomerUniqueId + ":4+3");

            var fourthOrder = selected.ItemsToProduce.Last();
            Specify.That(fourthOrder as TiledCarton).Should.Not.BeNull();
            Specify.That(fourthOrder).Should.Not.BeNull();
            Specify.That(fourthOrder.CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId + ":" + order3.Producible.CustomerUniqueId + ":2+1");

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(0);
            Specify.That(order.InProgressQuantity).Should.BeEqualTo(1);
            Specify.That(order.ProducibleInProgress.Count()).Should.BeEqualTo(1);
            Specify.That(order.ProducibleInProgress.First().Id).Should.BeEqualTo(selected.Id);

            firstOrder.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.InProgressQuantity).Should.BeEqualTo(1);
            Specify.That(order.ProducibleInProgress.Count()).Should.BeEqualTo(1);
            Specify.That(order.ProducibleInProgress.First().Id).Should.BeEqualTo(selected.Id);

            secondOrder.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.InProgressQuantity).Should.BeEqualTo(1);
            Specify.That(order.ProducibleInProgress.Count()).Should.BeEqualTo(1);
            Specify.That(order.ProducibleInProgress.First().Id).Should.BeEqualTo(selected.Id);

            thirdOrder.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.InProgressQuantity).Should.BeEqualTo(1);
            Specify.That(order.ProducibleInProgress.Count()).Should.BeEqualTo(1);
            Specify.That(order.ProducibleInProgress.First().Id).Should.BeEqualTo(selected.Id);


            fourthOrder.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.InProgressQuantity).Should.BeEqualTo(0);
            Specify.That(order.ProducibleInProgress.Count()).Should.BeEqualTo(0);

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;
            Specify.That(selected).Should.BeNull();

            Specify.That(order.ProducibleStatus).Should.BeLogicallyEqualTo(ProducibleStatuses.ProducibleCompleted);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleOrders_WhereTheProducibleIs_A_Kit_Containing_Multiple_Tiled_Boxes_And_Labels()
        {
            /*
             *  Before we send the kit to production we prepare it by tiling the ´cartons that can be tiled:
             *               
             *  From:
             *  Order x 1             
             *      |-Kit
             *          |-ItemsToProduce
             *                 |-Carton
             *                 |-Carton
             *                 |-Carton
             *                 |-Label
             *                 |-Label
             *  To:
             *  Order x 1             
             *      |-Kit
             *          |-ItemsToProduce
             *                 |-TiledCarton
             *                 |-Carton
             *                 |-Label
             *                 |-Label
             */

            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "coc1", null, OrientationEnum.Degree0) { TileCount = 1 };
            var coc2 = new CartonOnCorrugate(new Carton(), cor1, "coc2", null, OrientationEnum.Degree0) { TileCount = 2 };
            coc.ProducibleMachines.Add(machineId, "");
            coc2.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, 3)).Returns(coc);
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, 5)).Returns(coc2);


            var singleOutCartonOrder = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "singleCarton", CartonOnCorrugate = coc, Length = 15, Width = 15, Height = 15 }, 3) { OrderId = "Single carton order in the kit" };
            var tileableCartonOrder = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "tiledCarton", CartonOnCorrugate = coc2, Length = 5, Width = 5, Height = 5 }, 5) { OrderId = "Tiled carton order in the kit" };
            var labelOrder = new Order(new Label() { Id = Guid.NewGuid(), CustomerUniqueId = "labels" }, 3) { OrderId = "label Order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { singleOutCartonOrder, tileableCartonOrder, labelOrder }),
            };

            var order = new Order(kit, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            var ordersSA = OrdersSelectionAlgorithm;

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);
            Kit selected = null;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected.ItemsToProduce.Count()).Should.BeEqualTo(9);

            var tiled = selected.ItemsToProduce.OfType<TiledCarton>();
            Specify.That(tiled.Count()).Should.BeEqualTo(2);
            Specify.That(tiled.First().CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId + ":" + tileableCartonOrder.Producible.CustomerUniqueId + ":5+4", "First tiled carton should contain cartons from tileableOrder");
            Specify.That(tiled.Last().CustomerUniqueId).Should.BeLogicallyEqualTo(order.OrderId + ":" + tileableCartonOrder.Producible.CustomerUniqueId + ":3+2", "Second tiled carton should contain cartons from tileableOrder");

            var singleOuts = selected.ItemsToProduce.OfType<Carton>();
            Specify.That(singleOuts.Count()).Should.BeEqualTo(4);
            Specify.That(singleOuts.Count(c => c.CustomerUniqueId.Contains(tileableCartonOrder.Producible.CustomerUniqueId))).Should.BeEqualTo(1, "There should be one and only one carton in the single outs from the tileable order");
            Specify.That(singleOuts.Count(c => c.CustomerUniqueId.Contains(singleOutCartonOrder.Producible.CustomerUniqueId))).Should.BeEqualTo(3, "There should be three cartons in the single outs from the single out order");

            var labels = selected.ItemsToProduce.OfType<IPrintable>();
            Specify.That(labels.Count()).Should.BeEqualTo(labelOrder.OriginalQuantity, String.Format("There should be {0} labels in the selected kit", labelOrder.OriginalQuantity));
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleQuantitiesWhenTiling()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var mg = new MachineGroup { Id = machineGroupId };
            mg.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            mg.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(mg);
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 2 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), mg, It.IsAny<int>())).Returns(coc);

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 2)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(2, "The order started with a quantity of one. Remaining quantity should be 2");

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);


            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as TiledCarton;
            Specify.That(selected).Should.Not.BeNull("Get job didn't return any.");
            Specify.That(order.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(selected.CustomerUniqueId).Should.BeEqualTo(order.OrderId + ":2+1");
            Specify.That(selected.Tiles.Count()).Should.BeEqualTo(2, "Expected carton to be tileable.");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(2, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[1]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(0, "The order started with a quantity of two, now we have produced two tiles. Remaining quantity should be 0");

            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.ProducedQuantity).Should.BeEqualTo(2, "The order started with a quantity of two, now we have produced two tiles. Produced quantity should be 2");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected items should have been removed from the InProgress list");
            Specify.That(order.Produced.Count).Should.BeEqualTo(2, "The selected items should have been added to the produced list");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleOddQuantitiesWhenTiling()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var mg = new MachineGroup { Id = machineGroupId };
            mg.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            mg.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(mg);
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 2 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), mg, It.IsAny<int>())).Returns(coc);

            var ordersSA = OrdersSelectionAlgorithm;

            var order2 = new Order(producible, 3)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            Specify.That(order2.RemainingQuantity).Should.BeEqualTo(3, "The order started with a quantity of one. Remaining quantity should be 3");

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order2);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as TiledCarton;
            Specify.That(selected).Should.Not.BeNull("Get job didn't return any.");
            Specify.That(selected.CustomerUniqueId).Should.BeEqualTo(order2.OrderId + ":3+2");
            Specify.That(order2.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(selected.Tiles.Count()).Should.BeEqualTo(2, "Expected carton to be tileable.");
            Specify.That(order2.ProducibleInProgress.Count).Should.BeEqualTo(2, "The selected item should have been added to the InProgress list");
            Specify.That(order2.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");
            Specify.That(order2.ProducibleInProgress[1]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");

            Specify.That(order2.RemainingQuantity).Should.BeEqualTo(1, "The order started with a quantity of two, now we have produced two tiles. Remaining quantity should be 0");

            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order2.ProducedQuantity).Should.BeEqualTo(2, "The order started with a quantity of two, now we have produced two tiles. Produced quantity should be 2");
            Specify.That(order2.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected items should have been removed from the InProgress list");
            Specify.That(order2.Produced.Count).Should.BeEqualTo(2, "The selected items should have been added to the produced list");

            var selected2 = ordersSA.GetManualJobForMachineGroup(machineGroupId) as ICarton;
            Specify.That(selected2.CartonOnCorrugate.TileCount).Should.BeEqualTo(1, "Only expected 1 tile");
            Specify.That(order2.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(selected2.CustomerUniqueId).Should.BeEqualTo(order2.OrderId + ":1");
            Specify.That(order2.ProducibleInProgress[0]).Should.BeEqualTo(selected2, "The selected item should have been added to the InProgress list");
            Specify.That(order2.RemainingQuantity).Should.BeEqualTo(0, "The order started with a quantity of two, now we have produced two tiles. Remaining quantity should be 0");

            selected2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order2.ProducedQuantity).Should.BeEqualTo(3, "The order started with a quantity of two, now we have produced two tiles. Produced quantity should be 2");
            Specify.That(order2.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected items should have been removed from the InProgress list");
            Specify.That(order2.Produced.Count).Should.BeEqualTo(3, "The selected items should have been added to the produced list");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ReproduceOrderThatWasCompletedShouldUpdateFailedQuantityAnRaiseNotificationToGetWorkForPgAndUpdateUI()
        {
            var getWorkCalled = false;
            eventAggregator.GetEvent<Message<Guid>>()
                .Where(m => m.MessageType == ProductionGroupMessages.GetWorkForProductionGroup)
                .Subscribe(m =>
                {
                    getWorkCalled = true;
                    Specify.That(m.Data == productionGroupId);
                });

            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            machineGroupService.Setup(m => m.FindByMachineGroupId(It.IsAny<Guid>())).Returns(new MachineGroup { ProductionMode = MachineGroupProductionModes.AutoProductionMode });
            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(producible, 2)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"

            };
            order1.Restrictions.Add(new ProductionGroupSpecificRestriction(productionGroupId));
            orders.Setup(o => o.Find(order1.Id)).Returns(order1);
            orders.Setup(o => o.GetOpenOrders(It.IsAny<int>())).Returns(new List<Order> { order1 });
            //to test
            eventAggregator.Publish(new Message<Order> { MessageType = OrderMessages.ReproduceCartonForOrder, MachineGroupId = machineGroupId, Data = new Order(new Kit(), 2) { Id = order1.Id, ReproducedQuantity = 2 } });

            Specify.That(order1.ReproducedQuantity).Should.BeEqualTo(2);
            Specify.That(getWorkCalled).Should.BeTrue("Get work was not called.");

        }
        [TestMethod]
        [TestCategory("Unit")]
        public void ReproduceOrderThatWasCompletedShouldUpdateFailedQuantityAnRaiseNotificationToGetWorkForMgAndUpdateUI()
        {
            var getWorkCalled = false;
            eventAggregator.GetEvent<Message<Guid>>()
                .Where(m => m.MessageType == MachineGroupMessages.GetWorkForMachineGroup)
                .Subscribe(m =>
                {
                    getWorkCalled = true;
                    Specify.That(m.Data == machineGroupId);
                });

            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            machineGroupService.Setup(m => m.FindByMachineGroupId(It.IsAny<Guid>())).Returns(new MachineGroup { ProductionMode = MachineGroupProductionModes.AutoProductionMode });
            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(producible, 2)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"

            };
            order1.Restrictions.Add(new MachineGroupSpecificRestriction(machineGroupId));
            orders.Setup(o => o.Find(order1.Id)).Returns(order1);
            orders.Setup(o => o.GetOpenOrders(It.IsAny<int>())).Returns(new List<Order> { order1 });

            //to test
            eventAggregator.Publish(new Message<Order> { MessageType = OrderMessages.ReproduceCartonForOrder, MachineGroupId = machineGroupId, Data = new Order(new Kit(), 2) { Id = order1.Id, ReproducedQuantity = 2 } });

            Thread.Sleep(500);
            Specify.That(order1.ReproducedQuantity).Should.BeEqualTo(2);
            Specify.That(getWorkCalled).Should.BeTrue("Get work was not called.");

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ReproduceOrderWithTwoIdenticalCartonsShouldTileIfPossible()
        {
            var getWorkCalled = false;
            eventAggregator.GetEvent<Message<Guid>>()
                .Where(m => m.MessageType == MachineGroupMessages.GetWorkForMachineGroup)
                .Subscribe(m =>
                {
                    getWorkCalled = true;
                    Specify.That(m.Data == machineGroupId);
                });

            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            machineGroupService.Setup(m => m.FindByMachineGroupId(It.IsAny<Guid>())).Returns(new MachineGroup { ProductionMode = MachineGroupProductionModes.AutoProductionMode });

            var ordersSA = OrdersSelectionAlgorithm;
            var producible = new Kit();
            producible.AddProducible(new Carton() { Length = 5, Width = 5, Height = 2, DesignId = 201 });
            producible.AddProducible(new Carton() { Length = 5, Width = 5, Height = 2, DesignId = 201 });
            var order1 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order",
                Producible = producible
            };
            order1.Produced.Add(Guid.NewGuid());
            order1.Restrictions.Add(new MachineGroupSpecificRestriction(machineGroupId));
            orders.Setup(o => o.Find(order1.Id)).Returns(order1);
            orders.Setup(o => o.GetOpenOrders(It.IsAny<int>())).Returns(new List<Order> { order1 });

            //to test
            eventAggregator.Publish(new Message<Order> { MessageType = OrderMessages.ReproduceCartonForOrder, MachineGroupId = machineGroupId, Data = new Order(new Kit(), 1) { Id = order1.Id, ReproducedQuantity = 1 } });

            Thread.Sleep(500);
            Specify.That(order1.ReproducedQuantity).Should.BeEqualTo(1);
            Specify.That(getWorkCalled).Should.BeTrue("Get work was not called.");

            corrugateServiceMock.Verify(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), It.IsAny<MachineGroup>(), 2), Times.Exactly(2));
            corrugateServiceMock.Verify(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), It.IsAny<MachineGroup>(), 1), Times.Never());
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ReproduceOrderWithTwoIdenticalCartonsAndOneOtherShouldTileIdenticalAndProduceOtherAsSingle()
        {
            var getWorkCalled = false;
            eventAggregator.GetEvent<Message<Guid>>()
                .Where(m => m.MessageType == MachineGroupMessages.GetWorkForMachineGroup)
                .Subscribe(m =>
                {
                    getWorkCalled = true;
                    Specify.That(m.Data == machineGroupId);
                });

            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            machineGroupService.Setup(m => m.FindByMachineGroupId(It.IsAny<Guid>())).Returns(new MachineGroup { ProductionMode = MachineGroupProductionModes.AutoProductionMode });

            var ordersSA = OrdersSelectionAlgorithm;
            var c1 = new Carton() { Length = 5, Width = 5, Height = 5, DesignId = 201 };
            var c2 = new Carton() { Length = 5, Width = 5, Height = 5, DesignId = 201 };
            var c3 = new Carton() { Length = 15, Width = 5, Height = 5, DesignId = 201 };

            var producible = new Kit();
            producible.AddProducible(c1);
            producible.AddProducible(c2);
            producible.AddProducible(c3);

            var order1 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order",
                Producible = producible
            };
            order1.Produced.Add(Guid.NewGuid());
            order1.Restrictions.Add(new MachineGroupSpecificRestriction(machineGroupId));
            orders.Setup(o => o.Find(order1.Id)).Returns(order1);
            orders.Setup(o => o.GetOpenOrders(It.IsAny<int>())).Returns(new List<Order> { order1 });

            //to test
            eventAggregator.Publish(new Message<Order> { MessageType = OrderMessages.ReproduceCartonForOrder, MachineGroupId = machineGroupId, Data = new Order(new Kit(), 1) { Id = order1.Id, ReproducedQuantity = 1 } });

            Thread.Sleep(500);
            Specify.That(order1.ReproducedQuantity).Should.BeEqualTo(1);
            Specify.That(getWorkCalled).Should.BeTrue("Get work was not called.");

            corrugateServiceMock.Verify(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), It.IsAny<MachineGroup>(), 2), Times.Exactly(2));
            corrugateServiceMock.Verify(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), It.IsAny<MachineGroup>(), 1), Times.Exactly(1));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ReproduceOrderWithTwoIdenticalCartonsAndOneOtherShouldInBetweenShouldProduceAllAsSingle()
        {
            //TODO: test that c1, c2, c3 do not tile if c1 and c3 CAN tile but has c2 in between

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ReproduceOrderWithTwoIdenticalCartonsWithAnOtherInBetweenShouldNotTile()
        {
            var getWorkCalled = false;
            eventAggregator.GetEvent<Message<Guid>>()
                .Where(m => m.MessageType == MachineGroupMessages.GetWorkForMachineGroup)
                .Subscribe(m =>
                {
                    getWorkCalled = true;
                    Specify.That(m.Data == machineGroupId);
                });

            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            machineGroupService.Setup(m => m.FindByMachineGroupId(It.IsAny<Guid>())).Returns(new MachineGroup { ProductionMode = MachineGroupProductionModes.AutoProductionMode });

            var ordersSA = OrdersSelectionAlgorithm;
            var c1 = new Carton() { Length = 5, Width = 5, Height = 5, DesignId = 201 };
            var c2 = new Carton() { Length = 15, Width = 5, Height = 5, DesignId = 201 };
            var c3 = new Carton() { Length = 5, Width = 5, Height = 5, DesignId = 201 };

            var producible = new Kit();
            producible.AddProducible(c1);
            producible.AddProducible(c2);
            producible.AddProducible(c3);

            var order1 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order",
                Producible = producible
            };
            order1.Produced.Add(Guid.NewGuid());
            order1.Restrictions.Add(new MachineGroupSpecificRestriction(machineGroupId));
            orders.Setup(o => o.Find(order1.Id)).Returns(order1);
            orders.Setup(o => o.GetOpenOrders(It.IsAny<int>())).Returns(new List<Order> { order1 });

            //to test
            eventAggregator.Publish(new Message<Order> { MessageType = OrderMessages.ReproduceCartonForOrder, MachineGroupId = machineGroupId, Data = new Order(new Kit(), 1) { Id = order1.Id, ReproducedQuantity = 1 } });

            Thread.Sleep(500);
            Specify.That(order1.ReproducedQuantity).Should.BeEqualTo(1);
            Specify.That(getWorkCalled).Should.BeTrue("Get work was not called.");

            corrugateServiceMock.Verify(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), It.IsAny<MachineGroup>(), 1), Times.Exactly(3));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleKitsContainingMultipleBoxes()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            var mg = new MachineGroup();



            var corrugate = new Corrugate { Id = Guid.NewGuid(), Width = 19, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), corrugate, "key1", null, OrientationEnum.Degree0) { TileCount = 2 };
            var coc1 = new CartonOnCorrugate(new Carton(), corrugate, "key2", null, OrientationEnum.Degree0) { TileCount = 2 };
            coc.ProducibleMachines.Add(machineId, "");
            coc1.ProducibleMachines.Add(machineId, "");

            var carton1 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc, Length = 5, Width = 5, Height = 5 };
            var carton2 = new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "222", CartonOnCorrugate = coc1, Length = 10, Width = 5, Height = 5 };

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(carton1, productionGroup, It.IsAny<int>()))
                .Returns(coc);

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(carton2, productionGroup, It.IsAny<int>()))
                .Returns(coc1);

            var emMachine = new EmMachine() { Alias = "Em7", Id = machineId };

            emMachine.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { corrugate })),
            });
            mg.ConfiguredMachines.Add(emMachine.Id);
            mg.AddOrUpdateCapabilities(emMachine.CurrentCapabilities);

            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(mg);
            var ordersSA = OrdersSelectionAlgorithm;


            var order1 = new Order(carton1, 10) { OrderId = "First order in the kit" };
            var order2 = new Order(carton2, 1) { OrderId = "Second order in the kit" };
            var order3 = new Order(new Label { Id = Guid.NewGuid(), CustomerUniqueId = "333" }, 1) { OrderId = "Third order in the kit" };

            Specify.That(order1.RemainingQuantity)
                .Should.BeEqualTo(10, "The order started with a quantity of one. Remaining quantity should be 10");

            var kit = new Kit();

            kit.AddProducible(order1);
            kit.AddProducible(order2);
            kit.AddProducible(order3);

            var newOrder = new Order(kit, 1);

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, newOrder);

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId);

            Specify.That(selected).Should.Not.BeNull("Expected something to be returned");
            Specify.That(selected.ProducibleType).Should.BeLogicallyEqualTo(ProducibleTypes.Kit);
            var selectedKit = selected as Kit;
            Specify.That(selectedKit.ItemsToProduce.OfType<TiledCarton>().Count()).Should.BeLogicallyEqualTo(5, "Expected five tiled cartons");
            Specify.That(selectedKit.ItemsToProduce.OfType<Carton>().Count()).Should.BeLogicallyEqualTo(1, "Expected one untiled carton");
            Specify.That(selectedKit.ItemsToProduce.OfType<Label>().Count()).Should.BeLogicallyEqualTo(1, "Expected one label");
            Specify.That(selectedKit.ItemsToProduce.Count).Should.BeLogicallyEqualTo(7, "Expected seven producibles");

        }
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendOrderUpdateMessageWhenStatusOfOrderChanges()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31.49, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.AddOrUpdateCapabilities(new List<ICapability> { new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 })) });
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree0) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>())).Returns(coc);

            var order1 = new Order(new Carton { Id = Guid.NewGuid(), CustomerUniqueId = "111", CartonOnCorrugate = coc }, 2) { OrderId = "First order in the kit" };

            var kit = new Kit
            {
                Id = Guid.NewGuid(),
                ItemsToProduce = new ConcurrentList<IProducible>(new List<IProducible> { order1 })
            };

            bool wasCalled = false;

            eventAggregator.GetEvent<Message<Order>>()
                .Where(m => m.MessageType == MachineGroupMessages.GetWorkForMachineGroup)
                .Subscribe(m =>
                {
                    wasCalled = true;
                });

            var ordersSA = OrdersSelectionAlgorithm;

            var order = new Order(kit, 2)
            {
                Id = Guid.NewGuid(),
                OrderId = "Kit order"
            };

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Kit;
            Specify.That(selected).Should.Not.BeNull();
            selected.ItemsToProduce.First().ProducibleStatus = InProductionProducibleStatuses.ProducibleProductionStarted;

            Retry.For(() => wasCalled == false, TimeSpan.FromSeconds(5));
            uiCommunicationService.Verify(ser => ser.SendMessageToUI(It.Is<Message<Order>>(msg => msg.Data.Id == order.Id), It.IsAny<bool>(), It.IsAny<bool>()), Times.Exactly(1));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OrderNotReturnedViaProductionGroupWhenOptimalCorrugateNotLoaded()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            var mg = new MachineGroup() { ProductionMode = MachineGroupProductionModes.AutoProductionMode };
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 19, Quality = 1, Thickness = 0.11 };
            var cor2 = new Corrugate { Id = Guid.NewGuid(), Width = 12, Quality = 1, Thickness = 0.11 };

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>()))
                .Returns(new CartonOnCorrugate(new Carton(), cor2, "", null, OrientationEnum.Degree90));

            mg.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });

            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(mg);
            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            var order2 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 2"
            };

            var order3 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 3"
            };

            Specify.That(order1.RemainingQuantity)
                .Should.BeEqualTo(1, "The order started with a quantity of one. Remaining quantity should be 1");

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order1);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order2);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order3);

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected).Should.BeNull("Item should not have been returned because restrictions are not set to capabilities");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnFirstOrderThatMatchesRestrictionsInProducible()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            var mg = new MachineGroup();
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 19, Quality = 1, Thickness = 0.11 };
            var cor2 = new Corrugate { Id = Guid.NewGuid(), Width = 12, Quality = 1, Thickness = 0.11 };
            var producible2 = new Carton
            {
                Id = cartonId,
                Length = 5,
                Width = 5,
                Height = 5,
                DesignId = 201
            };

            var coc1 = new CartonOnCorrugate(new Carton(), cor2, "", null, OrientationEnum.Degree90);
            var coc2 = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90);
            coc2.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(producible, productionGroup, It.IsAny<int>())).Returns(coc1);
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(producible2, productionGroup, It.IsAny<int>())).Returns(coc2);

            optimalCorrugateCalculator.Setup(
                o =>
                    o.GetOptimalCorrugate(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
                        producible, It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(coc1);
            optimalCorrugateCalculator.Setup(
                o =>
                    o.GetOptimalCorrugate(It.IsAny<IEnumerable<Corrugate>>(), It.IsAny<IEnumerable<IPacksizeCutCreaseMachine>>(),
                        producible2, It.IsAny<int>(), It.IsAny<bool>()))
                .Returns(coc2);

            mg.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            mg.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(mg);
            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            var order2 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 2"
            };

            var order3 = new Order(producible2, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 3"
            };

            Specify.That(order1.RemainingQuantity)
                .Should.BeEqualTo(1, "The order started with a quantity of one. Remaining quantity should be 1");

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order1);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order2);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order3);

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected).Should.Not.BeNull("Item should have been returned because restrictions are not set to capabilities");
            Specify.That(order3.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRespondToSearchOrdersRequest()
        {
            IMessage<IEnumerable<Order>> results = null;

            uiCommunicationService.Setup(m => m.SendMessageToUI(It.IsAny<IMessage<IEnumerable<Order>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<Order>>, bool, bool>((message, b, b2) => results = message);

            var order1 = new Order(new Carton(), 120) { CustomerUniqueId = "lots o love" };
            orders.Setup(o => o.Search("order1", It.IsAny<int>())).Returns(new List<Order> { order1 });
            var ordersSA = OrdersSelectionAlgorithm;

            //do the test
            eventAggregator.Publish(new Message<SearchProducibles>
            {
                Data =
                    new SearchProducibles
                    {
                        SelectionAlgorithmTypes = SelectionAlgorithmTypes.Order,
                        Criteria = "order1"
                    },
                ReplyTo = "yoMomma"
            });

            Retry.UntilTrue(() => results != null, TimeSpan.FromSeconds(5));

            Specify.That(results).Should.Not.BeNull("Results were still null after 5 seconds");
            Specify.That(results.ReplyTo).Should.BeEqualTo("yoMomma");
            Specify.That(results.MessageType).Should.BeEqualTo(OrderMessages.SearchOrdersResponse);
            Specify.That(results.Data.First().CustomerUniqueId).Should.BeEqualTo(order1.CustomerUniqueId);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AddingToProductionGroupQueueIsTrackedInOrder()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);


            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            var order2 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 2"
            };

            var order3 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 3"
            };

            Specify.That(order1.RemainingQuantity).Should.BeEqualTo(1, "The order started with a quantity of one. Remaining quantity should be 1");

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order1);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order2);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order3);

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order1.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(order1.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order1.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");
            Specify.That(order1.RemainingQuantity).Should.BeEqualTo(0, "The order started with a quantity of one, now we have produced one. Remaining quantity should be 0");
            var list = ordersSA.GetProduciblesSentToMachineGroup(machineGroupId);
            Specify.That(list.Contains(selected)).Should.BeTrue("selected item not linked to machine group that is to produce the item.");

            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order1.ProducedQuantity).Should.BeEqualTo(1, "The order started with a quantity of one, now we have produced one. Produced quantity should be 1");
            Specify.That(order1.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected item should have been removed to the InProgress list");
            Specify.That(order1.Produced.Count).Should.BeEqualTo(1, "The selected item should have been added to the produced list");
            list = ordersSA.GetProduciblesSentToMachineGroup(machineGroupId);
            Specify.That(list.Contains(selected)).Should.BeFalse("selected item not removed from machine group that is to produce the item.");

            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order2.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(order2.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order2.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");
            Specify.That(order2.RemainingQuantity).Should.BeEqualTo(0, "The order started with a quantity of one, now we have produced one. Remaining quantity should be 0");

            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order2.ProducedQuantity).Should.BeEqualTo(1, "The order started with a quantity of one, now we have produced one. Produced quantity should be 1");
            Specify.That(order2.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected item should have been removed to the InProgress list");
            Specify.That(order2.Produced.Count).Should.BeEqualTo(1, "The selected item should have been added to the produced list");

            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order3.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(order3.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order3.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");
            Specify.That(order3.RemainingQuantity).Should.BeEqualTo(0, "The order started with a quantity of one, now we have produced one. Remaining quantity should be 0");

            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order3.ProducedQuantity).Should.BeEqualTo(1, "The order started with a quantity of one, now we have produced one. Produced quantity should be 1");
            Specify.That(order3.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected item should have been removed to the InProgress list");
            Specify.That(order3.Produced.Count).Should.BeEqualTo(1, "The selected item should have been added to the produced list");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void DistributeValueIsSetBasedOnProductionGroupOptions()
        {
            productionGroup.Options.Add("DefaultDistributeValue", true.ToString());
            productionGroupService.Setup(m => m.Find(productionGroupId)).Returns(productionGroup);
            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            var order2 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 2"
            };

            var order3 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 3"
            };

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order1);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order2);

            productionGroup.Options.Clear();
            productionGroup.Options.Add("DefaultDistributeValue", false.ToString());

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order3);

            Specify.That(order1.IsDistributable).Should.BeTrue();
            Specify.That(order2.IsDistributable).Should.BeTrue();
            Specify.That(order3.IsDistributable).Should.Not.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OrderQuantityIsTrackedCorrectlyWhenSentToMachine()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var mg = new MachineGroup { Id = machineGroupId };
            mg.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            mg.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(mg);
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), mg, It.IsAny<int>())).Returns(coc);

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(10, "The order started with a quantity of ten. Remaining quantity should be 10");

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(9, "The order started with a quantity of ten, now we have produced one. Remaining quantity should be 9");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            Specify.That(order.ProducedQuantity).Should.BeEqualTo(1, "The order started with a quantity of one, now we have produced one. Produced quantity should be 1");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected item should have been removed to the InProgress list");
            Specify.That(order.Produced.Count).Should.BeEqualTo(1, "The selected item should have been added to the produced list");

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(8, "The order started with a quantity of ten, now we have produced two. Remaining quantity should be 8");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            var remainingAndInProgress = order.RemainingQuantity + order.ProducibleInProgress.Count;
            var percent = (double)remainingAndInProgress / (double)order.OriginalQuantity;
            Specify.That(percent * 100).Should.BeEqualTo(80d);
        }

        [TestMethod]
        public void ManualJobsWillNotReturnOrderThatDoesNotHaveOptimalCorrugateAndNotifyMGThatOrderSkipped()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var carton = new Carton { Length = 50, Width = 50, Height = 50 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(producible, machineGroup, It.IsAny<int>()))
                .Returns(coc);

            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(carton, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 1"
            };

            var order2 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 2"
            };


            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order1);
            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order2);

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.BeEqualTo("test order 2:1");
            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Order should not be producible due to optimal corrugate maissing.");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;
            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Order should not be producible due to optimal corrugate maissing.");

        }
        [TestMethod]
        public void AutoJobsWillNotReturnOrderThatDoesNotHaveOptimalCorrugate()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var carton = new Carton { Length = 50, Width = 50, Height = 50 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(producible, productionGroup, It.IsAny<int>())).Returns(coc);

            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(carton, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 1"
            };

            var order2 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 2"
            };


            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order1);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order2);

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;

            Specify.That(selected.CustomerUniqueId).Should.BeEqualTo("test order 2:1");
            Specify.That(ordersSA.GetAutoJobForMachineGroup(machineGroupId)).Should.BeNull("Order should not be producible due to optimal corrugate maissing.");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;
            Specify.That(ordersSA.GetAutoJobForMachineGroup(machineGroupId)).Should.BeNull("Order should not be producible due to optimal corrugate maissing.");

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ManualJobsAlwaysPauseBetweenOrders()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>())).Returns(coc);

            machineGroupService.Setup(m => m.CompleteQueueAndPause(machineGroupId))
                .Callback<Guid>(mg => machineGroup.CurrentStatus = MachineGroupAvailableStatuses.ProduceQueueAndPause);

            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 1"
            };

            var order2 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 2"
            };

            var order3 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 3"
            };

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order1);
            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order2);
            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order3);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.BeEqualTo("test order 1:1");
            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            Specify.That(machineGroup.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.ProduceQueueAndPause);
            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.BeEqualTo("test order 2:1");
            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            Specify.That(machineGroup.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.ProduceQueueAndPause);
            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.BeEqualTo("test order 3:1");
            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            //TODO: Specify.That(machineGroup.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.ProduceQueueAndPause);

            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;
        }

        /// <summary>
        /// Manual mode is always pause between orders so we don't need to set that value anywhere.
        /// </summary>
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldComeOutOfPauseAndReturnManaulJobAfterPausingBetweenOrdersFromAuto()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };

            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);
            machineGroupService.Setup(m => m.CompleteQueueAndPause(machineGroupId))
               .Callback<Guid>(mg => machineGroup.CurrentStatus = MachineGroupAvailableStatuses.ProduceQueueAndPause);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>())).Returns(coc);
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "auto order 1"
            };

            var order2 = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "manual order 2"
            };

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order1);
            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order2);

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.Contain(order1.OrderId);

            //simulate user clicking manual mode button in ui which puts us in this mode
            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.ProduceQueueAndPause;

            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            Specify.That(machineGroup.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.ProduceQueueAndPause);
            Specify.That(ordersSA.GetManualJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.Contain(order2.OrderId);
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.Contain(order2.OrderId);

        }

        /// <summary>
        /// Manual mode is always pause between orders so we don't need to set that value anywhere.
        /// </summary>
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldComeOutOfPauseAndReturnAutoJobAfterPausingBetweenOrdersFromManual()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };

            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);
            machineGroupService.Setup(m => m.CompleteQueueAndPause(machineGroupId))
               .Callback<Guid>(mg => machineGroup.CurrentStatus = MachineGroupAvailableStatuses.ProduceQueueAndPause);

            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>())).Returns(coc);
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            productionGroup.Options.Add("PauseMachineBetweenOrders", "true");

            var ordersSA = OrdersSelectionAlgorithm;
            var order1 = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "auto order 1"
            };

            var order2 = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "manual order 2"
            };

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order1);
            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order2);

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.Contain(order2.OrderId);

            //simulate user clicking manual mode button in ui which puts us in this mode
            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.ProduceQueueAndPause;

            Specify.That(ordersSA.GetAutoJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            Specify.That(machineGroup.CurrentStatus).Should.BeEqualTo(MachineGroupAvailableStatuses.ProduceQueueAndPause);
            Specify.That(ordersSA.GetAutoJobForMachineGroup(machineGroupId)).Should.BeNull("Switching orders, job shouldn't be added to queue");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;

            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.Contain(order1.OrderId);
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(selected.CustomerUniqueId).Should.Contain(order1.OrderId);

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void AddingAndSelectingFromMachineQueueIsTrackedInOrder()
        {

            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForMachineGroup(It.IsAny<IProducible>(), machineGroup, It.IsAny<int>())).Returns(coc);
            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order",
            };

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(1, "The order started with a quantity of one. Remaining quantity should be 1");

            ordersSA.AddOrderToMachineGroupQueue(machineGroupId, order);

            var selected = ordersSA.GetManualJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(0, "The order started with a quantity of one, now we have produced one. Remaining quantity should be 0");
            var list = ordersSA.GetProduciblesSentToMachineGroup(machineGroupId);
            Specify.That(list.Contains(selected)).Should.BeTrue("selected item not linked to machine group that is to produce the item.");


            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.ProducedQuantity).Should.BeEqualTo(1, "The order started with a quantity of one, now we have produced one. Produced quantity should be 1");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected item should have been removed to the InProgress list");
            Specify.That(order.Produced.Count).Should.BeEqualTo(1, "The selected item should have been added to the produced list");
            list = ordersSA.GetProduciblesSentToMachineGroup(machineGroupId);
            Specify.That(list.Contains(selected)).Should.BeFalse("selected item not removed from machine group that is to produce the item.");

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OrderCanBeSentFromProductionGroupQueue()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(10, "The order started with a quantity of ten. Remaining quantity should be 10");

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order);

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(9, "The order started with a quantity of ten, now we have produced one. Remaining quantity should be 9");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;


            Specify.That(order.ProducedQuantity).Should.BeEqualTo(1, "The order started with a quantity of one, now we have produced one. Produced quantity should be 1");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(0, "The selected item should have been removed to the InProgress list");
            Specify.That(order.Produced.Count).Should.BeEqualTo(1, "The selected item should have been added to the produced list");

            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order.Producible.Id).Should.Not.BeEqualTo(selected.Id, "The item produced was not copied from the order correctly, Id's need to be unique");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(8, "The order started with a quantity of ten, now we have produced two. Remaining quantity should be 8");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            var remainingAndInProgress = order.RemainingQuantity + order.ProducibleInProgress.Count;
            var percent = (double)remainingAndInProgress / (double)order.OriginalQuantity;
            Specify.That(percent * 100).Should.BeEqualTo(80d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SelectedJobStatusChangedHandledForProducibleFailed()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(10, "The order started with a quantity of ten. Remaining quantity should be 10");

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order);

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(9, "The order started with a quantity of ten, now we have produced one. Remaining quantity should be 9");
            Specify.That(order.FailedQuantity).Should.BeEqualTo(0, "Failed quantity should be zero");

            selected.ProducibleStatus = ErrorProducibleStatuses.ProducibleFailed;

            Retry.UntilTrue(() => order.ProducibleInProgress.Count == 0, TimeSpan.FromSeconds(2));
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(10, "The order started with a quantity of ten, the failed one should be reproduced. Remaining quantity should be 10");
            Specify.That(order.FailedQuantity).Should.BeEqualTo(0, "Failed quantity should be zero");
            Specify.That(order.Failed.Count).Should.BeEqualTo(0, "Item should not be added to failed list");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(0, "Item should be removed from in progress");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SelectedJobStatusChangedHandledForNotProducible()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order"
            };

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(10, "The order started with a quantity of ten. Remaining quantity should be 10");

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order);

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[0]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(9, "The order started with a quantity of ten, now we have produced one. Remaining quantity should be 9");
            Specify.That(order.FailedQuantity).Should.BeEqualTo(0, "Failed quantity should be zero");

            selected.ProducibleStatus = ErrorProducibleStatuses.NotProducible;

            Retry.UntilTrue(() => order.ProducibleInProgress.Count == 0, TimeSpan.FromSeconds(2));
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(9, "The order started with a quantity of ten, now we have produced one. Remaining quantity should be 9");
            Specify.That(order.FailedQuantity).Should.BeEqualTo(1, "Failed quantity should be zero");
            Specify.That(order.Failed[0]).Should.BeEqualTo(selected.Id, "Failed quantity should be zero");

            // update should only happen once on the wireup.  do gain dad
            selected.ProducibleStatus = ErrorProducibleStatuses.NotProducible;
            Retry.UntilTrue(() => order.ProducibleInProgress.Count == 0, TimeSpan.FromSeconds(2));
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(9, "The order started with a quantity of ten, now we have produced one. Remaining quantity should be 9");
            Specify.That(order.FailedQuantity).Should.BeEqualTo(1, "Failed quantity should be zero");
            Specify.That(order.Failed[0]).Should.BeEqualTo(selected.Id, "Failed quantity should be zero");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OrderCanBeDistributedToMultipleMachineGroups()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var machineGroup = new MachineGroup { Id = machineGroupId };
            var machineGroup2 = new MachineGroup { Id = machineGroup2Id };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup2.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup2.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroup2Id)).Returns(machineGroup2);

            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroup2Id)).Returns(productionGroup);
            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order",
                IsDistributable = true
            };

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(10, "The order started with a quantity of ten. Remaining quantity should be 10");

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order);

            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            var selected2 = ordersSA.GetAutoJobForMachineGroup(machineGroup2Id) as Carton;

            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(2, "Two machines should be working this order");
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(8, "The order started with a quantity of ten, now we have dispatched two. Remaining quantity should be 8");
            //Complete the job on machine group 1
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            Specify.That(order.ProducedQuantity).Should.BeEqualTo(1, "The order started with a quantity of one, now we have produced one. Produced quantity should be 1");
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(1, "The selected item should have been removed to the InProgress list, the item for the second machine remains");
            Specify.That(order.Produced.Count).Should.BeEqualTo(1, "The selected item should have been added to the produced list");

            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            Specify.That(order.ProducibleInProgress.Count).Should.BeEqualTo(2, "The selected item should have been added to the InProgress list");
            Specify.That(order.ProducibleInProgress[1]).Should.BeEqualTo(selected, "The selected item should have been added to the InProgress list");
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(7, "The order started with a quantity of ten, now we have dispatched three. Remaining quantity should be 7");
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            selected2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            var remainingAndInProgress = order.RemainingQuantity + order.ProducibleInProgress.Count;
            var percent = (double)remainingAndInProgress / (double)order.OriginalQuantity;
            Specify.That(percent * 100).Should.BeEqualTo(70d);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OrderCanBeMarkedAsNonDistributed()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);

            var machineGroup = new MachineGroup { Id = machineGroupId };
            var machineGroup2 = new MachineGroup { Id = machineGroup2Id };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroup.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;
            machineGroup2.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup2.ConfiguredMachines.Add(machineId);
            machineGroup2.CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline;
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroup2Id)).Returns(machineGroup2);

            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroup2Id)).Returns(productionGroup);
            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order",
                IsDistributable = true
            };

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order);

            //Get work for two machine groups
            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            var selected2 = ordersSA.GetAutoJobForMachineGroup(machineGroup2Id) as Carton;

            //Complete the job on machine group 1
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            //Ask for more work
            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;

            Specify.That(order.RemainingQuantity).Should.BeEqualTo(7, "The order started with a quantity of ten, now we have dispatched three. Remaining quantity should be 7");

            //Trigger distribute order event
            eventAggregator.Publish(new Message<Guid>
            {
                MessageType = OrderMessages.DistributeOrders,
                Data = order.Id
            });

            Thread.Sleep(500); //TODO: not cool. Figure out how to do this without thread (Retry.For)

            //Complete both orders
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            selected2.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;

            //Ask for work but order is no longer distributed
            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;
            selected2 = ordersSA.GetAutoJobForMachineGroup(machineGroup2Id) as Carton;

            Specify.That(selected).Should.Not.BeNull();
            Specify.That(selected2).Should.BeNull("The second machine group should not get work");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void OrderCanBePushedToTheTopOfTheQueue()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");
            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order",
                IsDistributable = true
            };
            var order2 = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 2",
                IsDistributable = true
            };
            var order3 = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 3",
                IsDistributable = true
            };

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order2);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order3);

            //Get work for two machine groups
            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;

            //Create Order 2 next
            eventAggregator.Publish(new Message<List<Guid>>
            {
                MessageType = OrderMessages.UpdateOrdersPriority,
                Data = new List<Guid> { order2.Id }
            });

            Thread.Sleep(500); //TODO: not cool. Figure out how to do this without thread (Retry.For)

            //Complete the job on machine group 1
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(9, "The order started with a quantity of ten, now we have dispatched one. Remaining quantity should be 9");

            //Ask for more work
            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;

            //Complete the job on machine group 1
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order2.RemainingQuantity).Should.BeEqualTo(9, "The order started with a quantity of ten, now we have dispatched one. Remaining quantity should be 9");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void MultipleOrdersCanBePushedToTheTopOfTheQueue()
        {
            serviceLocator.Setup(sl => sl.Locate<IRestrictionResolverService>()).Returns(new RestrictionResolverService());
            var cor1 = new Corrugate { Id = Guid.NewGuid(), Width = 31, Quality = 1, Thickness = 0.11 };
            var coc = new CartonOnCorrugate(new Carton(), cor1, "", null, OrientationEnum.Degree90) { TileCount = 1 };
            coc.ProducibleMachines.Add(machineId, "");

            corrugateServiceMock.Setup(m => m.GetOptimalCorrugateForProductionGroup(It.IsAny<IProducible>(), productionGroup, It.IsAny<int>())).Returns(coc);
            productionGroupService.Setup(m => m.GetProductionGroupForMachineGroup(machineGroupId)).Returns(productionGroup);

            var machineGroup = new MachineGroup { Id = machineGroupId };
            machineGroup.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machineId, new List<Corrugate> { cor1 }))
            });
            machineGroup.ConfiguredMachines.Add(machineId);
            machineGroupService.Setup(m => m.FindByMachineGroupId(machineGroupId)).Returns(machineGroup);

            var ordersSA = OrdersSelectionAlgorithm;
            var order = new Order(producible, 10)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order",
                IsDistributable = true
            };
            var order2 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 2",
                IsDistributable = true
            };
            var order3 = new Order(producible, 1)
            {
                Id = Guid.NewGuid(),
                OrderId = "test order 3",
                IsDistributable = true
            };

            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order2);
            ordersSA.AddOrderToProductionGroupQueue(productionGroupId, order3);

            //Get work for two machine groups
            var selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;

            //Create Order 2 and Order 3 next
            eventAggregator.Publish(new Message<List<Guid>>
            {
                MessageType = OrderMessages.UpdateOrdersPriority,
                Data = new List<Guid> { order2.Id, order3.Id }
            });

            Thread.Sleep(500); //TODO: not cool. Figure out how to do this without thread (Retry.For)

            //Complete the job on machine group 1
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order.RemainingQuantity).Should.BeEqualTo(9, "The order started with a quantity of ten, now we have dispatched one. Remaining quantity should be 9");

            //Ask for more work
            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;

            //Complete the job on machine group 1
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order2.RemainingQuantity).Should.BeEqualTo(0, "The order started with a quantity of ten, now we have dispatched one. Remaining quantity should be 0");

            //Ask for more work
            selected = ordersSA.GetAutoJobForMachineGroup(machineGroupId) as Carton;

            //Complete the job on machine group 1
            selected.ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            Specify.That(order3.RemainingQuantity).Should.BeEqualTo(0, "The order started with a quantity of ten, now we have dispatched one. Remaining quantity should be 0");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldLoadAllOpenOrders_WhenInitializingTheOrderSAS()
        {
            var uiCommunicationService = new Mock<IUICommunicationService>().Object;
            var userNotificationService = new Mock<IUserNotificationService>().Object;

            var emId = Guid.NewGuid();
            var mgId = Guid.NewGuid();
            var pgId = Guid.NewGuid();

            var orderRepo = new MongoOrderTestRepo();
            var llogger = new Mock<ILogger>().Object;
            var eventAggregator = new EventAggregator();
            IEventAggregatorPublisher publisher = eventAggregator;
            IEventAggregatorSubscriber subscriber = eventAggregator;
            var iorders = new Orders(orderRepo);
            var serviceLocator = new ServiceLocator("", new List<IService>(), llogger);

            var corrugate = new Corrugate()
            {
                Id = Guid.NewGuid(),
                Alias = "50",
                Quality = 1,
                Thickness = 0.16d,
                Width = 50.0d
            };

            var emMachine = new EmMachine() { Alias = "Em1", Id = emId, PhysicalMachineSettings = new EmPhysicalMachineSettings() { TrackParameters = new EmTrackParameters() }, Tracks = new ConcurrentList<Track>() };
            emMachine.Tracks.Add(new Track() { TrackNumber = 1, TrackOffset = 10d });
            emMachine.LoadCorrugate(emMachine.Tracks.First(), corrugate);

            var mg = new MachineGroup() { Id = mgId, Alias = "MGTest" };
            mg.ConfiguredMachines.Add(emMachine.Id);

            var pg = new ProductionGroup() { Id = pgId, Alias = "PGTest" };
            pg.ConfiguredCorrugates.Add(corrugate.Id);
            pg.ConfiguredMachineGroups.Add(mg.Id);

            orderRepo.DeleteAll();

            var ordersToCreate = new List<Order>();
            var numberOfOrders = new Random(Guid.NewGuid().GetHashCode()).Next(30, 50);
            for (int i = 0; i < numberOfOrders; i++)
            {
                var carton = new Carton() { DesignId = 2010001, Length = 6d, Width = 6d, Height = 6d, ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged };

                var cartonOnCorrugate = new CartonOnCorrugate(carton, corrugate, "", null, OrientationEnum.Degree0) { TileCount = 1 };
                cartonOnCorrugate.ProducibleMachines.Add(mgId, "");
                carton.CartonOnCorrugate = cartonOnCorrugate;

                var order = new Order(carton, 1);

                order.Restrictions.Add(new MachineGroupSpecificRestriction(mgId));
                ordersToCreate.Add(order);
            }

            ordersToCreate.ElementAt(0).ProducibleStatus = ProducibleStatuses.ProducibleCompleted;
            ordersToCreate.ElementAt(1).ProducibleStatus = ErrorProducibleStatuses.NotProducible;
            ordersToCreate.ElementAt(2).ProducibleStatus = ProducibleStatuses.ProducibleRemoved;

            orderRepo.Create(ordersToCreate);

            var pgServiceMock = GetProductionGroupServiceMock(pg);
            var mgServiceMock = GetMachineGroupServiceMock(mg);
            var machineServiceMock = GetMachineServiceMock(emMachine);
            var designServiceMock = new Mock<IPackagingDesignService>();
            var aggregateMachineService = new Mock<IAggregateMachineService>();
            var corrugatesManagerMock = GetCorrugatesManagerMock(new List<Corrugate> { corrugate });
            var packagingDesignManagerMock = GetPackagingDesignManagerMock(new List<ICarton> { ordersToCreate.First().Producible as Carton });
            packagingDesignManagerMock.Setup(s => s.CalculateUsage(It.Is<ICarton>(c => c.DesignId == 2010001), It.Is<Corrugate>(c => c.Width == corrugate.Width), It.IsAny<int>(), It.IsAny<bool>())).Returns(new PackagingDesignCalculation(){Length = 22, Width = 12});
            aggregateMachineService.Setup(s => s.CanProduce(It.Is<Guid>(g => g == emMachine.Id), It.IsAny<IProducible>())).Returns(true);

            serviceLocator.RegisterAsService(subscriber);
            serviceLocator.RegisterAsService(publisher);
            serviceLocator.RegisterAsService(llogger);
            serviceLocator.RegisterAsService(new RestrictionResolverService());
            serviceLocator.RegisterAsService(userNotificationService);
            serviceLocator.RegisterAsService(uiCommunicationService);
            serviceLocator.RegisterAsService(pgServiceMock.Object);
            serviceLocator.RegisterAsService(mgServiceMock.Object);
            serviceLocator.RegisterAsService(designServiceMock.Object);
            serviceLocator.RegisterAsService(aggregateMachineService.Object);

            // set up classes under test
            var realOptimalCorrugateCalculator = new OptimalCorrugateCalculator(packagingDesignManagerMock.Object,
                serviceLocator, mgServiceMock.Object);

            var corrugateService = new CorrugateService(realOptimalCorrugateCalculator, corrugatesManagerMock.Object,
                 serviceLocator, machineServiceMock.Object,
                new Mock<IUICommunicationService>().Object, eventAggregator,
                eventAggregator, new Mock<ILogger>().Object);

            var orderSA = new OrdersSelectionAlgorithmServiceTestGuy(iorders, pgServiceMock.Object, mgServiceMock.Object, designServiceMock.Object, eventAggregator, eventAggregator, serviceLocator, uiCommunicationService, userNotificationService, corrugateService, logger);

            Specify.That(orderSA.QueueCount(mgId)).Should.BeEqualTo(numberOfOrders - 2, "We expect all but completed and removed order to be loaded when the selection algorithm starts.");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldOnlyNotifyUIOnce_WhenFailingToSelectAJob_UntilEnvironmentChanges()
        {
            var uiCommServiceMock = new Mock<IUICommunicationService>();
            var userNotServiceMock = new Mock<IUserNotificationService>();

            var emId = Guid.NewGuid();
            var mgId = Guid.NewGuid();
            var pgId = Guid.NewGuid();

            var orderRepo = new MongoOrderTestRepo();
            var llogger = new Mock<ILogger>().Object;
            var eventAggregator = new EventAggregator();
            IEventAggregatorPublisher publisher = eventAggregator;
            IEventAggregatorSubscriber subscriber = eventAggregator;
            var iorders = new Orders(orderRepo);
            var serviceLocator = new ServiceLocator("", new List<IService>(), llogger);

            var corrugate1 = new Corrugate() { Alias = "50", Quality = 1, Thickness = 0.16d, Width = 18.0d };
            var corrugate2 = new Corrugate() { Alias = "50", Quality = 1, Thickness = 0.16d, Width = 27.0d };

            var emMachine = new EmMachine() { Alias = "Em1", Id = emId, PhysicalMachineSettings = new EmPhysicalMachineSettings() { TrackParameters = new EmTrackParameters() }, Tracks = new ConcurrentList<Track>() };
            emMachine.Tracks.Add(new Track() { TrackNumber = 1, TrackOffset = 10d });
            emMachine.LoadCorrugate(emMachine.Tracks.First(), corrugate1);

            var mg = new MachineGroup() { Id = mgId, Alias = "MGTest" };
            mg.ConfiguredMachines.Add(emMachine.Id);

            var pg = new ProductionGroup() { Id = pgId, Alias = "PGTest" };
            pg.ConfiguredCorrugates.Add(corrugate1.Id);
            pg.ConfiguredCorrugates.Add(corrugate2.Id);
            pg.ConfiguredMachineGroups.Add(mg.Id);

            orderRepo.DeleteAll();

            var ordersToCreate = new List<Order>();
            var order =
                new Order(
                    new Carton()
                    {
                        DesignId = 2010001,
                        Length = 15d,
                        Width = 15d,
                        Height = 15d,
                        ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStaged
                    }, 1);
            order.Restrictions.Add(new ProductionGroupSpecificRestriction(pg.Id));

            ordersToCreate.Add(order);

            orderRepo.Create(ordersToCreate);
            var pgServiceMock = GetProductionGroupServiceMock(pg);
            pgServiceMock.Setup(s => s.GetProductionGroupForMachineGroup(It.Is<Guid>(m => m == mg.Id))).Returns(pg);
            var mgServiceMock = GetMachineGroupServiceMock(mg);
            var machineServiceMock = GetMachineServiceMock(emMachine);
            var designServiceMock = new Mock<IPackagingDesignService>();
            var aggregateMachineService = new Mock<IAggregateMachineService>();
            var corrugatesManagerMock = GetCorrugatesManagerMock(new List<Corrugate> { corrugate1, corrugate2 });
            var packagingDesignManagerMock = GetPackagingDesignManagerMock(new List<ICarton> { ordersToCreate.First().Producible as Carton });
            packagingDesignManagerMock.Setup(s => s.CalculateUsage(It.Is<ICarton>(c => c.DesignId == 2010001), It.IsAny<Corrugate>(), It.IsAny<int>(), It.IsAny<bool>())).Returns(new PackagingDesignCalculation() { Length = 22, Width = 22 });
            aggregateMachineService.Setup(s => s.CanProduce(It.Is<Guid>(g => g == emMachine.Id), It.IsAny<IProducible>())).Returns(true);

            serviceLocator.RegisterAsService(subscriber);
            serviceLocator.RegisterAsService(publisher);
            serviceLocator.RegisterAsService(llogger);
            serviceLocator.RegisterAsService(new RestrictionResolverService());
            serviceLocator.RegisterAsService(userNotServiceMock.Object);
            serviceLocator.RegisterAsService(uiCommServiceMock.Object);
            serviceLocator.RegisterAsService(pgServiceMock.Object);
            serviceLocator.RegisterAsService(mgServiceMock.Object);
            serviceLocator.RegisterAsService(designServiceMock.Object);
            serviceLocator.RegisterAsService(aggregateMachineService.Object);

            // set up classes under test
            var realOptimalCorrugateCalculator = new OptimalCorrugateCalculator(packagingDesignManagerMock.Object,
                serviceLocator, mgServiceMock.Object);

            var corrugateService = new CorrugateService(realOptimalCorrugateCalculator, corrugatesManagerMock.Object,
                 serviceLocator, machineServiceMock.Object,
                new Mock<IUICommunicationService>().Object, eventAggregator,
                eventAggregator, new Mock<ILogger>().Object);

            var orderSA = new OrdersSelectionAlgorithmServiceTestGuy(iorders, pgServiceMock.Object, mgServiceMock.Object, designServiceMock.Object, eventAggregator, eventAggregator, serviceLocator, uiCommServiceMock.Object, userNotServiceMock.Object, corrugateService, logger);

            var selectedOrder = orderSA.GetAutoJobForMachineGroup(mg.Id);
            //userNotServiceMock.Verify(s => s.SendNotificationToMachineGroup(It.Is<NotificationSeverity>(n => n.Value == NotificationSeverity.Warning.Value), It.Is<string>(msg => msg.Contains("Order Selection skipped")), It.IsAny<string>(), It.IsAny<Guid>()), Times.Once(), "Expected to be notified that the order was skipped because our most optimal corrugate is not loaded");
            Specify.That(selectedOrder).Should.BeNull("No order should have been selected since we don't have the corrugate loaded");
            userNotServiceMock.ResetCalls();
            selectedOrder = orderSA.GetAutoJobForMachineGroup(mg.Id);
            //userNotServiceMock.Verify(s => s.SendNotificationToMachineGroup(It.Is<NotificationSeverity>(n => n.Value == NotificationSeverity.Warning.Value), It.Is<string>(msg => msg.Contains("Order Selection skipped")), It.IsAny<string>(), It.IsAny<Guid>()), Times.Never(), "We have already been notified that this order is skipped, no need to do it again until the produciton group changes.");
            Specify.That(selectedOrder).Should.BeNull("No order should have been selected since we don't have the corrugate loaded");

            publisher.Publish(new Message<ProductionGroup>
            {
                Data = pg,
                MessageType = ProductionGroupMessages.ProductionGroupChanged
            });

            selectedOrder = orderSA.GetAutoJobForMachineGroup(mg.Id);
            //userNotServiceMock.Verify(s => s.SendNotificationToMachineGroup(It.Is<NotificationSeverity>(n => n.Value == NotificationSeverity.Warning.Value), It.Is<string>(msg => msg.Contains("Order Selection skipped")), It.IsAny<string>(), It.IsAny<Guid>()), Times.Once(), "Expected to be notified that the order was skipped because our production group got a changed event (even though nothing changed)");
            Specify.That(selectedOrder).Should.BeNull("No order should have been selected since we don't have the corrugate loaded");
        }

        private OrdersSelectionAlgorithmService OrdersSelectionAlgorithm
        {
            get
            {
                return new OrdersSelectionAlgorithmService(
                    orders.Object,
                    productionGroupService.Object,
                    machineGroupService.Object,
                    eventAggregator,
                    eventAggregator,
                    serviceLocator.Object,
                    uiCommunicationService.Object,
                    userNotificationService.Object,
                    corrugateServiceMock.Object,
                    logger);
            }
        }
        private class OrdersSelectionAlgorithmServiceTestGuy : OrdersSelectionAlgorithmService
        {
            public OrdersSelectionAlgorithmServiceTestGuy(
                IOrders orders,
                IProductionGroupService productionGroupService,
                IMachineGroupService machineGroupService,
                IPackagingDesignService packagingDesignService,
                IEventAggregatorSubscriber subscriber,
                IEventAggregatorPublisher publisher,
                IServiceLocator serviceLocator,
                IUICommunicationService uiCommunicationService,
                IUserNotificationService userNotificationService,
                ICorrugateService corrugateService,
                ILogger logger)
                : base(
                    orders,
                    productionGroupService,
                    machineGroupService,
                    subscriber,
                    publisher,
                    serviceLocator,
                    uiCommunicationService,
                    userNotificationService,
                    corrugateService,
                    logger)
            {
            }

            public int QueueCount(Guid id)
            {
                return GetQueue(id).Count;
            }
        }

        private class MongoOrderTestRepo : OrdersRepository
        {
            public MongoOrderTestRepo()
                : base(PackNet.Data.Defaults.Default.ConnectionString, "PackNetTestRepo", "Orders")
            {
                MongoDbHelpers.TryRegisterSerializer<MicroMeter>(new MicroMeterSerializer());
            }

            public void DropDatabase()
            {
                Database.Drop();
            }
        }

        private static Mock<IPackagingDesignManager> GetPackagingDesignManagerMock(IEnumerable<ICarton> cartons)
        {
            var packagingDesignManagerMock = new Mock<IPackagingDesignManager>();
            packagingDesignManagerMock.Setup(pdm => pdm.HasDesignWithId(It.IsAny<int>())).Returns(true);

            cartons.ForEach(carton =>
            {
                // rotated
                packagingDesignManagerMock.Setup(
                    dm =>
                        dm.CalculateUsage(It.Is<ICarton>(crt => crt.Id == carton.Id), It.IsAny<Corrugate>(), It.IsAny<int>(),
                            It.Is<bool>(b => b)))
                    .Returns(new PackagingDesignCalculation
                    {
                        Length = carton.Height + carton.Width,
                        Width = (2 * carton.Length) + (2 * carton.Width)
                    });

                // nonRotated
                packagingDesignManagerMock.Setup(
                dm =>
                    dm.CalculateUsage(It.Is<ICarton>(crt => crt.Id == carton.Id), It.IsAny<Corrugate>(), It.IsAny<int>(),
                        It.Is<bool>(b => b == false)))
                .Returns(new PackagingDesignCalculation
                {
                    Length = (2 * carton.Length) + (2 * carton.Width),
                    Width = carton.Height + carton.Width
                });

            });
            return packagingDesignManagerMock;
        }

        private static Mock<IProductionGroupService> GetProductionGroupServiceMock(ProductionGroup pg)
        {
            var productionGroupServiceMock = new Mock<IProductionGroupService>();
            productionGroupServiceMock.Setup(pgs => pgs.Find(It.IsAny<Guid>())).Returns(pg);
            productionGroupServiceMock.SetupGet(pgs => pgs.ProductionGroups).Returns(new List<ProductionGroup> { pg });
            return productionGroupServiceMock;
        }

        private static Mock<ICorrugates> GetCorrugatesManagerMock(IEnumerable<Corrugate> corrugates)
        {
            var corrugatesManagerMock = new Mock<ICorrugates>();

            corrugatesManagerMock.Setup(cm => cm.GetCorrugates()).Returns(corrugates);
            return corrugatesManagerMock;
        }

        private static Mock<IAggregateMachineService> GetMachineServiceMock(EmMachine machine)
        {

            //        var machinesInMachineGroup = machineService.Machines
            //.Where(m => m is IPacksizeCutCreaseMachine && machineGroup.ConfiguredMachines.Contains(m.Id))
            //.Cast<IPacksizeCutCreaseMachine>().ToList();

            var machineServiceMock = new Mock<IAggregateMachineService>();
            machineServiceMock.SetupGet(ams => ams.Machines).Returns(new List<IMachine> { machine });
            machineServiceMock.Setup(ams => ams.CanProduce(It.IsAny<Guid>(), It.IsAny<IProducible>())).Returns(true);
            return machineServiceMock;
        }

        private static Mock<IServiceLocator> GetServiceLocatorMock(Mock<IMachineGroupService> machineGroupServiceMock, Mock<IAggregateMachineService> machineServiceMock)
        {
            var serviceLocatorMock = new Mock<IServiceLocator>();
            serviceLocatorMock.Setup(sl => sl.Locate<ILogger>()).Returns(new Mock<ILogger>().Object);
            serviceLocatorMock.Setup(sl => sl.Locate<IAggregateMachineService>()).Returns(machineServiceMock.Object);
            serviceLocatorMock.Setup(s => s.Locate<IMachineGroupService>()).Returns(machineGroupServiceMock.Object);
            return serviceLocatorMock;
        }

        private static Mock<IMachineGroupService> GetMachineGroupServiceMock(MachineGroup mg)
        {
            var machineGroupServiceMock = new Mock<IMachineGroupService>();
            machineGroupServiceMock.SetupGet(mgs => mgs.MachineGroups).Returns(new List<MachineGroup> { mg });
            machineGroupServiceMock.Setup(mgs => mgs.FindByMachineGroupId(It.IsAny<Guid>())).Returns(mg);
            return machineGroupServiceMock;
        }

        private MachineGroup GetMachineGroup(EmMachine machine, IEnumerable<Corrugate> corrugates)
        {
            var mg = new MachineGroup
            {
                Alias = "MG1",
                Id = Guid.NewGuid(),
                CurrentStatus = MachineGroupAvailableStatuses.MachineGroupOnline,
                ConfiguredMachines = new ConcurrentList<Guid> { machine.Id }
            };

            mg.AddOrUpdateCapabilities(new List<ICapability>
            {
                new CorrugatesOnMachineIdCapability(new CorrugatesOnMachineId(machine.Id, corrugates))
            });
            return mg;
        }

        private static ProductionGroup GetProductionGroup(IEnumerable<Corrugate> corrugates, MachineGroup mg)
        {
            return new ProductionGroup
            {
                Alias = "PG1",
                Id = Guid.NewGuid(),
                ConfiguredCorrugates = new ConcurrentList<Guid>(corrugates.Select(c => c.Id)),
                SelectionAlgorithm = SelectionAlgorithmTypes.Order,
                ConfiguredMachineGroups = new ConcurrentList<Guid> { mg.Id },
                Options = new Dictionary<string, string> { { "DefaultDistributeValue", "false" } }
            };
        }

        private static EmMachine GetEMMachine(IEnumerable<Corrugate> loadedCorrugates)
        {
            var i = 0;
            var trackOffset = 4;

            var tracks = new ConcurrentList<Track>();

            foreach (var corrugate in loadedCorrugates)
            {
                i++;

                if (i != 1)
                {
                    trackOffset += corrugate.Width + 2;
                }

                tracks.Add(new Track
                {
                    TrackNumber = 1,
                    LoadedCorrugate = corrugate,
                    TrackOffset = trackOffset
                });
            }

            return new EmMachine
            {
                Id = Guid.NewGuid(),
                Alias = "Machine1",
                CurrentStatus = MachineStatuses.MachineOnline,
                CurrentProductionStatus = MachineProductionStatuses.ProductionIdle,
                Tracks = tracks
            };
        }

        private OrderSATestClass GetTestOrderSA(Mock<IProductionGroupService> productionGroupServiceMock, Mock<IMachineGroupService> machineGroupServiceMock, CorrugateService corrugateService)
        {
            return new OrderSATestClass(
                orders.Object,
                productionGroupServiceMock.Object,
                machineGroupServiceMock.Object,
                eventAggregator,
                eventAggregator,
                serviceLocator.Object,
                uiCommunicationService.Object,
                userNotificationService.Object,
                corrugateService,
                logger);
        }

        class OrderSATestClass : OrdersSelectionAlgorithmService
        {
            public OrderSATestClass(IOrders orders,
            IProductionGroupService productionGroupService,
            IMachineGroupService machineGroupService,
            IEventAggregatorSubscriber subscriber,
            IEventAggregatorPublisher publisher,
            IServiceLocator serviceLocator,
            IUICommunicationService uiCommunicationService,
            IUserNotificationService userNotificationService,
            ICorrugateService corrugateService,
            ILogger logger)
                : base(orders, productionGroupService, machineGroupService, subscriber, publisher, serviceLocator, uiCommunicationService, userNotificationService, corrugateService, logger)
            {

            }

            public ConcurrentList<Order> GetOrderQueue(Guid id)
            {
                return base.GetQueue(id);
            }

        }
    }
}