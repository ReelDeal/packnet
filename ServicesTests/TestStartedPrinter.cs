using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Utils;

namespace ServicesTests
{
    using PackNet.Common.Interfaces.Enums;

    public class TestStartedPrinter : IPrinter
    {
        Subject<PrintJobStatus> printJobStatusChangedSubject = new Subject<PrintJobStatus>();
        Subject<PrinterStatus> printerStatusChangedSubject = new Subject<PrinterStatus>();

        public List<Printable> ItemsPrinted { get; set; }

        public IObservable<PrinterStatus> PrinterStatusChangedObservable { get { return printerStatusChangedSubject.AsObservable(); } }

        public IObservable<PrintJobStatus> PrintJobStatusChangedObservable { get { return printJobStatusChangedSubject.AsObservable(); } }

        public TestStartedPrinter()
        {
            ItemsPrinted = new List<Printable>();
        }

        public void Print(Printable itemToPrint)
        {
            ItemsPrinted.Add(itemToPrint);
            var task = Task.Factory.StartNew(
                () =>
                {
                    Debug.WriteLine(DateTime.Now + " TestPrinter new thread started " + Thread.CurrentThread.ManagedThreadId);
                    Thread.Sleep(500);
                    printJobStatusChangedSubject.OnNext(
                        new PrintJobStatus
                        {
                            JobStatus = InProductionProducibleStatuses.ProducibleProductionStarted,
                            PrinterId = Id,
                            ItemToPrint = itemToPrint
                        });
                });
            task.ContinueWith(
                (t) =>
                {
                    if (!t.IsFaulted) // notify that this job was printed successfully
                    {
                        Debug.WriteLine(DateTime.Now + " TestPrinter new thread started " + Thread.CurrentThread.ManagedThreadId);
                        Thread.Sleep(500);
                        printJobStatusChangedSubject.OnNext(
                            new PrintJobStatus
                            {
                                JobStatus = ProducibleStatuses.ProducibleCompleted,
                                PrinterId = Id,
                                ItemToPrint = itemToPrint
                            });
                    }
                    else // notify of an error
                    {
                        Debug.WriteLine(t.Exception.InnerExceptions.Aggregate("", (s, exception) => exception.Message + ", "));
                        printJobStatusChangedSubject.OnNext(
                            new PrintJobStatus
                            {
                                JobStatus = ErrorProducibleStatuses.ProducibleRemoved,
                                ErrorMessage = t.Exception.InnerExceptions.Aggregate("", (s, exception) => exception.Message + ", "),
                                PrinterId = Id,
                                ItemToPrint = itemToPrint
                            });
                    }
                });
        }

        public Guid Id { get; set; }

        public MachineTypes MachineType { get; private set; }


        public MachineStatuses CurrentStatus { get; set; }
        public IObservable<MachineProductionStatuses> CurrentProductionStatusObservable { get; private set; }
        public IEnumerable<ICapability> CurrentCapabilities { get; private set; }
        public MachineProductionStatuses CurrentProductionStatus { get; set; }
        public List<MachineError> Errors { get; set; }
        public string Alias { get; set; }
        public string Description { get; set; }


        public IObservable<MachineStatuses> CurrentStatusChangedObservable { get; private set; }
        public IObservable<MachineProductionStatuses> MachineProductionStatusesObservable { get; private set; }
        public IObservable<IEnumerable<ICapability>> CurrentCapabilitiesChangedObservable { get; private set; }

        public void ResetEventCommunication()
        {
            throw new NotImplementedException();
        }

        public void BeginEventCommunication()
        {
            throw new NotImplementedException();
        }
    }
}