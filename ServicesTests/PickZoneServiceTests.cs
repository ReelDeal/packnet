﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Business.PickZone;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.BoxFirst;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PickZones;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Utils;
using PackNet.Services;
using Testing.Specificity;

namespace ServicesTests
{
    using PackNet.Common.Interfaces.ExtensionMethods;
    using PackNet.Data.PickArea;

    [TestClass]
    public class PickZoneServiceTests
    {
        private EventAggregator eventAggregator;

        private Mock<IPickZones> pickZoneManager;
        private Mock<ILogger> logger;
        private Mock<IUICommunicationService> uiCommunicationServiceMock;
        private Mock<IServiceLocator> serviceLocatorMock;

        [TestInitialize]
        public void Setup()
        {
            eventAggregator = new EventAggregator();
            pickZoneManager = new Mock<IPickZones>();
            logger = new Mock<ILogger>();
            uiCommunicationServiceMock = new Mock<IUICommunicationService>();
            serviceLocatorMock = new Mock<IServiceLocator>();
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldUpdateNumberOfRequestsInRepo_OnMessage_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var pz1 = new PickZone() { Id = Guid.NewGuid() };
            var pz2 = new PickZone() { Id = Guid.NewGuid() };

            pickZoneManager.Setup(m => m.GetPickZones())
                .Returns(new List<PickZone>() { pz1, pz2 });

            var pickZonesService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<PickZone>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<PickZone>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.Data.Count()).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(0).NumberOfRequests).Should.BeEqualTo(2);
                    Specify.That(msg.Data.ElementAt(1).NumberOfRequests).Should.BeEqualTo(1);
                    Specify.That(msg.MessageType).Should.BeEqualTo(PickZoneMessages.PickZones);
                    responsePublished = true;
                });

            pickZoneManager.Setup(m => m.Update(It.IsAny<PickZone>())).Callback<PickZone>(pz =>
            {
                if (pz.Id.Equals(pz1.Id))
                {
                    Specify.That(pz.NumberOfRequests).Should.BeEqualTo(2);
                    cpg1Updated = true;
                }
                else if (pz.Id.Equals(pz2.Id))
                {
                    Specify.That(pz.NumberOfRequests).Should.BeEqualTo(1);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
                {
                    GetBoxFirstProducibleCarton("1", pz1),
                    GetBoxFirstProducibleCarton("2", pz2),
                    GetBoxFirstProducibleCarton("3", pz1),
                }
            });

            Retry.For(() => responsePublished, TimeSpan.FromSeconds(1));
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldClearNumberOfRequestsInRepo_OnMessage__WhenNoProduciblesForPickZoneIsPassed_AndPublishNewCountToUi()
        {
            var responsePublished = false;
            var cpg1Updated = false;
            var cpg2Updated = false;

            var pz1 = new PickZone() { Id = Guid.NewGuid() };
            var pz2 = new PickZone() { Id = Guid.NewGuid() };

            pickZoneManager.Setup(m => m.GetPickZones())
                .Returns(new List<PickZone>() { pz1, pz2 });

            var pickZonesService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock
                .Setup(m => m.SendMessageToUI(It.IsAny<Message<IEnumerable<PickZone>>>(), It.IsAny<bool>(), It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<PickZone>>, bool, bool>((msg, b, b2) =>
                {
                    Specify.That(msg.MessageType).Should.BeEqualTo(PickZoneMessages.PickZones);
                    responsePublished = true;
                });

            pickZoneManager.Setup(m => m.Update(It.IsAny<PickZone>())).Callback<PickZone>(pz =>
            {
                if (pz.Id.Equals(pz1.Id))
                {
                    Specify.That(pz.NumberOfRequests).Should.BeEqualTo(0);
                    cpg1Updated = true;
                }
                else if (pz.Id.Equals(pz2.Id))
                {
                    Specify.That(pz.NumberOfRequests).Should.BeEqualTo(0);
                    cpg2Updated = true;
                }
                else
                {
                    Specify.That(true).Should.BeFalse("Trying to update nonexistant CPG");
                }
            });

            eventAggregator.Publish(new Message<IEnumerable<BoxFirstProducible>>()
            {
                MessageType = StagingMessages.ProduciblesStaged,
                Data = new List<BoxFirstProducible>()
            });

            Retry.For(() => responsePublished, TimeSpan.FromSeconds(1));
            Specify.That(responsePublished).Should.BeTrue();
            Specify.That(cpg1Updated).Should.BeTrue();
            Specify.That(cpg2Updated).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleUpdatePickZoneStatusMessage()
        {
            var eventCalled = false;
            var messagePublished = false;
            var pickZoneId = Guid.NewGuid();

            pickZoneManager.Setup(m => m.SetPickZoneStatus(It.IsAny<Guid>(), It.IsAny<PickZoneStatuses>()))
                .Callback<Guid, PickZoneStatuses>(
                    (id, status) =>
                    {
                        Specify.That(id).Should.BeEqualTo(pickZoneId, "SetPickZoneStatus called with incorrect PickZone Id");
                        Specify.That(status)
                            .Should.BeEqualTo(PickZoneStatuses.Stop, "SetPickZoneStatus called with incorrect ZoneStatus");
                        eventCalled = true;
                    });

            eventAggregator.GetEvent<IMessage>()
                .Where(msg => msg.MessageType == SelectionAlgorithmMessages.SelectionEnvironmentChanged)
                .Subscribe(
                    (msg) =>
                    {
                        messagePublished = true;
                    });

            var pickZonesService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<PickZone>()
            {
                MessageType = PickZoneMessages.UpdatePickZoneStatus,
                Data = new PickZone()
                {
                    Id = pickZoneId,
                    Status = PickZoneStatuses.Stop
                }
            });

            Retry.For(() => eventCalled && messagePublished, TimeSpan.FromSeconds(1));
            Specify.That(eventCalled).Should.BeTrue("SetPickZoneStatus never called");
            Specify.That(messagePublished).Should.BeTrue("Message to restage producibles never published");

        }

        private BoxFirstProducible GetBoxFirstProducibleCarton(string customerId, PickZone pickZone)
        {
            var p1 = new BoxFirstProducible();
            p1.Restrictions.Add(new BasicRestriction<PickZone>(pickZone));
            p1.CustomerUniqueId = customerId;
            p1.Producible = new Carton();
            p1.Producible.CustomerUniqueId = customerId;

            return p1;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleDeleteMessage()
        {
            var responsePublished = false;

            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            var pickZoneToDelete =
                new PickZone
                {
                    Alias = "PZ1",
                    Id = Guid.NewGuid()
                };

            uiCommunicationServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<IResponseMessage<PickZone>>(), false, It.IsAny<bool>()))
                .Callback<IMessage<PickZone>, bool, bool>((x, b, b2) =>
            {
                Specify.That((x as IResponseMessage<PickZone>).Result).Should.BeEqualTo(ResultTypes.Success);
                Specify.That(x.MessageType).Should.BeEqualTo(PickZoneMessages.PickZoneDeleted);
                responsePublished = true;
            });

            eventAggregator.Publish(new Message<PickZone> { MessageType = PickZoneMessages.DeletePickZone, Data = pickZoneToDelete });

            Specify.That(responsePublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldLogMessagesWhenHandlingCrudMessages()
        {
            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<PickZone>
            {
                MessageType = PickZoneMessages.CreatePickZone,
                Data = new PickZone
                {
                    Alias = "AZ1",
                    Id = Guid.Empty,
                    Status = PickZoneStatuses.Normal
                },
                UserName = "User1"
            });
            logger.Verify(l => l.Log(LogLevel.Info, "Pickzone with Id: AZ1 and Alias: AZ1 was Created by User: User1"));

            eventAggregator.Publish(new Message<PickZone>
            {
                MessageType = PickZoneMessages.UpdatePickZone,
                Data = new PickZone
                {
                    Alias = "AZ1",
                    Id = Guid.NewGuid(),
                    Status = PickZoneStatuses.Normal
                },
                UserName = "User1"
            });
            logger.Verify(l => l.Log(LogLevel.Info, "Pickzone with Id: AZ1 and Alias: AZ1 was Edited by User: User1"));

            eventAggregator.Publish(new Message<PickZone>
            {
                MessageType = PickZoneMessages.DeletePickZone,
                Data =
                new PickZone
                {
                    Alias = "AZ1",
                    Id = Guid.NewGuid(),
                    Status = PickZoneStatuses.Normal
                },
                UserName = "User1"
            });
            logger.Verify(l => l.Log(LogLevel.Info, "Pickzone with Id: AZ1 and Alias: AZ1 was Deleted by User: User1"));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldLogFailingCreateMessages()
        {
            pickZoneManager.Setup(m => m.Create(It.IsAny<PickZone>())).Returns<PickZone>((pz) =>
            {
                if (pz.Alias == "AZ1")
                    throw new Exception("Ex1");

                if (pz.Alias == "AZ2")
                    throw new AlreadyPersistedException("Persisted");

                return null;
            });

            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<PickZone>
            {
                MessageType = PickZoneMessages.CreatePickZone,
                Data = new PickZone
                {
                    Alias = "AZ1",
                    Id = Guid.NewGuid(),
                    Status = PickZoneStatuses.Normal
                },
                UserName = "User1"
            });
            logger.Verify(
                l =>
                    l.Log(LogLevel.Error, It.Is<string>(s => s.Contains("Unable to create Pickzone with Id: AZ1 and Alias: AZ1, message: Ex1"))));

            eventAggregator.Publish(new Message<PickZone>
            {
                MessageType = PickZoneMessages.CreatePickZone,
                Data = new PickZone
                {
                    Alias = "AZ2",
                    Status = PickZoneStatuses.Normal
                },
                UserName = "User1"
            });
            logger.Verify(
                l =>
                    l.Log(LogLevel.Error,
                        It.Is<string>(s => s.Contains("Pickzone with Id: AZ2 and Alias: AZ2 already exists in the database"))));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldLogFailingEditMessages()
        {
            pickZoneManager.Setup(m => m.Update(It.IsAny<PickZone>())).Returns<PickZone>((pz) =>
            {
                if (pz.Id == Guid.Empty)
                    throw new Exception("Ex1");

                if (pz.Alias == "2")
                    throw new UnableToLocateByPacksizeIdException("Not locatable");

                return null;
            });

            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<PickZone>
            {
                MessageType = PickZoneMessages.UpdatePickZone,
                Data = new PickZone
                {
                    Alias = "AZ1",
                    Id = Guid.Empty,
                    Status = PickZoneStatuses.Normal
                },
                UserName = "User1"
            });

            logger.Verify(l => l.Log(LogLevel.Error, "Unable to edit Pickzone with Id: AZ1 and Alias: AZ1, message: Ex1"));

            eventAggregator.Publish(new Message<PickZone>
            {
                MessageType = PickZoneMessages.UpdatePickZone,
                Data = new PickZone
                {
                    Alias = "AZ1",
                    Id = Guid.NewGuid(),
                    Status = PickZoneStatuses.Normal
                },
                UserName = "User1"
            });

            logger.Verify(l => l.Log(LogLevel.Info, "Pickzone with Id: AZ1 and Alias: AZ1 was Edited by User: User1"));
        }

        [TestMethod]
        [TestCategory("")]
        public void ShouldLogFailingDeleteMessages()
        {
            pickZoneManager.Setup(m => m.Delete(It.IsAny<PickZone>())).Callback(() => { throw new Exception("e"); });

            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<PickZone>
            {
                MessageType = PickZoneMessages.DeletePickZone,
                Data =
                new PickZone
                {
                    Alias = "AZ1",
                    Id = Guid.Empty,
                    Status = PickZoneStatuses.Normal
                },
                UserName = "User1"
            });

            logger.Verify(l => l.Log(LogLevel.Error, "Unable to delete Pickzone with Id: AZ1 and Alias: AZ1, message: e"));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleCreateMessage()
        {
            var responsePublished = false;

            pickZoneManager.Setup(manager => manager.Create(It.IsAny<PickZone>())).Returns<PickZone>(c => c);

            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            var pickzoneToCreate = new PickZone()
            {
                Alias = "PZ1",
                Id = Guid.NewGuid()
            };

            uiCommunicationServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<IResponseMessage<PickZone>>(), false, It.IsAny<bool>()))
                .Callback<IMessage<PickZone>, bool, bool>((x, b, b2) =>
            {
                Specify.That((x as IResponseMessage<PickZone>).Result).Should.BeEqualTo(ResultTypes.Success);
                Specify.That(x.MessageType).Should.BeEqualTo(PickZoneMessages.PickZoneCreated);

                responsePublished = true;
            });

            eventAggregator.Publish(new Message<PickZone> { MessageType = PickZoneMessages.CreatePickZone, Data = pickzoneToCreate });

            Specify.That(responsePublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleEditMessage()
        {
            var responsePublished = false;

            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            var pickZoneToEdit = new PickZone()
            {
                Alias = "PZ1",
                Id = Guid.NewGuid()
            };

            uiCommunicationServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<IResponseMessage<PickZone>>(), false, It.IsAny<bool>()))
                .Callback<IMessage<PickZone>, bool, bool>((x, b, b2) =>
            {
                Specify.That((x as IResponseMessage<PickZone>).Result).Should.BeEqualTo(ResultTypes.Success);
                Specify.That(x.MessageType).Should.BeEqualTo(PickZoneMessages.PickZoneUpdated);

                responsePublished = true;
            });

            eventAggregator.Publish(new Message<PickZone> { MessageType = PickZoneMessages.UpdatePickZone, Data = pickZoneToEdit });

            Specify.That(responsePublished).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotAllowTwoPickzonesWithSameAlias()
        {
            var pickZoneToEdit = new PickZone()
            {
                Alias = "PZ1",
                Id = Guid.NewGuid()
            };

            var responsePublished = false;
            var repoMock = new Mock<IPickZoneRepository>();
            repoMock.Setup(r => r.FindByAlias(It.Is<string>(s => s == pickZoneToEdit.Alias)))
                .Returns(new PickZone()
                         {
                             Alias = pickZoneToEdit.Alias, 
                             Id = Guid.NewGuid()
                         });
            var pzManager = new PickZones(repoMock.Object);

            var pickZoneService = new PickZoneService(pzManager, uiCommunicationServiceMock.Object, eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            eventAggregator.GetEvent<ResponseMessage<PickZone>>().Where(msg => msg.Result == ResultTypes.Exists)
                .DurableSubscribe(
                    msg =>
                    {
                        responsePublished = true;   
                    });

            eventAggregator.Publish(new Message<PickZone> { MessageType = PickZoneMessages.UpdatePickZone, Data = pickZoneToEdit });

            Specify.That(responsePublished).Should.BeTrue("We should have gotten a response telling us that we can't save the pickzone because the alias is used by another pickzone.");
            repoMock.Verify(r => r.Update(It.IsAny<PickZone>()), Times.Never, "Update was called anyway");
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldHandleGetPickzonesMessage()
        {
            var responsePosted = false;

            var pickZones = new List<PickZone>()
            {
                new PickZone(),
                new PickZone()
            };

            pickZoneManager.Setup(manager => manager.GetPickZones())
                .Returns(pickZones);

            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            uiCommunicationServiceMock.Setup(s => s.SendMessageToUI(It.IsAny<IMessage<IEnumerable<PickZone>>>(), false, It.IsAny<bool>()))
                .Callback<IMessage<IEnumerable<PickZone>>, bool, bool>((x, b, b2) =>
            {
                Specify.That(x.MessageType).Should.BeEqualTo(PickZoneMessages.PickZones);
                Specify.That(x.Data.SequenceEqual(pickZones)).Should.BeTrue();
                responsePosted = true;
            });

            eventAggregator.Publish(new Message { MessageType = PickZoneMessages.GetPickZones });

            Specify.That(responsePosted).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPostGetPickzonesMessageWhenUpdateIsFinished()
        {
            var responsePosted = false;

            var pickZones = new List<PickZone>()
            {
                new PickZone(),
                new PickZone()
            };

            pickZoneManager.Setup(manager => manager.GetPickZones())
                .Returns(pickZones);

            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object,
                eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);
            
            eventAggregator.GetEvent<IMessage>()
                .Where(m=>m.MessageType == PickZoneMessages.GetPickZones)
                .Subscribe(msg =>
                {
                    responsePosted = true;
                });

            eventAggregator.Publish(new Message<PickZone> { MessageType = PickZoneMessages.UpdatePickZone, Data = new PickZone() });
            Specify.That(responsePosted).Should.BeTrue();
            responsePosted = false;

            eventAggregator.Publish(new Message<PickZone> { MessageType = PickZoneMessages.CreatePickZone, Data = new PickZone() });
            Specify.That(responsePosted).Should.BeTrue();
            responsePosted = false;

            eventAggregator.Publish(new Message<PickZone> { MessageType = PickZoneMessages.DeletePickZone, Data = new PickZone() });
            Specify.That(responsePosted).Should.BeTrue();
            responsePosted = false;
        }
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldNotifyWhenAPickZoneChangesStatusToStop()
        {
            PickZone latestEventData = null;
            bool waitForEvent;

            eventAggregator.GetEvent<Message<PickZone>>()
                .Where(m => m.MessageType == PickZoneMessages.StatusChanged)
                .Subscribe(m =>
                {
                    latestEventData = m.Data;
                    waitForEvent = false;
                });

            waitForEvent = true;

            pickZoneManager.Setup(m => m.Update(It.IsAny<PickZone>()))
                .Returns(new PickZone());

            var corrugateService = new PickZoneService(pickZoneManager.Object, uiCommunicationServiceMock.Object,
             eventAggregator, eventAggregator, serviceLocatorMock.Object, logger.Object);

            eventAggregator.Publish(new Message<PickZone>
            {
                MessageType = PickZoneMessages.UpdatePickZone,
                Data = new PickZone
                {
                    Alias = "PZ1",
                    Id = Guid.NewGuid(),
                    Status = PickZoneStatuses.Stop
                },
                UserName = "User1"
            });

            Retry.For(() => waitForEvent == false, TimeSpan.FromSeconds(2));

            Specify.That(latestEventData.Alias).Should.BeEqualTo("PZ1");
            Specify.That(latestEventData.Status).Should.BeEqualTo(PickZoneStatuses.Stop);
        }
    }
}