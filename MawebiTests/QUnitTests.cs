﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MawebiTests
{
    using System.IO;
    using System.Linq;
    using System.Threading;

    using Testing.Specificity;

    using WatiN.Core;

    [TestClass]
    public class UnitTest1
    {
        private TestContext testContextInstance;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        //todo: fix unit test
#if DEBUG
        /// <summary>
        /// Hack to run the QUnit tests in CI. Currently there is a bug in TFS online where we can't add multiple test runs... otherwise chutzpuh should run QUnit test
        /// </summary>
        [TestMethod]
        public void RunQUnitTests()
        {
            var currentDir = Directory.GetCurrentDirectory();
            var path = Path.Combine(currentDir, @"..\..\..\Mawebi\tests\unittests.html");
            if (!File.Exists(path))
            {
                //Build server path
                path = Path.Combine(currentDir, @"..\..\..\src\Mawebi\tests\unittests.html");
            }
            if (!File.Exists(path))
                Assert.Fail("Could not locate QUnit test file");

            TestContext.WriteLine("Executing QUnit tests at " + path);
            var failedCount = 0;
            var testCount = 0;
            var passedCount = 0;
            using (var ie = new IE(path))
            {
                Thread.Sleep(2000); //Can't let IE close too quickly
                testCount = ie.ListItems.Count(li => li.Id != null);
                TestContext.WriteLine("Total tests {0}", testCount);
                var failedTests = ie.ListItems.Filter(Find.ByClass("fail")).Where(li => li.Id != null).ToList();
                failedCount = failedTests.Count;
                TestContext.WriteLine("Total failed tests {0}", failedCount);
                foreach (var failedTest in failedTests)
                {
                    TestContext.WriteLine(failedTest.Text);
                }
                var passedTests = ie.ListItems.Filter(Find.ByClass("pass"));
                passedCount = passedTests.Count;
                TestContext.WriteLine("Total passed tests {0}", passedCount);
                ie.Close();
                if (ie.NativeBrowser != null)
                {
                    ie.ForceClose();
                }
            }
            Specify.That(failedCount).Should.BeEqualTo(0, "Failed tests detected");
            Specify.That(testCount > 0).Should.BeTrue("No tests detected");
            Specify.That(passedCount > 0).Should.BeTrue("No passed tests detected");
        }
#endif
    }
}
