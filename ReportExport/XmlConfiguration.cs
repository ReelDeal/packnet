﻿namespace Packsize.MachineManager.ReportExport
{
    using System.IO;
    using System.Runtime.Serialization;
    using System.Xml;

    [DataContract(Name = "XmlConfiguration", Namespace = "http://Packsize.MachineManager.com")]
    public class XmlConfiguration<T>
    {
        public static T Deserialize(string path)
        {
            FileStream stream = new FileStream(path, FileMode.OpenOrCreate);

            try
            {
                var reader = XmlDictionaryReader.CreateTextReader(stream, new XmlDictionaryReaderQuotas());

                DataContractSerializer serializer = new DataContractSerializer(typeof(T));

                T supplier = (T)serializer.ReadObject(reader);

                return supplier;
            }
            finally
            {
                stream.Close();
            }
        }

        public void Serialize(string path)
        {
            FileStream stream = new FileStream(path, FileMode.Create);

            XmlWriter xmlWriter = null;

            try
            {
                xmlWriter = XmlWriter.Create(stream, new XmlWriterSettings { Indent = true, NewLineHandling = NewLineHandling.Entitize });

                DataContractSerializer serializer = new DataContractSerializer(typeof(T));

                serializer.WriteObject(xmlWriter, this);

                xmlWriter.Flush();
            }
            finally
            {
                stream.Close();
            }
        }
    }
}
