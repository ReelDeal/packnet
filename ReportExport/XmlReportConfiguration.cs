﻿namespace Packsize.MachineManager.ReportExport
{
    using System.Runtime.Serialization;

    [DataContract(Name = "ReportConfiguration", Namespace = "http://Packsize.MachineManager.com")]
    public class XmlReportConfiguration : XmlConfiguration<XmlReportConfiguration>
    {
        [DataMember(Name = "ExcelReportTemplate")]
        public string ExcelReportTemplate { get; set; }

        [DataMember(Name = "HistoryDB")]
        public string HistoryDB { get; set; }

        [DataMember(Name = "LastNumberOfDays")]
        public string LastNumberOfDays { get; set; }

        [DataMember(Name = "ReportDestinationPath")]
        public string ReportDestinationPath { get; set; }

        [DataMember(Name = "ServerHostUri")]
        public string ServerHostUri { get; set; }
    }
}
