﻿namespace Packsize.MachineManager.ReportExport
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using DocumentFormat.OpenXml;
    using DocumentFormat.OpenXml.Packaging;
    using DocumentFormat.OpenXml.Spreadsheet;
    using DocumentFormat.OpenXml.Validation;
    using Npgsql;

    using Packsize.MachineManager.ReportExport.Properties;

    public class Program
    {
        private const string CustomerDataSqlQuery =
            @"select p.id as ""JobId"", p.completed as ""Completed"", p.username as ""UserName"", p.corrugatequality as ""CorrugateQuality"", p.corrugatethickness as ""CorrugateThickness"", 
                     p.corrugatewidth as ""CorrugateWidth"", p.corrugateexternalid as ""CorrugateExternalId"", p.corrugatefeededlength as ""CorrugateFeededLength"", p.packagingwidth as ""PackagingWidth"", 
                     p.packagingusedwidth as ""PackagingUsedWidth"", p.packaginglength as ""PackagingLength"", p.failure as ""Failure"", p.productiontime as ""ProductionTime"", p.machinename as ""MachineName"", p.numberofrequests as ""NumberOfRequests"", 
                     c.id as ""CartonRequestId"", c.created as ""CartonRequestCreated"", c.serialnumber as ""SerialNumber"", c.name as ""Name"", c.length as ""Length"", c.width as ""Width"", 
                     c.height as ""Height"", c.classificationnumber as ""ClassificationNumber"", c.corrugatequality as ""CorrugateQuality"", c.pickzone as ""PickZone"", c.cartonpropertygroup as ""CartonPropertyGroup"", c.designid as ""DesignId"", 
                     c.nextlabelrequestnumber as ""NextLabelRequestNumber"", p.corrugatefeededlength/p.numberofrequests as ""FeedLengthPerRequest"", (p.corrugatewidth * p.corrugatefeededlength)/p.numberofrequests as ""AreaPerRequest"",
                     c.custom as ""CustomMeasures"", c.articleid as ""ArticleId"", c.orderId as ""OrderId""
              from cartonproductiondata p RIGHT JOIN cartonrequest c on p.id = c.productionid 
              [whereClause] 
              order by p.completed desc";

        private static string datesFilterWhereClause = " where (p IS NULL AND c.created between '{fromDate}' and '{toDate}') OR p.completed between '{fromDate}' and '{toDate}'";
        private static XmlReportConfiguration reportConfiguration;
        private static bool useDateFilter = false;

        internal static void Main(string[] arguments)
        {
            reportConfiguration = LoadReportConfiguration();

            var argument = arguments.Where(s => s.StartsWith("-consoleout:")).SingleOrDefault();
            StreamWriter streamWriter = null;
            if (argument != null)
            {
                var target = argument.Split(':')[1];
                streamWriter = new StreamWriter(target);
                streamWriter.AutoFlush = true;
                Console.SetOut(streamWriter);
            }

            argument = arguments.Where(s => s.StartsWith("-server:")).SingleOrDefault();
            var server = reportConfiguration.ServerHostUri;
            if (argument != null)
            {
                server = argument.Replace("-server:", string.Empty);
            }

            argument = arguments.Where(s => s.StartsWith("-historyDB:")).SingleOrDefault();
            var historyDB = reportConfiguration.HistoryDB;
            if (argument != null)
            {
                historyDB = argument.Replace("-historyDB:", string.Empty);
            }
      
            argument = arguments.Where(s => s.StartsWith("-fromDate:")).SingleOrDefault();
            if (argument != null)
            {
                useDateFilter = true;
                var date = DateTime.Parse(argument.Split(':')[1]);
                datesFilterWhereClause = datesFilterWhereClause.Replace("{fromDate}", TimeZoneInfo.ConvertTimeToUtc(date, TimeZoneInfo.Local).ToShortDateString());
            }

            argument = arguments.Where(s => s.StartsWith("-toDate:")).SingleOrDefault();
            if (argument != null)
            {
                useDateFilter = true;
                var date = DateTime.Parse(argument.Split(':')[1]);
                datesFilterWhereClause = datesFilterWhereClause.Replace("{toDate}", TimeZoneInfo.ConvertTimeToUtc(date, TimeZoneInfo.Local).ToShortDateString());
            }

            useDateFilter = true;
            argument = arguments.Where(s => s.StartsWith("-lastNumberOfDays:")).SingleOrDefault();
            var numberOfDaysString = reportConfiguration.LastNumberOfDays;
            if (argument != null)
            {
                numberOfDaysString = argument.Replace("-lastNumberOfDays:", string.Empty);
            }

            var numberOfDays = int.Parse(numberOfDaysString);
            datesFilterWhereClause = datesFilterWhereClause.Replace("{fromDate}", TimeZoneInfo.ConvertTimeToUtc(DateTime.Now.AddDays(-numberOfDays + 1), TimeZoneInfo.Local).ToShortDateString());
            datesFilterWhereClause = datesFilterWhereClause.Replace("{toDate}", TimeZoneInfo.ConvertTimeToUtc(DateTime.Now.AddDays(1), TimeZoneInfo.Local).ToShortDateString());

            try
            {
                string queryToExecute = CustomerDataSqlQuery;

                if (useDateFilter)
                {
                    queryToExecute = queryToExecute.Replace("[whereClause]", datesFilterWhereClause);
                }
                else
                {
                    queryToExecute = queryToExecute.Replace("[whereClause]", string.Empty);
                }

                var rawData = GetRawData(server, historyDB, queryToExecute);
                var reportFile = CopyTemplateToDestination(reportConfiguration.ExcelReportTemplate, reportConfiguration.ReportDestinationPath);
                var document = GetWorkbook(reportFile);
                WriteValuesToSheet(document, rawData);
                if (arguments.Where(s => s == "-validate").Count() > 0)
                {
                    Validate(document);
                }

                SaveDataCopy(document);
            }
            catch (Exception ex)
            {
                WriteExceptionInfo(ex);
            }

            if (streamWriter != null)
            {
                var sw = new StreamWriter(Console.OpenStandardOutput());
                sw.AutoFlush = true;
                Console.SetOut(sw);
                streamWriter.Close();
                streamWriter.Dispose();
            }
        }

        private static void WriteValuesToSheet(SpreadsheetDocument myDoc, List<object[]> rows)
        {
            WorkbookPart workbookPart = myDoc.WorkbookPart;

            Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().Where(s => s.Name.Value.Equals("RawData")).First();

            workbookPart.DeletePart(workbookPart.CalculationChainPart);

            WorksheetPart worksheetPart = (WorksheetPart)workbookPart.GetPartById(sheet.Id.Value);

            WorksheetPart replacementPart = workbookPart.AddNewPart<WorksheetPart>();
            sheet.Id.Value = workbookPart.GetIdOfPart(replacementPart);

            OpenXmlReader reader = OpenXmlReader.Create(worksheetPart);
            OpenXmlWriter writer = OpenXmlWriter.Create(replacementPart);

            Row r = new Row();
            Cell c = new Cell();
            CellValue v = new CellValue();
            c.Append(v);

            while (reader.Read())
            {
                if (reader.ElementType == typeof(SheetData))
                {
                    if (reader.IsEndElement)
                    {
                        continue;
                    }

                    writer.WriteStartElement(new SheetData());

                    foreach (var dataRow in rows)
                    {
                        writer.WriteStartElement(r);

                        for (var i = 0; i < dataRow.Length; i++)
                        {
                            c.StyleIndex = UInt32Value.FromUInt32(0U);
                            c.CellFormula = null;
                            if (dataRow[i] is DateTime)
                            {
                                c.CellValue.Text = string.Empty;
                                string dateString = TimeZoneInfo.ConvertTimeFromUtc((DateTime)dataRow[i], TimeZoneInfo.Local).ToString("yyyy-MM-dd HH:mm:ss");
                                c.CellFormula = new CellFormula("=Datevalue(\"" + dateString + "\") + Timevalue(\"" + dateString + "\")");
                                c.CellFormula.CalculateCell = new BooleanValue(true);
                                c.StyleIndex = UInt32Value.FromUInt32(1U);
                                c.DataType = new EnumValue<CellValues>(CellValues.Date);
                            }
                            else if (dataRow[i] is int || dataRow[i] is long || dataRow[i] is short || dataRow[i] is byte)
                            {
                                c.DataType = new EnumValue<CellValues>(CellValues.Number);
                                c.CellValue.Text = dataRow[i].ToString();
                            }
                            else if (dataRow[i] is double || dataRow[i] is float)
                            {
                                c.DataType = new EnumValue<CellValues>(CellValues.Number);
                                c.CellValue.Text = dataRow[i].ToString().Replace(',', '.');
                            }
                            else if (dataRow[i] is bool)
                            {
                                c.DataType = new EnumValue<CellValues>(CellValues.Boolean);
                                c.CellValue.Text = ((bool)dataRow[i]) ? "1" : "0";
                            }
                            else
                            {
                                c.DataType = new EnumValue<CellValues>(CellValues.String);
                                c.CellValue.Text = dataRow[i].ToString();
                            }

                            writer.WriteElement(c);
                        }

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();
                }
                else
                {
                    if (reader.IsStartElement)
                    {
                        writer.WriteStartElement(reader);
                    }
                    else if (reader.IsEndElement)
                    {
                        writer.WriteEndElement();
                    }
                }
            }

            reader.Close();
            writer.Close();

            workbookPart.DeletePart(worksheetPart);
            AddDefinedName(workbookPart.Workbook, rows.Count, rows[0].Length);
        }

        private static void WriteExceptionInfo(Exception ex)
        {
            Console.Out.WriteLine("Failed to create reports. Contact a Packsize Support Technician.\n Detailed error message:\n" + ex.Message);
            Console.WriteLine("Press any key to exit");
            Console.ReadLine();
        }

        private static void Validate(SpreadsheetDocument document)
        {
            var validator = new OpenXmlValidator(FileFormatVersions.Office2010);
            var errors = validator.Validate(document).ToList();
            errors.ForEach(e => Console.Out.WriteLine(e.Path + ": " + e.Description + " - " + e.Node));
        }

        private static void SaveDataCopy(SpreadsheetDocument document)
        {
            document.Close();
            document.Dispose();
        }

        private static string CopyTemplateToDestination(string templatePath, string destinationPath)
        {
            var saveAs = "reports_" + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".xlsx";
            saveAs = Path.Combine(destinationPath, saveAs);
            if (!Directory.Exists(destinationPath))
            {
                Directory.CreateDirectory(destinationPath);
            }

            File.Copy(templatePath, saveAs);

            return saveAs;
        }

        private static void AddDefinedName(Workbook book, long rowCount, long columnCount)
        {
            var definedName = new DefinedName { Name = "History" };
            definedName.Text = "RawData!$A$1:$" + GetExcelColumnHeaderLetter(columnCount) + "$" + rowCount;

            book.DefinedNames = new DefinedNames();
            book.DefinedNames.Append(definedName);
        }

        private static SpreadsheetDocument GetWorkbook(string filePath)
        {
            SpreadsheetDocument document = SpreadsheetDocument.Open(filePath, true, new OpenSettings { AutoSave = true });
            return document;
        }

        private static List<object[]> GetRawData(string server, string historyDB, string sqlQuery)
        {
            var connStr = "Server=" + server + ";database=" + historyDB + ";User ID=postgres;Password=trazan123@";
            var conn = new NpgsqlConnection(connStr);
            var cmd = new NpgsqlCommand();
            cmd.Connection = conn;
            conn.Open();

            cmd.CommandText = sqlQuery;
            var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection);

            var rows = new List<object[]>();
            InsertHeaders(reader, rows);

            while (reader.Read())
            {
                var row = new object[reader.FieldCount];
                reader.GetValues(row);
                rows.Add(row);
            }

            reader.Close();
            reader.Dispose();
            cmd.Dispose();
            conn.Dispose();

            return rows;
        }

        private static void InsertHeaders(IDataReader reader, List<object[]> rows)
        {
            var row = new object[reader.FieldCount];
            for (int i = 0; i < reader.FieldCount; i++)
            {
                row[i] = reader.GetName(i);
            }

            rows.Add(row);
        }

        private static string GetExcelColumnHeaderLetter(long columnNumber)
        {
            int offset = 64;
            int range = 90 - offset;

            if (columnNumber > range)
            {
                var firstPart = columnNumber / range;
                var lastPart = columnNumber % range;
                return GetExcelColumnHeaderLetter(firstPart) + GetExcelColumnHeaderLetter(lastPart);
            }

            return ((char)(offset + columnNumber)).ToString();
        }

        private static XmlReportConfiguration LoadReportConfiguration()
        {
            if (File.Exists(Settings.Default.ReportConfigurationPath) == false)
            {
                Console.Out.WriteLine("Report Configuration file could not be found. Contact a Packsize Support Technician.\n");
                Console.WriteLine("Press any key to exit");
                Console.ReadLine();
            }

            return XmlReportConfiguration.Deserialize(Settings.Default.ReportConfigurationPath);
        }
    }
}