﻿using System;
using System.Activities.Tracking;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Threading;

using Newtonsoft.Json;

using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Interfaces;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.DTO.Scanning;
using PackNet.Common.Interfaces.DTO.Settings;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.WorkflowTracking;
using PackNet.Data.Workflows;
using PackNet.Services;
using PackNet.Common.Interfaces.Utils;

using Moq;

using PackNet.Services.SelectionAlgorithm.BoxLast;

namespace TestHarnesses
{
    using System.Windows.Controls;

    using PackNet.Business.Classifications;
    using PackNet.Business.PackagingDesigns;
    using PackNet.Common.Eventing;
    using PackNet.Common.Interfaces;
    using PackNet.Common.Interfaces.DTO.Corrugates;
    using PackNet.Common.Interfaces.DTO.PackagingDesigns;
    using PackNet.Common.Interfaces.DTO.PickZones;
    using PackNet.Common.Interfaces.DTO.ProductionGroups;
    using PackNet.Common.Interfaces.Enums;
    using PackNet.Common.Utils;
    using PackNet.Data.Classifications;
    using PackNet.Data.Corrugates;
    using PackNet.Data.PickArea;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ILogger logger;
        private readonly PrinterFactoryComposer printerFactory;
        private readonly List<IPrinter> printers;
        //private PrintService printService;
        private Dictionary<Guid /*printer key*/, List<Printable>> printJob = new Dictionary<Guid, List<Printable>>();
        private Guid usePrinter;
        private readonly Random random = new Random();
        public static readonly EventAggregator eventAggregator = new EventAggregator();

        private readonly ObservableCollection<ProductionGroupDto> productionGroups = new ObservableCollection<ProductionGroupDto>();
        private IDisposable createCartonDisposable;
        private IDisposable errorConditionSubscription;


        public ObservableCollection<ProductionGroupDto> ProductionGroups { get { return productionGroups; } }

        public MainWindow()
        {
            InitializeComponent();

            SetUpLogging();


            printerFactory = new PrinterFactoryComposer(logger);


            printers = new List<IPrinter>();
            txtSerialNumber.Text = GenerateRandomId();
            eventAggregator.GetEvent<PrinterStatus>()
                .ObserveOn(Scheduler.Default)
                .Subscribe(e => logger.Log(LogLevel.Info, "event aggregator PrinterStatus event:" + e.PrinterMachineStatus));
            PopulatePrintFormatComboBox();
            SubscribeForScanner();
            PopulateDropFileTab();
            DataContext = this;
            InitWorkflowTab();
        }

        private void SubscribeForScanner()
        {
            var createCartonSubscription = eventAggregator.GetEvent<CreateCartonTrigger>();
            createCartonDisposable = createCartonSubscription
                .ObserveOn(Scheduler.Default)
                .Subscribe(CreateCartonTriggerReceived);
        }

        private void CreateCartonTriggerReceived(CreateCartonTrigger carton)
        {
            if (!TxtBlockConsole.Dispatcher.CheckAccess())
            {
                TxtBlockConsole.Dispatcher.Invoke(DispatcherPriority.Normal, (Action)(() =>
                    CreateCartonTriggerReceived(carton)));
                return;
            }

            const string stringFormat = "Scanned Item: {0} for production group {1} at {2}{3}";
            TxtBlockConsole.Text += String.Format(stringFormat, carton.BarcodeData, carton.ProductionGroupAlias,
                carton.TriggeredOn.ToLongTimeString(), Environment.NewLine);
        }


        private string GenerateRandomId()
        {
            return random.Next(int.MaxValue).ToString().PadLeft(20, '0');
        }

        private void PopulateDropFileTab()
        {
            TxtDataDirectory.Text = "TODO";//Path.Combine(TestUtilities.MachineManagerInstallPath??@"c:\_packnet\server\bin\debug", "Data");
            PopulateDropBoxListBoxes();
        }

        private char DropFileDelimiter { get; set; }
        private string DropFileExtension { get; set; }
        private List<KeyValuePair<string, string>> DropFileFields { get; set; }

        

        private ObservableCollection<PickZone> pickZones = new ObservableCollection<PickZone>();
        private ObservableCollection<Corrugate> corrugates = new ObservableCollection<Corrugate>();
        private ObservableCollection<PackNet.Common.Interfaces.DTO.Classifications.Classification> classifications = new ObservableCollection<PackNet.Common.Interfaces.DTO.Classifications.Classification>();
        private ObservableCollection<IPackagingDesign> packagingDesigns = new ObservableCollection<IPackagingDesign>();
        private WorkflowTrackingRepository workflowRepo;

        public ObservableCollection<PickZone> PickZones
        {
            get { return pickZones; }
            set { pickZones = value; }
        }

        public ObservableCollection<Corrugate> Corrugates
        {
            get { return corrugates; }
            set { corrugates = value; }
        }

        public ObservableCollection<IPackagingDesign> PackagingDesigns
        {
            get { return packagingDesigns; }
            set { packagingDesigns = value; }
        }

        public ObservableCollection<PackNet.Common.Interfaces.DTO.Classifications.Classification> Classifications
        {
            get { return classifications; }
            set { classifications = value; }
        }

        private void PopulateDropBoxListBoxes()
        {
            // Packaging Designs
            var packagingDesignManager = new PackagingDesignManager(Path.Combine(TxtDataDirectory.Text, "Packaging Designs"), null, logger);
            packagingDesignManager.ReloadDesignsFromDisk();
            packagingDesignManager.GetDesigns().ForEach(p => PackagingDesigns.Add(p));

            // Classifications
            //var classificationsProvider = new XmlClassificationsProvider(Path.Combine(TxtDataDirectory.Text, "Classifications.xml"));
            var classificationsProvider = new PackNet.Business.Classifications.Classifications(new ClassificationRepository(), logger);

            // Corrugates
            var corrugatesBiz = new PackNet.Business.Corrugates.Corrugates(new CorrugateRepository());
            corrugatesBiz.GetCorrugates().ForEach(c => Corrugates.Add(c));

            // Packsize.ImportExportLogic.Types
        }

        private void BtnBrowseDataDirectory_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();

            TxtDataDirectory.Text = dialog.SelectedPath;
            PopulateDropBoxListBoxes();
        }

        private void AssignDropFileField(string key, string value)
        {
            var index = DropFileFields.FindIndex(v => v.Key == key);
            if (index != -1)
            {
                var tmpValKey = new KeyValuePair<string, string>(key, value);
                DropFileFields[index] = tmpValKey;
            }
        }

        private async void BtnGenerateDropFile_OnClick(object sender, RoutedEventArgs e)
        {
            var fileIncrement = 0;
            var dropFilePath = Path.Combine(TxtDropFolder.Text, TxtFileName.Text + DropFileExtension);
            while (File.Exists(dropFilePath))
            {
                fileIncrement++;
                dropFilePath = Path.Combine(TxtDropFolder.Text, TxtFileName.Text + fileIncrement + DropFileExtension);
            }

            var qty = Int32.Parse(TxtQuantity.Text);

            StringBuilder jobs = new StringBuilder();

            int pickZoneCount = 1;
            int classificationCount = 1;
            for (int current = 0; current < qty; current++)
            {
                if (current > ((qty / tbPickZones.SelectedItems.Count) * pickZoneCount))
                    if (pickZoneCount < tbPickZones.SelectedItems.Count)
                        pickZoneCount++;

                if (current > ((qty / tbClassifications.SelectedItems.Count) * classificationCount))
                    if (classificationCount < tbClassifications.SelectedItems.Count)
                        classificationCount++;

                AssignDropFileField("Command", TxtCommand.Text);
                AssignDropFileField("SerialNumber", Guid.NewGuid().ToString("N"));
                AssignDropFileField("OrderId", Guid.NewGuid().ToString("N"));
                AssignDropFileField("Name", Guid.NewGuid().ToString("N"));
                AssignDropFileField("Length", TxtLength.Text);
                AssignDropFileField("Width", TxtWidth.Text);
                AssignDropFileField("Height", TxtHeight.Text);
                AssignDropFileField("PickZone", ((PickZone)tbPickZones.SelectedItems[pickZoneCount - 1]).Alias);
                AssignDropFileField("DesignId", ((IPackagingDesign)tbPackagingDesigns.SelectedItem).Id.ToString());
                AssignDropFileField("ClassificationNumber", ((PackNet.Common.Interfaces.DTO.Classifications.Classification)tbClassifications.SelectedItems[classificationCount - 1]).Number);
                AssignDropFileField("CorrugateQuality", ((Corrugate)tbCorrugates.SelectedItem).Quality.ToString());

                jobs.AppendLine(String.Join(DropFileDelimiter.ToString(), DropFileFields.Select(d => d.Value).ToList()));
            }

            using (StreamWriter outfile = new StreamWriter(dropFilePath, false))
            {
                await outfile.WriteAsync(jobs.ToString());
            }

        }

        private void PopulatePrintFormatComboBox()
        {
            ObservableCollection<PrintFormats> printFormatses = new ObservableCollection<PrintFormats>();
            printFormatses.Add(PrintFormats.BarCode);
            printFormatses.Add(PrintFormats.PlainText);

            cbPrintableType.ItemsSource = printFormatses;
            cbPrintableType.SelectedItem = PrintFormats.BarCode;
        }

        private void SetUpLogging()
        {
            var mockLogger = new Mock<ILogger>();
            mockLogger
                .Setup(l => l.Log(It.IsAny<LogLevel>(), It.IsAny<string>()))
                .Callback<LogLevel, string>(WriteLogMessage);

            logger = mockLogger.Object;
            errorConditionSubscription = eventAggregator.GetEvent<IErrorCondition>().Subscribe(ErrorCondition);
        }

        private void ErrorCondition(IErrorCondition errorCondition)
        {
            WriteLogMessage(LogLevel.Debug, errorCondition.Message);
        }

        private void WriteLogMessage(LogLevel level, string message)
        {
            const string format = "*****************************************{2}Message: {0} with log level {3} was recorded at {1}{2}";
            //Debug.WriteLine("MainWindowLogger: " + s);
            Dispatcher.BeginInvoke(new Action(() => LogMessages.Text = LogMessages.Text.Insert(0, String.Format(format, message, DateTime.Now.ToString(@"MM-dd-yyyy a\t HH:mm:ss.fff"), Environment.NewLine, level))));
        }

        private void SetupPrinters()
        {
            if (printers.Any())
                return;

            printers.Add(printerFactory.GetPrinter(printerFactory.ProvidedPrinterTypes.First(), CreatePrinterSettings(Guid.Parse("DD7E1390-8060-4D15-92F9-365DFE93EE0E"), "Printer 1", TxtPrinterName1.Text, Int32.Parse(TxtPrinterPort1.Text))));
            printers.Add(printerFactory.GetPrinter(printerFactory.ProvidedPrinterTypes.First(), CreatePrinterSettings(Guid.Parse("0933F166-A62E-42D6-8297-7B0FE0D57DBD"), "Printer 2", TxtPrinterName2.Text, Int32.Parse(TxtPrinterPort2.Text))));
            printers.Add(printerFactory.GetPrinter(printerFactory.ProvidedPrinterTypes.First(), CreatePrinterSettings(Guid.Parse("AB525C76-80BC-4FBB-9C6D-B160593EBF1B"), "Printer 3", TxtPrinterName3.Text, Int32.Parse(TxtPrinterPort3.Text))));
        }

        private ZebraPrinterSettings CreatePrinterSettings(Guid id, string printerName, string printerAddress, int printerPort)
        {
            var zebraPrinterSettings = new ZebraPrinterSettings
            {
                Id = id,
                PrinterName = printerName,
                Address = new IPEndPoint(IPAddress.Parse(printerAddress), printerPort),
                PollingIntervalMilliseconds = 200
            };

            return zebraPrinterSettings;
        }

        private void addPrintableToRequest_Click(object sender, RoutedEventArgs e)
        {
            var printable = new Printable(txtSerialNumber.Text);

            if (!printJob.ContainsKey(usePrinter))
            {
                printJob.Add(usePrinter, new List<Printable>());
            }

            printJob[usePrinter].Add(printable);

            tbPendingPrintJob.Items.Add(usePrinter + " : '" + txtSerialNumber.Text + "'");
            txtSerialNumber.Text = GenerateRandomId();
        }

        private void SendPrintRequest_Click(object sender, RoutedEventArgs e)
        {
            //SetupPrinters();
            //if (printService == null)
            //{
            //    printService = new PrintService(printers, eventAggregator, logger);
            //    printService.PrintRequestStatusChangedObservable.Subscribe(
            //        p => Dispatcher.BeginInvoke(
            //            new Action(
            //                () =>
            //                {
            //                    if (p.RequestStatus == PrintRequestStatuses.Error)
            //                    {
            //                        txtRequestStatuses.AppendText(
            //                            DateTime.Now + ": " + p.RequestStatus + " -- " + p.PrintRequests.Aggregate(
            //                                "",
            //                                (s, r) =>
            //                                {
            //                                    return s + r.ErrorMessage + ",";
            //                                }) + Environment.NewLine);
            //                    }
            //                    else txtRequestStatuses.AppendText(DateTime.Now + ": " + p.RequestStatus + Environment.NewLine);

            //                })));
            //}

            //try
            //{
            //    var batchId = printService.Print(printJob);
            //    txtLog.AppendText(DateTime.Now + ": " + batchId + Environment.NewLine);
            //}
            //catch (Exception ex)
            //{
            //    txtLog.AppendText(DateTime.Now + ": Exception Sending batch" + ex.Message + Environment.NewLine);
            //}

            //printJob = new Dictionary<Guid, List<Printable>>();
            //tbPendingPrintJob.Items.Clear();
        }

        private void rbUsePrinter_Checked(object sender, RoutedEventArgs e)
        {
            var button = sender as RadioButton;

            usePrinter = Guid.Parse(button.Content.ToString());
        }

        private void ButtonAddProductionGroup_OnClick(object sender, RoutedEventArgs e)
        {
            var pg = GetMockProductionGroup(true, TextBoxProductionGroupName.Text, int.Parse(TextBoxListenPort.Text));

          
            eventAggregator.Publish(pg);
        }

        public static ProductionGroup GetMockProductionGroup(bool boxLast, string alias, int port)
        {
            var mockIProductionGroup = new Mock<ProductionGroup>();

            mockIProductionGroup.Setup(x => x.Alias).Returns(alias);
            mockIProductionGroup.Setup(x => x.SelectionAlgorithm).Returns(boxLast ? SelectionAlgorithmTypes.BoxLast : SelectionAlgorithmTypes.BoxFirst);
            return mockIProductionGroup.Object;
        }

        private void RemoveItemFromList(object sender, RoutedEventArgs e)
        {
            var obj = (sender as Button).DataContext;
            if (obj is ProductionGroupDto)
            {
                var dto = obj as ProductionGroupDto;
                ProductionGroups.Remove(dto);
                var pg = GetMockProductionGroup(false, dto.Name, 0);
                eventAggregator.Publish(pg);

            }
        }

        private void ScannerSimulatorStart_Click(object sender, RoutedEventArgs e)
        {
            var ipEndpoint = Networking.FindIpEndPointFromAddressWithPort(txtScannerServiceIpAddress.Text, true);
            var messagesPerSec = Convert.ToInt32(sliderMessagesPerSecond.Value);

            int messageStartRange = int.Parse(txtBarcodeRangeStart.Text);
            int messageEndRange = int.Parse(txtBarcodeRangeEnd.Text);
            int lastMessageSent = messageStartRange;

            Task.Factory.StartNew(() =>
            {
                bool run = true;
                while (run)
                {
                    var tasksToExecute = new List<Task>();

                    for (int i = 0; i < messagesPerSec; i++)
                    {
                        if (lastMessageSent < messageEndRange)
                        {
                            int sent = lastMessageSent;
                            tasksToExecute.Add(new Task(() => SendMessage(sent.ToString(CultureInfo.InvariantCulture), ipEndpoint)));
                        }
                        else
                        {
                            run = false;
                            break;
                        }
                        lastMessageSent++;
                    }

                    tasksToExecute.ForEach(x => x.Start());
                    tasksToExecute.Clear();
                    Thread.Sleep(1000);
                }
            });

            }
        private void SendMessage(string barCode, IPEndPoint ipEndpoint)
        {
            try
            {
                using (var cli = new TcpClient())
                {
                    cli.Connect(ipEndpoint);
                    using (var sw = new StreamWriter(cli.GetStream()))
                    {
                        sw.Write(barCode);
                    }
                }
            }
            catch (Exception)
            {
                
            }


        }

        private void FilterWorklfows_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbWorkflowId.Text))
                loadedWorkflowLifetimes = workflowRepo.All().Where(w=>w.Id.ToString() == tbWorkflowId.Text).ToList();
            else if (cbxWorkflowTypes.SelectedItem != null)
                loadedWorkflowLifetimes = workflowRepo.All().Where(w=>w.Type.DisplayName == cbxWorkflowTypes.SelectedValue.ToString()).ToList();
            else
                loadedWorkflowLifetimes = workflowRepo.All().ToList();
            gridWorkflowLifetimes.ItemsSource = loadedWorkflowLifetimes.Select(wl => new WorkflowLifetimeRowDisplay(wl));
            gridWorkflowLifetimes.SelectedIndex = 0;
        }

        private List<WorkflowLifetime> loadedWorkflowLifetimes;

        private void gridWorkflowLifetimes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }

        private void gridRecords_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                if (e.AddedItems[0] is PacksizeActivityTrackingRecordWrapper)
                    tbRecordDetail.Text = JsonConvert.SerializeObject((PacksizeActivityTrackingRecordWrapper)e.AddedItems[0]);
                else
                    tbRecordDetail.Text = JsonConvert.SerializeObject(e.AddedItems[0]);
            }
        }

        private void InitWorkflowTab()
        {
            workflowRepo = new PackNet.Data.Workflows.WorkflowTrackingRepository("Server=localhost:27017", "PackNetServer");
            this.cbxWorkflowTypes.Items.Add(WorkflowTypes.ImportWorkflow.DisplayName);
            this.cbxWorkflowTypes.Items.Add(WorkflowTypes.MachineGroupWorkflow.DisplayName);
            this.cbxWorkflowTypes.Items.Add(WorkflowTypes.SelectionWorkflow.DisplayName);
            this.cbxWorkflowTypes.Items.Add(WorkflowTypes.StagingWorkflow.DisplayName);
            loadedWorkflowLifetimes = workflowRepo.All().ToList();
            gridWorkflowLifetimes.ItemsSource = loadedWorkflowLifetimes.Take(10).Select(wl => new WorkflowLifetimeRowDisplay(wl));
            gridWorkflowLifetimes.SelectedIndex = 0;
        }

    }

    class WorkflowLifetimeRowDisplay
    {
        public WorkflowLifetimeRowDisplay(WorkflowLifetime lt)
        {
            WorkflowInstanceId = lt.Id;
           Type = lt.Type;
               Workflow = lt.WorkflowPath;
            Started = lt.Started;
                Ended = lt.Ended;
                TotalTimeToRun = lt.TotalTimeToRun;
        }

        public DateTime? Ended { get; set; }

        public TimeSpan? TotalTimeToRun { get; set; }

        public DateTime? Started { get; set; }

        public string Workflow { get; set; }

        public WorkflowTypes Type { get; set; }

        public Guid WorkflowInstanceId { get; set; }
    }
}
