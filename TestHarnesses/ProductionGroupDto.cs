using System.ComponentModel;

namespace TestHarnesses
{
    public class ProductionGroupDto : INotifyPropertyChanged
    {
        private string name;
        private int port;

        public ProductionGroupDto(string pgName, int port)
        {
            name = pgName;
            this.port = port;
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value == name) return;
                name = value;
                OnPropertyChanged("Name");
                PublishChange();
            }
        }

        public int Port
        {
            get { return port; }
            set
            {
                if (value == port) return;
                port = value;
                OnPropertyChanged("Port");
                PublishChange();
            }
        }

        private void PublishChange()
        {
            var pg = MainWindow.GetMockProductionGroup(true, Name, Port);

            MainWindow.eventAggregator.Publish(pg);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}