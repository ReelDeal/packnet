﻿using System;
using System.Collections.Generic;

namespace PackNet.CloudReportingService
{
    /// <summary>
    /// Used only for storing data in the logs and reporting
    /// </summary>
    //TODO; we need to update reporting
    public class BoxProductionResult
    {
        public DateTime DateTimeStamp { get; set; }
        public Guid MachineId { get; set; }
        public string MachineName { get; set; }
        /// <summary>
        /// Time in seconds that it took to produce
        /// </summary>
        public double TimeToProduce { get; set; }
        /// <summary>
        /// Did the packaging complete sucessuflly
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// Size of corrugate/fanfold used to produce this item
        /// </summary>
        public double CorrugateWidth { get; set; }
        public string CorrugateName { get; set; }
        public double CorrugateThickness { get; set; }
        public int CorrugateQuality { get; set; }
        /// <summary>
        /// The width used to create this packaging.  Use this value along with corrugate width to calculate trim
        /// </summary>
        public double CorrugateUsedWidth { get; set; }
        /// <summary>
        /// The amount of fanfold fed to create this packaging
        /// </summary>
        public double CorrugateUsedLength { get; set; }
        /// <summary>
        /// Unique Id's for all items produced
        /// </summary>
        public List<string> Producibles { get; set; }
        /// <summary>
        /// Basic value for the box.  Does not include design calculated values
        /// </summary>
        public double PackagingLength { get; set; }
        /// <summary>
        /// Basic value for the box.  Does not include design calculated values
        /// </summary>
        public double PackagingWidth { get; set; }
        /// <summary>
        /// Basic value for the box.  Does not include design calculated values
        /// </summary>
        public double PackagingHeight { get; set; }
        public int PackagingDesignId { get; set; }
        public string Username { get; set; }
    }
}