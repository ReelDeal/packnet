﻿using Newtonsoft.Json;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using System;
using System.Reactive.Concurrency;
using System.Reactive.Linq;

namespace PackNet.CloudReportingService
{
    public class ProducibleCompleteRecorder
    {
        private readonly ILogger logger;
        private readonly IEventAggregatorSubscriber subscriber;

        public ProducibleCompleteRecorder(IEventAggregatorSubscriber subscriber, ILogger logger)
        {
            this.subscriber = subscriber;
            this.logger = logger;

            Setup();
        }

        private void Setup()
        {
            subscriber.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .ObserveOn(NewThreadScheduler.Default)
                .DurableSubscribe(m =>
                {
                    var machineProductionData = m.Data.Item2;
                    var time = TimeSpan.FromMilliseconds(machineProductionData.ProductionTime);
                    
                    var pr = new BoxProductionResult
                    {
                        DateTimeStamp = DateTime.UtcNow,
                        MachineId = machineProductionData.Machine.Id,
                        MachineName = machineProductionData.Machine.Alias,
                        CorrugateName = machineProductionData.Corrugate.Alias,
                        CorrugateQuality = machineProductionData.Corrugate.Quality,
                        CorrugateThickness = machineProductionData.Corrugate.Thickness,
                        CorrugateWidth = machineProductionData.Corrugate.Width,
                        CorrugateUsedLength = machineProductionData.FeededLength,
                        CorrugateUsedWidth = machineProductionData.Corrugate.Width,
                        IsSuccess = !machineProductionData.Failure,
                        PackagingDesignId = machineProductionData.PackagingDesignId,
                        PackagingHeight = machineProductionData.PackagingHeight,
                        PackagingWidth = machineProductionData.PackagingWidth,
                        PackagingLength = machineProductionData.PackagingLength,
                        //Producibles = producedOrder.Requests.Select(r => r.SerialNumber).ToList(),
                        TimeToProduce = time.TotalSeconds,
                        Username = machineProductionData.UserName
                    };
                    logger.Log(LogLevel.Info, "BoxProductionResult {0}", JsonConvert.SerializeObject(pr));
                }, logger);
        }
    }
}
