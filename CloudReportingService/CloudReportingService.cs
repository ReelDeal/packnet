﻿using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Services;
using System.ComponentModel.Composition;

namespace PackNet.CloudReportingService
{
    [Export(typeof(IService))]
    public class CloudReportingService : IService
    {

        [ImportingConstructor]
        public CloudReportingService(IServiceLocator serviceLocator)
        {
            var logger = serviceLocator.Locate<ILogger>();
            var subscriber = serviceLocator.Locate<IEventAggregatorSubscriber>();
            var recorder = new ProducibleCompleteRecorder(subscriber, logger);
        }

        public void Dispose()
        {
            
        }

        public string Name { get { return "CloudReportingService"; } }
    }
}
