﻿using System;

namespace InstallerCustomActions.Actions
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    using Microsoft.Deployment.WindowsInstaller;

    public class WebRolesCustomAction
    {

        /// <summary>
        /// The check web server roles.
        /// </summary>
        /// <param name="session">
        /// The session.
        /// </param>
        /// <returns>
        /// The<see cref="ActionResult"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// throw new ArgumentNullException("session");
        /// </exception>
        [CustomAction]
        public static ActionResult CheckWebServerRoles(Session session)
        {
            session.Log("++++++++++++++++++++++++++Begin CheckWebServerRoles++++++++++++++++++++++++++");

            var productName = string.Empty;
            try
            {
                var tempString = GetSessionProperty(session, "CustomActionData", false);
                var parts = tempString.Split(new[] { '|' });
                productName = parts[0];

                var platformVersion = Environment.OSVersion.VersionString;
                session.Log("CheckWebServerRoles platformVersion {0}", platformVersion);

                var cmdLineParameters = string.Empty;

                // Windows Versions: http://msdn.microsoft.com/en-us/library/windows/desktop/ms724832(v=vs.85).aspx

                if (platformVersion.Contains("6.1") && !Os.IsWindowsServer())
                {
                    session.Log("Detected Windows 7");
                    // Windows 7
                    cmdLineParameters = "/Online /NoRestart /Enable-Feature /featurename:IIS-WebServerRole /featurename:IIS-CommonHttpFeatures /featurename:IIS-StaticContent /featurename:IIS-WebServerRole /featurename:IIS-ApplicationDevelopment /featurename:IIS-ISAPIFilter /featurename:IIS-ISAPIExtensions /featurename:IIS-NetFxExtensibility /featurename:IIS-ASPNET /featurename:IIS-WebServerRole /featurename:IIS-Security /featurename:IIS-RequestFiltering /featurename:IIS-WindowsAuthentication /featurename:WAS-ProcessModel /featurename:WAS-WindowsActivationService /featurename:WAS-NetFxEnvironment /featurename:WAS-ConfigurationAPI /featurename:NetFx3 /featurename:WCF-HTTP-Activation";
                }
                else if (platformVersion.Contains("6.1") && Os.IsWindowsServer())
                {
                    session.Log("Detected Server 2008 R2");
                    // Windows Server 2008 R2
                    //cmdLineParameters = "/online /norestart /enable-feature /ignorecheck /featurename:\"IIS-WebServerRole\" /featurename:\"IIS-WebServer\" /featurename:\"IIS-CommonHttpFeatures\" /featurename:\"IIS-StaticContent\" /featurename:\"IIS-DefaultDocument\" /featurename:\"IIS-DirectoryBrowsing\" /featurename:\"IIS-HttpErrors\" /featurename:\"IIS-HttpRedirect\" /featurename:\"IIS-ApplicationDevelopment\" /featurename:\"IIS-ASPNET\" /featurename:\"IIS-NetFxExtensibility\" /featurename:\"IIS-CGI\" /featurename:\"IIS-ISAPIExtensions\" /featurename:\"IIS-ISAPIFilter\" /featurename:\"IIS-HealthAndDiagnostics\" /featurename:\"IIS-HttpLogging\" /featurename:\"IIS-LoggingLibraries\" /featurename:\"IIS-RequestMonitor\" /featurename:\"IIS-HttpTracing\" /featurename:\"IIS-Security\" /featurename:\"IIS-BasicAuthentication\" /featurename:\"IIS-WindowsAuthentication\" /featurename:\"IIS-URLAuthorization\" /featurename:\"IIS-RequestFiltering\" /featurename:\"IIS-IPSecurity\" /featurename:\"IIS-Performance\" /featurename:\"IIS-HttpCompressionStatic\" /featurename:\"IIS-HttpCompressionDynamic\" /featurename:\"IIS-WebServerManagementTools\" /featurename:\"IIS-ManagementConsole\" /featurename:\"IIS-ManagementScriptingTools\" /featurename:\"IIS-ManagementService\" /featurename:\"IIS-IIS6ManagementCompatibility\" /featurename:\"IIS-Metabase\" /featurename:\"IIS-WMICompatibility\" /featurename:\"IIS-LegacyScripts\" /featurename:\"IIS-LegacySnapIn\" /featurename:\"IIS-FTPServer\" /featurename:\"IIS-FTPSvc\" /featurename:\"IIS-FTPExtensibility\" /featurename:\"WAS-WindowsActivationService\" /featurename:\"WAS-ProcessModel\" /featurename:\"WAS-NetFxEnvironment\" /featurename:\"WAS-ConfigurationAPI\" /featurename:\"IIS-ManagementService\"";
                    cmdLineParameters = "/online /norestart /enable-feature /ignorecheck /featurename:IIS-WebServerRole /featurename:IIS-WebServer /featurename:IIS-CommonHttpFeatures /featurename:IIS-StaticContent /featurename:IIS-DefaultDocument /featurename:IIS-DirectoryBrowsing /featurename:IIS-HttpErrors /featurename:IIS-HttpRedirect /featurename:IIS-ApplicationDevelopment /featurename:IIS-ASPNET /featurename:IIS-NetFxExtensibility /featurename:IIS-CGI /featurename:IIS-ISAPIExtensions /featurename:IIS-ISAPIFilter /featurename:IIS-HealthAndDiagnostics /featurename:IIS-HttpLogging /featurename:IIS-LoggingLibraries /featurename:IIS-RequestMonitor /featurename:IIS-HttpTracing /featurename:IIS-Security /featurename:IIS-BasicAuthentication /featurename:IIS-WindowsAuthentication /featurename:IIS-URLAuthorization /featurename:IIS-RequestFiltering /featurename:IIS-IPSecurity /featurename:IIS-Performance /featurename:IIS-HttpCompressionStatic /featurename:IIS-HttpCompressionDynamic /featurename:IIS-WebServerManagementTools /featurename:IIS-ManagementConsole /featurename:IIS-ManagementScriptingTools /featurename:IIS-ManagementService /featurename:IIS-IIS6ManagementCompatibility /featurename:IIS-Metabase /featurename:IIS-WMICompatibility /featurename:IIS-LegacyScripts /featurename:IIS-LegacySnapIn /featurename:IIS-FTPServer /featurename:IIS-FTPSvc /featurename:IIS-FTPExtensibility /featurename:WAS-WindowsActivationService /featurename:WAS-ProcessModel /featurename:WAS-NetFxEnvironment /featurename:WAS-ConfigurationAPI /featurename:IIS-ManagementService";
                }
                else if (platformVersion.Contains("6.2") && !Os.IsWindowsServer() || platformVersion.Contains("6.3") && !Os.IsWindowsServer())
                {
                    session.Log("Detected Windows 8");
                    // Windows 8
                    cmdLineParameters = "/Online /NoRestart /Enable-Feature /featurename:IIS-WebServerRole /featurename:IIS-CommonHttpFeatures /featurename:IIS-StaticContent /featurename:IIS-ApplicationDevelopment /featurename:IIS-ISAPIFilter /featurename:IIS-ISAPIExtensions /featurename:IIS-NetFxExtensibility45 /featurename:IIS-ASPNET45 /featurename:IIS-Security /featurename:IIS-RequestFiltering /featurename:IIS-WindowsAuthentication /featurename:NetFx4-AdvSrvs /featurename:NetFx4Extended-ASPNET45 /featurename:WCF-Services45 /featurename:WCF-HTTP-Activation45 /featurename:IIS-WebSockets /All";
                }
                else if (platformVersion.Contains("6.2") && Os.IsWindowsServer())
                {
                    session.Log("Detected Server 2012");
                    // Windows Server 2012
                    cmdLineParameters = "/Online /NoRestart /Enable-Feature /featurename:IIS-WebServerRole /featurename:IIS-CommonHttpFeatures /featurename:IIS-StaticContent /featurename:IIS-ApplicationDevelopment /featurename:IIS-ISAPIFilter /featurename:IIS-ISAPIExtensions /featurename:IIS-NetFxExtensibility45 /featurename:IIS-ASPNET45  /featurename:IIS-Security /featurename:IIS-RequestFiltering /featurename:IIS-WindowsAuthentication /featurename:NetFx4 /featurename:NetFx4Extended-ASPNET45 /featurename:WCF-Services45 /featurename:WCF-HTTP-Activation45 /featurename:IIS-WebSockets /All";
                }
                else if (platformVersion.Contains("6.3") && Os.IsWindowsServer())
                {
                    session.Log("Detected Server 2012 R2");
                    // Windows Server 2012 R2
                    cmdLineParameters = "/Online /NoRestart /Enable-Feature /featurename:IIS-WebServerRole /featurename:IIS-CommonHttpFeatures /featurename:IIS-StaticContent /featurename:IIS-ApplicationDevelopment /featurename:IIS-ISAPIFilter /featurename:IIS-ISAPIExtensions /featurename:IIS-NetFxExtensibility45 /featurename:IIS-ASPNET45  /featurename:IIS-Security /featurename:IIS-RequestFiltering /featurename:IIS-WindowsAuthentication /featurename:NetFx4 /featurename:NetFx4Extended-ASPNET45 /featurename:WCF-Services45 /featurename:WCF-HTTP-Activation45 /featurename:IIS-WebSockets /All";
                }
                else if (platformVersion.Contains("Microsoft Windows NT 10") && !Os.IsWindowsServer()) //Microsoft Windows NT 6.3.9600.0
                {
                    session.Log("Detected Windows 10");
                    // Windows 10
                    cmdLineParameters = "/Online /NoRestart /Enable-Feature /featurename:IIS-WebServerRole /featurename:IIS-CommonHttpFeatures /featurename:IIS-StaticContent /featurename:IIS-ApplicationDevelopment /featurename:IIS-ISAPIFilter /featurename:IIS-ISAPIExtensions /featurename:IIS-NetFxExtensibility45 /featurename:IIS-ASPNET45  /featurename:IIS-Security /featurename:IIS-RequestFiltering /featurename:IIS-WindowsAuthentication /featurename:NetFx4 /featurename:NetFx4Extended-ASPNET45 /featurename:WCF-Services45 /featurename:WCF-HTTP-Activation45 /featurename:IIS-WebSockets /All";
                }
                else
                {
                    // Other Operating Systems
                    throw new Exception("IIS install is not supported on this version of Windows.");
                }

                var systemDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), "System32");
                if (Environment.Is64BitOperatingSystem)
                {
                    systemDirectory = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Windows), "Sysnative");
                }
                // Windows 7 / 8 / 2008R2 / 2012
                var cmdLineExe = Path.Combine(systemDirectory, "dism.exe");
                session.Log("CheckWebServerRoles command {0} {1}", cmdLineExe, cmdLineParameters);

                // Install Web Server features if missing
                var runDismInfo = new ProcessStartInfo
                {
                    UseShellExecute = true,
                    //RedirectStandardError = true,
                    //RedirectStandardOutput = true,
                    Arguments = cmdLineParameters,
                    FileName = cmdLineExe,
                    Verb = "runas",
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = false

                };

                // Run the external process & wait for it to finish
                using (var runDismProc = Process.Start(runDismInfo))
                {
                    runDismProc.WaitForExit();
                }


            }
            catch (Exception ex)
            {
                session.Log("CheckWebServerRoles failed: {0}", ex);

                session.Message(
                    InstallMessage.User + (int)MessageBoxIcon.Error + (int)MessageBoxButtons.OK,
                    new Record { FormatString = productName + " failed to install specific Web Server features\nException: " + ex.Message });

                session.Log("++++++++++++++++++++++++++End CheckWebServerRoles++++++++++++++++++++++++++");
                return ActionResult.Failure;
            }
            session.Log("CheckWebServerRoles Succeeded");
            session.Log("++++++++++++++++++++++++++End CheckWebServerRoles++++++++++++++++++++++++++");
            return ActionResult.Success;
        }

        private static string GetSessionProperty(Session session, string propertyName, bool isCustomActionData)
        {
            string sessionProperty = string.Empty;

            try
            {
                if (session == null)
                {
                    throw new ArgumentNullException("session");
                }

                sessionProperty = isCustomActionData ? session.CustomActionData[propertyName] : session[propertyName];
            }
            catch (Exception ex)
            {
                session.Log("Exception when executing GetSessionProperty. {0}", ex);
            }

            return sessionProperty;
        }

        /// <summary>
        /// The OS.
        /// </summary>
        private class Os
        {
            /// <summary>
            /// The OS ANYSERVER.
            /// </summary>
            [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1310:FieldNamesMustNotContainUnderscore", Justification = "Reviewed. Suppression is OK here.")]
            private const int OS_ANYSERVER = 29;

            /// <summary>
            /// Prevents a default instance of the <see cref="Os"/> class from being created. 
            /// </summary>
            private Os()
            {
            }

            /// <summary>
            /// The is windows server.
            /// </summary>
            /// <returns>
            /// The <see cref="bool"/>.
            /// </returns>
            public static bool IsWindowsServer()
            {
                return IsOS(OS_ANYSERVER);
            }

            [DllImport("shlwapi.dll", SetLastError = true, EntryPoint = "#437")]
            private static extern bool IsOS(int os);
        }
    }
}

