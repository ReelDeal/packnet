﻿using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Communication;
using Testing.Specificity;

namespace PackNet.CommunicationTests.WebRequestCreator
{
    using WebRequestCreator = Communication.WebRequestCreator.WebRequestCreator;

    [TestClass]
    public class WebRequestFactoryTests
    {
        [TestMethod]
        public void ShouldCreateExpectedGetRequest()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("Main", "TestVariable");
            var res = webRequestCreator.CreateGetRequest(variable);
            Specify.That(res.Method).Should.BeEqualTo("GET");
            Specify.That(res.ContentType).Should.BeNull();
            Specify.That(res.RequestUri.AbsoluteUri).Should.BeEqualTo("http://127.0.0.1/GetVariable?Main:TestVariable");
        }

        [TestMethod]
        public void ShouldCreateExpectedUriForGetOfVariableWithTask()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("Main", "TestVariable");
            var res = webRequestCreator.CreateGetRequest(variable);
            Specify.That(res.Method).Should.BeEqualTo("GET");
            Specify.That(res.ContentType).Should.BeNull();
            Specify.That(res.RequestUri.AbsoluteUri).Should.BeEqualTo("http://127.0.0.1/GetVariable?Main:TestVariable");
        }

        [TestMethod]
        public void ShouldCreateExpectedUriForGetOfVariableWithoutTask()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable(string.Empty, "TestVariable");
            var res = webRequestCreator.CreateGetRequest(variable);
            Specify.That(res.Method).Should.BeEqualTo("GET");
            Specify.That(res.ContentType).Should.BeNull();
            Specify.That(res.RequestUri.AbsoluteUri).Should.BeEqualTo("http://127.0.0.1/GetVariable?TestVariable");
        }

        [TestMethod]
        public void ShouldCreateExpectedPostRequest()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("Main", "TestVariable");
            var res = webRequestCreator.CreatePostRequest(variable, string.Empty);
            Specify.That(res.Method).Should.BeEqualTo("POST");
            Specify.That(res.ContentType).Should.BeEqualTo("application/json");
            Specify.That(res.RequestUri.AbsoluteUri).Should.BeEqualTo("http://127.0.0.1/PostVariable");
        }

        [TestMethod]
        public void ShouldCreateExpectedPostRequestForGetOfVariableWithoutTask()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable(string.Empty, "TestVariable");
            webRequestCreator.CreatePostRequest(variable, string.Empty);

            var s = webRequestCreator.LastWrittenValue;
            Specify.That(s).Should.BeEqualTo("{\"TestVariable\":\"\"}");
        }

        [TestMethod]
        public void ShouldCreateExpectedPostRequestForGetOfVariableWithTask()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("Main", "TestVariable");
            webRequestCreator.CreatePostRequest(variable, string.Empty);

            var s = webRequestCreator.LastWrittenValue;
            Specify.That(s).Should.BeEqualTo("{\"Main:TestVariable\":\"\"}");
        }

        [TestMethod]
        public void ShouldCreateExpectedPostRequestForGetOfDoubleVariable()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("Main", "TestVariable");
            webRequestCreator.CreatePostRequest(variable, 12.1);

            var s = webRequestCreator.LastWrittenValue;
            Specify.That(s).Should.BeEqualTo("{\"Main:TestVariable\":12.1}");
        }

        [TestMethod]
        public void ShouldCreateExpectedPostRequestForGetOfIntVariable()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("Main", "TestVariable");
            webRequestCreator.CreatePostRequest(variable, 12);

            var s = webRequestCreator.LastWrittenValue;
            Specify.That(s).Should.BeEqualTo("{\"Main:TestVariable\":12}");
        }

        [TestMethod]
        public void ShouldCreateExpectedPostRequestForGetOfBoolVariable()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("Main", "TestVariable");
            webRequestCreator.CreatePostRequest(variable, true);

            var s = webRequestCreator.LastWrittenValue;
            Specify.That(s).Should.BeEqualTo("{\"Main:TestVariable\":1}");
        }

        [TestMethod]
        public void ShouldCreateExpectedPostRequestForGetOfMicroMeterVariable()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("Main", "TestVariable");
            webRequestCreator.CreatePostRequest(variable, new MicroMeter(12000));

            var s = webRequestCreator.LastWrittenValue;
            Specify.That(s).Should.BeEqualTo("{\"Main:TestVariable\":12.0}");
        }

        [TestMethod]
        public void ShouldCreateExpectedPostRequestForGetOfArrayOfMicroMeterVariable()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("Main", "TestVariable");
            webRequestCreator.CreatePostRequest(variable, new[] { new MicroMeter(12000), new MicroMeter(1100), new MicroMeter(163100) });

            var s = webRequestCreator.LastWrittenValue;
            Specify.That(s).Should.BeEqualTo("{\"Main:TestVariable\":[12.0,1.1,163.1]}");
        }

        [TestMethod]
        public void ShouldCreateExpectedPostRequestForRegisterNotificationVariables()
        {
            var webRequestCreator = new WebRequestCreator("127.0.0.1", 80);
            var variable = new PacksizePlcVariable("HttpClientCyclic", "VariablesToNotifyChanges");
            List<NotificationVariable> notificationVariables = new List<NotificationVariable>();
            notificationVariables.Add(new NotificationVariable("test1"));
            notificationVariables.Add(new NotificationVariable("test2"));
            webRequestCreator.CreatePostRequest(variable, notificationVariables);

            var s = webRequestCreator.LastWrittenValue;
            string expectedValue = "{\"HttpClientCyclic:VariablesToNotifyChanges\":[{\"Name\":\"test1\",\"Value\":null},{\"Name\":\"test2\",\"Value\":null}]}";
            Specify.That(s==expectedValue).Should.BeTrue("Strings did not match");
        }
    }
}