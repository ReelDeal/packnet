﻿using System.IO;
using System.Net;

using Moq;

namespace PackNet.CommunicationTests
{
    public class PlcCommunicatorHelpers
    {
        protected static Mock<HttpWebRequest> GetFailedToConnectRequest(string errorMessage)
        {
            var mockWebRequest = new Mock<HttpWebRequest>();
            mockWebRequest.Setup(m => m.GetResponse())
                .Throws(new WebException(errorMessage));
            return mockWebRequest;
        }

        protected static Mock<HttpWebRequest> GetMockRequest(string textToReturn)
        {
            var mockWebRequest = new Mock<HttpWebRequest>();
            mockWebRequest.Setup(m => m.GetResponse())
                .Returns(() => GetMockResponse(textToReturn).Object);
            return mockWebRequest;
        }

        protected static Mock<HttpWebResponse> GetMockResponse(string textToReturn)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(textToReturn);
            writer.Flush();
            stream.Position = 0;
            var mockResponse = new Mock<HttpWebResponse>();
            mockResponse.Setup(m => m.GetResponseStream()).Returns(stream);
            return mockResponse;
        }
    }
}
