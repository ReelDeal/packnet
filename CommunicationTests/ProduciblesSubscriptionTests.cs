﻿//TODO: Any value in these tests?

//namespace PackNet.CommunicationTests
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Reactive.Linq;
//    using System.Text;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using Newtonsoft.Json;

//    using Common.Eventing;
//    using Common.Interfaces.DTO;
//    using Common.Interfaces.Enums;
//    using Common.Interfaces.Eventing;
//    using Common.Logging;
//    using Common.Utils;
//    using Communication.RabbitMQ.MessageHangling;

//    using RabbitMQ.Client;
//    using RabbitMQ.Client.Events;

//    using Testing.Specificity;

//    // TODO: Probably should delete this class when removal of Article clojure code complete
//    [TestClass]
//    public class ProduciblesSubscriptionTests
//    {

//        private EventAggregator eventAggregator;
//        private TestableRabbitSubscriber rabbitSubscriber;

//        [TestInitialize]
//        public void TestInit()
//        {
//            eventAggregator = new EventAggregator();
//        }

//        [TestCleanup]
//        public void TestCleanup()
//        {
//            if (rabbitSubscriber != null)
//                rabbitSubscriber.Dispose();

//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void WhenMessageReceivedForGetProduciblesItGetsSentOnTheEventAggregator()
//        {
//            var called = false;
//            rabbitSubscriber = new TestableRabbitSubscriber("localhost", "mm", "mm.replys", new Mock<ILogger>(MockBehavior.Loose).Object, eventAggregator);

//            const string replyQ = "yo yo yo";
//            var filter = new Dictionary<string, string> { { "Type", "SerialNumber" }, { "SearchText", "search" } };
//            var data = new Dictionary<string, object> { { "Type", "someDataSource" }, { "Filter", filter }, { "UserName", "jon" }, { "Chunk", 0 }, { "ChunkSize", 25 }, { "ReplyQueue", replyQ } };
//            var fact = new Dictionary<string, object> { { "Type", "GetProducibles" }, { "Data", data } };
//            var message = new Dictionary<string, object> { { "Fact", fact } };

//            IMessage<DataRequestMessage> received = null;

//            eventAggregator.GetEvent<IMessage<DataRequestMessage>>()
//                .Where(s => s.MessageType == Enums.MessageTypes.GetProducibles && s.Data.DataSource == "someDataSource")
//                 .Subscribe(
//                    m =>
//                    {
//                        received = m;
//                        called = true;
//                    });

//            rabbitSubscriber.OnMessageReceived(null, new BasicDeliverEventArgs { Body = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(message)) });

//            Retry.For(() => received != null, TimeSpan.FromSeconds(2));

//            Specify.That(received).Should.Not.BeNull();
//            Specify.That(received.Data.Chunk).Should.BeLogicallyEqualTo(0);
//            Specify.That(received.Data.ChunkSize).Should.BeLogicallyEqualTo(25);
//            Specify.That(called).Should.BeTrue();
//        }
//    }

//    public class TestableRabbitSubscriber : RabbitSubscriber
//    {
//        public TestableRabbitSubscriber(string hostName, string exchangeName, string routingKey, ILogger logger, IEventAggregatorPublisher eventPublisher)
//            : base(hostName, exchangeName, routingKey, logger, eventPublisher)
//        {
//        }

//        internal new void OnMessageReceived(IBasicConsumer sender, BasicDeliverEventArgs args)
//        {
//            base.OnMessageReceived(sender, args);
//        }
//    }
//}
