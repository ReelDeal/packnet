﻿using PackNet.Business.WmsIntegration;

namespace PackNet.CommunicationTests.WmsIntegration
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Common.Interfaces.ExtensionMethods;

    [TestClass]
    public class ServiceCommunicatorSettingsTests
    {
        private const string ServiceCommunicatorSettingsExample =
            "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
            "<ServiceCommunicatorSettings xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://Packsize.MachineManager.com\">" +
            "<Url>http://localhost:8080/EventNotifcationService.asmx</Url></ServiceCommunicatorSettings>";

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateServiceCommunicationSettingsObject()
        {
            var settings = new ServiceCommunicatorSettings();
            Assert.AreEqual("http://localhost:80/EventNotifcationService.asmx", settings.Url);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateServiceCommunicationSettingsObjectFromFileExample()
        {
            using (var stream = ServiceCommunicatorSettingsExample.ToStream())
            {
                var settings = ServiceCommunicatorSettings.DeserializeFromStream(stream, Type.EmptyTypes);
                Assert.IsNotNull(settings);
                Assert.AreEqual("http://localhost:8080/EventNotifcationService.asmx", settings.Url);    
            }
        }
    }
}
