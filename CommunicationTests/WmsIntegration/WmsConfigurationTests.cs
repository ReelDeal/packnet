﻿using PackNet.Data.WMS;

namespace PackNet.CommunicationTests.WmsIntegration
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Common.Interfaces.ExtensionMethods;
    using Testing.Specificity;

    [TestClass]
    public class WmsConfigurationTests
    {
        private const string WmsConfigurationExample =
            "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "<WmsConfiguration xmlns:i=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://Packsize.MachineManager.com\">" +
            "<Enabled>true</Enabled>" +
            "<ExternalPrint>true</ExternalPrint>" +
            "<PrinterAlertPort>9106</PrinterAlertPort>" +
            "<PrinterPollingTimeout>600</PrinterPollingTimeout>" +
            "<PrinterPollingTimes>6</PrinterPollingTimes>" +
            "<ReportProduced>false</ReportProduced>" +
            "<SequenceNumberLowerBound>100</SequenceNumberLowerBound>" +
            "<SequenceNumberUpperBound>999</SequenceNumberUpperBound>" +
            "<UseSocketCommunication>true</UseSocketCommunication>" +
            "<Version>2.1</Version>" +
            "</WmsConfiguration>";
        
        [TestMethod]
        [TestCategory("Unit")]
        public void ConstructorTest()
        {
            var config = new WmsConfiguration();

            Specify.That(config.Enabled).Should.BeFalse();
            Specify.That(config.ExternalPrint).Should.BeFalse();
            Specify.That(config.PrinterAlertPort).Should.BeEqualTo(9101);
            Specify.That(config.PrinterPollingTimeout).Should.BeEqualTo(500);
            Specify.That(config.PrinterPollingTimes).Should.BeEqualTo(3);
            Specify.That(config.ReportProduced).Should.BeTrue();
            Specify.That(config.SequenceNumberLowerBound).Should.BeEqualTo(1);
            Specify.That(config.SequenceNumberUpperBound).Should.BeEqualTo(99999);
            Specify.That(config.UseSocketCommunication).Should.BeFalse();
            Specify.That(config.Version).Should.BeEqualTo("1.2");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldDeserializeFromFile()
        {
            using (var stream = WmsConfigurationExample.ToStream())
            {
                var config = WmsConfiguration.DeserializeFromStream(stream,Type.EmptyTypes);

                Specify.That(config.Enabled).Should.BeTrue();
                Specify.That(config.ExternalPrint).Should.BeTrue();
                Specify.That(config.PrinterAlertPort).Should.BeEqualTo(9106);
                Specify.That(config.PrinterPollingTimes).Should.BeEqualTo(6);
                Specify.That(config.PrinterPollingTimeout).Should.BeEqualTo(600);
                Specify.That(config.ReportProduced).Should.BeFalse();
                Specify.That(config.SequenceNumberLowerBound).Should.BeEqualTo(100);
                Specify.That(config.SequenceNumberUpperBound).Should.BeEqualTo(999);
                Specify.That(config.UseSocketCommunication).Should.BeTrue();
            }
        }
    }
}