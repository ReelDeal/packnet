﻿using PackNet.Business.WmsIntegration;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.CommunicationTests.WmsIntegration
{
    using System.Linq;
    using System.Xml;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Common.WebServiceConsumer;

    [TestClass]
    public class ServiceCommunicatorTests
    {
        private Mock<ILogger> logger;
        private Mock<IServiceCommunicatorSettings> settings;
        private Mock<IEventNotificationConsumer> consumer;
        private ServiceCommunicator communicator;
        
        [TestInitialize]
        public void TestInitialize()
        {
            logger = new Mock<ILogger>();
            settings = new Mock<IServiceCommunicatorSettings>();
            consumer = new Mock<IEventNotificationConsumer>();            
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTrueWhenMessageIsReturnedFromWebService()
        {
            const string Message = "blah";
            SetupResponseMock(Message);
            SetupSettingsMock();
            communicator = new ServiceCommunicator(settings.Object, logger.Object);

            Assert.IsTrue(communicator.Connect());
            Assert.IsTrue(communicator.Send(Message));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnFalseWhenMessageReturnedFromWebServiceDiffersFromMessageSent()
        {
            const string Message = "blah";
            const string DifferentMessage = "not_blah";
            SetupResponseMock(DifferentMessage);
            SetupSettingsMock();
            communicator = new ServiceCommunicator(settings.Object, logger.Object);
            
            Assert.IsTrue(communicator.Connect());
            Assert.IsFalse(communicator.Send(Message));            
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRaiseEventIfConsumerIsNotSetWhenConnectIsCalled()
        {
            settings = new Mock<IServiceCommunicatorSettings>();
            communicator = new ServiceCommunicator(settings.Object, logger.Object);

            var eventRaised = false;
            communicator.ConnectionFailed += (o, s) => eventRaised = true;

            Assert.IsFalse(communicator.Connect());
            Assert.IsTrue(eventRaised);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnFalseWhenSendIsCalledIfCommunicatorIsNotConnected()
        {
            communicator = new ServiceCommunicator(settings.Object, logger.Object);
            Assert.IsFalse(communicator.Send("blah"));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldLogErrorIfWebServiceReturnsInvalidResponse()
        {
            SetupInvalidResponseMock();
            SetupSettingsMock();
            communicator = new ServiceCommunicator(settings.Object, logger.Object);
            Assert.IsTrue(communicator.Connect());
            Assert.IsFalse(communicator.Send("blah"));
            logger.Verify(l => l.Log(LogLevel.Debug, It.IsRegex("^Exception in ServiceCommunicator.")));
        }

        private void SetupResponseMock(string message)
        {
            XmlNode[] resp = CreateResponse(message);
            consumer.Setup(c => c.SendEventNotification(It.IsAny<XmlNode[]>())).Returns(resp);
        }

        private void SetupInvalidResponseMock()
        {
            XmlNode[] resp = null;
            consumer.Setup(c => c.SendEventNotification(It.IsAny<XmlNode[]>())).Returns(resp);
        }

        private void SetupSettingsMock()
        {
            settings.Setup(s => s.EventNotificationConsumer).Returns(consumer.Object);
        }

        private XmlNode[] CreateRequest(string message)
        {
            var doc = new XmlDocument();
            doc.LoadXml(string.Format("<SendEventNotification><message>{0}</message></SendEventNotification>", message));
            return CreateNodeList(doc);
        }

        private XmlNode[] CreateResponse(string message)
        {
            var doc = new XmlDocument();
            doc.LoadXml(string.Format("<SendEventNotificationResponse><result>{0}</result></SendEventNotificationResponse>", message));
            return CreateNodeList(doc);
        }

        private XmlNode[] CreateNodeList(XmlDocument doc)
        {
            return doc.SelectNodes("/").Cast<XmlNode>().ToArray();
        }
    }
}
