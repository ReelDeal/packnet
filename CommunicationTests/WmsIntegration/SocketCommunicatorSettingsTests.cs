﻿using PackNet.Business.WmsIntegration;

namespace PackNet.CommunicationTests.WmsIntegration
{
    using System.Net.Sockets;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SocketCommunicatorSettingsTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateSocketCommunicatorSettingsObject()
        {
            SocketCommunicatorSettings settings = new SocketCommunicatorSettings();
            Assert.AreEqual(9090, settings.Port);
            Assert.AreEqual(10000, settings.ReceiveTimeout);
            Assert.AreEqual("localhost", settings.Host);
            Assert.AreEqual(3, settings.NumberOfRetries);
            Assert.AreEqual(5000, settings.KeepAliveInterval);
            Assert.AreEqual("UTF-8", settings.EncodingName);
            Assert.AreEqual(ProtocolType.Tcp, settings.ProtocolType);
            Assert.AreEqual(SocketType.Unknown, settings.SocketType);
            Assert.AreEqual(AddressFamily.InterNetwork, settings.AddressFamily);
        }
    }
}
