﻿using PackNet.Business.WmsIntegration;
using PackNet.Business.WmsIntegration.MessageProducer;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.Logging;

namespace PackNet.CommunicationTests.WmsIntegration
{
    using System;
    using System.Net.Sockets;
    using System.Text;
    using System.Threading;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    [TestClass]
    public class SocketCommunicatorTests
    {
        private Mock<ISocketCommunicatorSettings> communicatorSettings;
        private Mock<IWmsMessageProducer> messageProducer;
        private Mock<IAnswerResolver> answerResolver;
        private Mock<ILogger> logger;
        private Mock<ISocketWrapper> socket;
        private WmsSocketCommunicator communicator;
        private byte[] ackMessage;
        private byte[] nakMessage;

        [TestInitialize]
        public void TestInitialize()
        {
            communicatorSettings = new Mock<ISocketCommunicatorSettings>();
            messageProducer = new Mock<IWmsMessageProducer>();
            answerResolver = new Mock<IAnswerResolver>();
            logger = new Mock<ILogger>();
            socket = new Mock<ISocketWrapper>(MockBehavior.Loose);

            const string AckString = "2" + "1ACK" + "3";
            const string NakString = "2" + "1NAK" + "3";
            ackMessage = Encoding.UTF8.GetBytes(AckString);
            nakMessage = Encoding.UTF8.GetBytes(NakString);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendMessage()
        {
            SetupCommunicatorSettingsMock();
            answerResolver.Setup(a => a.IsAnswerOk(It.IsAny<string>())).Returns(true);
            communicator = new WmsSocketCommunicator(communicatorSettings.Object, socket.Object, messageProducer.Object, answerResolver.Object, logger.Object);
            communicator.Send("blah");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRetryConfiguredNumberOfTimes()
        {
            SetupCommunicatorSettingsMock();
            socket.Setup(s => s.Connect(It.IsAny<string>(), It.IsAny<int>())).Throws(new SocketException());
            communicator = new WmsSocketCommunicator(communicatorSettings.Object, socket.Object, messageProducer.Object, answerResolver.Object, logger.Object);
            bool eventFired = false;
            int eventFiredCount = 0;

            communicator.ConnectionFailed += (o, s) => { eventFired = true; eventFiredCount++; };
            communicator.Connect();

            Assert.IsTrue(eventFired);
            Assert.AreEqual(1, eventFiredCount);
            socket.Verify(c => c.Connect(It.IsAny<string>(), It.IsAny<int>()), Times.Exactly(3));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSendKeepAliveMessageEveryFifthSecond()
        {
            SetupCommunicatorSettingsMock();
            messageProducer.Setup(m => m.GetKeepAliveMessage()).Returns("KPA");
            socket.Setup(s => s.Receive(It.IsAny<byte[]>())).Returns(1).Callback<byte[]>(b => ByteSwap(b, ackMessage));
            answerResolver.Setup(a => a.IsAnswerOk(It.IsAny<string>())).Returns(true);
            communicator = new WmsSocketCommunicator(communicatorSettings.Object, socket.Object, messageProducer.Object, answerResolver.Object, logger.Object);
            communicator.Connect();
            Thread.Sleep(11000);
            socket.Verify(s => s.Connect(It.IsAny<string>(), It.IsAny<int>()), Times.Exactly(1));
            socket.Verify(s => s.Send(It.IsAny<byte[]>()), Times.Exactly(2));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRaiseEventWhenKeepAliveFails()
        {
            SetupCommunicatorSettingsMock();
            messageProducer.Setup(m => m.GetKeepAliveMessage()).Returns("KPA");
            socket.Setup(s => s.Connect(It.IsAny<string>(), It.IsAny<int>()));
            socket.Setup(s => s.Send(It.IsAny<byte[]>())).Throws<SocketException>();
            communicator = new WmsSocketCommunicator(communicatorSettings.Object, socket.Object, messageProducer.Object, answerResolver.Object, logger.Object);

            var eventRaised = false;
            communicator.ConnectionFailed += (o, s) => eventRaised = true;

            Assert.IsTrue(communicator.Connect());
            Thread.Sleep(6000);
            Assert.IsTrue(eventRaised);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRaiseEventWhenGetResponseFails()
        {
            SetupCommunicatorSettingsMock();
            messageProducer.Setup(m => m.GetKeepAliveMessage()).Returns("KPA");
            socket.Setup(s => s.Connect(It.IsAny<string>(), It.IsAny<int>()));
            socket.Setup(s => s.Receive(It.IsAny<byte[]>())).Throws<SocketException>();
            communicator = new WmsSocketCommunicator(communicatorSettings.Object, socket.Object, messageProducer.Object, answerResolver.Object, logger.Object);

            var eventRaised = false;
            communicator.ConnectionFailed += (o, s) => eventRaised = true;

            Assert.IsTrue(communicator.Connect());
            Thread.Sleep(6000);
            Assert.IsTrue(eventRaised);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRaiseEventWhenKeepAliveReceivesNack()
        {
            SetupCommunicatorSettingsMock();
            messageProducer.Setup(m => m.GetKeepAliveMessage()).Returns(Encoding.UTF8.GetString(nakMessage));
            answerResolver.Setup(a => a.IsAnswerOk(It.IsAny<string>())).Returns(false);
            socket.Setup(s => s.Connect(It.IsAny<string>(), It.IsAny<int>()));
            socket.Setup(s => s.Send(It.IsAny<byte[]>())).Returns(1).Callback<byte[]>(b => ByteSwap(b, nakMessage));

            communicator = new WmsSocketCommunicator(communicatorSettings.Object, socket.Object, messageProducer.Object, answerResolver.Object, logger.Object);

            bool eventRaised = false;
            communicator.ConnectionFailed += (o, s) => eventRaised = true;
            Assert.IsTrue(communicator.Connect());
            Thread.Sleep(6000);
            Assert.IsTrue(eventRaised);
        }
       
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldDisposeSocketWhenDisposing()
        {
            SetupCommunicatorSettingsMock();
            communicator = new WmsSocketCommunicator(communicatorSettings.Object, socket.Object, messageProducer.Object, answerResolver.Object, logger.Object);
            communicator.Dispose();
            socket.Verify(s => s.Dispose());
        }

        private void ByteSwap(byte[] to, byte[] from)
        {
            for (int i = 0; i < from.Length; i++)
            {
                to[i] = from[i];
            }
        }

        private bool IsKpaMessage(int messageNo, byte[] bytes)
        {
            string correct = Convert.ToChar(2) + "0000" + messageNo + "KPA" + Convert.ToChar(3);
            string message = Encoding.UTF8.GetString(bytes);
            return correct == message;
        }

        private void SetupCommunicatorSettingsMock()
        {
            communicatorSettings = new Mock<ISocketCommunicatorSettings>();
            communicatorSettings.Setup(s => s.EncodingName).Returns("UTF-8");
            communicatorSettings.Setup(s => s.Host).Returns("blah");
            communicatorSettings.Setup(s => s.Port).Returns(8888);
            communicatorSettings.Setup(s => s.ReceiveTimeout).Returns(10);
            communicatorSettings.Setup(s => s.NumberOfRetries).Returns(3);
            communicatorSettings.Setup(s => s.KeepAliveInterval).Returns(5000);
            socket.Setup(s => s.Receive(It.IsAny<byte[]>()));
        }
    }
}
