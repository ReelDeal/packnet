﻿using PackNet.Business.WmsIntegration.MessageProducer;

namespace PackNet.CommunicationTests.WmsIntegration.MessageProducer
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Testing.Specificity;

    [TestClass]
    public class AnswerResolverTests
    {
        private IAnswerResolver resolver;

        [TestInitialize]
        public void TestInitialize()
        {
            IWmsMessageProducerConfiguration config = new MessageProducerConfigurationMock();
            resolver = new AnswerResolver(config);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnTrueWhenAnswerIsACK()
        {
            var answer = (char)2 + "00001ACK" + (char)3;
            var isOk = resolver.IsAnswerOk(answer);

            Specify.That(isOk).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReturnFalseWhenAnswerIsNAK()
        {
            var answer = (char)2 + "00001NAK" + (char)3;
            var isOk = resolver.IsAnswerOk(answer);

            Specify.That(isOk).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowExceptionIfAnswerIsInvalid()
        {
            const string answer = "mk00001ACK3";
            var reply = string.Empty;

            try
            {
                resolver.IsAnswerOk(answer);
            }
            catch (InvalidAnswerException ex)
            {
                reply = ex.Message;
            }

            Specify.That(reply).Should.BeLogicallyEqualTo("mk00001ACK3");
        }
    }
}
