﻿using PackNet.Business.WmsIntegration.MessageProducer;

namespace PackNet.CommunicationTests.WmsIntegration.MessageProducer
{
    using System.Collections.Generic;

    public class MessageProducerConfigurationMock : MessageProducerConfiguration
    {
        private const string ACK = "^[0-9][0-9][0-9][0-9][0-9]ACK";
        private const string NAK = "^[0-9][0-9][0-9][0-9][0-9]NAK";

        private const string CartonProducedMessage = "[SequenceNumber]RCP;[SerialNumber];[Length];[Width];[Height];[NextLabelRequestNumber]";
        private const string LabelPrintMessage = "[SequenceNumber]RLP;[SerialNumber];[Length];[Width];[Height];[PrinterId];[NextLabelRequestNumber]";
        private const string KeepAliveMessage = "[SequenceNumber]KPA";

        public MessageProducerConfigurationMock() 
        {
            PopulateAnswers();
            PopulateMessages();
        }

        private void PopulateAnswers()
        {
            Answers = new Dictionary<string, string>
            {
                { "ACK", ACK }, 
                { "NAK", NAK }
            };
        }

        private void PopulateMessages()
        {
            Messages = new Dictionary<string, string>
            {
                {"CartonProducedMessage", CartonProducedMessage},
                {"LabelPrintMessage", LabelPrintMessage},
                {"KeepAliveMessage", KeepAliveMessage}
            };
        }
    }
}