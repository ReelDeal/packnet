﻿using PackNet.Business.WmsIntegration.MessageProducer;

namespace PackNet.CommunicationTests.WmsIntegration.MessageProducer
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Testing.Specificity;

    [TestClass]
    public class ConfigurationTests
    { 
        private IWmsMessageProducerConfiguration config;

        [TestInitialize]
        public void TestInitialize()
        {
            config = new MessageProducerConfigurationMock();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeAbleToLoadConfigurationFromFile()
        {
            var s = config.GetMessageFormat("CartonProducedMessage");
            
            Specify.That(s).Should.Not.BeNull();
            Specify.That(s.Length > 0).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowExceptionIfMessageTypeDoesNotExist()
        {
            var messageType = string.Empty;

            try
            {
                config.GetMessageFormat("blah");
            }
            catch (MessageTypeNotFoundException ex)
            {
                messageType = ex.Message;
            }

            Specify.That(messageType).Should.BeLogicallyEqualTo("blah");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowExceptionIfRegExKeyDoesNotExist()
        {
            var regExKey = string.Empty;

            try
            {
                config.GetAnswerRegEx("blah");
            }
            catch (MessageTypeNotFoundException ex)
            {
                regExKey = ex.Message;
            }

            Specify.That(regExKey).Should.BeLogicallyEqualTo("blah");
        }
    }
}
