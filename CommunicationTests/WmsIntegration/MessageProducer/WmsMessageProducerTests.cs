﻿using PackNet.Business.WmsIntegration.MessageProducer;

namespace PackNet.CommunicationTests.WmsIntegration.MessageProducer
{
    using System;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Common.Interfaces.DTO.Carton;

    using Testing.Specificity;
                  //todo: fix unit test
#if DEBUG  
 
    [TestClass]
    public class WmsMessageProducerTests
    {
       private ICarton carton;

        [TestInitialize]
        public void TestInitialize()
        {
            CreateCartonRequest();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldReplaceTags()
        {
            var cartonProducedMessage = GetProducer().GetCartonProducedMessage(carton);
            
            Specify.That(cartonProducedMessage.IndexOf('[')).Should.BeLogicallyEqualTo(-1);
            Specify.That(cartonProducedMessage.IndexOf(']')).Should.BeLogicallyEqualTo(-1);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateCartonProducedMessage()
        {
            var cartonProducedMessage = GetProducer().GetCartonProducedMessage(carton);
            
            Specify.That(cartonProducedMessage.IndexOf('[')).Should.BeLogicallyEqualTo(-1);
            Specify.That(cartonProducedMessage.IndexOf(']')).Should.BeLogicallyEqualTo(-1);

            Specify.That(cartonProducedMessage.Substring(0, 1)).Should.BeEqualTo(Convert.ToString((char)2));
            Specify.That(cartonProducedMessage.Substring(1, 5)).Should.BeLogicallyEqualTo("00001");
            Specify.That(cartonProducedMessage.Substring(10, 3)).Should.BeLogicallyEqualTo("100");
            Specify.That(cartonProducedMessage.Substring(14, 2)).Should.BeLogicallyEqualTo("10");
            Specify.That(cartonProducedMessage.Substring(17, 2)).Should.BeLogicallyEqualTo("10");
            Specify.That(cartonProducedMessage.Substring(20, 2)).Should.BeLogicallyEqualTo("10");
            Specify.That(cartonProducedMessage.Substring(23, 3)).Should.BeLogicallyEqualTo("101");
            Specify.That(cartonProducedMessage.Substring(26, 1)).Should.BeLogicallyEqualTo(Convert.ToString((char)3));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateLabelPrintMessage()
        {
            var labelPrintMessage = GetProducer().GetLabelPrintMessage("1", carton);

            Specify.That(labelPrintMessage.IndexOf('[')).Should.BeLogicallyEqualTo(-1);
            Specify.That(labelPrintMessage.IndexOf(']')).Should.BeLogicallyEqualTo(-1);

            Specify.That(labelPrintMessage.Substring(1, 5)).Should.BeLogicallyEqualTo("00001");
            Specify.That(labelPrintMessage.Substring(10, 3)).Should.BeLogicallyEqualTo("100");
            Specify.That(labelPrintMessage.Substring(14, 2)).Should.BeLogicallyEqualTo("10");
            Specify.That(labelPrintMessage.Substring(17, 2)).Should.BeLogicallyEqualTo("10");
            Specify.That(labelPrintMessage.Substring(20, 2)).Should.BeLogicallyEqualTo("10");
            Specify.That(labelPrintMessage.Substring(23, 1)).Should.BeLogicallyEqualTo("1");
            Specify.That(labelPrintMessage.Substring(25, 3)).Should.BeLogicallyEqualTo("101");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldCreateKeepAliveMessage()
        {
            var keepAliveMessage = GetProducer().GetKeepAliveMessage();

            Specify.That(keepAliveMessage.Substring(0, 1)).Should.BeLogicallyEqualTo(Convert.ToString((char)2));
            Specify.That(keepAliveMessage.Substring(1, 5)).Should.BeLogicallyEqualTo("00001");
            Specify.That(keepAliveMessage.Substring(6, 3)).Should.BeLogicallyEqualTo("KPA");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldIncreaseSequenceNumber()
        {
            var producer = GetProducer();
            var cartonProducedMessage = string.Empty;
            for (var i = 0; i < 99999; i++)
            {
                cartonProducedMessage = producer.GetCartonProducedMessage(carton);
            }

            Specify.That(cartonProducedMessage.Substring(1, 5)).Should.BeLogicallyEqualTo("99999");

            cartonProducedMessage = producer.GetCartonProducedMessage(carton);
            Specify.That(cartonProducedMessage.Substring(1, 5)).Should.BeLogicallyEqualTo("00001");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ExceptionShouldBeThrownWhenUnhandledTagIsSentToTagResolver()
        {
            var config = new Mock<IWmsMessageProducerConfiguration>();
            config.Setup(c => c.GetMessageFormat("CartonProducedMessage")).Returns("00[SequenceNumber]RCP;[CustomerUniqueId];[Length];[Width];[Height];[NextLabelRequestNumber]000;[blah]");
            
            var exceptionMessage = string.Empty;
            
            try
            {
                var producer = GetProducer(config.Object);
                producer.GetCartonProducedMessage(carton);
            }
            catch (TagNotFoundException ex)
            {
                exceptionMessage = ex.Message;
            }

            Specify.That(exceptionMessage).Should.BeLogicallyEqualTo("blah");
        }

        private IWmsMessageProducer GetProducer(IWmsMessageProducerConfiguration config)
        {
            ITagResolver resolver = new TagResolver(1, 99999);
            IWmsMessageProducer producer = new WmsMessageProducer(resolver, config);
            return producer;
        }

        private IWmsMessageProducer GetProducer()
        {
            IWmsMessageProducerConfiguration config = new MessageProducerConfigurationMock();
            return GetProducer(config);
        }

        private void CreateCartonRequest()
        {
            carton = new Carton
            {
                CustomerUniqueId = "100",
                Length = 10,
                Width = 10,
                Height = 10,
            };
        }
      
    }  
#endif
}
