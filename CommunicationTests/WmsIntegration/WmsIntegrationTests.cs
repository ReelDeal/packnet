﻿//using PackNet.Business.WmsIntegration;
//using PackNet.Business.WmsIntegration.MessageProducer;
//using PackNet.Common.Eventing;
//using PackNet.Common.Interfaces.DTO.Settings;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Machines;
//using PackNet.Data.WMS;

//namespace PackNet.CommunicationTests.WmsIntegration
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;

//    using Microsoft.VisualStudio.TestTools.UnitTesting;

//    using Moq;

//    using Business.Machines;
//    using Business.Orders;
//    using Common.Interfaces.DTO.Carton;

//    using Testing.Specificity;

//    [TestClass]
//    public class WmsIntegrationTests
//    {
//        private Guid MachineId = Guid.NewGuid();
//        private Guid FusionMachineId = Guid.NewGuid();
        
//        private ICartonRequestOrder order;
//        private IMachinesManager machineManager;

//        private Mock<ICartonRequestJobManager> orderManager;
//        private Mock<IWmsCommunicator> communicator;
//        private Mock<ILogger> logger;
//        private Mock<IWmsMessageProducer> messageProducer;
        
//        private EventAggregator eventAggregator = new EventAggregator();

//        [TestInitialize]
//        public void TestInitialize()
//        {
//            order = SetupCartonRequestOrder("1");
//            machineManager = SetupMachinesManager();
//            communicator = new Mock<IWmsCommunicator>(MockBehavior.Loose);
//            logger = new Mock<ILogger>();
//            messageProducer = new Mock<IWmsMessageProducer>();
//            orderManager = new Mock<ICartonRequestJobManager>();
//        }
        
//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldRaiseEventWhenWmsCommunicatorConnectionFails()
//        {
//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration(), eventAggregator);
//            var eventRaised = false;
//            notifier.ConnectionFailed += (o, s) => eventRaised = true;
//            communicator.Raise(c => c.ConnectionFailed += null, new EventArgs());
//            Assert.IsTrue(eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldConnectPrinterButNotRaiseEventWhenRequestLabelNorRaiseEventWhenReportProducedPrintReceivesNackWhenEnabledIsTrueExternalPrintIsFalseReportProducedIsFalse()
//        {
//            communicator.Setup(s => s.Connect()).Returns(false);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration(), eventAggregator);
            
//            var eventRaised = false;

//            notifier.ConnectToWMS();
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(!eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(!eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldConnectPrinterButNotRaiseEventWhenRequestLabelNorRaiseEventWhenReportProducedPrintReceivesNackWhenEnabledIsTrueExternalPrintIsFalseReportProducedIsTrue()
//        {
//            communicator.Setup(s => s.Connect()).Returns(false);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration(), eventAggregator);

//            var eventRaised = false;

//            notifier.ConnectToWMS();
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(!eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(!eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldConnectPrinterButNotRaiseEventWhenRequestLabelNorRaiseEventWhenReportProducedPrintReceivesNackWhenEnabledIsTrueExternalPrintIsTrueReportProducedIsFalse()
//        {
//            communicator.Setup(s => s.Connect()).Returns(false);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration(), eventAggregator);

//            var eventRaised = false;

//            notifier.ConnectToWMS();
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(!eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(!eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotConnectPrinterNorRaiseEventWhenRequestLabelNorRaiseEventWhenReportProducedPrintReceivesNackWhenEnabledIsFalseExternalPrintIsTrueReportProducedIsFalse()
//        {
//            communicator.Setup(s => s.Connect()).Returns(false);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration(), eventAggregator);
            
//            var eventRaised = false;
//            var connectionFailed = false;
//            notifier.ConnectionFailed += (o, s) => connectionFailed = true;

//            notifier.ConnectToWMS();
//            Assert.IsFalse(connectionFailed);
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(!eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(!eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotConnectPrinterNorRaiseEventWhenRequestLabelNorRaiseEventWhenReportProducedPrintReceivesNackWhenEnabledIsFalseExternalPrintIsFalseReportProducedIsFalse()
//        {
//            communicator.Setup(s => s.Connect()).Returns(false);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration(), eventAggregator);
                        
//            var eventRaised = false;
//            var connectionFailed = false;
//            notifier.ConnectionFailed += (o, s) => connectionFailed = true;

//            notifier.ConnectToWMS();
//            Assert.IsFalse(connectionFailed);
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(!eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(!eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldConnectPrinterAndRaiseEventWhenRequestLabelButNotRaiseEventWhenReportProducedPrintReceivesNackWhenEnabledIsTrue_ExternalPrintIsTrue_ReportProducedIsFalse()
//        {
//            communicator.Setup(s => s.Connect()).Returns(true);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration { Enabled = true, ExternalPrint = true, ReportProduced = false }, eventAggregator);            

//            var eventRaised = false;
            
//            notifier.ConnectToWMS();
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(!eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldConnectPrinterAndRaiseEventWhenReportProduced_ButNotRaiseEventWhenRequestLabelPrintReceivesNackWhenEnabledIsTrue_ExternalPrintIsFalse_ReportProducedIsTrue()
//        {
//            communicator.Setup(s => s.Connect()).Returns(true);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration{Enabled = true, ExternalPrint = false, ReportProduced = true}, eventAggregator);

//            var eventRaised = false;
            
//            notifier.ConnectToWMS();
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(!eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotConnectPrinterNorRaiseEventWhenRequestLabel_NorRaiseEventWhenReportProducedPrintReceivesNackWhenEnabledIsFalse_ExternalPrintIsTrue_ReportProducedIsTrue()
//        {
//            communicator.Setup(s => s.Connect()).Returns(false);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration { Enabled = false, ExternalPrint = true, ReportProduced = true }, eventAggregator);
            
//            var eventRaised = false;
//            var connectionFailed = false;
//            notifier.ConnectionFailed += (o, s) => connectionFailed = true;
            
//            notifier.ConnectToWMS();
//            Assert.IsFalse(connectionFailed);
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(!eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(!eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotConnectPrinterNorRaiseEventWhenRequestLabelNorRaiseEventWhenReportProducedPrintReceivesNackWhenEnabledIsFalseExternalPrintIsFalseReportProducedIsTrue()
//        {
//            communicator.Setup(s => s.Connect()).Returns(false);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration(), eventAggregator);
            
//            var eventRaised = false;
//            var connectionFailed = false;
//            notifier.ConnectionFailed += (o, s) => connectionFailed = true;

//            notifier.ConnectToWMS();
//            Assert.IsFalse(connectionFailed);
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(!eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(!eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldConnectPrinterAndRaiseEventWhenRequestLabelAndRaiseEventWhenReportProducedPrintReceivesNackWhenEnabledIsTrue_ExternalPrintIsTrue_ReportProducedIsTrue()
//        {
//            communicator.Setup(s => s.Connect()).Returns(true);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration { Enabled = true, ExternalPrint = true, ReportProduced = true }, eventAggregator);
            
//            var eventRaised = false;            
//            notifier.ConnectToWMS();
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            Assert.IsTrue(eventRaised);
//            eventRaised = false;
//            notifier.ReportProduced(order);
//            Assert.IsTrue(eventRaised);
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldSendOneLabelRequestForeachCartonRequest()
//        {
//            communicator = new Mock<IWmsCommunicator>(MockBehavior.Loose);
//            communicator.Setup(s => s.Connect()).Returns(true);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(true);

//            var printerMock = new Mock<PrinterInformation>();
//            printerMock.SetupGet(x => x.Id).Returns(Guid.NewGuid());

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration { Enabled = true, ExternalPrint = true, ReportProduced = true }, eventAggregator);

//            notifier.ConnectToWMS();
//            notifier.PrintLabelsFor(orderManager.Object, order);
//            communicator.Verify(s => s.Send(It.IsAny<string>()), Times.Exactly(2));
//            orderManager.Verify(o => o.HandleLabelRequested(order, true), Times.Exactly(2));
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldNotSendLabelRequestAndNotRaiseLabelSentEventWhenCartonRequestOrderPrinterIsNull()
//        {
//            var orderWithNullPrinter = SetupCartonRequestOrderWithNullPrinter("1");
            
//            communicator = new Mock<IWmsCommunicator>(MockBehavior.Loose);
//            communicator.Setup(s => s.Connect()).Returns(true);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(true);

//            var notifier = new WmsNotification(
//                communicator.Object,
//                messageProducer.Object,
//                logger.Object,
//                machineManager,
//                new WmsConfiguration(), eventAggregator);

//            notifier.ConnectToWMS();
//            notifier.PrintLabelsFor(orderManager.Object, orderWithNullPrinter);
//            communicator.Verify(s => s.Send(It.IsAny<string>()), Times.Exactly(0));
//            Specify.That(orderWithNullPrinter.LabelRequested).Should.BeFalse();
//        }

//        [TestMethod]
//        [TestCategory("Unit")]
//        public void ShouldRaiseMachineErrorWhenCommunicatorSendReturnsFalse()
//        {
//            // Arrange
//            communicator = new Mock<IWmsCommunicator>(MockBehavior.Loose);
//            communicator.Setup(s => s.Connect()).Returns(true);
//            communicator.Setup(s => s.Send(It.IsAny<string>())).Returns(false);

//            var printerMock = new Mock<PrinterInformation>();
//            printerMock.SetupGet(x => x.Id).Returns(Guid.NewGuid());

//            var notifier = new WmsNotification(communicator.Object, messageProducer.Object, logger.Object, machineManager, new WmsConfiguration { Enabled = true, ExternalPrint = true, ReportProduced = true }, eventAggregator);

//            var labelFailed = new List<string>();
//            //notifier.LabelRequestFailed += (a, b) => labelFailed.Add(b.ErrorMessage);

//            notifier.ConnectToWMS();
//            notifier.PrintLabelsFor(orderManager.Object, order);

//            Specify.That(labelFailed.Count).Should.BeEqualTo(2);
//            Specify.That(labelFailed.Any(l => l.Contains("request1"))).Should.BeTrue();
//            Specify.That(labelFailed.Any(l => l.Contains("request2"))).Should.BeTrue();
//        }
        
//        private IMachinesManager SetupMachinesManager()
//        {
//            var manager = new Mock<IMachinesManager>();
//            var machine = SetupMachineMock();
//            var fusionMachine = SetupFusionMachineMock();
//            var machinesList = new List<IPacksizeCutCreaseMachine> { machine, fusionMachine };
//            manager.SetupGet(mm => mm.Machines).Returns(machinesList);
//            manager.Setup(m => m.IsFusionMachine(FusionMachineId)).Returns(true);
//            manager.Setup(m => m.IsFusionMachine(MachineId)).Returns(false);
            
//            return manager.Object;
//        }

//        private IPacksizeCutCreaseMachine SetupMachineMock()
//        {
//            var machine = new Mock<IPacksizeCutCreaseMachine>();
//            machine.SetupGet(m => m.Id).Returns(MachineId);

//            return machine.Object;
//        }

//        private IPacksizeCutCreaseMachine SetupFusionMachineMock()
//        {
//            var fusionMachine = new Mock<IPacksizeCutCreaseMachine>();
//            fusionMachine.SetupGet(m => m.Id).Returns(FusionMachineId);

//            return fusionMachine.Object;
//        }

//        private ICartonRequestOrder SetupCartonRequestOrder(string classificationNumber)
//        {
//            var orderMock = new Mock<ICartonRequestOrder>();
//            var requestMock1 = new Mock<ICarton>();
//            var requestMock2 = new Mock<ICarton>();
//            var requestList = new List<ICarton> { requestMock1.Object, requestMock2.Object };
//            var printerMock = new Mock<PrinterInformation>();

//            requestMock1.SetupGet(r => r.SerialNumber).Returns("request1");
//            requestMock1.SetupGet(r => r.ClassificationNumber).Returns(classificationNumber);
//            requestMock2.SetupGet(r => r.SerialNumber).Returns("request2");
//            requestMock2.SetupGet(r => r.ClassificationNumber).Returns(classificationNumber);
            
//            orderMock.SetupGet(o => o.Requests).Returns(requestList);
//            orderMock.SetupGet(o => o.MachineId).Returns(FusionMachineId);
//            orderMock.Setup(o => o.RunsOnMachine(FusionMachineId)).Returns(true);
//            orderMock.Setup(o => o.RunsOnMachine(MachineId)).Returns(false);
//            orderMock.SetupGet(o => o.Printer).Returns(printerMock.Object);

//            return orderMock.Object;
//        }

//        private ICartonRequestOrder SetupCartonRequestOrderWithNullPrinter(string classificationNumber)
//        {
//            var orderMock = new Mock<ICartonRequestOrder>();
//            var requestMock1 = new Mock<ICarton>();
//            var requestMock2 = new Mock<ICarton>();
//            var requestList = new List<ICarton> { requestMock1.Object };

//            requestMock1.SetupGet(r => r.ClassificationNumber).Returns(classificationNumber);
//            requestMock2.SetupGet(r => r.ClassificationNumber).Returns(classificationNumber);
//            requestList.Add(requestMock2.Object);

//            orderMock.SetupGet(o => o.Requests).Returns(requestList);
//            orderMock.Setup(o => o.RunsOnMachine(It.IsAny<Guid>())).Returns(true);
//            orderMock.SetupGet(o => o.Printer).Returns((PrinterInformation)null);

//            return orderMock.Object;
//        }
//    }
//}
