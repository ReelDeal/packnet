﻿using System;
using System.Net;
using System.Reactive.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Utils;
using PackNet.Communication;
using PackNet.Communication.Communicators;
using PackNet.Communication.PLCBase;

using Testing.Specificity;

namespace PackNet.CommunicationTests.MachineCommunicatorTests
{
    [TestClass]
    public class MachineCommunicatorBaseTests
    {

        private EventAggregator aggregator;
        private MachineCommunicatorBaseTester communicator;

        private Mock<IPLCHeartBeatCommunicator> plcCommunicatorMock;
        private Mock<ILogger> loggerMock;

        private EmMachine machine;
        private readonly IPackagingMachineVariableMap variableMap = new BaseMachinVariableMap();

        [TestInitialize]
        public void Setup()
        {
            loggerMock = new Mock<ILogger>();
            plcCommunicatorMock = new Mock<IPLCHeartBeatCommunicator>();
            plcCommunicatorMock.SetupGet(x => x.PlcIpAddressWithPort).Returns("127.0.0.1");


            machine = new EmMachine()
            {
                Alias = "e-mils awesomeness",
                IpOrDnsName = "127.0.0.1",
                Port = 80
            };

            aggregator = new EventAggregator();
            communicator = new MachineCommunicatorBaseTester(machine, aggregator, aggregator, plcCommunicatorMock.Object, variableMap, loggerMock.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRaiseVariableChangedWhenChangedIsNotMatchingAnySpecificVariable()
        {
            var communicator = new Mock<IPLCHeartBeatCommunicator>();
            var services = new MachineCommunicatorBaseTester(machine, aggregator, aggregator, communicator.Object, variableMap, loggerMock.Object);
            bool variableChangedEvent = false;

            aggregator.GetEvent<Message<VariableChangedEventArgs>>()
                .Subscribe(m =>
                {
                    variableChangedEvent = true;
                });

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs("Crap", (short)1) });
            Retry.For(() => variableChangedEvent, TimeSpan.FromSeconds(1));
            Specify.That(variableChangedEvent).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishDataToTheInternalAggregator_OnMachineProductionCompleted()
        {
            this.machine.AssignedOperator = "asd";
            var carton = new Carton()
            {
                Id = Guid.NewGuid(),
                Length = 1,
                Width = 2,
                Height = 3,
                DesignId = 1,
                CartonOnCorrugate = new CartonOnCorrugate() { Corrugate = new Corrugate(), TileCount = 3 },
            };

            communicator.SetOrder(carton);

            bool packagingCompletedChangedEvent = false;

            plcCommunicatorMock.Setup(m => m.ReadValue<double>(variableMap.ProductionHistoryItemCorrugateUsage, It.IsAny<int>()))
                .Returns(25.0);
            plcCommunicatorMock.Setup(m => m.ReadValue<int>(variableMap.ProductionHistoryItemCompletionStatus, It.IsAny<int>()))
                .Returns((int)PackagingStatus.Completed);
            plcCommunicatorMock.Setup(m => m.ReadValue<long>(variableMap.ProductionHistoryItemCompletionTime, It.IsAny<int>()))
                .Returns(1000);

            aggregator.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .Subscribe(m =>
                {
                    packagingCompletedChangedEvent = true;
                });

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ProductionHistoryItemId.VariableName, JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(carton.Id))) });
            Retry.For(() => packagingCompletedChangedEvent, TimeSpan.FromSeconds(1));
            Specify.That(packagingCompletedChangedEvent).Should.BeTrue();
        }

        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 8907")]
        public void ShouldCheckMachineForNewErrorCodesWhenErrorClearedIsCalled()
        {
            SetUpErrorCodeTest(0);
            SetUpErrorCodeTest(1);

            machine.CurrentStatus = MachineErrorStatuses.MachineError;

            aggregator.Publish(new Message<VariableChangedEventArgs>
            {
                Data = new VariableChangedEventArgs("ERROR_CODES[0].Code", (ushort)0),
            });

            Retry.For(() => machine.CurrentStatus.Equals(MachineErrorStatuses.EmergencyStop), TimeSpan.FromSeconds(1));

            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.EmergencyStop);
            Specify.That(machine.Errors).Should.Not.BeNull();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldPublishMachineProductionCompletedEventWhenMachineGoesOfflineDuringProduction()
        {
            machine.AssignedOperator = "asd";
            machine.CurrentStatus = MachineStatuses.MachineOnline;
            
            var carton = new Carton()
            {
                Id = Guid.NewGuid(),
                Length = 1,
                Width = 2,
                Height = 3,
                DesignId = 1,
                CartonOnCorrugate = new CartonOnCorrugate() { Corrugate = new Corrugate(), TileCount = 3 },
            };

            communicator.SetOrder(carton);

            bool packagingCompletedChangedEvent = false;

            aggregator.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .Subscribe(m =>
                {
                    Specify.That(m.Data.Item2.Failure).Should.BeTrue();
                    packagingCompletedChangedEvent = true;
                });


            aggregator.Publish(new Message<Tuple<string, bool>> { MessageType = SimplePLCMessages.SimplePLCConnectionStatus, Data = new Tuple<string, bool>("127.0.0.1", false) });

            Retry.For(() => packagingCompletedChangedEvent, TimeSpan.FromSeconds(1));
            Specify.That(packagingCompletedChangedEvent).Should.BeTrue();
            Specify.That(carton.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.ProducibleFailed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        [Description("When a machine goes into error state we do not want to change status of item in production since the machine is still available it will publish completed as failure for items in production when error occurs")]
        public void ShouldNotPublishMachineProductionCompletedEvent_OrSetProducibleStatusFailedOnItemInProduction_WhenMachineGoesIntoErrorDuringProduction()
        {
            machine.AssignedOperator = "asd";
            machine.CurrentStatus = MachineStatuses.MachineOnline;

            var carton = new Carton()
            {
                Id = Guid.NewGuid(),
                Length = 1,
                Width = 2,
                Height = 3,
                DesignId = 1,
                CartonOnCorrugate = new CartonOnCorrugate() { Corrugate = new Corrugate(), TileCount = 3 },
            };

            communicator.SetOrder(carton);

            bool packagingCompletedChangedEvent = false;

            aggregator.GetEvent<Message<Tuple<IPacksizeCutCreaseMachine, IMachineProductionData>>>()
                .Where(m => m.MessageType == MachineMessages.MachineProductionCompleted)
                .Subscribe(m =>
                {
                    packagingCompletedChangedEvent = true;
                });

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, "1") });
            Retry.For(() => machine.CurrentStatus is MachineErrorStatuses, TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus is MachineErrorStatuses).Should.BeTrue();
            Thread.Sleep(200);
            Specify.That(packagingCompletedChangedEvent).Should.BeFalse();
            Specify.That(carton.ProducibleStatus).Should.Not.BeEqualTo(ErrorProducibleStatuses.ProducibleFailed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetProducibleStatusOnOrderToCompletedWhenMachineReportsCompletion()
        {
            this.machine.AssignedOperator = "asd";
            var carton = new Carton()
            {
                Id = Guid.NewGuid(),
                Length = 1,
                Width = 2,
                Height = 3,
                DesignId = 1,
                CartonOnCorrugate = new CartonOnCorrugate() { Corrugate = new Corrugate(), TileCount = 3 },
            };

            communicator.SetOrder(carton);

            plcCommunicatorMock.Setup(m => m.ReadValue<double>(variableMap.ProductionHistoryItemCorrugateUsage, It.IsAny<int>()))
                .Returns(25.0);
            plcCommunicatorMock.Setup(m => m.ReadValue<int>(variableMap.ProductionHistoryItemCompletionStatus, It.IsAny<int>()))
                .Returns((int)PackagingStatus.Completed);
            plcCommunicatorMock.Setup(m => m.ReadValue<double>(variableMap.ProductionHistoryItemCompletionTime, It.IsAny<int>()))
                .Returns(25.0);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ProductionHistoryItemId.VariableName, JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(carton.Id))) });

            Retry.For(() => carton.ProducibleStatus == ProducibleStatuses.ProducibleCompleted, TimeSpan.FromSeconds(1));
            Specify.That(carton.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleCompleted);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldSetProducibleStatusOnOrderToFailedWhenMachineReportsFailiure()
        {
            this.machine.AssignedOperator = "asd";
            var carton = new Carton()
            {
                Id = Guid.NewGuid(),
                Length = 1,
                Width = 2,
                Height = 3,
                DesignId = 1,
                CartonOnCorrugate = new CartonOnCorrugate() { Corrugate = new Corrugate(), TileCount = 3 },
            };

            communicator.SetOrder(carton);

            plcCommunicatorMock.Setup(m => m.ReadValue<double>(variableMap.ProductionHistoryItemCorrugateUsage, It.IsAny<int>()))
                .Returns(25.0);
            plcCommunicatorMock.Setup(m => m.ReadValue<int>(variableMap.ProductionHistoryItemCompletionStatus, It.IsAny<int>()))
                .Returns((int)PackagingStatus.Failure);
            plcCommunicatorMock.Setup(m => m.ReadValue<double>(variableMap.ProductionHistoryItemCompletionTime, It.IsAny<int>()))
                .Returns(25.0);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ProductionHistoryItemId.VariableName, JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(carton.Id))) });

            Retry.For(() => carton.ProducibleStatus == ErrorProducibleStatuses.ProducibleFailed, TimeSpan.FromSeconds(1));
            Specify.That(carton.ProducibleStatus).Should.BeEqualTo(ErrorProducibleStatuses.ProducibleFailed);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void SettingVariableShouldUseCommunicatorWrite()
        {
            var communicator = new Mock<IPLCHeartBeatCommunicator>();
            communicator.Setup(com => com.WriteValue(It.IsAny<PacksizePlcVariable>(), It.IsAny<object>(), It.IsAny<int>())).Verifiable();

            var services = new MachineCommunicatorBaseTester(machine, aggregator, aggregator, communicator.Object, variableMap, loggerMock.Object);
            services.SetVariableValue(variableMap.PcLifeSign, 1);
            communicator.VerifyAll();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void GettingVariableShouldUseCommunicatorRead()
        {
            var communicator = new Mock<IPLCHeartBeatCommunicator>();
            communicator.Setup(com => com.ReadValue<object>(variableMap.PcLifeSign, It.IsAny<int>())).Verifiable();

            var services = new MachineCommunicatorBaseTester(machine, aggregator, aggregator, communicator.Object, variableMap, loggerMock.Object);
            services.GetVariableValue<object>(variableMap.PcLifeSign);
            communicator.VerifyAll();
        }
        
        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldRemoveProducible_WhenRemoved_FromMachineQueue()
        {
            var id = Guid.NewGuid();
            var producible = new Mock<IPacksizeCarton>();
            producible.Setup(m => m.Id).Returns(id);
            producible.SetupProperty(m => m.ProducibleStatus);
            producible.Object.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStatusUnknown;

            communicator.SetOrder(producible.Object);

            aggregator.Publish(new Message<VariableChangedEventArgs>()
            {
                Data = new VariableChangedEventArgs(variableMap.RemovedProductionItemId1.VariableName, JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(id))),
            });

            Retry.For(() => producible.Object.ProducibleStatus == ProducibleStatuses.ProducibleRemoved, TimeSpan.FromSeconds(2));

            Specify.That(producible.Object.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleRemoved);
            Specify.That(machine.CurrentProductionStatus).Should.BeEqualTo(MachineProductionStatuses.ProductionIdle);

            producible.Object.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleStatusUnknown;

            producible.Setup(m => m.Id).Returns(id);

            aggregator.Publish(new Message<VariableChangedEventArgs>()
            {
                Data = new VariableChangedEventArgs(variableMap.RemovedProductionItemId2.VariableName, JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(id))),
            });

            Retry.For(() => producible.Object.ProducibleStatus == ProducibleStatuses.ProducibleRemoved, TimeSpan.FromSeconds(2));

            Specify.That(producible.Object.ProducibleStatus).Should.BeEqualTo(ProducibleStatuses.ProducibleRemoved);
            Specify.That(machine.CurrentProductionStatus).Should.BeEqualTo(MachineProductionStatuses.ProductionIdle);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_SetMachine_ToOnline_OnStartUpProcedureCompleted()
        {
            communicator.Dispose();
            var fusion = new FusionMachine { IpOrDnsName = "127.0.0.1", Port = 80, PhysicalMachineSettings = new FusionPhysicalMachineSettings() };
            communicator = new MachineCommunicatorBaseTester(fusion, aggregator, aggregator, plcCommunicatorMock.Object, variableMap, loggerMock.Object);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.StartupProcedureCompleted.VariableName, true) });
            Retry.For(() => fusion.CurrentStatus == MachineStatuses.MachineOnline, TimeSpan.FromSeconds(1));
            Specify.That(fusion.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOnline);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_SetMachine_ToInitialize_OnStartUpProcedureNotCompleted()
        {
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.StartupProcedureCompleted.VariableName, false) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineInitializing), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineInitializing);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_SetMachine_ToPaused_OnPlcVariableChanged()
        {
            machine.CurrentStatus = MachineStatuses.MachineOnline;
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, true) });
            Retry.For(() => machine.CurrentStatus == MachinePausedStatuses.MachinePaused, TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachinePausedStatuses.MachinePaused);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_SetMachine_ToOnline_OnPlcVariableChanged()
        {
            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, false) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineOnline), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOnline);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_SetMachine_ToOutOfCorrugate_OnPlcVariableChanged()
        {
            plcCommunicatorMock.Setup(m => m.ReadValue<bool>(variableMap.StartupProcedureCompleted, It.IsAny<int>()))
                .Returns(true);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.OutOfCorrugate.VariableName, true) });
            Retry.For(() => machine.CurrentStatus == PacksizePackagingMachineErrorStatuses.MachineInOutOfCorrugate, TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.MachineInOutOfCorrugate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_SetMachine_ToChangeCorrugate_OnPlcVariableChanged()
        {
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ChangeCorrugate.VariableName, true) });
            Retry.For(() => machine.CurrentStatus.Equals(MachinePausedStatuses.MachineChangingCorrugate), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachinePausedStatuses.MachineChangingCorrugate);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_SetMachine_ToEStop_OnPlcVariableChanged()
        {
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, "1") });
            Retry.For(() => machine.CurrentStatus.Equals(MachineErrorStatuses.EmergencyStop), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.EmergencyStop  );
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void Should_SetMachine_ToError_OnPlcVariableChanged()
        {
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, "2") });
            Retry.For(() => machine.CurrentStatus.Equals(MachineErrorStatuses.MachineError), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.MachineError);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ErrorCodeOccurredEventTest()
        {
            for (int i = 0; i < 5; i++)
            {
                SetUpErrorCodeTest(i);

                machine.CurrentStatus = MachineStatuses.MachineOnline;

                aggregator.Publish(new Message<VariableChangedEventArgs>
                {
                    Data = new VariableChangedEventArgs("ERROR_CODES[" + i + "].Code", (ushort)2)
                });

                Retry.For(() => machine.CurrentStatus.Equals(MachineErrorStatuses.MachineError), TimeSpan.FromSeconds(2));

                Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.MachineError);
                Specify.That(machine.Errors).Should.Not.BeNull();

                ClearErrorCodeForTest(i);
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ErrorCodeClearedEventTest()
        {
            for (int i = 0; i < 5; i++)
            {
                machine.CurrentStatus = MachineErrorStatuses.MachineError;

                aggregator.Publish(new Message<VariableChangedEventArgs>
                {
                    Data = new VariableChangedEventArgs("ERROR_CODES[" + i + "].Code", (ushort)0),
                });

                Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineInitializing), TimeSpan.FromSeconds(2));

                Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineInitializing);
                Specify.That(machine.Errors.Count).Should.BeEqualTo(0);
            }
        }

        [TestMethod]
        [TestCategory("Unit")]
        //The order is offline->(error->[Red Button]->)Init->Paused->[White Button]->Online\
        //A machine should never be able to skip steps only go back in this sequence
        public void MachineShouldFollowCorrectStateTransitionOrder()
        {
            machine.CurrentStatus = MachineStatuses.MachineOffline;
            machine.PhysicalMachineSettings = new EmPhysicalMachineSettings()
            {
                CrossHeadParameters = new EmCrossHeadParameters(),
                LongHeadParameters = new EmLongHeadParameters()
            };
            plcCommunicatorMock.Setup(m => m.ReadValue<bool>(variableMap.MachinePaused, It.IsAny<int>())).Returns(true);
            SetUpErrorCodeTest(0);

            aggregator.Publish(new Message<Tuple<string, bool>> { MessageType = SimplePLCMessages.SimplePLCConnectionStatus, Data = new Tuple<string, bool>("127.0.0.1", true) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineErrorStatuses.MachineError), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.MachineError);

            ClearErrorCodeForTest(0);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, (ushort)0) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineInitializing), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineInitializing);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.StartupProcedureCompleted.VariableName, true) });
            Retry.For(() => machine.CurrentStatus.Equals(MachinePausedStatuses.MachinePaused), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachinePausedStatuses.MachinePaused);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, false) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineOnline), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOnline);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldGoToLastStatusWhenMovingOutOfLightBarrierBroken()
        {
            machine.CurrentStatus = MachineStatuses.MachineOnline;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, PacksizeMachineErrorCodes.LightBarrierBroken) });
            Retry.For(() => machine.CurrentStatus == MachinePausedStatuses.LightBarrierBroken, TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachinePausedStatuses.LightBarrierBroken);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, 0) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineOnline, TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOnline);


            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, PacksizeMachineErrorCodes.InfeedLightBarrierBroken) });
            Retry.For(() => machine.CurrentStatus == MachinePausedStatuses.LightBarrierBroken, TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachinePausedStatuses.LightBarrierBroken);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, 0) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineOnline, TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachinePausedStatuses.MachinePaused);
        }

        [TestMethod]
        [Ignore]
        [TestCategory("Unit")]
        //The order is offline->(error->[Red Button]->)Init->Paused->[White Button]->Online\
        //A machine should never be able to skip steps only go back in this sequence
        public void MachineShouldNotBeAbleToJumpForwardInStateTransitionOrder()
        {
            //offline -> online
            machine.CurrentStatus = MachineStatuses.MachineOffline;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, false) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineOffline, TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOffline);

            //error -> online
            machine.CurrentStatus = MachineErrorStatuses.MachineError;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, false) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineOffline, TimeSpan.FromSeconds(1)); 
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.MachineError);

            //init -> online
            machine.CurrentStatus = MachineStatuses.MachineInitializing;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, false) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineInitializing, TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineInitializing);

            //offline -> paused
            machine.CurrentStatus = MachineStatuses.MachineOffline;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, true) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineOffline, TimeSpan.FromSeconds(1)); 
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOffline);

            //error -> paused
            machine.CurrentStatus = MachineErrorStatuses.MachineError;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, true) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineOffline, TimeSpan.FromSeconds(1)); 
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.MachineError);

            //init -> paused, due to plc variable flipped not through startupcomplete event
            machine.CurrentStatus = MachineStatuses.MachineInitializing;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, true) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineInitializing, TimeSpan.FromSeconds(1)); 
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineInitializing);

            //offline -> paused through startup complete
            machine.CurrentStatus = MachineStatuses.MachineOffline;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.StartupProcedureCompleted.VariableName, true) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineOffline, TimeSpan.FromSeconds(1)); 
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOffline);

            //online -> error cleared
            machine.CurrentStatus = MachineStatuses.MachineOnline;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, 0) });
            Retry.For(() => machine.CurrentStatus == MachineStatuses.MachineOnline, TimeSpan.FromSeconds(1)); 
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOnline);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeAbleToMoveToAnyPreviousStepFromOnline()
        {
            //online -> paused
            machine.CurrentStatus = MachineStatuses.MachineOnline;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachinePaused.VariableName, true) });
            Retry.For(() => machine.CurrentStatus.Equals(MachinePausedStatuses.MachinePaused), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachinePausedStatuses.MachinePaused);

            //online -> Init
            machine.CurrentStatus = MachineStatuses.MachineOnline;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.StartupProcedureCompleted.VariableName, false) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineInitializing), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineInitializing);

            //online -> error
            machine.CurrentStatus = MachineStatuses.MachineOnline;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, 2) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineErrorStatuses.MachineError), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.MachineError);

            //online -> offline
            machine.CurrentStatus = MachineStatuses.MachineOnline;

            aggregator.Publish(new Message<Tuple<string, bool>> { MessageType = SimplePLCMessages.SimplePLCConnectionStatus, Data = new Tuple<string, bool>("127.0.0.1", false) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineOffline), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOffline);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeAbleToMoveToAnyPreviousStepFromPaused()
        {
            //paused -> Init
            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.StartupProcedureCompleted.VariableName, false) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineInitializing), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineInitializing);

            //paused -> error
            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, 2) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineErrorStatuses.MachineError), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.MachineError);

            //paused -> offline
            machine.CurrentStatus = MachinePausedStatuses.MachinePaused;

            aggregator.Publish(new Message<Tuple<string, bool>> { MessageType = SimplePLCMessages.SimplePLCConnectionStatus, Data = new Tuple<string, bool>("127.0.0.1", false) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineOffline), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOffline);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeAbleToMoveToAnyPreviousStepFromInit()
                {   

            //init -> error
            machine.CurrentStatus = MachineStatuses.MachineInitializing;

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.ErrorCode1.VariableName, 2) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineErrorStatuses.MachineError), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineErrorStatuses.MachineError);

            //init -> offline
            machine.CurrentStatus = MachineStatuses.MachineInitializing;

            aggregator.Publish(new Message<Tuple<string, bool>> { MessageType = SimplePLCMessages.SimplePLCConnectionStatus, Data = new Tuple<string, bool>("127.0.0.1", false) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineOffline), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOffline);
            }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldBeAbleToMoveToAnyPreviousStepFromError()
        {
            //error -> offline
            machine.CurrentStatus = MachineErrorStatuses.MachineError;

            aggregator.Publish(new Message<Tuple<string, bool>> { MessageType = SimplePLCMessages.SimplePLCConnectionStatus, Data = new Tuple<string, bool>("127.0.0.1", false) });
            Retry.For(() => machine.CurrentStatus.Equals(MachineStatuses.MachineOffline), TimeSpan.FromSeconds(1));
            Specify.That(machine.CurrentStatus).Should.BeEqualTo(MachineStatuses.MachineOffline);
        }

        [TestMethod]
        [TestCategory("Bug")]
        [TestCategory("Bug 7993")]
        public void MachineCommunicator_ShouldNotReactOnEmptyGuidMessagesFromThePlc()
        {
            var order = new Mock<IPacksizeCarton>();
            order.Setup(p => p.ProducibleStatus).Callback(() => Specify.That(true).Should.BeFalse("Trying to change producible status. This should not happen"));

            communicator.SetOrder(order.Object);

            aggregator.Publish(new Message<VariableChangedEventArgs>()
            {
                Data = new VariableChangedEventArgs(variableMap.RemovedProductionItemId2.VariableName, JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(Guid.Empty))),
            });
            
            aggregator.Publish(new Message<VariableChangedEventArgs>()
            {
                Data = new VariableChangedEventArgs(variableMap.RemovedProductionItemId1.VariableName, JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(Guid.Empty))),
            });

            aggregator.Publish(new Message<VariableChangedEventArgs>()
            {
                Data = new VariableChangedEventArgs(variableMap.StartedProductionItemId.VariableName, JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(Guid.Empty))),
            });

            aggregator.Publish(new Message<VariableChangedEventArgs>()
            {
                Data = new VariableChangedEventArgs(variableMap.ProductionHistoryItemId.VariableName, JsonConvert.SerializeObject(GuidHelpers.ConvertGuidToIntArray(Guid.Empty))),
            });

            Retry.For(() => communicator.OnVariableChangedCallCount.Equals(4), TimeSpan.FromSeconds(1));
            Specify.That(communicator.OnVariableChangedCallCount).Should.BeEqualTo(4);
        }


        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldFailJob_IfMachineRunsOutOfCorrugate_DuringProduction()
        {
            this.machine.AssignedOperator = "asd";
            var carton = new Carton()
            {
                Id = Guid.NewGuid(),
                Length = 1,
                Width = 2,
                Height = 3,
                DesignId = 1,
                CartonOnCorrugate = new CartonOnCorrugate() { Corrugate = new Corrugate(), TileCount = 3 },
            };

            carton.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;

            plcCommunicatorMock.Setup(m => m.ReadValue<bool>(variableMap.StartupProcedureCompleted, It.IsAny<int>())).Returns(true);
            communicator.SetOrder(carton);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.OutOfCorrugate.VariableName, "1")});

            Retry.For(() => carton.ProducibleStatus == ErrorProducibleStatuses.ProducibleFailed, TimeSpan.FromSeconds(5));
            Specify.That(carton.ProducibleStatus).Should.BeLogicallyEqualTo(ErrorProducibleStatuses.ProducibleFailed);
        }

        [TestMethod]
        [TestCategory("Bug")]
        [TestCategory("Bug7836")]
        public void ShouldUpdateEventNotifierOnMachineSide_WhenItIsUpdatedInTheUi()
        {
            machine.PhysicalMachineSettings = new EmPhysicalMachineSettings()
            {
                EventNotifierIp = new IPEndPoint(9,6)
            };
            var addressUpdated = false;
            var portUpdated = false;

            plcCommunicatorMock.Setup(m => m.WriteValue(It.IsAny<PacksizePlcVariable>(), It.IsAny<object>(), It.IsAny<int>()))
                .Callback<PacksizePlcVariable, object, int>(
                    (variable, value, someInt) =>
                    {
                        if (variable.VariableName == variableMap.EventNotifierAddress.VariableName)
                        {
                            Specify.That(value as string).Should.BeEqualTo(new IPAddress(1).ToString());
                            addressUpdated = true;
                        }
                        else if (variable.VariableName == variableMap.EventNotifierPort.VariableName)
                        {
                            Specify.That(value).Should.BeEqualTo(123);
                            portUpdated = true;
                        }
                        else
                        {
                            Specify.That(true).Should.BeFalse(String.Format("Unkown variable write caught var:{0}, value:{1}", variable.VariableName, value));
                        }
                    });

            aggregator.Publish(new Message<Common.Interfaces.DTO.Settings.PackNetServerSettings>()
            {
                MessageType = PackNetServerSettingsMessages.ServerSettingsUpdated,
                Data = new Common.Interfaces.DTO.Settings.PackNetServerSettings
                {
                    MachineCallbackIpAddress = new IPAddress(1),
                    MachineCallbackPort = 123
                },
            });
            
            Retry.For(() => addressUpdated && portUpdated, TimeSpan.FromSeconds(1));
            Specify.That(portUpdated).Should.BeTrue();
            Specify.That(addressUpdated).Should.BeTrue();
        }

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void ShouldPublishMessageToAggregatorWhenMachineCommandIsDone()
        //{
        //    var eventCalled = false;
        //    aggregator.GetEvent<Message<Guid>>().Where(msg => msg.MessageType == MachineCommandMessages.MachineCommandDoneResponse).Subscribe(
        //        (msg) =>
        //        {
        //            Specify.That(msg.Data).Should.BeEqualTo(machine.Id);
        //            eventCalled = true;
        //        });

        //    aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(variableMap.MachineCommandDone.VariableName, true) });
        //    Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));
        //    Specify.That(eventCalled).Should.BeTrue();
        //}

        private void SetUpErrorCodeTest(int errorCodeIndex)
        {
            plcCommunicatorMock.Setup(s => s.ReadValue<ushort>(It.Is<PacksizePlcVariable>(v => v.VariableName == "ERROR_CODES[" + errorCodeIndex.ToString() + "].Code"), It.IsAny<int>())).Returns(1);
            plcCommunicatorMock.Setup(s => s.ReadValue<double[]>(It.Is<PacksizePlcVariable>(v => v.VariableName == "ERROR_CODES[" + errorCodeIndex.ToString() + "].Arguments"), It.IsAny<int>())).Returns(new double[] { 1, 2, 3, 4, 5 });
        }

        private void ClearErrorCodeForTest(int errorCodeIndex)
        {
            plcCommunicatorMock.Setup(s => s.ReadValue<ushort>(It.Is<PacksizePlcVariable>(v => v.VariableName == "ERROR_CODES[" + errorCodeIndex.ToString() + "].Code"), It.IsAny<int>())).Returns(0);
            plcCommunicatorMock.Setup(s => s.ReadValue<double[]>(It.Is<PacksizePlcVariable>(v => v.VariableName == "ERROR_CODES[" + errorCodeIndex.ToString() + "].Arguments"), It.IsAny<int>())).Returns(new double[] { 0,0,0,0,0 });
        }
    }

    class BaseMachinVariableMap : PacksizeBaseMachineVariableMap
    {
        public BaseMachinVariableMap()
        {
            
        }
    }

    class MachineCommunicatorBaseTester : PacksizeMachineCommunicatorBase
    {
        public MachineCommunicatorBaseTester(IPacksizeCutCreaseMachine machine, IEventAggregatorPublisher publisher,
            IEventAggregatorSubscriber subscriber, IPLCHeartBeatCommunicator communicator, IPackagingMachineVariableMap variableMap,
            ILogger logger)
            : base(machine, publisher, subscriber, communicator, variableMap, logger)
        {
        }

        public int OnVariableChangedCallCount { get; set; }

        public void SetOrder(IPacksizeCarton carton)
        {
            Order = carton;
        }

        public override void OnVariableChanged(VariableChangedEventArgs e)
        {
            OnVariableChangedCallCount++;
            base.OnVariableChanged(e);
        }

        protected override void GetSettingsFromMachine()
        {
            
        }

        public override void SynchronizeMachine(bool isReloadMachineConfig = false)
        {
        }
    }
}
