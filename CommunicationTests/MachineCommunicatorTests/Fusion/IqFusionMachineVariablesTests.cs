﻿using System;
using System.Linq;
using System.Linq.Expressions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Communication.Communicators.Fusion;

using Testing.Specificity;

namespace PackNet.CommunicationTests.MachineCommunicatorTests.Fusion
{
    [TestClass]
    public class IqFusionMachineVariablesTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 8968")]
        public void PlcVariablesShouldNotContainDuplicates()
        {
            Specify.That(IqFusionMachineVariables.Instance.AsEnumerable().Count()).Should.BeEqualTo(IqFusionMachineVariables.Instance.AsEnumerable().Distinct().Count(), "Variable map should not contain duplicates");
        }

        [TestMethod]
        public void RunPrestartTest()
        {
            VerifyMappingFor(var => var.RunPrestart, "Main", "RUN_PRESTART", 1);
        }

        [TestMethod]
        public void HostNameMappingTest()
        {
            VerifyMappingFor(var => var.HostName, "RestPostSe", "hostName", 2);
        }

        [TestMethod]
        public void EventNotifierAddressTest()
        {
            VerifyMappingFor(var => var.EventNotifierAddress, "VariableEv", "eventNotifierAddress", 2);
        }

        [TestMethod]
        public void EventNotifierPortTest()
        {
            VerifyMappingFor(var => var.EventNotifierPort, "VariableEv", "eventNotifierPort", 2);
        }

        [TestMethod]
        public void CrossHeadHomingPositionMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadHomingPosition, "Main", "CH_SETTINGS.HomingPosition", 3);
        }

        [TestMethod]
        public void LeftRollerDisconnectPositionMappingTest()
        {
            VerifyMappingFor(var => var.LeftRollerDisconnectPosition, "Main", "FEED_ROLLER_SETTINGS.LeftRollerDisconnectPosition", 3);
        }

        [TestMethod]
        public void CrossHeadSensorToToolMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadTrackSensorToTool, "Main", "CH_SETTINGS.TrackSensorToKnifeOffset", 2);
        }

        [TestMethod]
        public void RightRollerDisconnectPositionMappingTest()
        {
            VerifyMappingFor(var => var.RightRollerDisconnectPosition, "Main", "FEED_ROLLER_SETTINGS.RightRollerDisconnectPosition", 3);
        }

        [TestMethod]
        public void FeedRollerSpeedMappingTest()
        {
            VerifyMappingFor(var => var.FeedRollerSpeedInPercent, "Main", "FEED_ROLLER_SETTINGS.SpeedInPercent", 2);
        }

        [TestMethod]
        public void FeedRollerAccelerationMappingTest()
        {
            VerifyMappingFor(var => var.FeedRollerAccelerationInPercent, "Main", "FEED_ROLLER_SETTINGS.AccelerationInPercent", 2);
        }

        [TestMethod]
        public void FeedRollerLoweringOffsetMappingTest()
        {
            VerifyMappingFor(var => var.FeedRollerLoweringOffset, "Main", "FEED_ROLLER_SETTINGS.LoweringOffset", 2);
        }

        [TestMethod]
        public void FeedRollerCleanCutLengthMappingTest()
        {
            VerifyMappingFor(var => var.FeedRollerCleanCutLength, "Main", "FEED_ROLLER_SETTINGS.CleanCutLength", 2);
        }

        [TestMethod]
        public void FeedRollerOutFeedLengtMappingTest()
        {
            VerifyMappingFor(var => var.FeedRollerOutFeedLength, "Main", "FEED_ROLLER_SETTINGS.OutFeedLength", 2);
        }

        [TestMethod]
        public void FeedRollerGearRatioTest()
        {
            VerifyMappingFor(var => var.FeedRollerGearRatio, "Main", "FEED_ROLLER_SETTINGS.GearRatio", 2);
        }

        [TestMethod]
        public void CrossHeadSpeedMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadSpeedInPercent, "Main", "CH_SETTINGS.SpeedInPercent", 2);
        }

        [TestMethod]
        public void CrossHeadAccelerationMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadAccelerationInPercent, "Main", "CH_SETTINGS.AccelerationInPercent", 2);
        }

        [TestMethod]
        public void CrossHeadMinimumPositionMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadMinimumPosition, "Main", "CH_SETTINGS.MinPosition", 2);
        }

        [TestMethod]
        public void CrossHeadMaximumPositionMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadMaximumPosition, "Main", "CH_SETTINGS.MaxPosition", 2);
        }

        [TestMethod]
        public void CrossHeadMaximumSpeedMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadMaximumSpeed, string.Empty, "chAxis.move.basis.parameter.v_pos", 2);
        }

        [TestMethod]
        public void CrossHeadMaximumAccelerationMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadMaximumAcceleration, string.Empty, "chAxis.move.basis.parameter.a1_pos", 2);
        }

        [TestMethod]
        public void CrossHeadMaximumDecelerationMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadMaximumDeceleration, string.Empty, "chAxis.move.basis.parameter.a2_pos", 2);
        }

        [TestMethod]
        public void CrossHeadSensorToKnifeMappingTest()
        {
            VerifyMappingFor(var => var.CrossHeadSensorToKnife, "Main", "CH_SETTINGS.SensorToKnifeOffset", 2);
        }

        [TestMethod]
        public void ErrorCode1MappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode1, "Main", "ERROR_CODES[0].Code", 1, true);
        }

        [TestMethod]
        public void ErrorCode1ArgumentsMappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode1Arguments, "Main", "ERROR_CODES[0].Arguments", 1);
        }

        [TestMethod]
        public void ErrorCode2MappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode2, "Main", "ERROR_CODES[1].Code", 1, true);
        }

        [TestMethod]
        public void ErrorCode2ArgumentsMappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode2Arguments, "Main", "ERROR_CODES[1].Arguments", 1);
        }

        [TestMethod]
        public void ErrorCode3MappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode3, "Main", "ERROR_CODES[2].Code", 1, true);
        }

        [TestMethod]
        public void ErrorCode3ArgumentsMappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode3Arguments, "Main", "ERROR_CODES[2].Arguments", 1);
        }

        [TestMethod]
        public void ErrorCode4MappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode4, "Main", "ERROR_CODES[3].Code", 1, true);
        }

        [TestMethod]
        public void ErrorCode4ArgumentsMappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode4Arguments, "Main", "ERROR_CODES[3].Arguments", 1);
        }

        [TestMethod]
        public void ErrorCode5MappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode5, "Main", "ERROR_CODES[4].Code", 1, true);
        }

        [TestMethod]
        public void ErrorCode5ArgumentsMappingTest()
        {
            VerifyMappingFor(var => var.ErrorCode5Arguments, "Main", "ERROR_CODES[4].Arguments", 1);
        }

        [TestMethod]
        public void MachineWarningTest()
        {
            VerifyMappingFor(var => var.WhiteLEDState, "IOControll", "WhiteLEDSignal", 3);
        }

        [TestMethod]
        public void EmergencyStopTest()
        {
            VerifyMappingFor(var => var.RedLEDState, "IOControll", "RedLEDSignal", 3);
        }

        [TestMethod]
        public void LongHeadNumberOfLongheadsMappingTest()
        {
            VerifyMappingFor(var => var.NumberOfLongHeads, "Main", "LH_SETTINGS.NumberOfLongHeads", 2);
        }

        [TestMethod]
        public void LongHeadConnectionDelayMappingTest()
        {
            VerifyMappingFor(var => var.LongHeadConnectionDelay, "Main", "LH_SETTINGS.ConnectionDelay", 2);
        }

        [TestMethod]
        public void LongHeadPositionsMappingTest()
        {
            VerifyMappingFor(var => var.LongHeadPositions, "Main", "LH_SETTINGS.Positions", 1);
        }

        [TestMethod]
        public void OutOfCorrugateMappingTest()
        {
            VerifyMappingFor(var => var.OutOfCorrugate, "Main", "outOfCorrugate", 3, true);
        }

        [TestMethod]
        public void ChangeCorrugateMappingTest()
        {
            VerifyMappingFor(var => var.ChangeCorrugate, "Main", "changeCorrugate", 3, true);
        }

        [TestMethod]
        public void TrackOffsetMappingTest()
        {
            VerifyMappingFor(var => var.TrackOffset, "Main", "TRACK_SETTINGS.TrackStartingPositions", 1, true);
        }

        [TestMethod]
        public void TrackWidthsMappingTest()
        {
            VerifyMappingFor(var => var.TrackWidths, "Main", "TRACK_SETTINGS.TrackWidth", 2);
        }

        [TestMethod]
        public void TrackSensorPlateToCorrugateMappingTest()
        {
            VerifyMappingFor(var => var.TrackSensorPlateToCorrugateTrack1, "Main", "TRACK_SETTINGS.SensorPlateToCorrugateTrack1", 2);
            VerifyMappingFor(var => var.TrackSensorPlateToCorrugateTrack2, "Main", "TRACK_SETTINGS.SensorPlateToCorrugateTrack2", 2);
        }

        [TestMethod]
        public void TrackActivationDelayMappingTest()
        {
            VerifyMappingFor(var => var.TrackActivationDelay, "Main", "TRACK_SETTINGS.ActivationDelay", 2);
        }

        [TestMethod]
        public void TrackIsRightSideFixedMappingTest()
        {
            VerifyMappingFor(var => var.TrackIsRightSideFixed, "Main", "TRACK_SETTINGS.IsRightSideFixed", 2);
        }

        [TestMethod]
        public void TrackSensorCompensationLeft1MappingTest()
        {
            VerifyMappingFor(var => var.TrackSensorCompensationLeftTrack1, "Main", "TRACK_SETTINGS.SensorCompensationLeft.Track1", 2);
        }

        [TestMethod]
        public void TrackSensorCompensationRight1MappingTest()
        {
            VerifyMappingFor(var => var.TrackSensorCompensationRightTrack1, "Main", "TRACK_SETTINGS.SensorCompensationRight.Track1", 2);
        }

        [TestMethod]
        public void TrackSensorCompensationLeft2MappingTest()
        {
            VerifyMappingFor(var => var.TrackSensorCompensationLeftTrack2, "Main", "TRACK_SETTINGS.SensorCompensationLeft.Track2", 2);
        }

        [TestMethod]
        public void TrackSensorCompensationRight2MappingTest()
        {
            VerifyMappingFor(var => var.TrackSensorCompensationRightTrack2, "Main", "TRACK_SETTINGS.SensorCompensationRight.Track2", 2);
        }

        [TestMethod]
        public void TrackOutOfCorrugatePositionMappingTest()
        {
            VerifyMappingFor(var => var.TrackOutOfCorrugatePositions, "Main", "TRACK_SETTINGS.OutOfCorrugatePositions", 2);
        }

        [TestMethod]
        public void TrackSideSteeringToleranceTest()
        {
            VerifyMappingFor(var => var.TrackSideSteeringTolerance, "Main", "TRACK_SETTINGS.SideSteeringTolerance", 2);
        }

        [TestMethod]
        public void TrackOffsetToleranceTest()
        {
            VerifyMappingFor(var => var.TrackOffsetTolerance, "Main", "TRACK_SETTINGS.OffsetTolerance", 2);
        }

        [TestMethod]
        public void CorrugateWidthToleranceTest()
        {
            VerifyMappingFor(var => var.CorrugateWidthTolerance, "Main", "TRACK_SETTINGS.CorrugateWidthTolerance", 2);
        }

        [TestMethod]
        public void TransferBufferMappingTest()
        {
            VerifyMappingFor(var => var.TransferBuffer, string.Empty, "PcTransferBuffer", 3);
        }

        [TestMethod]
        public void ProductionHistoryItemCompletionStatusMappingTest()
        {
            VerifyMappingFor(var => var.ProductionHistoryItemCompletionStatus, "Main", "CurrentProductionHistoryItem.CompletionStatus", 1);
        }

        [TestMethod]
        public void ProductionHistoryItemIdMappingTest()
        {
            VerifyMappingFor(var => var.ProductionHistoryItemId, "Main", "CurrentProductionHistoryItem.Id.Parts", 1, true);
        }

        [TestMethod]
        public void PacksizePlcVariableMappingTest()
        {
            VerifyMappingFor(var => var.PlcLifeSign, "PCCommunic", "PlcLifeSign", 3);
        }

        [TestMethod]
        public void PacksizePcVariableMappingTest()
        {
            VerifyMappingFor(var => var.PcLifeSign, "PCCommunic", "PcLifeSign", 3);
        }
        
        [TestMethod]
        public void StartupProcedureCompletedMappingTest()
        {
            VerifyMappingFor(var => var.StartupProcedureCompleted, "Main", "StartupProcedureCompleted", 1, true);
        }

        [TestMethod]
        public void RemovedProductionItemIdMappingTest()
        {
            VerifyMappingFor(var => var.RemovedProductionItemId1, "Main", "RemovedProductionItemId[0].Parts", 1, true);
            VerifyMappingFor(var => var.RemovedProductionItemId2, "Main", "RemovedProductionItemId[1].Parts", 1, true);
        }

        [TestMethod]
        public void StartedProductionItemIdMappingTest()
        {
            VerifyMappingFor(var => var.StartedProductionItemId, "Main", "StartedProductionItemId.Parts", 1, true);
        }

        [TestMethod]
        public void ToolAxisScalingTest()
        {
            VerifyMappingFor(var => var.ToolAxisScaling, "Main", "chAxis.encoder_if.parameter.scaling.load.units", 2);
        }

        [TestMethod]
        public void RollAxisScalingTest()
        {
            VerifyMappingFor(var => var.RollAxisScaling, "Main", "rollAxis.encoder_if.parameter.scaling.load.units", 2);
        }       

        private static void VerifyMappingFor(Expression<Func<IqFusionMachineVariables, PacksizePlcVariable>> property, string taskName, string variableName, int accessMode, bool notifyChanges = false)
        {
            var sut = property.Compile().Invoke(IqFusionMachineVariables.Instance);
            Assert.IsNotNull(sut, "Variable was not mapped");
            Assert.AreEqual(sut.VariableName, variableName, "Variable name was not mapped as expected");
            Assert.AreEqual(sut.TaskName, taskName, "Task name was not mapped as expected");
            Assert.AreEqual(sut.NotifyChanges, notifyChanges, "NotifyChanges was not mapped as expected");
        }
    }
}
