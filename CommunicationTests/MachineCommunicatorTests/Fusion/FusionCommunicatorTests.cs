﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.CodeGenerator.InstructionList;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.Fusion;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Communication;
using PackNet.Communication.Communicators.Fusion;

using Testing.Specificity;

namespace PackNet.CommunicationTests.MachineCommunicatorTests.Fusion
{
    [TestClass]
    [DeploymentItem(@"MachineCommunicatorTests/Fusion", @"FusionData")]
    public class FusionCommunicatorTests
    {
        private FusionCommunicator communicator;
        private readonly IPEndPoint ipEndPoint;
        private readonly EventAggregator eventAggregator = new EventAggregator();
        private Mock<ILogger> logger = new Mock<ILogger>();
        private Mock<IPLCHeartBeatCommunicator> plcCommunicatorMock;
        private readonly FusionMachine machine = new FusionMachine();
        private readonly Mock<IMachineInstructionCreator<FusionPhysicalMachineSettings>> jobCreator = new Mock<IMachineInstructionCreator<FusionPhysicalMachineSettings>>();
        
        public FusionCommunicatorTests()
        {
            IPAddress ip;
            IPAddress.TryParse("127.0.0.1", out ip);
            ipEndPoint = new IPEndPoint(ip, 9998);
            machine.IpOrDnsName = "127.0.0.1";
            machine.Port = 80;
            machine.Tracks = new ConcurrentList<Track>()
            {
                new Track()
                {
                    TrackNumber = 1
                },
                new Track()
                {
                    TrackNumber = 2
                },
            };
        }

        [TestInitialize]
        public void TestSetUp()
        {
            logger = new Mock<ILogger>();
            plcCommunicatorMock = new Mock<IPLCHeartBeatCommunicator>();
            plcCommunicatorMock.SetupGet(x => x.PlcIpAddressWithPort).Returns("127.0.0.1");


            communicator = new FusionCommunicatorTester(machine, eventAggregator, eventAggregator, plcCommunicatorMock.Object,
                IqFusionMachineVariables.Instance, logger.Object);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddProductionItemToQueueWithIdWhenCallingProduce()
        {
            var variableMap = IqFusionMachineVariables.Instance;
            var service = new Mock<IPLCHeartBeatCommunicator>();

            service.Setup(s => s.WriteValue(variableMap.TransferBuffer, It.IsAny<PLCProducible>(), It.IsAny<int>())).Verifiable();
            service.Setup(s => s.WriteValue(variableMap.ReadyForProcessing, true, It.IsAny<int>())).Verifiable();

            service.Setup(s => s.ReadValue<bool>(variableMap.StartupProcedureCompleted, -1)).Returns(true);

            jobCreator.Setup(m => m.GetInstructionListForJob(It.IsAny<IPacksizeCarton>(), It.IsAny<PhysicalDesign>(), It.IsAny<Track>(), It.IsAny<FusionPhysicalMachineSettings>())).Returns(new List<InstructionItem>());


            communicator = new FusionCommunicator(this.machine, this.eventAggregator, this.eventAggregator, service.Object, IqFusionMachineVariables.Instance, this.logger.Object);

            var job = new Carton
            {
                TrackNumber = 1
            };

            communicator.Produce(job, new List<InstructionItem>());

            service.VerifyAll();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void RunPrestartTest()
        {
            var service = new Mock<IPLCHeartBeatCommunicator>();
            var variables = IqFusionMachineVariables.Instance;

            machine.PhysicalMachineSettings = FusionPhysicalMachineSettings.CreateFromFile(ipEndPoint, @"FusionData/machineExample.xml");
           
            communicator = new FusionCommunicator(this.machine, this.eventAggregator, this.eventAggregator, service.Object, variables, this.logger.Object);
            communicator.SynchronizeMachine();

            service.Verify(s => s.WriteValue(variables.RunPrestart, true, It.IsAny<int>()), Times.Exactly(1));
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void SyncronizeMachineSetsExpectedVariables()
        {
            var mockService = new Mock<IPLCHeartBeatCommunicator>();
            var machineSettings = FusionPhysicalMachineSettings.CreateFromFile(ipEndPoint, @"FusionData/machineExample.xml");
            
            machine.PhysicalMachineSettings = machineSettings;
            machine.NumTracks = 2;

            var vars = IqFusionMachineVariables.Instance;

            AddTrackWidthsToMachine(machine);

            communicator = new FusionCommunicator(this.machine, this.eventAggregator, this.eventAggregator, mockService.Object, vars, this.logger.Object);
            communicator.SynchronizeMachine();

            mockService.Verify(s => s.WriteValue(vars.EventNotifierAddress, "127.0.0.1", It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.EventNotifierPort, 9998, It.IsAny<int>()));

            mockService.Verify(s => s.WriteValue(vars.StartupProcedureCompleted, false, It.IsAny<int>()));

            Retry.For(() => mockService.Verify(s => s.WriteValue(vars.TrackSensorPlateToCorrugateTrack1, new[] { 0.95, 1.12 }, It.IsAny<int>())), TimeSpan.FromSeconds(5));
            mockService.Verify(s => s.WriteValue(vars.TrackSensorPlateToCorrugateTrack2, new[] { 0.88, 0.99 }, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackActivationDelay, (double)50, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackSideSteeringTolerance, (double)30, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackOffsetTolerance, (double)5, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackLatchMinimumDistance, 12.7, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackIsRightSideFixed, new[] { true, false }, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackSensorCompensationLeftTrack1, new[] { 9.11, 8.66, 8.07, 7.39, 6.61, 6.02, 6.02, 5.99, 6.04, 6.0 }, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackSensorCompensationRightTrack1, new[] { 10.5, 9.95, 9.41, 8.81, 8.29, 7.71, 7.13, 6.61, 6.02, 5.53 }, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackSensorCompensationLeftTrack2, new[] { 9.36, 8.79, 8.25, 7.76, 7.17, 6.67, 5.88, 5.48, 4.92, 4.36 }, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackSensorCompensationRightTrack2, new[] { 8.87, 8.2, 7.61, 6.95, 6.31, 5.65, 5.53, 5.46, 5.51, 5.57 }, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.TrackOutOfCorrugatePositions, new[] { 120.0, 1250.0 }, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CorrugateWidthTolerance, (double)7, It.IsAny<int>()));

            mockService.Verify(s => s.WriteValue(vars.NumberOfLongHeads, 4, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.LongHeadConnectionDelay, (double)150, It.IsAny<int>()));

            mockService.Verify(s => s.WriteValue(vars.CrossHeadMaximumPosition, 1300D, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CrossHeadMinimumPosition, -4D, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CrossHeadSpeedInPercent, 11, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CrossHeadAccelerationInPercent, 99, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CrossHeadSensorToKnife, 3D, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CrossHeadTrackSensorToTool, 7D, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CrossHeadMaximumSpeed, 180000, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CrossHeadMaximumAcceleration, 1000000, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CrossHeadMaximumDeceleration, 1000000, It.IsAny<int>()));

            mockService.Verify(s => s.WriteValue(vars.TrackWidths, new double[] { 430, 610 }, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.CorrugateThicknesses, new double[] { 3, 4 }, It.IsAny<int>()));

            mockService.Verify(s => s.WriteValue(vars.FeedRollerLoweringOffset, 50D, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.FeedRollerCleanCutLength, 60D, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.FeedRollerOutFeedLength, 70D, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.FeedRollerSpeedInPercent, 33D, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.FeedRollerAccelerationInPercent, 77D, It.IsAny<int>()));
            mockService.Verify(s => s.WriteValue(vars.FeedRollerGearRatio, 1.0, It.IsAny<int>()));

            mockService.Verify(s => s.WriteValue(vars.CrossHeadHomingPosition, 123.5, It.IsAny<int>()));

            mockService.Verify(s => s.WriteValue(vars.ClearMachineErrors, true, It.IsAny<int>()));

            mockService.Verify(s => s.WriteValue(vars.RunPrestart, true, It.IsAny<int>()));

            //TODO add back in when 9660
          //  mockService.Verify(s => s.WriteValue(vars.IsInches, true, It.IsAny<int>()));
        }

        
        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 10375")]
        public void Bug10442_PoweredOnCorrugateChangeShouldWriteCorrectRightSideFixedValuesToMachine()
        {
            machine.NumTracks = 2;
            communicator.SetAlignments();

            plcCommunicatorMock.Verify(
                m => m.WriteValue(IqFusionMachineVariables.Instance.TrackIsRightSideFixed, new[] { true, false }, -1), Times.Once);

            machine.NumTracks = 1;
            communicator.SetAlignments();

            plcCommunicatorMock.Verify(
                m => m.WriteValue(IqFusionMachineVariables.Instance.TrackIsRightSideFixed, new[] { false, false }, -1), Times.Once);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void SyncronizeMachineGetsExpectedVariablesFromPlc()
        {
            var mockService = new Mock<IPLCHeartBeatCommunicator>();
            var machineSettings = FusionPhysicalMachineSettings.CreateFromFile(ipEndPoint, @"FusionData/machineExample.xml");
            machine.PhysicalMachineSettings = machineSettings;

            var vars = IqFusionMachineVariables.Instance;

            mockService
                .Setup(m => m.WriteValue(vars.RunPrestart, It.IsAny<object>(), It.IsAny<int>()))
                .Callback(() => new Thread(InvokeEvent).Start());

            mockService.Setup(m => m.ReadValue<double[]>(vars.TrackOffset, It.IsAny<int>())).Returns(new double[] { 500, 700 });
            var longheadPositions = new[] { 130.5, 436.4, 500.4, 640.7 };
            mockService.Setup(m => m.ReadValue<double[]>(vars.LongHeadPositions, It.IsAny<int>())).Returns(longheadPositions);

            communicator = new FusionCommunicator(this.machine, this.eventAggregator, this.eventAggregator, mockService.Object, vars, this.logger.Object);
            bool startUpProcedureDone = false;

            communicator.SynchronizeMachine();

            Retry.For(() => startUpProcedureDone, TimeSpan.FromSeconds(2));

            Specify.That(machine.Tracks.Single(t => t.TrackNumber == 1).TrackOffset).Should.BeLogicallyEqualTo(500);
            Specify.That(machine.Tracks.Single(t => t.TrackNumber == 2).TrackOffset).Should.BeLogicallyEqualTo(700);

            var longheads = machineSettings.LongHeadParameters.LongHeads;
            var sensorOffset = machineSettings.LongHeadParameters.LeftSideToSensorPin;
            for (var i = 0; i < longheadPositions.Length; i++)
            {
                Assert.AreEqual(
                    GetLongHeadToolPositionFromSensorPosition(longheads.ElementAt(i), sensorOffset, longheadPositions[i]),
                    longheads.ElementAt(i).Position);
            }

            var homingpos = machineSettings.CrossHeadParameters.HomingPosition;

            Specify.That(homingpos).Should.BeLogicallyEqualTo(123.5);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_SynchronizeMachine_OnAggregatorEvent()
        {
            this.machine.PhysicalMachineSettings = FusionPhysicalMachineSettings.CreateFromFile(ipEndPoint, @"FusionData/machineExample.xml");

            eventAggregator.Publish(new Message<Tuple<string, bool>>()
            {
                MessageType = SimplePLCMessages.SimplePLCConnectionStatus,
                Data = new Tuple<string, bool>("127.0.0.1", true)
            });
            
            Retry.For(() =>
            {
                plcCommunicatorMock.Verify(s => s.WriteValue(IqFusionMachineVariables.Instance.RunPrestart, true, It.IsAny<int>()));
                return true;
            }, TimeSpan.FromSeconds(2));
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_SetRightSideFixed_WhenSynchronizing_SingleOrMultiTrack()
        {
            var service = new Mock<IPLCHeartBeatCommunicator>();
            var variables = IqFusionMachineVariables.Instance;

            machine.PhysicalMachineSettings = FusionPhysicalMachineSettings.CreateFromFile(ipEndPoint, @"FusionData/machineExample.xml");

            machine.NumTracks = 1;
            communicator = new FusionCommunicator(this.machine, this.eventAggregator, this.eventAggregator, service.Object, variables, this.logger.Object);
            communicator.SynchronizeMachine();
            Specify.That((machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).TrackParameters.Tracks.All(t => t.RightSideFixed == false)).Should.BeTrue();

            machine.NumTracks = 2;
            communicator = new FusionCommunicator(this.machine, this.eventAggregator, this.eventAggregator, service.Object, variables, this.logger.Object);
            communicator.SynchronizeMachine();
            Specify.That((machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).TrackParameters.Tracks.ElementAt(0).RightSideFixed == true).Should.BeTrue();
            Specify.That((machine.PhysicalMachineSettings as FusionPhysicalMachineSettings).TrackParameters.Tracks.ElementAt(1).RightSideFixed == false).Should.BeTrue();
        }


        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldGetMachineErrors_FromMachine_WhenMachine_ComesOnline()
        {
            this.machine.PhysicalMachineSettings = FusionPhysicalMachineSettings.CreateFromFile(ipEndPoint, @"FusionData/machineExample.xml");

            for (int i = 0; i < 5; i++)
            {
                SetupErrorOnVariable(i);

                eventAggregator.Publish(new Message<Tuple<string, bool>>()
                {
                    MessageType = SimplePLCMessages.SimplePLCConnectionStatus,
                    Data = new Tuple<string, bool>("127.0.0.1", true)
                });

               // Retry.For(() => machine.CurrentStatus.Value.Equals(MachineErrorStatuses.MachineError), TimeSpan.FromSeconds(2));
                Specify.That(machine.Errors.Count).Should.BeEqualTo(1);
                Specify.That(machine.Errors.First().Error).Should.BeEqualTo(MachineErrorStatuses.EmergencyStop);
                Specify.That(machine.Errors.First().Arguments).Should.Not.BeNull();
                Specify.That(machine.Errors.First().Arguments).Should.Not.BeEmpty();


                TearDownErrorOnVariableSetup(i);
            }
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldAddMachineVersionCapabilityWhenSynchronizingMachine()
        {
            plcCommunicatorMock.Setup(m => m.ReadValue<string>(IqFusionMachineVariables.Instance.MachinePlcVersion, It.IsAny<int>())).Returns("BestVersion");
            machine.PhysicalMachineSettings = FusionPhysicalMachineSettings.CreateFromFile(ipEndPoint, @"FusionData/machineExample.xml");
           

            eventAggregator.Publish(new Message<Tuple<string, bool>>()
            {
                MessageType = SimplePLCMessages.SimplePLCConnectionStatus,
                Data = new Tuple<string, bool>("127.0.0.1", true)
            });

            Retry.For(() =>
            {
                plcCommunicatorMock.Verify(s => s.ReadValue<string>(IqFusionMachineVariables.Instance.MachinePlcVersion, It.IsAny<int>()));
                return true;
            }, TimeSpan.FromSeconds(2));

            Specify.That(this.machine.PlcVersion).Should.BeEqualTo("BestVersion"); 
        }

        private void TearDownErrorOnVariableSetup(int errorCodeIndex)
        {
            plcCommunicatorMock.Setup(s => s.ReadValue<ushort>(It.Is<PacksizePlcVariable>(v => v.VariableName == "ERROR_CODES[" + errorCodeIndex.ToString() + "].Code"), It.IsAny<int>())).Returns(0);
            plcCommunicatorMock.Setup(s => s.ReadValue<double[]>(It.Is<PacksizePlcVariable>(v => v.VariableName == "ERROR_CODES[" + errorCodeIndex.ToString() + "].Arguments"), It.IsAny<int>())).Returns(new double[] { 1, 2, 3, 4, 5 });
        }

        private void SetupErrorOnVariable(int errorCodeIndex)
        {
            plcCommunicatorMock.Setup(s => s.ReadValue<ushort>(It.Is<PacksizePlcVariable>(v => v.VariableName == "ERROR_CODES[" + errorCodeIndex.ToString() + "].Code"), It.IsAny<int>())).Returns(1);
            plcCommunicatorMock.Setup(s => s.ReadValue<double[]>(It.Is<PacksizePlcVariable>(v => v.VariableName == "ERROR_CODES[" + errorCodeIndex.ToString() + "].Arguments"), It.IsAny<int>())).Returns(new double[] { 1, 2, 3, 4, 5 });
        }

        private MicroMeter GetLongHeadToolPositionFromSensorPosition(FusionLongHead longHead, MicroMeter sensorOffset, double sensorPosition)
        {
            return sensorPosition - sensorOffset + longHead.LeftSideToTool;
        }

        private void InvokeEvent()
        {
            Thread.Sleep(500);
            communicator.OnVariableChanged(new VariableChangedEventArgs("StartupProcedureCompleted", true));
        }

        private static void AddTrackWidthsToMachine(ITrackBasedCutCreaseMachine machine)
        {
            machine.Tracks.Single(t => t.TrackNumber == 1).LoadedCorrugate = new Corrugate() { Width = 430, Thickness = 3 };
            machine.Tracks.Single(t => t.TrackNumber == 2).LoadedCorrugate = new Corrugate() { Width = 610, Thickness = 4 };
        }
    }

    class FusionCommunicatorTester : FusionCommunicator
    {
        public FusionCommunicatorTester(FusionMachine machine, IEventAggregatorPublisher publisher, IEventAggregatorSubscriber subscriber, IPLCHeartBeatCommunicator communicator, IFusionMachineVaribleMap variableMap, ILogger logger)
            : base(machine, publisher, subscriber, communicator, variableMap, logger)
        {
        }

        public void SetOrder(IPacksizeCarton carton)
        {
            Order = carton;
        }
    }
}
