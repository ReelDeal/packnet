﻿using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.Enums;
using PackNet.Communication.Communicators;

using Testing.Specificity;

namespace PackNet.CommunicationTests.MachineCommunicatorTests
{
    [TestClass]
    public class ErrorFactoryTests
    {
        [TestMethod]
        public void ShouldBeAbleToCreateErrorsOfAllErrorTypes()
        {
            //TODO supply with real arguments and fix additional info

            var error = ErrorFactory.Create(1, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(MachineErrorStatuses.EmergencyStop);
            Specify.That(error.Arguments).Should.BeNull();

            error = ErrorFactory.Create(2, new double[] { 1, 2, 3, 4, 5 }, 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.RollAxis);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "1", "2", "3", "4", "5" });
            
            error = ErrorFactory.Create(3, new double[] { 1, 2, 3, 4, 5 }, 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.ToolAxis);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "1", "2", "3", "4", "5" });
            
            error = ErrorFactory.Create(5, new double[] { 1 }, 1);
            Specify.That(error.Error).Should.BeEqualTo(PackagingMachineErrorStatuses.OutOfCorrugate);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "1" });
            
            error = ErrorFactory.Create(6, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.PaperJam);
            Specify.That(error.Arguments).Should.BeNull();
            
            error = ErrorFactory.Create(7, new double[] { 1, 2 }, 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.CorrugateMismatch);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "1", "2" });
            
            error = ErrorFactory.Create(8, new double[] { 1, 2 }, 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.LongHeadQuantity);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "1", "2" });
            
            error = ErrorFactory.Create(9, new double[] { 1, 2 }, 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.LongHeadPosition);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "1", "2" });
            
            error = ErrorFactory.Create(10, new double[] { 1, 2, 3 }, 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.TrackOffset);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "1", "2", "3" });
            
            error = ErrorFactory.Create(11, new double[] { 1, 2, 3, 4 }, 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.TrackQuantity);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "1", "2", "3", "4" });
            
            error = ErrorFactory.Create(12, new double[] {1}, 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.InstructionList);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "1" });
            
            error = ErrorFactory.Create(13, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.MachineError);
            Specify.That(error.Arguments).Should.BeNull();
            
            error = ErrorFactory.Create(15, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.AirPressureLow);
            Specify.That(error.Arguments).Should.BeNull();
            
            error = ErrorFactory.Create(16, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.MovablePressureRollerOutOfPosition);
            Specify.That(error.Arguments).Should.BeNull();
            
            error = ErrorFactory.Create(17, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(MachinePausedStatuses.LightBarrierBroken);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> {"Front"});
            
            error = ErrorFactory.Create(18, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.ReferenceQuantityMissmatch);
            Specify.That(error.Arguments).Should.BeNull();
            
            error = ErrorFactory.Create(19, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.LongHeadPositionOutsideOfTolerance);
            Specify.That(error.Arguments).Should.BeNull();
            
            error = ErrorFactory.Create(20, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.CrossHeadPositionOutsideOfTolerance);
            Specify.That(error.Arguments).Should.BeNull();

            error = ErrorFactory.Create(21, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(PacksizePackagingMachineErrorStatuses.PositionLatchError);
            Specify.That(error.Arguments).Should.BeNull();

            error = ErrorFactory.Create(23, new double[0], 1);
            Specify.That(error.Error).Should.BeEqualTo(MachinePausedStatuses.LightBarrierBroken);
            Specify.That(error.Arguments).Should.BeEquivalentTo(new List<string> { "Back" });

        }
    }
}
