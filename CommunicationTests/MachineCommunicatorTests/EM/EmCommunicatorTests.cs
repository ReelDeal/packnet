﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Business.Machines;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO;
using PackNet.Common.Interfaces.DTO.Carton;
using PackNet.Common.Interfaces.DTO.Corrugates;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PhysicalMachine;
using PackNet.Common.Interfaces.DTO.PhysicalMachine.EM;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Machines;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Utils;
using PackNet.Common.Utils;
using PackNet.Communication;
using PackNet.Communication.Communicators.EM;
using PackNet.Data.Machines;

using Testing.Specificity;

using Track = PackNet.Common.Interfaces.DTO.Machines.Track;

namespace PackNet.CommunicationTests.MachineCommunicatorTests.EM
{
    using PackNet.Common.Interfaces.ExtensionMethods;
    using PackNet.Communication.Communicators;

    [TestClass]
    [DeploymentItem("MachineCommunicatorTests/Em", "Em")]
    public class EmCommunicatorTests
    {
        private EmMachine machine;
        private EmCommunicator communicator;

        private Mock<IMachineCommunicatorFactory> machineCommunicatorFactoryMock;
        private Mock<IPLCHeartBeatCommunicator> heartBeatCommunicator;
        private Mock<IEmMachineRepository> repoMock;
        private Mock<ILogger> loggerMock;
        private Mock<IServiceLocator> serviceLocator;

        private IPEndPoint ipEndPoint;

        private EventAggregator aggregator;

        private EmMachines emMachines;

        [TestInitialize]
        public void Setup()
        {
            loggerMock = new Mock<ILogger>();
            serviceLocator = new Mock<IServiceLocator>();
            ipEndPoint = new IPEndPoint(new IPAddress(1), 1);
            aggregator = new EventAggregator();
            machine = new EmMachine()
            {
                Id = Guid.NewGuid(),
                IpOrDnsName = "127.0.0.1",
                Port = 80,
                Tracks = new ConcurrentList<Track> { new Track() { LoadedCorrugate = new Corrugate() { Width = 100 }, TrackOffset = 50, TrackNumber = 1 }, new Track() { LoadedCorrugate = new Corrugate() { Width = 200 }, TrackOffset = 1000, TrackNumber = 2 } }
            };

            SetupCommunication();
            SetupEmMachines();
        }

        private void SetupEmMachines()
        {

            repoMock = new Mock<IEmMachineRepository>();
            repoMock.Setup(m => m.All()).Returns(new List<EmMachine>() { machine });

            emMachines = new EmMachines(repoMock.Object, machineCommunicatorFactoryMock.Object, aggregator, aggregator, serviceLocator.Object, loggerMock.Object);
        }

        private void SetupCommunication()
        {
            machineCommunicatorFactoryMock = new Mock<IMachineCommunicatorFactory>();
            heartBeatCommunicator = new Mock<IPLCHeartBeatCommunicator>();

            communicator = new EmCommunicator(machine, aggregator, aggregator, heartBeatCommunicator.Object,
                EmMachineVariables.Instance, loggerMock.Object);

            machineCommunicatorFactoryMock.Setup(m => m.GetMachineCommunicator(It.IsAny<IMachine>()))
                .Returns(communicator);

            heartBeatCommunicator.SetupGet(x => x.PlcIpAddressWithPort).Returns("127.0.0.1");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void SynchronizeMachine_SetsExpectedValues()
        {
            Assert.Inconclusive("This test is using hard coded values");
            var map = EmMachineVariables.Instance;
            machine.PhysicalMachineSettings = EmPhysicalMachineSettings.CreateFromFile(this.ipEndPoint, "Em/Em7PhysicalMachineSettings_mm.cfg.xml");

            communicator.SynchronizeMachine();

            heartBeatCommunicator.Verify(s => s.WriteValue(map.EventNotifierAddress, "1.0.0.0", It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.EventNotifierPort, 1, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.CorrugateWidthTolerance, 0.1, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.TrackChangeDelay, 150, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.NumberOfTracks, 2, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.TrackOffsets, new double[] { 50d, 1000d }, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadDisconnectDelay, 80d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongHeadConnectionDelay, 80d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadCreaseDelayOn, 53d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadCreaseDelayOff, 9d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadCutDelayOn, 13d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadCutDelayOff, 1d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadPositionInWindow, new MicroMeter(0.6), It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadSpeedInPercent, 55, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadAccelerationInPercent, 55, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadTorque, 100, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadMaximumPosition, new MicroMeter(2494d), It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.LongheadMinimumPosition, new MicroMeter(50d), It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.NumberOfLongHeads, 6, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadMinimumPosition, new MicroMeter(-20d), It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadMaximumPosition, new MicroMeter(2560d), It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadConnectDelayOn, 80d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadConnectDelayOff, 80d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadCreaseDelayOn, 46d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadCreaseDelayOff, 23d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadCutDelayOff, 11d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadCutDelayOn, 15d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadReferenceQuantity, 3, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadToolPositionToSensor, -25d, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadNormalSpeedInPercent, 100, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadNormalAccelerationInPercent, 100, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.CrossHeadNormalTorque, 100, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.TrackWidths, new double[] { 100, 200 }, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.FeedRollerToolsGearRatio, 1d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.FeedRollerOutFeedLength, 400d, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.FeedRollerNormalSpeedInPercent, 100, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.FeedRollerNormalAccelerationInPercent, 100, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.FeedRollerNormalTorque, 75, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.FeedRollerReverseSpeedInPercent, 30, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.FeedRollerReverseAccelerationInPercent, 30, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.FeedRollerReverseTorque, 100, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.ClearMachineErrors, true, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.RunPrestart, true, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.TrackServiceFeedingDistance, 200d, It.IsAny<int>()));
            heartBeatCommunicator.Verify(s => s.WriteValue(map.UseCorrugateSensors, true, It.IsAny<int>()));

            heartBeatCommunicator.Verify(s => s.WriteValue(map.IsInches, false, It.IsAny<int>()));
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void SyncronizeMachine_GetsExpectedVariables_FromPlc()
        {
            var eventCalled = false;

            this.machine.PhysicalMachineSettings = EmPhysicalMachineSettings.CreateFromFile(this.ipEndPoint,
                "Em/Em7PhysicalMachineSettings_mm.cfg.xml");

            heartBeatCommunicator.Setup(m => m.ReadValue<double[]>(EmMachineVariables.Instance.LongHeadPositions, It.IsAny<int>()))
                .Returns(new double[] { 100, 200, 300, 400, 500, 600 });
            heartBeatCommunicator.Setup(m => m.ReadValue<double>(EmMachineVariables.Instance.CrossHeadPosition, It.IsAny<int>())).Returns(123.123).Callback(
                () =>
                {
                    eventCalled = true;
                });

            heartBeatCommunicator.Setup(m => m.ReadValue<double>(EmMachineVariables.Instance.ACP10Scaling, It.IsAny<int>())).Returns(100);
            heartBeatCommunicator.Setup(m => m.ReadValue<double>(EmMachineVariables.Instance.CrossHeadSpeed, It.IsAny<int>())).Returns(1337);
            heartBeatCommunicator.Setup(m => m.ReadValue<double>(EmMachineVariables.Instance.CrossHeadAcceleration, It.IsAny<int>())).Returns(1338);
            heartBeatCommunicator.Setup(m => m.ReadValue<double>(EmMachineVariables.Instance.FeedRollerSpeed, It.IsAny<int>())).Returns(1339);
            heartBeatCommunicator.Setup(m => m.ReadValue<double>(EmMachineVariables.Instance.FeedRollerAcceleration, It.IsAny<int>())).Returns(1340);

            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(EmMachineVariables.Instance.StartupProcedureCompleted.VariableName, true) });

            Retry.For(() => eventCalled, TimeSpan.FromSeconds(1));

            Specify.That((machine.PhysicalMachineSettings as EmPhysicalMachineSettings).CrossHeadParameters.Position).Should.BeLogicallyEqualTo(123.123);

            for (var i = 0; i < 6; i++)
            {
                Specify.That((machine.PhysicalMachineSettings as EmPhysicalMachineSettings).LongHeadParameters.LongHeads.ElementAt(i).Position).Should.BeLogicallyEqualTo(GetLongHeadToolPositionFromSensorPosition(
                    (machine.PhysicalMachineSettings as EmPhysicalMachineSettings).LongHeadParameters.LongHeads.ElementAt(i),
                    (machine.PhysicalMachineSettings as EmPhysicalMachineSettings).LongHeadParameters.LongHeads.ElementAt(i).LeftSideToSensorPin,
                    (i + 1) * 100));
            }

            Specify.That((machine.PhysicalMachineSettings as EmPhysicalMachineSettings).CrossHeadParameters.MovementData.Speed).Should.BeLogicallyEqualTo(13.37);
            Specify.That((machine.PhysicalMachineSettings as EmPhysicalMachineSettings).CrossHeadParameters.MovementData.Acceleration).Should.BeLogicallyEqualTo(13.38);
            Specify.That((machine.PhysicalMachineSettings as EmPhysicalMachineSettings).LongHeadParameters.MovementData.Speed).Should.BeLogicallyEqualTo(13.37);
            Specify.That((machine.PhysicalMachineSettings as EmPhysicalMachineSettings).LongHeadParameters.MovementData.Acceleration).Should.BeLogicallyEqualTo(13.38);
            Specify.That((machine.PhysicalMachineSettings as EmPhysicalMachineSettings).FeedRollerParameters.MovementData.Speed).Should.BeLogicallyEqualTo(13.39);
            Specify.That((machine.PhysicalMachineSettings as EmPhysicalMachineSettings).FeedRollerParameters.MovementData.Acceleration).Should.BeLogicallyEqualTo(13.40);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldAddMachineVersionCapabilityWhenSynchronizingMachine()
        {
            heartBeatCommunicator.Setup(m => m.ReadValue<string>(EmMachineVariables.Instance.MachinePlcVersion, It.IsAny<int>())).Returns("BestVersion");
            machine.PhysicalMachineSettings = EmPhysicalMachineSettings.CreateFromFile(this.ipEndPoint,
                  "Em/Em7PhysicalMachineSettings_mm.cfg.xml");
            
            aggregator.Publish(new Message<Tuple<string, bool>>()
            {
                MessageType = SimplePLCMessages.SimplePLCConnectionStatus,
                Data = new Tuple<string, bool>("127.0.0.1", true)
            });

            Retry.For(() =>
            {
                heartBeatCommunicator.Verify(s => s.ReadValue<string>(EmMachineVariables.Instance.MachinePlcVersion, It.IsAny<int>()));
                return true;
            }, TimeSpan.FromSeconds(2));

            Specify.That(this.machine.PlcVersion).Should.BeEqualTo("BestVersion"); 
        }

        private static MicroMeter GetLongHeadToolPositionFromSensorPosition(LongHead longHead, MicroMeter sensorOffset, double sensorPosition)
        {
            return sensorPosition - sensorOffset + longHead.LeftSideToTool;
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void Should_SynchronizeMachine_OnAggregatorEvent()
        {
            this.machine.PhysicalMachineSettings = EmPhysicalMachineSettings.CreateFromFile(this.ipEndPoint, "Em/Em7PhysicalMachineSettings_mm.cfg.xml");
        
            aggregator.Publish(new Message<Tuple<string, bool>>()
            {
                MessageType = SimplePLCMessages.SimplePLCConnectionStatus,
                Data = new Tuple<string, bool>("127.0.0.1",true)
            });

            Retry.For(() =>
            {
                heartBeatCommunicator.Verify(s => s.WriteValue(EmMachineVariables.Instance.RunPrestart, true, It.IsAny<int>()));
                return true;
            }, TimeSpan.FromSeconds(2));
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldAddProductionItemToQueue()
        {
            heartBeatCommunicator.Setup(m => m.ReadValue<bool>(EmMachineVariables.Instance.StartupProcedureCompleted, It.IsAny<int>())).Returns(true);
            heartBeatCommunicator.Setup(m => m.ReadValue<bool>(EmMachineVariables.Instance.ReadyForProcessing, It.IsAny<int>())).Returns(true);

            var id = Guid.NewGuid();

            heartBeatCommunicator.Setup(
                m => m.WriteValue(EmMachineVariables.Instance.TransferBuffer, It.IsAny<PLCProducible>(), It.IsAny<int>()))
                .Callback<PacksizePlcVariable, PLCProducible, int>(
                    (variable, sentProducible, someInt) =>
                    {
                        var zip = GuidHelpers.ConvertGuidToIntArray(id).Zip(sentProducible.Id, (a, b) => a.Equals(b));
                        Specify.That(zip.All(value => value)).Should.BeTrue();
                    }
                );

            var producible = new Carton { Id = id, TrackNumber = 1 };

            emMachines.Produce(machine.Id, producible, new List<IInstructionItem>());

            heartBeatCommunicator.Verify(m => m.WriteValue(EmMachineVariables.Instance.ReadyForProcessing, true, It.IsAny<int>()), Times.Once);
            heartBeatCommunicator.Verify(m => m.WriteValue(EmMachineVariables.Instance.TransferBuffer, It.IsAny<PLCProducible>(), It.IsAny<int>()), Times.Once);

            Specify.That(producible.ProducibleStatus).Should.BeEqualTo(InProductionProducibleStatuses.ProducibleSentToMachine);
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldTriggerOnVariableChanged_OnLongheadsOutOfPositionWarning()
        {
            machine.PhysicalMachineSettings = new EmPhysicalMachineSettings();
            (machine.PhysicalMachineSettings as EmPhysicalMachineSettings).LongHeadParameters = new EmLongHeadParameters{OutOfPositionWarningThreshold = 5.0};

            var map = EmMachineVariables.Instance;
            heartBeatCommunicator.Setup(hbc => hbc.ReadValue<double[]>(map.LongheadsOutOfPositionWarningArguments, -1)).Returns(new[] { 5.0, 5.0, 5.0, 5.0 });

            var eventCalled = false;
            aggregator.GetEvent<Message<PlcMachineWarning>>().DurableSubscribe(msg => { eventCalled = true; });        
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(map.LongheadsOutOfPositionWarning.VariableName, 102) });
            Retry.For(() => eventCalled, TimeSpan.FromMilliseconds(5000));
            Specify.That(eventCalled).Should.BeTrue("We should get this message when at least one arguments is greator or equal to the OutOfPositionWarningThreshold value");

            heartBeatCommunicator.Setup(hbc => hbc.ReadValue<double[]>(map.LongheadsOutOfPositionWarningArguments, -1)).Returns(new[] { 3.0, 3.0, 3.0, 3.0 });
            eventCalled = false;
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(map.LongheadsOutOfPositionWarning.VariableName, 102) });
            Retry.For(() => eventCalled, TimeSpan.FromMilliseconds(5000));
            Specify.That(eventCalled).Should.BeFalse("We should not get this message when all arguments are lower than the OutOfPositionWarningThreshold value");
            loggerMock.Verify(lm => lm.Log(LogLevel.Warning, It.IsAny<string>()), Times.Exactly(2), "Expected the logger to be called even if the values are below the OutOfPositionWarningThreshold value");
        }

        [TestMethod]
        [TestCategory("Integration")]
        public void ShouldTriggerOnVariableChanged_OnCrossheadOutOfPositionWarning()
        {
            machine.PhysicalMachineSettings = new EmPhysicalMachineSettings();
            (machine.PhysicalMachineSettings as EmPhysicalMachineSettings).CrossHeadParameters = new EmCrossHeadParameters { OutOfPositionWarningThreshold = 5.0 };

            var map = EmMachineVariables.Instance;
            heartBeatCommunicator.Setup(hbc => hbc.ReadValue<double[]>(map.CrossheadOutOfPositionWarningArguments, -1)).Returns(new[] { 5.0 });

            var eventCalled = false;
            aggregator.GetEvent<Message<PlcMachineWarning>>().DurableSubscribe(msg => { eventCalled = true; });
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(map.CrossheadOutOfPositionWarning.VariableName, 101) });           
            Retry.For(() => eventCalled, TimeSpan.FromMilliseconds(5000));
            Specify.That(eventCalled).Should.BeTrue("We should get this message when at least one arguments is greator or equal to the OutOfPositionWarningThreshold value");

            heartBeatCommunicator.Setup(hbc => hbc.ReadValue<double[]>(map.CrossheadOutOfPositionWarningArguments, -1)).Returns(new[] { 3.0 });
            eventCalled = false;
            aggregator.Publish(new Message<VariableChangedEventArgs> { Data = new VariableChangedEventArgs(map.CrossheadOutOfPositionWarning.VariableName, 101) });
            Retry.For(() => eventCalled, TimeSpan.FromMilliseconds(5000));
            Specify.That(eventCalled).Should.BeFalse("We should not get this message when all arguments are lower than the OutOfPositionWarningThreshold value");
            loggerMock.Verify(lm => lm.Log(LogLevel.Warning, It.IsAny<string>()), Times.Exactly(2), "Expected the logger to be called even if the values are below the OutOfPositionWarningThreshold value");
        }
    }
}
