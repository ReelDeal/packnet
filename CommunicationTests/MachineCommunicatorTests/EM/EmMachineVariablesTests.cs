﻿using System;
using System.Linq;
using System.Linq.Expressions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Communication.Communicators.EM;

using Testing.Specificity;

namespace PackNet.CommunicationTests.MachineCommunicatorTests.EM
{
    [TestClass]
    public class EmMachineVariablesTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        [TestCategory("Bug")]
        [TestCategory("Bug 8968")]
        public void PlcVariablesShouldNotContainDuplicates()
        {
            Specify.That(EmMachineVariables.Instance.AsEnumerable().Count()).Should.BeEqualTo(EmMachineVariables.Instance.AsEnumerable().Distinct().Count(), "Variable map should not contain duplicates");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void RemovedProductionItemIdMappingTest()
        {
            VerifyMappingFor(var => var.RemovedProductionItemId1, "Main", "RemovedProductionItemId[0].Parts", 1, true);
            VerifyMappingFor(var => var.RemovedProductionItemId2, "Main", "RemovedProductionItemId[1].Parts", 1, true);
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void StartedProductionItemIdMappingTest()
        {
            VerifyMappingFor(var => var.StartedProductionItemId, "Main", "StartedProductionItemId.Parts", 1, true);
        }

        private static void VerifyMappingFor(Expression<Func<EmMachineVariables, PacksizePlcVariable>> property, string taskName, string variableName, int accessMode, bool notifyChanges = false)
        {
            var sut = property.Compile().Invoke(EmMachineVariables.Instance);
            Assert.IsNotNull(sut, "Variable was not mapped");
            Assert.AreEqual(sut.VariableName, variableName, "Variable name was not mapped as expected");
            Assert.AreEqual(sut.TaskName, taskName, "Task name was not mapped as expected");
            Assert.AreEqual(sut.NotifyChanges, notifyChanges, "NotifyChanges was not mapped as expected");
        }
    }
}
