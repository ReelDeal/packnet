﻿using System;
using System.Reactive.Linq;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Exceptions;
using PackNet.Common.Interfaces.Logging;
using PackNet.Communication;
using PackNet.Communication.Communicators.Fusion;

using Testing.Specificity;

namespace PackNet.CommunicationTests
{
    using Common.Utils;

    [TestClass]
    public class SimplePLCCommunicatorTests : PlcCommunicatorHelpers
    {
        private Mock<ILogger> mockLogger;
        private Mock<IWebRequestCreator> webRequestFactory;
        private EventAggregator eventAggregator;

        [TestInitialize]
        public void SetUp()
        {
            eventAggregator = new EventAggregator();
            mockLogger = new Mock<ILogger>(MockBehavior.Loose);
            webRequestFactory = new Mock<IWebRequestCreator>();
            webRequestFactory.Setup(m => m.ServerAddress).Returns("127.0.0.1");
        }

        //todo: fix unit test
#if DEBUG
        [TestMethod]
        [TestCategory("Unit-ReplicateBug")]
        public void Bug7208()
        {
            var map = IqFusionMachineVariables.Instance;

            webRequestFactory
               .Setup(m => m.CreateGetRequest(It.IsAny<PacksizePlcVariable>()))
               .Returns(() => GetMockRequest("\\\"00000000-0000-0000-0000-000000000000\\\"").Object);

            var plcComm = new PLCHeartBeatCommunicator(webRequestFactory.Object, IqFusionMachineVariables.Instance, eventAggregator, mockLogger.Object);

            while (!plcComm.IsConnected)
            {
                Thread.Sleep(10);
            }

          //  var result = plcComm.ReadValue<Int32[]>(map.TransferBufferId);

         //   Specify.That(result).Should.Not.BeNull();

        }
#endif

        [TestMethod]
        [TestCategory("Unit")]
        public void ReadIntegerTest()
        {
            webRequestFactory
                .Setup(m => m.CreateGetRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.PlcLifeSign.VariableName)))
                .Returns(() => GetMockRequest("{\"Main:state\":10}").Object);

            var communicator = GetMockConnectedCommunicator();

            Specify.That(communicator.ReadValue<int>(IqFusionMachineVariables.Instance.PlcLifeSign)).Should.BeEqualTo(10);
            Specify.That(communicator.ReadValue<short>(IqFusionMachineVariables.Instance.PlcLifeSign)).Should.BeEqualTo((short)10);
            Specify.That(communicator.ReadValue<long>(IqFusionMachineVariables.Instance.PlcLifeSign)).Should.BeEqualTo((long)10);
            communicator.Dispose();

        }

        [TestMethod]
        [TestCategory("Unit")]
        public void WriteIntegerValue()
        {
            var communicator = GetMockConnectedCommunicator();
            Specify.ThatAction(() => communicator.WriteValue(IqFusionMachineVariables.Instance.PcLifeSign, 1)).Should.Not.HaveThrown();
            Specify.ThatAction(() => communicator.WriteValue(IqFusionMachineVariables.Instance.PcLifeSign, (byte)2)).Should.Not.HaveThrown();
            Specify.ThatAction(() => communicator.WriteValue(IqFusionMachineVariables.Instance.PcLifeSign, (short)3)).Should.Not.HaveThrown();
            Specify.ThatAction(() => communicator.WriteValue(IqFusionMachineVariables.Instance.PcLifeSign, (long)4)).Should.Not.HaveThrown();
            communicator.Dispose();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ReadUnavailableVariableThrowsException()
        {
            webRequestFactory
                .Setup(m => m.CreateGetRequest(It.Is<PacksizePlcVariable>(v => v.TaskName == "testTaskName")))
                .Returns(() => GetMockRequest("").Object);
            var communicator = GetMockConnectedCommunicator();

            MissingVariableException ex = null;
            try
            {
                var value = communicator.ReadValue<int>(new PacksizePlcVariable("testTaskName", "testVariableName"));
                Specify.That(value).Should.BeEqualTo(0);
            }
            catch (MissingVariableException e)
            {
                ex = e;
            }

            Specify.That(ex).Should.Not.BeNull();
            communicator.Dispose();

        }
        
        //todo: fix unit test
#if DEBUG
        [TestMethod]
        public void WriteUnavailableVariableThrowExceptionTest()
        {
            webRequestFactory
                .Setup(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.EventNotificationVariablesCounter.VariableName), It.IsAny<object>()))
                .Returns(() => GetMockRequest("{\"t:t\":true}").Object);
            webRequestFactory
                .Setup(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.EventNotificationRegistrationBuffer.VariableName), It.IsAny<object>()))
                .Returns(() => GetMockRequest("{\"t:t\":true}").Object);
            webRequestFactory
                .Setup(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.TaskName == "testTaskName"), It.IsAny<object>()))
                .Returns(() => GetMockRequest("{\"t:t\":false}").Object); //Failed to write
            var communicator = GetMockConnectedCommunicator();
            Specify.ThatAction(() => communicator.WriteValue(new PacksizePlcVariable("testTaskName", "testVariableName"), 10)).Should.HaveThrown(typeof(WriteException));
        }
#endif


        //todo: fix unit test
#if DEBUG
        [TestMethod]
        public void ShouldVerifyConnectivityOnTimerElapsed()
        {
            webRequestFactory
                .Setup(m => m.CreateGetRequest(It.IsAny<PacksizePlcVariable>()))
                .Returns(() => GetFailedToConnectRequest("Unable to connect to the remote server").Object);
            var called = false;
            eventAggregator.GetEvent<Message<Tuple<string, bool>>>()
                .Where(m => m.MessageType == SimplePLCMessages.SimplePLCConnectionStatus)
                .Subscribe(m =>
                {
                    called = true;
                });
            var communicator = GetMockConnectedCommunicator(TimeSpan.FromSeconds(0.3));

            Retry.For(() => called, TimeSpan.FromSeconds(2));
            Retry.For(() => webRequestFactory.Verify(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.PcLifeSign.VariableName), 0), Times.Once), TimeSpan.FromSeconds(2));
            Specify.That(called).Should.BeTrue();
            webRequestFactory.Verify(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.PcLifeSign.VariableName), 0), Times.Once);
        }
        
#endif
        private SimplePLCCommunicator GetMockConnectedCommunicator(TimeSpan pingTime = default(TimeSpan), TimeSpan connectRetryTime = default(TimeSpan))
        {
            webRequestFactory.Setup(m => m.CreateGetRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.ConnectionStatus.VariableName)))
                .Returns(() => GetMockRequest("{\"PCCommunic:ConnectionStatus\":\"false\"}").Object);
            webRequestFactory
                .Setup(m => m.CreateGetRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.PcLifeSign.VariableName)))
                .Returns(() => GetMockRequest("{\"Main:state\":10}").Object);
            webRequestFactory
                .Setup(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.PcLifeSign.VariableName), It.IsAny<object>()))
                .Returns(() => GetMockRequest("{\"Main:state\":10}").Object);
            webRequestFactory
                .Setup(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.EventNotificationRegistrationBuffer.VariableName), It.IsAny<object>()))
                .Returns(() => GetMockRequest("{\"t:t\":true}").Object);
            webRequestFactory
                .Setup(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.EventNotificationVariablesCounter.VariableName), It.IsAny<object>()))
                .Returns(() => GetMockRequest("{\"t:t\":true}").Object);

            if (pingTime == default(TimeSpan))
                pingTime = TimeSpan.FromSeconds(2);

            if (connectRetryTime == default(TimeSpan))
                connectRetryTime = TimeSpan.FromSeconds(0.1);

            var communicator = new SimplePLCCommunicator(
                webRequestFactory.Object,
                mockLogger.Object);
            return communicator;
        }



    }
}
