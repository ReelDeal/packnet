﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Communication.PLCBase;
using Testing.Specificity;

namespace PackNet.CommunicationTests.PLCBase
{
    [TestClass]
    public class VariableChangedEventArgsTests
    {
        [TestMethod]
        public void ShouldContainNameAndValue()
        {
            var arg = new VariableChangedEventArgs("Test", 666);

            Specify.That(arg.VariableName).Should.BeEqualTo("Test");
            Specify.That(arg.Value).Should.BeEqualTo(666);
        }
    }
}
