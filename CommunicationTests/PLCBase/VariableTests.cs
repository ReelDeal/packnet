﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Communication.PLCBase;
using Testing.Specificity;

namespace PackNet.CommunicationTests.PLCBase
{
    [TestClass]
    public class VariableTests
    {
        private const string TaskName = "task";
        private const string VariableName = "variable";

        [TestMethod]
        public void ConstructorTests()
        {
            var v1 = new PacksizePlcVariable(TaskName, VariableName, notifyChanges: true);
            Specify.That(v1.TaskName).Should.BeEqualTo(TaskName);
            Specify.That(v1.VariableName).Should.BeEqualTo(VariableName);
            Specify.That(v1.NotifyChanges).Should.BeTrue();
        }

        [TestMethod]
        public void ToStringTests()
        {
            var v1 = new PacksizePlcVariable(TaskName, VariableName, notifyChanges: false);
            Specify.That(v1.ToString()).Should.BeEqualTo(TaskName + ":" + VariableName);
        }

        [TestMethod]
        public void ShouldBePossibleToActivateNotification()
        {
            var variable = new PacksizePlcVariable(TaskName, VariableName, notifyChanges: true);

            Specify.That(variable.NotifyChanges).Should.BeEqualTo(true);
        }
    }
}
