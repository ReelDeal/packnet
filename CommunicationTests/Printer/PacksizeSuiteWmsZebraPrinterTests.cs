﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Reactive.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;

//using PackNet.Common.Interfaces.DTO.PrintingMachines;
//using PackNet.Common.Interfaces.Logging;
//using PackNet.Common.Interfaces.Networking;
//using PackNet.Common.Printing;

//using Testing.Specificity;

//namespace CommonTests.Printing
//{
//    using PackNet.Common.Interfaces.Enums;
//    using PackNet.Common.Utils;

//    [TestClass]
//    public class PacksizeSuiteWmsZebraPrinterTests
//    {
//        private Mock<ISocketCommunicator> socketCommunicatorMock;
//        private Mock<IPrintableLabelFormatter> labelFormatterMock;
//        private Mock<ILogger> loggerMock;

//        [TestInitialize]
//        public void Setup()
//        {
//            socketCommunicatorMock = new Mock<ISocketCommunicator>();
//            labelFormatterMock = new Mock<IPrintableLabelFormatter>();
//            loggerMock = new Mock<ILogger>();
//        }

//        [TestMethod]
//        public void ShouldPublishPrinterErrorAndPrintJobErrorAndRecycleSocketWhenExceptionThrownBySocketCommunicator()
//        {
//            // Arrange
//            var printMessage = "print this";
//            var printable = new Printable(new Dictionary<string, string> {{"SerialNumber", printMessage}});
//            labelFormatterMock.Setup(l => l.ConvertToPrintableFormat(printable)).Returns(printMessage);

//            const string errorMessage = "Error sending data";
//            socketCommunicatorMock.Setup(s => s.SendMessage(printMessage,5)).Throws(new IOException(errorMessage));

//            var printerId = Guid.NewGuid();
//            var printer = new PacksizeSuiteWmsZebraPrinter(printerId, socketCommunicatorMock.Object, labelFormatterMock.Object, loggerMock.Object);

//            var returnedErrorMessage = string.Empty;
//            var printJobStatus = PrintJobStatuses.Unknown;
//            Printable item = null;
//            printer.PrintJobStatusChangedObservable
//                .Where(x => x.PrinterId == printerId)
//                .Subscribe(x =>
//                {
//                    returnedErrorMessage = x.ErrorMessage;
//                    printJobStatus = x.JobStatus;
//                    item = x.ItemToPrint;

//                });

//            var printerStatus = PrinterMachineStatuses.Started;
//            printer.PrinterStatusChangedObservable
//                .Where(x => x.UniqueID == printerId)
//                .Subscribe(x => printerStatus = x.PrinterMachineStatus);

//            // Act
//            printer.Print(printable);
//            Retry.For(() => item != null, TimeSpan.FromSeconds(2));

//            // Assert
//            Specify.That(returnedErrorMessage.Contains(errorMessage));
//            Specify.That(printJobStatus).Should.BeEqualTo(PrintJobStatuses.Error);
//            Specify.That(printable).Should.BeLogicallyEqualTo(item);
//            Specify.That(printerStatus).Should.BeEqualTo(PrinterMachineStatuses.Error);
//            socketCommunicatorMock.Verify(s => s.RecycleSocket(), Times.Once);
//        }

//        [TestMethod]
//        public void ShouldPublishPrintJobStatusErrorWhenLabelFormatterArgumentExceptionThrown()
//        {
//            // Arrange
//            const string printMessage = "unformattable message";
//            var printable = new Printable(new Dictionary<string, string> {{"SerialNumber", printMessage}});
//            const string errorMessage = "Failed to format message";
//            labelFormatterMock.Setup(l => l.ConvertToPrintableFormat(printable)).Throws(new ArgumentException(errorMessage));

//            var id = Guid.NewGuid();
//            var printer = new PacksizeSuiteWmsZebraPrinter(id, socketCommunicatorMock.Object, labelFormatterMock.Object, loggerMock.Object);

//            var returnedErrorMessage = string.Empty;
//            var printJobStatus = PrintJobStatuses.Unknown;
//            Printable item = null;
//            printer.PrintJobStatusChangedObservable
//                .Where(x => x.PrinterId == id && x.JobStatus == PrintJobStatuses.Error)
//                .Subscribe(x =>
//                {
//                    returnedErrorMessage = x.ErrorMessage;
//                    printJobStatus = x.JobStatus;
//                    item = x.ItemToPrint;
//                });

//            // Act
//            printer.Print(printable);
//            Retry.For(() => item != null, TimeSpan.FromSeconds(2));

//            // Arrange
//            Specify.That(returnedErrorMessage.Contains(errorMessage));
//            Specify.That(printJobStatus).Should.BeEqualTo(PrintJobStatuses.Error);
//            Specify.That(printable).Should.BeLogicallyEqualTo(item);
//        }
//    }
//}
