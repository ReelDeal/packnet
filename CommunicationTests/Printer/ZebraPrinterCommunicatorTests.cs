﻿using System;
using System.Linq;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Networking;
using PackNet.Common.Interfaces.Producible;
using PackNet.Common.Interfaces.RestrictionsAndCapabilities.ExternalSystems;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Interfaces.Services.ExternalSystems;
using PackNet.Common.Utils;
using PackNet.Communication.Printer;

using Testing.Specificity;

namespace PackNet.CommunicationTests.Printer
{
    [TestClass]
    public class ZebraPrinterCommunicatorTests
    {
        private const string ZplPrinterReadyAndPaused =
            "\u0002aaa,0,1,dddd,000,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterPausedWithLabelInQueue =
            "\u0002aaa,0,1,dddd,001,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterStartedWithLabelInQueue =
            "\u0002aaa,0,0,dddd,001,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterNotReady =
            "\u0002aaa,1,0,dddd,001,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,1,1,0,2,s,1,00000001,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterReadyAndLabelPeeledOff =
            "\u0002aaa,0,0,dddd,000,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,0,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";

        [TestMethod]
        public void ShouldGetLabelStatusFailed_WithPreviousStatusAsZebraErrorProducibleStatus_WhenVerifyInQueueReturnsFalse_ExternalPrint()
        {
            var heartBeatSubject = new Subject<Tuple<bool, string>>();
            var socketMock = new Mock<ISocketCommunicatorWithHeartBeat>();
            socketMock.Setup(s => s.HeartbeatObservable).Returns(heartBeatSubject.AsObservable());
            socketMock.Setup(s => s.SendReceiveLines(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(ZplPrinterReadyAndPaused);

            var printerCom = CreateZebraPrinterCommunicator(heartBeatSubject, socketMock.Object);

            heartBeatSubject.OnNext(new Tuple<bool, string>(true, string.Empty));

            //With external print system
            var label = new Label() { PrintData = string.Empty };
            label.Restrictions.Add(new ExternalPrintRestriction(Guid.NewGuid()));
            label.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            printerCom.Print(label);
            Retry.For(() => label.ProducibleStatus == ErrorProducibleStatuses.ProducibleFailed, TimeSpan.FromSeconds(20));
            Specify.That(label.History.Last().NewStatus).Should.BeEqualTo(ErrorProducibleStatuses.NotProducible);
            Specify.That(label.History.Last().PreviousStatus).Should.BeEqualTo(ZebraErrorProducibleStatus.LabelRequestedButNotConfirmedInPrinterQueue);            
        }

        [TestMethod]
        public void ShouldGetLabelStatusFailed_WithPreviousStatusAsZebraErrorProducibleStatus_WhenVerifyInQueueReturnsFalse()
        {
            var heartBeatSubject = new Subject<Tuple<bool, string>>();
            var socketMock = new Mock<ISocketCommunicatorWithHeartBeat>();
            socketMock.Setup(s => s.HeartbeatObservable).Returns(heartBeatSubject.AsObservable());
            socketMock.Setup(s => s.SendReceiveLines(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(ZplPrinterReadyAndPaused);

            var printerCom = CreateZebraPrinterCommunicator(heartBeatSubject, socketMock.Object);

            heartBeatSubject.OnNext(new Tuple<bool, string>(true, string.Empty));
            //Without external print system
            var label = new Label() { PrintData = string.Empty };
            label.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;

            printerCom.Print(label);
            Retry.For(() => label.ProducibleStatus == ErrorProducibleStatuses.ProducibleFailed, TimeSpan.FromSeconds(6));
            Specify.That(label.History.Last().NewStatus).Should.BeEqualTo(ErrorProducibleStatuses.ProducibleFailed);
            Specify.That(label.History.Last().PreviousStatus).Should.BeEqualTo(ZebraErrorProducibleStatus.FillPrinterQueueNotConfirmed);
        }

        [TestMethod]
        public void ShouldGetLabelStatusFailed_WithPreviousStatusFailedToStart_WhenStartingPrinterFails()
        {
            var heartBeatSubject = new Subject<Tuple<bool, string>>();
            var socketMock = new Mock<ISocketCommunicatorWithHeartBeat>();
            socketMock.Setup(s => s.HeartbeatObservable).Returns(heartBeatSubject.AsObservable());
            socketMock.SetupSequence(s => s.SendReceiveLines(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(ZplPrinterReadyAndPaused)
                .Returns(ZplPrinterReadyAndPaused)
                .Returns(ZplPrinterReadyAndPaused)
                .Returns(ZplPrinterReadyAndPaused)
                .Returns(ZplPrinterPausedWithLabelInQueue)
                .Returns(ZplPrinterPausedWithLabelInQueue);

            var printerCom = CreateZebraPrinterCommunicator(heartBeatSubject, socketMock.Object);

            heartBeatSubject.OnNext(new Tuple<bool, string>(true, string.Empty));
            var label = new Label() { PrintData = string.Empty };
            label.ProducibleStatus = InProductionProducibleStatuses.ProducibleSentToMachine;
            printerCom.Print(label);
            Retry.For(() => label.ProducibleStatus == ErrorProducibleStatuses.ProducibleFailed, TimeSpan.FromSeconds(15));
            Specify.That(label.History.Last().NewStatus).Should.BeEqualTo(ErrorProducibleStatuses.ProducibleFailed);
            Specify.That(label.History.Last().PreviousStatus)
                .Should.BeEqualTo(ZebraErrorProducibleStatus.FailedToStart);
            Specify.That(label.History.Select(h => h.NewStatus).Contains(InProductionProducibleStatuses.ProducibleProductionStarted))
                .Should.BeTrue();
        }

        [TestMethod]
        public void ShouldGetLabelStatusCompleted_IfPrintLoopExecuteSuccessfully()
        {
            var heartBeatSubject = new Subject<Tuple<bool, string>>();
            var socketMock = new Mock<ISocketCommunicatorWithHeartBeat>();
            socketMock.Setup(s => s.HeartbeatObservable).Returns(heartBeatSubject.AsObservable());
            socketMock.SetupSequence(s => s.SendReceiveLines(It.IsAny<string>(), It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(ZplPrinterReadyAndPaused)
                .Returns(ZplPrinterReadyAndPaused)
                .Returns(ZplPrinterReadyAndPaused)
                .Returns(ZplPrinterPausedWithLabelInQueue)
                .Returns(ZplPrinterStartedWithLabelInQueue)
                .Returns(ZplPrinterReadyAndLabelPeeledOff)
                .Returns(ZplPrinterReadyAndLabelPeeledOff);

            var printerCom = CreateZebraPrinterCommunicator(heartBeatSubject, socketMock.Object);

            heartBeatSubject.OnNext(new Tuple<bool, string>(true, string.Empty));

            var label = new Label() { PrintData = string.Empty };
            printerCom.Print(label);
            Retry.For(() => label.ProducibleStatus == ProducibleStatuses.ProducibleCompleted, TimeSpan.FromSeconds(6));
        }

        private ZebraPrinterCommunicatorTester CreateZebraPrinterCommunicator(Subject<Tuple<bool, string>> heartBeatSubject, ISocketCommunicatorWithHeartBeat socket)
        {
            var serviceLocatorMock = new Mock<IServiceLocator>();
            serviceLocatorMock.Setup(s => s.Locate<IExternalCustomerSystemService>())
                .Returns(new Mock<IExternalCustomerSystemService>().Object);

            var printer = new ZebraPrinter() { IsPeelOff = true };

            var printerCom = new ZebraPrinterCommunicatorTester(printer, new Mock<ILogger>().Object, new EventAggregator(),
                serviceLocatorMock.Object, socket);

            printer.CurrentStatus = MachineStatuses.MachineOnline;

            return printerCom;
        }

        private class ZebraPrinterCommunicatorTester : ZebraPrinterCommunicator
        {
            public ZebraPrinterCommunicatorTester(ZebraPrinter printer, ILogger logger, IEventAggregatorPublisher publisher, IServiceLocator serviceLocator, ISocketCommunicatorWithHeartBeat socket)
                : base(printer, logger, publisher, serviceLocator, socket)
            {
            }

            public new void Print(IProducible producible)
            {
                base.Print(producible);
            }
        }
    }
}
