﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Interfaces.DTO.Machines;
using PackNet.Common.Interfaces.DTO.PrintingMachines;
using PackNet.Common.Interfaces.Enums;
using PackNet.Common.Interfaces.Enums.ProducibleStates;
using PackNet.Common.Interfaces.Eventing;
using PackNet.Common.Interfaces.ExtensionMethods;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Interfaces.Networking;
using PackNet.Common.Interfaces.Services;
using PackNet.Common.Network;
using PackNet.Common.Printing;
using PackNet.Common.Utils;
using PackNet.Communication.Printer;

using Testing.Specificity;

using TestUtils;

namespace PackNet.CommunicationTests.Printer
{
    [TestClass]
    [DeploymentItem("Template.prn", @"Data/Labels")]
    public class ZebraPrinterTests
    {
        private const int PrinterCommunicationPort = 9100;
        private const int WaitTimeForPrinterToStopPrinting = 300;
        private Guid PrinterId = Guid.NewGuid();
        private const string PrinterName = "testPrinter";
        private const string LabelText = "00009999991873344170";
        private const string RealPrinterIpAddressString = "192.168.30.56";
        private const string UnreachablePrinterIpAddressString = "192.168.30.254"; // Enter an IP address on the network that does not have anything connected to it
        private IPAddress realPrinterIpAddress;
        private IPAddress unreachablePrinterIpAddress;
        private Mock<ISocketCommunicator> mockSocketCommunicator;
        private ZebraPrinter printer;
        private ZebraPrinter realPrinter;

        private Printable printableBarcodeLabel;
        private List<PrinterStatus> printerStatuses;
        private List<PrintJobStatus> printJobStatuses;
        private Mock<IPrintableLabelFormatter> mockLabelFormatter;
        private IEventAggregatorPublisher publisher;
        private double secondsToWaitForRealPrinterToConnect = 15;
        private IServiceLocator serviceLocator = new Mock<IServiceLocator>().Object;
        private ILogger logger;

        // Happy path
        private const string ZplPrinterReady =
            "\u0002aaa,0,0,dddd,001,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterReadyAndPaused =
            "\u0002aaa,0,1,dddd,000,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterPausedWithLabelInQueue =
            "\u0002aaa,0,1,dddd,001,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterStartedWithLabelInQueue =
            "\u0002aaa,0,0,dddd,001,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterNotReady =
            "\u0002aaa,1,0,dddd,001,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,1,1,0,2,s,1,00000001,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterReadyAndLabelPeeledOff =
            "\u0002aaa,0,0,dddd,000,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,0,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";

        // Errors
        private const string ZplPrinterDoesNotPause =
            "\u0002aaa,0,0,dddd,000,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterPausedNoLabel =
            "\u0002aaa,0,1,dddd,000,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";
        private const string ZplPrinterNotStartedWithLabelInQueue =
            "\u0002aaa,0,1,dddd,001,f,g,h,iii,j,k,l\u0003\r\n\u0002mmm,n,0,p,q,1,s,t,00000000,v,www\u0003\r\n\u0002xxxx,y\u0003\r\n";


        private void WaitUntilRealPrinterOnline(int waitTimeoverride = -1)
        {
            var wait = waitTimeoverride == -1 ? secondsToWaitForRealPrinterToConnect : waitTimeoverride;
            Func<bool> isOnline = () =>
            {
                return realPrinter.CurrentStatus == MachineStatuses.MachineOnline;
            };
            Retry.UntilTrue(isOnline, TimeSpan.FromSeconds(wait));
            if (!isOnline())
                Assert.Inconclusive("Unable to connect to real machine at " + realPrinter.IpOrDnsName);
        }
        private void WaitUntilRealPrinterOffline(int waitTimeoverride = 15)
        {
            Func<bool> isOffline = () => realPrinter.CurrentStatus == MachineStatuses.MachineOffline;
            Retry.UntilTrue(isOffline, TimeSpan.FromSeconds(waitTimeoverride));
            if (!isOffline())
                Assert.Inconclusive("Status never set to offline " + realPrinter.IpOrDnsName);
        }


        private void WaitUntilProducibleStatus(Printable item, ProducibleStatuses status, int waitTimeInSeconds)
        {
            Func<bool> check = () => item.ProducibleStatus == status;
            Retry.UntilTrue(check, TimeSpan.FromSeconds(waitTimeInSeconds));
            Specify.That(check()).Should.BeTrue();
        }

        private void WaitUntilHeadOpen()
        {
            Func<bool> isHeadOpen = () => realPrinter.CurrentStatus == ZebraMachineErrorStatuses.HeadOpen;
            Retry.UntilTrue(isHeadOpen, TimeSpan.FromSeconds(30));
            Specify.That(isHeadOpen()).Should.BeTrue();
        }

        private void WaitUntilUnexpectedLabel(int timeToWaitInSeconds = 10)
        {
            Func<bool> check = () => realPrinter.CurrentStatus == ZebraMachineErrorStatuses.UnexpectedLabelOnPrinter;
            Retry.UntilTrue(check, TimeSpan.FromSeconds(timeToWaitInSeconds));
            Specify.That(check()).Should.BeTrue();
        }
        [TestInitialize]
        public void Setup()
        {
            printerStatuses = new List<PrinterStatus>();
            printJobStatuses = new List<PrintJobStatus>();
            realPrinterIpAddress = IPAddress.Parse(RealPrinterIpAddressString);
            unreachablePrinterIpAddress = IPAddress.Parse(UnreachablePrinterIpAddressString);
            mockSocketCommunicator = new Mock<ISocketCommunicator>();
            mockLabelFormatter = new Mock<IPrintableLabelFormatter>();
            mockSocketCommunicator.SetupSequence(
                c => c.SendReceiveLines(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead, 5, 5))
                .Returns(ZplPrinterReady)
                .Returns(ZplPrinterReadyAndPaused)
                .Returns(ZplPrinterPausedWithLabelInQueue)
                .Returns(ZplPrinterStartedWithLabelInQueue)
                .Returns(ZplPrinterReadyAndLabelPeeledOff)
                .Returns(ZplPrinterReadyAndLabelPeeledOff); // for printer double check
            logger = new ConsoleLogger();
            publisher = new PackNet.Common.Eventing.EventAggregator();

            printer = new ZebraPrinter { Id = Guid.NewGuid(), PollInterval = 1, RetryCount = 1 };

            realPrinter = new ZebraPrinter
            {
                IpOrDnsName = RealPrinterIpAddressString,
                Port = PrinterCommunicationPort,
                Id = Guid.NewGuid(),
                PollInterval = 800,
                RetryCount = 3
            };

            printableBarcodeLabel = new Printable(new Dictionary<string, string> { { "SerialNumber", LabelText } });
        }
#if DEBUG
        //Commenting this out since I can't compile in debug and I need to do that because some symbols are being optimized out
        //[TestMethod]
        //[TestCategory("PrinterIntegration")]
        //[TestCategory("Acceptance")]
        //[TestCategory("RealMachine")]
        //public void TestRealPrinter_ChangeToTearOff()
        //{
        //    // Arrange
        //    var socketCommunicator = new SocketCommunicator(new ConsoleLogger(), new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
        //    realPrinter.IsPeelOff = false;
        //    // this really runs the test.
        //    realPrinter.CurrentStatusChangedObservable.Subscribe(status =>
        //    {
        //        if (status == MachineStatuses.MachineOffline)
        //            Assert.Inconclusive("Real machine is not available for test to run.");
        //    });

        //    var comm = new ZebraPrinterCommunicator(realPrinter, logger, publisher, serviceLocator);

        //    //ensure we have the printer we need to test.
        //    WaitUntilRealPrinterOnline();

        //    // Act
        //    var hostStatus = socketCommunicator.SendReceiveLines(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead);
        //    var isPeeloff = Zebra.Conditions.IsPeelOffMode(hostStatus);

        //    Specify.That(isPeeloff).Should.BeFalse();
        //}

       //Commenting this out since I can't compile in debug and I need to do that because some symbols are being optimized out
       // [TestMethod]
       // [TestCategory("PrinterIntegration")]
       // [TestCategory("Acceptance")]
       // [TestCategory("RealMachine")]
       //public void TestRealPrinter_ChangeToPeelOff()
       // {
       //     // Arrange
       //     var socketCommunicator = new SocketCommunicator(new ConsoleLogger(), new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
       //     realPrinter.IsPeelOff = true;
       //     // this really runs the test.
       //     realPrinter.CurrentStatusChangedObservable.Subscribe(status =>
       //     {
       //         if (status == MachineStatuses.MachineOffline)
       //             Assert.Inconclusive("Real machine is not available for test to run.");
       //     });

       //     var comm = new ZebraPrinterCommunicator(realPrinter, logger, publisher, serviceLocator);

       //     //ensure we have the printer we need to test.
       //     WaitUntilRealPrinterOnline();

       //     // Act
       //     var hostStatus = socketCommunicator.SendReceiveLines(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead);
       //     var isPeeloff = Zebra.Conditions.IsPeelOffMode(hostStatus);

       //     Specify.That(isPeeloff).Should.BeTrue();
       // }

        [TestMethod]
        [TestCategory("PrinterIntegration")]
        [TestCategory("Acceptance")]
        [TestCategory("RealMachine")]
        public void TestRealPrinter_ShouldMessageWhenLabelOnPrinterAtStartupInPeeloffMode()
        {
            /*
             Click the feed on the printer before starting this test and leave the label hanging.
             */

            //var socketCommunicator = new SocketCommunicator(new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
            realPrinter.IsPeelOff = true;
            var initializing = false;
            realPrinter.CurrentStatusChangedObservable.Subscribe(status =>
            {
                if (!initializing && status == MachineStatuses.MachineInitializing)
                    initializing = true;
            });
            realPrinter.CurrentProductionStatusObservable.Subscribe(Console.WriteLine);
            realPrinter.CurrentStatusChangedObservable.Subscribe(Console.WriteLine);
            var comm = new ZebraPrinterCommunicator(realPrinter, new ConsoleLogger(), publisher, serviceLocator);

            WaitUntilUnexpectedLabel(30);

        }
        [TestMethod]
        [TestCategory("PrinterIntegration")]
        [TestCategory("Acceptance")]
        [TestCategory("RealMachine")]
        public void TestRealPrinter_ShouldMessageWhenLabelOnPrinterAtStartupInPeeloffModeThenCanComeOnlineWhenRemoved()
        {
            /*
             Click the feed on the printer before starting this test and leave the label hanging.
             */

            //var socketCommunicator = new SocketCommunicator(new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
            realPrinter.IsPeelOff = true;
            var initializing = false;
            realPrinter.CurrentStatusChangedObservable.Subscribe(status =>
            {
                if (!initializing && status == MachineStatuses.MachineInitializing)
                    initializing = true;
            });
            realPrinter.CurrentProductionStatusObservable.Subscribe(Console.WriteLine);
            realPrinter.CurrentStatusChangedObservable.Subscribe(Console.WriteLine);
            var comm = new ZebraPrinterCommunicator(realPrinter, new ConsoleLogger(), publisher, serviceLocator);

            //wait until we have communication to the printer
            WaitUntilUnexpectedLabel(20);
            logger.Log(LogLevel.Info, "***Put Breakpoint here to run test.  Contiunue and remove labels");

            WaitUntilRealPrinterOnline(10);
        }

        [TestMethod]
        [TestCategory("PrinterIntegration")]
        [TestCategory("Acceptance")]
        [TestCategory("RealMachine")]
        public void TestRealPrinter_JobInQueuePrinterStarted1Hanging_HeadOpenRecovers()
        {
            /*
             Ensure no labels on the printer.
             Put breakpoints on log messages specified below.
             */

            //var socketCommunicator = new SocketCommunicator(new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
            realPrinter.IsPeelOff = true;

            var comm = new ZebraPrinterCommunicator(realPrinter, logger, publisher, serviceLocator);

            //ensure state is updated
            WaitUntilRealPrinterOnline();

            //add jobs to printer queue
            var label1 = new Printable("^XA\n^FO25,50^BY3^A0N,80,60^BCN,200,Y,N,N\n^FDLabel1^FS\n^FO25,400^BY4^B3N,N,300,Y,N\n^FDShipToName^FS\n^XZ")
            {
                CustomerUniqueId = "Label1"
            };
            var label2 = new Printable("^XA\n^FO25,50^BY3^A0N,80,60^BCN,200,Y,N,N\n^FDLabel2!^FS\n^FO25,400^BY4^B3N,N,300,Y,N\n^FDShipToName^FS\n^XZ")
            {
                CustomerUniqueId = "Label2"
            };
            var label3 = new Printable("^XA\n^FO25,50^BY3^A0N,80,60^BCN,200,Y,N,N\n^FDLabel3!^FS\n^FO25,400^BY4^B3N,N,300,Y,N\n^FDShipToName^FS\n^XZ")
            {
                CustomerUniqueId = "Label3"
            };
            label1.ProducibleStatusObservable.DurableSubscribe(l => logger.Log(LogLevel.Info, "Label1 Status Changed to:{0}", l), logger);
            label2.ProducibleStatusObservable.DurableSubscribe(l => logger.Log(LogLevel.Info, "Label2 Status Changed to:{0}", l), logger);
            label3.ProducibleStatusObservable.DurableSubscribe(l => logger.Log(LogLevel.Info, "Label3 Status Changed to:{0}", l), logger);

            comm.Print(label1);
            comm.Print(label2);
            comm.Print(label3);
            logger.Log(LogLevel.Info, "*** All jobs sent to printer.");

            //Wait for lable1 status to be waiting for peel off.
            WaitUntilProducibleStatus(label1, ZebraInProductionProducibleStatuses.LabelWaitingToBePeeledOff, 20);

            logger.Log(LogLevel.Info, "*** Place Breakpoint here. LabelWaiting.  Continue and open printer head DO NOT REMOVE Label.");


            //ensure state is updated
            WaitUntilHeadOpen();

            //Head open should have ended work on this producible
            WaitUntilProducibleStatus(label1, ErrorProducibleStatuses.ProducibleFailed, 5);

            logger.Log(LogLevel.Info, "*** Label1 done. {0}", label1.ProducibleStatus.DisplayName);

            //Queue should have been cleared.
            WaitUntilProducibleStatus(label2, ProducibleStatuses.ProducibleRemoved, 5);
            WaitUntilProducibleStatus(label3, ProducibleStatuses.ProducibleRemoved, 5);

            logger.Log(LogLevel.Info, "*** Place Breakpoint here. Head was opened.  Continue and close printer head but DO NOT REMOVE Label.");

            //wait to close the head
            WaitUntilUnexpectedLabel(30);

            logger.Log(LogLevel.Info, "*** Place Breakpoint here. Next labels will now be printed.  Continue and pull labels.");

            WaitUntilRealPrinterOnline();
            //ensure we can send another job
            label2.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleSelected;
            label3.ProducibleStatus = NotInProductionProducibleStatuses.ProducibleSelected;
            comm.Print(label2);
            comm.Print(label3);


            WaitUntilProducibleStatus(label2, ProducibleStatuses.ProducibleCompleted, 20);
            logger.Log(LogLevel.Info, "*** Label2 done.");

            WaitUntilProducibleStatus(label3, ProducibleStatuses.ProducibleCompleted, 20);

            logger.Log(LogLevel.Info, "*** Label3 done.");
        }



        [TestMethod]
        [TestCategory("PrinterIntegration")]
        [TestCategory("Acceptance")]
        [TestCategory("RealMachine")]
        public void TestRealPrinter_ShouldComeOutOfUnexpectedLabelOnPrinterAndPrintNextJobSent()
        {
            /*
             Click the feed on the printer before starting this test and leave the label hanging.
             */

            //var socketCommunicator = new SocketCommunicator(new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
            realPrinter.IsPeelOff = true;


            printer.CurrentStatusChangedObservable.Subscribe(Console.WriteLine);

            var comm = new ZebraPrinterCommunicator(realPrinter, logger, publisher, serviceLocator);

            WaitUntilUnexpectedLabel(15);

            logger.Log(LogLevel.Info, "***Put Breakpoint here to run test.  Hit continue and then peel label");

            WaitUntilRealPrinterOnline(30);

            var label1 = new Printable("^XA\n^FO25,50^BY3^A0N,80,60^BCN,200,Y,N,N\n^FDLabel1^FS\n^FO25,400^BY4^B3N,N,300,Y,N\n^FDShipToName^FS\n^XZ")
            {
                CustomerUniqueId = "Label1"
            };
            label1.ProducibleStatusObservable.DurableSubscribe(l => logger.Log(LogLevel.Info, "Label1 Status Changed to:{0}", l), logger);

            comm.Print(label1);
            WaitUntilProducibleStatus(label1, ProducibleStatuses.ProducibleCompleted, 30);

        }

        [TestMethod]
        [TestCategory("PrinterIntegration")]
        [TestCategory("Acceptance")]
        [TestCategory("RealMachine")]
        public void TestRealPrinter_PrinterOfflineShouldComeOnlineAndPrintLabel()
        {
            /*
             Turn off printer before starting this test, 
             * Start the printer just after the test begins.
             */

            realPrinter.IsPeelOff = true;
            var comm = new ZebraPrinterCommunicator(realPrinter, logger, publisher, serviceLocator);

            WaitUntilRealPrinterOffline();

            logger.Log(LogLevel.Info, "***Put Breakpoint here to run test.  Contiunue and turn on printer and remove any startup labels that are created");

            WaitUntilRealPrinterOnline(60);

            var label1 = new Printable("^XA\n^FO25,50^BY3^A0N,80,60^BCN,200,Y,N,N\n^FDLabel1^FS\n^FO25,400^BY4^B3N,N,300,Y,N\n^FDShipToName^FS\n^XZ")
            {
                CustomerUniqueId = "Label1"
            };
            label1.ProducibleStatusObservable.DurableSubscribe(l => logger.Log(LogLevel.Info, "Label1 Status Changed to:{0}", l), logger);

            logger.Log(LogLevel.Info, "***Put Breakpoint here to run test.  Contiunue and pull label when it's printed");

            comm.Print(label1);
            WaitUntilProducibleStatus(label1, ProducibleStatuses.ProducibleCompleted, 30);

        }

        [TestMethod]
        [TestCategory("PrinterIntegration")]
        [TestCategory("Acceptance")]
        [TestCategory("RealMachine")]
        public void TestRealPrinter_ShouldComeOnlineWithinAcceptableTimeFrame()
        {
            /*
             Turn off printer before starting this test.
             * Set breakpoints on log lines below that tell you to do so.
             * !!! This test is very printer specific.
             */

            //var socketCommunicator = new SocketCommunicator(new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
            realPrinter.IsPeelOff = true;
            //  realPrinter.SocketHeartbeatWaitTime = 1000;

            var comm = new ZebraPrinterCommunicator(realPrinter, logger, publisher, serviceLocator);

            logger.Log(LogLevel.Info, "***Put Breakpoint here to run test.  Hit continue and then turn on printer and pull label when sent.");

            WaitUntilRealPrinterOnline();
        }


        [TestMethod]
        [TestCategory("PrinterIntegration")]
        [TestCategory("Acceptance")]
        [TestCategory("RealMachine")]
        public void TestRealPrinter_ShouldComeNotifyOfflineInAnAcceptableTimeFrame()
        {
            /*
             Turn on printer before starting this test.
             * Set breakpoints on log lines below that tell you to do so.
             */

            //var socketCommunicator = new SocketCommunicator(new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
            realPrinter.IsPeelOff = true;
            //  realPrinter.SocketHeartbeatWaitTime = 1000;

            var comm = new ZebraPrinterCommunicator(realPrinter, logger, publisher, serviceLocator);


            WaitUntilRealPrinterOnline();

            logger.Log(LogLevel.Info, "***Put Breakpoint here to run test. turn off the printer then continue this test.");
            var sw = Stopwatch.StartNew();
            WaitUntilRealPrinterOffline(30);
            logger.Log(LogLevel.Trace, "Went offline in: " + sw.ElapsedMilliseconds);
        }


        [TestMethod]
        [TestCategory("PrinterIntegration")]
        [TestCategory("Acceptance")]
        [TestCategory("RealMachine")]
        public void TestRealPrinter_PeelOffWorking()
        {
            /*
             Run test and peel off label when it's complete.
             */
            realPrinter.IsPeelOff = true;
            var comm = new ZebraPrinterCommunicator(realPrinter, logger, publisher, serviceLocator);

            var label =
                new Printable(
                    "^XA\n^FO25,50^BY3^A0N,80,60^BCN,200,Y,N,N\n^FDHuHuPullMe^FS\n^FO25,400^BY4^B3N,N,300,Y,N\n^FDShipToName^FS\n^XZ");
            Func<bool> isComplete = () =>
                label.ProducibleStatus == ProducibleStatuses.ProducibleCompleted;

            //ensure we have the printer we need to test.
            WaitUntilRealPrinterOnline();

            comm.Print(label);

            WaitUntilProducibleStatus(label, ProducibleStatuses.ProducibleCompleted, 15);
        }

#endif
        //[TestMethod]
        //[TestCategory("PrinterIntegration")]
        //[TestCategory("Acceptance")]
        //[TestCategory("2784")]
        //public void Print_TestRealPrinter_WhenPrinterUnreachable()
        //{
        //    // Arrange
        //    var socketCommunicator = new SocketCommunicator(new IPEndPoint(unreachablePrinterIpAddress, PrinterCommunicationPort));

        //    var printer = new ZebraPrinter();//PrinterId, socketCommunicator, printFormatter, mockLogger.Object);
        //    var printable = new Printable(new Dictionary<string, string> { { "SerialNumber", LabelText } });

        //    var printerJobStatuses = new List<PrintJobStatus>();
        //    var printerStatuses = new List<PrinterStatus>();

        //    // Act
        //    printer.Produce(printable);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(60));

        //    // Assert
        //    Specify.That(printerStatuses.SingleOrDefault(p => p.PrinterMachineStatus == PrinterMachineStatuses.Error)).Should.Not.BeNull();
        //    Specify.That(printerStatuses.SingleOrDefault(p => p.Messages.Any(m => m.ToLower().Contains("socketexception")))).Should.Not.BeNull();
        //}

        //[TestMethod]
        //[TestCategory("PrinterIntegration")]
        //[TestCategory("Acceptance")]
        //[TestCategory("2782")]
        //public void Print_TestRealPrinter_PlainText_WithZebraPrinter()
        //{
        //    // Arrange
        //    var socketCommunicator = new SocketCommunicator(new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
        //    var printFormatter = new PrintableLabelFormatter();
        //    PrintableLabelFormatter.TemplateLabel = "^XA^FO50,200^ADN100,30^FD{SerialNumber}^FS^XZ";
        //    const int printerPollingIntervalMilliseconds = 800; // use 800 for G-series printer, 100 for 105SL or 140xi
        //    var printer = new ZebraPrinter();//PrinterId, socketCommunicator, printFormatter, mockLogger.Object);
        //    printer.PollInterval = printerPollingIntervalMilliseconds;
        //    var printable = new Printable(new Dictionary<string, string> { { "SerialNumber", LabelText } });

        //    var printerJobStatuses = new List<PrintJobStatus>();
        //    var printerStatuses = new List<PrinterStatus>();

        //    printer.PrintJobStatusChangedObservable.Subscribe(printerJobStatuses.Add);
        //    printer.PrinterStatusChangedObservable.Subscribe(printerStatuses.Add);

        //    // Act
        //    var sw = Stopwatch.StartNew();
        //    printer.Produce(printable);

        //    // Assert
        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(10));

        //    sw.Stop();
        //    Debug.WriteLine("Took {0} ms", new object[] { sw.ElapsedMilliseconds });
        //    Specify.That(printerJobStatuses[0].JobStatus == PrintJobStatuses.Queued).Should.BeTrue("First job status reported should be queued");
        //    Specify.That(printerJobStatuses[1].JobStatus == PrintJobStatuses.Started).Should.BeTrue("Second job status reported should be started");
        //    Specify.That(printerJobStatuses[2].JobStatus == PrintJobStatuses.Success).Should.BeTrue("Third job status reported should be success");
        //    Specify.That(printerStatuses[0].PrinterMachineStatus == zebra).Should.BeTrue("First machine status is ready");
        //    Specify.That(printerStatuses[1].PrinterMachineStatus == PrinterMachineStatuses.Paused).Should.BeTrue("Second machine status is paused");
        //    Specify.That(printerStatuses[2].PrinterMachineStatus == PrinterMachineStatuses.Started).Should.BeTrue("Third machine status is started");
        //}

        //[TestMethod]
        //[TestCategory("PrinterIntegration")]
        //[TestCategory("Acceptance")]
        //[Description("This tests the double barcode label required for the StoraPack. To run this test you will need load at least 4-inch wide by 6-inch long labels")]
        //[TestCategory("3683")]
        //public void Print_TestRealPrinter_DoubleBarcode_WithZebraPrinter()
        //{
        //    // Arrange
        //    var socketCommunicator = new SocketCommunicator(new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
        //    var printFormatter = new PrintableLabelFormatter();
        //    PrintableLabelFormatter.TemplateLabel = "^XA^FO25,50^BY3^A0N,100,80^BCN,200,Y,N,N^FD{SerialNumber}^FS^FO25,400^BY4^B3N,N,300,Y,N^FD{PrintInfoField1}^FS^XZ";
        //    const int printerPollingIntervalMilliseconds = 800; // use 800 for G-series printer, 100 for 105SL or 140xi
        //    var printer = new ZebraPrinter();//PrinterId, socketCommunicator, printFormatter, mockLogger.Object);
        //    printer.PollInterval = printerPollingIntervalMilliseconds;
        //    var printable =
        //        new Printable(new Dictionary<string, string>
        //        {
        //            {"SerialNumber", "12345678901234567890"},
        //            {"PrintInfoField1", "12345678"}
        //        });

        //    var printerJobStatuses = new List<PrintJobStatus>();
        //    var printerStatuses = new List<PrinterStatus>();

        //    printer.PrintJobStatusChangedObservable.Subscribe(printerJobStatuses.Add);
        //    printer.PrinterStatusChangedObservable.Subscribe(printerStatuses.Add);

        //    // Act
        //    var sw = Stopwatch.StartNew();
        //    printer.Produce(printable);

        //    // Assert
        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(10));

        //    sw.Stop();
        //    Debug.WriteLine("Took {0} ms", new object[] { sw.ElapsedMilliseconds });
        //    Specify.That(printerJobStatuses[0].JobStatus == PrintJobStatuses.Queued).Should.BeTrue("First job status reported should be queued");
        //    Specify.That(printerJobStatuses[1].JobStatus == PrintJobStatuses.Started).Should.BeTrue("Second job status reported should be started");
        //    Specify.That(printerJobStatuses[2].JobStatus == PrintJobStatuses.Success).Should.BeTrue("Third job status reported should be success");
        //    Specify.That(printerStatuses[0].PrinterMachineStatus == PrinterMachineStatuses.Ready).Should.BeTrue("First machine status is ready");
        //    Specify.That(printerStatuses[1].PrinterMachineStatus == PrinterMachineStatuses.Paused).Should.BeTrue("Second machine status is paused");
        //    Specify.That(printerStatuses[2].PrinterMachineStatus == PrinterMachineStatuses.Started).Should.BeTrue("Third machine status is started");
        //}

        //[TestMethod]
        //[TestCategory("PrinterIntegration")]
        //[TestCategory("Acceptance")]
        //[TestCategory("2783")]
        //public void Print_TestRealPrinter_Barcode_WithZebraPrinter_WhenTwoPrintJobsSentAtSameTime()
        //{
        //    // Arrange
        //    var socketCommunicator = new SocketCommunicator(new IPEndPoint(realPrinterIpAddress, PrinterCommunicationPort));
        //    var printFormatter = new PrintableLabelFormatter();
        //    PrintableLabelFormatter.TemplateLabel = "^XA^FO40,50^BY3^A0N,100,80^BCN,200,Y,N,N^FD{SerialNumber}^FS^XZ";
        //    const int printerPollingIntervalMilliseconds = 800; // use 800 for G-series printer, 100 for 105SL or 140xi
        //    var printer = new ZebraPrinter();//PrinterId, socketCommunicator, printFormatter, mockLogger.Object);
        //    printer.PollInterval = printerPollingIntervalMilliseconds;
        //    var printable = new Printable(new Dictionary<string, string> { { "SerialNumber", "Label1" } });
        //    var printable2 = new Printable(new Dictionary<string, string> { { "SerialNumber", "Label2" } });

        //    var printerJobStatuses = new List<PrintJobStatus>();
        //    var printerStatuses = new List<PrinterStatus>();

        //    printer.PrintJobStatusChangedObservable.Subscribe(printerJobStatuses.Add);
        //    printer.PrinterStatusChangedObservable.Subscribe(printerStatuses.Add);

        //    // Act
        //    var sw = Stopwatch.StartNew();
        //    printer.Produce(printable);
        //    Debug.WriteLine("First jobId: {0}", printable.Id.ToString());

        //    PrinterBusyException exception = null;
        //    try
        //    {
        //        printer.Produce(printable2);
        //    }
        //    catch (PrinterBusyException e)
        //    {
        //        exception = e;
        //    }
        //    Specify.That(exception).Should.Not.BeNull();
        //    Debug.WriteLine("Second jobId: {0}", printable2.Id.ToString());

        //    // Assert
        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(10));

        //    sw.Stop();
        //    Debug.WriteLine("Took {0} ms", new object[] { sw.ElapsedMilliseconds });

        //    // Specify that the first label succeeded
        //    Specify.That(printerJobStatuses.Where(p => p.ItemToPrint.Id == printable.Id).ElementAt(0).JobStatus ==
        //                 PrintJobStatuses.Queued).Should.BeTrue("First job status reported should be queued");
        //    Specify.That(printerJobStatuses.Where(p => p.ItemToPrint.Id == printable.Id).ElementAt(1).JobStatus ==
        //                 PrintJobStatuses.Started).Should.BeTrue("Second job status reported should be started");
        //    Specify.That(printerJobStatuses.Where(p => p.ItemToPrint.Id == printable.Id).ElementAt(2).JobStatus ==
        //                 PrintJobStatuses.Success).Should.BeTrue("Third job status reported should be success");

        //    Specify.That(printerJobStatuses.FirstOrDefault(p => p.JobStatus == PrintJobStatuses.Error)).Should.Not.BeNull();

        //    // Specify machine statuses
        //    var busyStatus = printerStatuses.FirstOrDefault(p => p.PrinterMachineStatus == PrinterMachineStatuses.Busy);
        //    Specify.That(busyStatus).Should.Not.BeNull();
        //    printerStatuses.Remove(busyStatus);
        //    Specify.That(printerStatuses[0].PrinterMachineStatus == PrinterMachineStatuses.Ready).Should.BeTrue("First machine status is ready");
        //    Specify.That(printerStatuses[1].PrinterMachineStatus == PrinterMachineStatuses.Paused).Should.BeTrue("Second machine status is paused");
        //    Specify.That(printerStatuses[2].PrinterMachineStatus == PrinterMachineStatuses.Started).Should.BeTrue("Third machine status is started");
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenPrinterReady_PrinterIsPaused()
        //{
        //    // Act
        //    printer.Produce(printableBarcodeLabel);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(6));

        //    // Assert
        //    mockSocketCommunicator.Verify(x => x.SendMessage(Zebra.Commands.PrinterPause, 5), Times.Once);
        //    Specify.That(printerStatuses.Any(x => x.PrinterMachineStatus == PrinterMachineStatuses.Paused)).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenPrinterReadyAndPrinterNotPaused_ErrorJobStatusPublished()
        //{
        //    // Arrange
        //    mockSocketCommunicator.SetupSequence(
        //        c => c.SendReceive(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead, 5))
        //        .Returns(ZplPrinterReady)
        //        .Returns(ZplPrinterDoesNotPause);

        //    // Act
        //    printer.Produce(printableBarcodeLabel);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(WaitTimeForPrinterToStopPrinting));

        //    // Assert
        //    Specify.That(printJobStatuses.FirstOrDefault(p => p.JobStatus == PrintJobStatuses.Error))
        //        .Should.Not.BeNull();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenLabelSent_ButCannotVerifyLabelInQueue_ErrorJobStatusPublished()
        //{
        //    // Arrange
        //    mockSocketCommunicator.SetupSequence(
        //        c => c.SendReceive(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead, 5))
        //        .Returns(ZplPrinterReady)
        //        .Returns(ZplPrinterPausedNoLabel)
        //        .Returns(ZplPrinterPausedNoLabel);

        //    // Act
        //    printer.Produce(printableBarcodeLabel);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(WaitTimeForPrinterToStopPrinting));

        //    // Assert
        //    Specify.That(printJobStatuses.FirstOrDefault(p => p.JobStatus == PrintJobStatuses.Error))
        //        .Should.Not.BeNull();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenPrinterPaused_LabelInQueue_StartPrinterCannotVerify_ErrorJobStatusPublished()
        //{
        //    // Arrange
        //    mockSocketCommunicator.SetupSequence(
        //        c => c.SendReceive(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead, 5))
        //        .Returns(ZplPrinterReady)
        //        .Returns(ZplPrinterReadyAndPaused)
        //        .Returns(ZplPrinterPausedWithLabelInQueue)
        //        .Returns(ZplPrinterNotStartedWithLabelInQueue);

        //    // Act
        //    printer.Produce(printableBarcodeLabel);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(WaitTimeForPrinterToStopPrinting));

        //    // Assert
        //    Specify.That(printJobStatuses.FirstOrDefault(p => p.JobStatus == PrintJobStatuses.Error))
        //        .Should.Not.BeNull();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]

        //public void WhenPrinterPaused_LabelPrintRequestIsQueued()
        //{
        //    // Arrange
        //    var labelFormatted = string.Format("^XA^FO50,50^BY3^BCN,200,Y,N,N^FD{0}^FS^XZ",
        //        printableBarcodeLabel.PrintData);
        //    mockLabelFormatter.Setup(x => x.ConvertToPrintableFormat(printableBarcodeLabel))
        //        .Returns(labelFormatted);

        //    // Act
        //    printer.Produce(printableBarcodeLabel);
        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(6));

        //    // Assert
        //    Specify.That(printJobStatuses.Any(x => x.JobStatus == PrintJobStatuses.Queued)).Should.BeTrue();
        //    mockSocketCommunicator.Verify(x => x.SendMessage(labelFormatted, 5), Times.Once);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenLabelPrintRequestIsQueued_PrinterIsStarted()
        //{
        //    // Act
        //    printer.Produce(printableBarcodeLabel);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(6));

        //    // Assert
        //    Specify.That(printerStatuses.Any(x => x.PrinterMachineStatus == PrinterMachineStatuses.Started)).Should.BeTrue();
        //    Specify.That(printJobStatuses.Any(x => x.JobStatus == PrintJobStatuses.Started)).Should.BeTrue();
        //    mockSocketCommunicator.Verify(x => x.SendMessage(Zebra.Commands.PrinterStart, 5), Times.Once);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenPrinterStartedAndLabelPrinted_ValidateLabelPeeledOff()
        //{
        //    // Act
        //    printer.Produce(printableBarcodeLabel);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(6));

        //    // Assert
        //    Specify.That(printJobStatuses.Any(x => x.JobStatus == PrintJobStatuses.Success)).Should.BeTrue();
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenPrinterNotReady_JobFailsAndPrinterNotReady()
        //{
        //    // Arrange
        //    mockSocketCommunicator.Setup(c => c.SendReceive(Zebra.Commands.HostStatus, Zebra.Constants.HostStatusLinesToRead, 5)).Returns(ZplPrinterNotReady);

        //    var printJobError = false;
        //    printer.PrintJobStatusChangedObservable
        //        .Where(s => s.JobStatus == PrintJobStatuses.Error && s.ItemToPrint.Id == printableBarcodeLabel.Id)
        //        .Subscribe(s => printJobError = true);

        //    PrinterStatus printerStatus = null;
        //    printer.PrinterStatusChangedObservable
        //        .Where(s => s.PrinterMachineStatus == PrinterMachineStatuses.NotReady && s.PrinterId == PrinterId)
        //        .Subscribe(s => printerStatus = s);

        //    // Act
        //    printer.Produce(printableBarcodeLabel);
        //    Retry.For(() => printJobError, TimeSpan.FromSeconds(6));
        //    Retry.For(() => printerStatus != null, TimeSpan.FromSeconds(6));

        //    // Assert
        //    Specify.That(printJobError).Should.BeTrue("Printer failed to publish printer job error");
        //    Specify.That(printerStatus).Should.Not.BeNull("Printer failed to publish printer busy");
        //    Specify.That(printerStatus.Messages.Count).Should.BeEqualTo(6);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenAlreadyPrinting_IfPrintCalledAgain_JobFailsAndPrinterBusy()
        //{
        //    // Arrange
        //    printer.IsPrinting = true; // Set printer to already printing

        //    PrintJobStatus printJob = null;
        //    PrinterStatus printerStatus = null;

        //    printer.PrintJobStatusChangedObservable.Subscribe(s => printJob = s);

        //    printer.PrinterStatusChangedObservable.Subscribe(s => printerStatus = s);

        //    // Act
        //    PrinterBusyException exception = null;
        //    try
        //    {
        //        printer.Produce(printableBarcodeLabel);
        //    }
        //    catch (PrinterBusyException e)
        //    {
        //        exception = e;
        //    }
        //    Specify.That(exception).Should.Not.BeNull();

        //    Retry.For(() => printJob != null, TimeSpan.FromSeconds(1));
        //    Retry.For(() => printerStatus != null, TimeSpan.FromSeconds(1));

        //    // Assert
        //    Specify.That(printJob.JobStatus).Should.BeEqualTo(PrintJobStatuses.Error);
        //    Specify.That(printerStatus.PrinterMachineStatus).Should.BeEqualTo(PrinterMachineStatuses.Busy);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenSocketExceptionThrown_PrinterAndJobErrorArePublishedAndSocketIsRecycled()
        //{
        //    mockSocketCommunicator.Setup(x => x.SendReceive(It.IsAny<string>(), It.IsAny<int>(), 5))
        //        .Throws(new SocketException(404));

        //    printer.Produce(printableBarcodeLabel);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(2));

        //    Specify.That(printJobStatuses.First().JobStatus).Should.BeEqualTo(PrintJobStatuses.Error);
        //    Specify.That(printerStatuses.First().PrinterMachineStatus).Should.BeEqualTo(PrinterMachineStatuses.Error);
        //    mockSocketCommunicator.Verify(s => s.RecycleSocket(), Times.Once);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenNullPrintableIsSent_ErrorIsPublished()
        //{
        //    // Act
        //    printer.Produce(null);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(6));

        //    // Assert
        //    Specify.That(
        //        printJobStatuses.Any(
        //            x =>
        //                x.JobStatus == PrintJobStatuses.Error &&
        //                x.ErrorMessage.ToLower().Contains("null printable is not allowed"))).Should.BeTrue();
        //}


        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenRetryCountIsZeroOrLess_DefaultValueIsSet()
        //{
        //    printer.RetryCount = 0;

        //    Specify.That(printer.RetryCount).Should.Not.BeEqualTo(0);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenPollIntervalIsZeroOrLess_DefaultValueIsSet()
        //{
        //    printer.PollInterval = 0;

        //    Specify.That(printer.PollInterval).Should.Not.BeEqualTo(0);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenLabelSecondCheckIntervalIsZeroOrLess_DefaultValueIsSet()
        //{
        //    printer.LabelSecondCheckWaitTime = 0;

        //    Specify.That(printer.LabelSecondCheckWaitTime).Should.BeEqualTo(100);
        //}

        //[TestMethod]
        //[TestCategory("Unit")]
        //public void WhenPrintableWithNullDataIsSent_ErrorIsPublished()
        //{
        //    // Arrange
        //    var printable = new Printable(null);
        //    // Act
        //    printer.Produce(printable);

        //    Retry.For(() => !printer.IsPrinting, TimeSpan.FromSeconds(6));

        //    // Assert
        //    Specify.That(
        //        printJobStatuses.Any(
        //            x =>
        //                x.JobStatus == PrintJobStatuses.Error &&
        //                x.ErrorMessage.ToLower().Contains("null printable data is not allowed"))).Should.BeTrue();
        //}


    }

}
