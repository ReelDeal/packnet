﻿using System;
using System.Data;
using System.Net;
using System.Reactive.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using PackNet.Common.Eventing;
using PackNet.Common.Interfaces.Communication;
using PackNet.Common.Interfaces.DTO.Messaging;
using PackNet.Common.Interfaces.DTO.PLC;
using PackNet.Common.Interfaces.Logging;
using PackNet.Common.Utils;
using PackNet.Communication;
using PackNet.Communication.Communicators.EM;
using PackNet.Communication.Communicators.Fusion;

using Testing.Specificity;

namespace PackNet.CommunicationTests 
{
    [TestClass]
    public class HeartBeatCommunicatorTests : PlcCommunicatorHelpers
    {
        private Mock<ILogger> mockLogger;
        private Mock<IWebRequestCreator> webRequestFactory;
        private EventAggregator eventAggregator;

        [TestInitialize]
        public void Setup()
        {
            eventAggregator = new EventAggregator();
            mockLogger = new Mock<ILogger>(MockBehavior.Loose);
            webRequestFactory = new Mock<IWebRequestCreator>();
            webRequestFactory.Setup(m => m.ServerAddress).Returns("127.0.0.1");
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ConstructorBroadcastsStatusWhenFailingToConnect()
        {
            webRequestFactory.Setup(m => m.CreateGetRequest(It.IsAny<PacksizePlcVariable>())).Throws(new WebException("Fail"));
            var called = false;
            var connected = true;
            eventAggregator.GetEvent<Message<Tuple<string, bool>>>()
                .Where(m => m.MessageType == SimplePLCMessages.SimplePLCConnectionStatus)
                .Subscribe(m =>
                {
                    called = true;
                    connected = m.Data.Item2;
                });

            new PLCHeartBeatCommunicator(webRequestFactory.Object, IqFusionMachineVariables.Instance, eventAggregator,
                mockLogger.Object);
            Retry.For(() => called, TimeSpan.FromSeconds(5));
            Specify.That(called).Should.BeTrue();
            Specify.That(connected).Should.BeFalse();
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ConstructorBroadcastsStatusWhenSuccessfulConnect()
        {
            webRequestFactory.Setup(m => m.CreateGetRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.ConnectionStatus.VariableName)))
                .Returns(() => GetMockRequest("{\"PCCommunic:ConnectionStatus\":false}").Object);
            //webRequestFactory
            //    .Setup(m => m.CreateGetRequest(It.IsAny<PacksizePlcVariable>()))
            //    .Returns(() => GetMockRequest("{\"Main:state\":10}").Object);
            //webRequestFactory
            //    .Setup(m => m.CreatePostRequest(It.IsAny<PacksizePlcVariable>(), It.IsAny<object>()))
            //    .Returns(() => GetMockRequest("{\"Main:state\":10}").Object);

            var called = false;
            var connected = false;
            eventAggregator.GetEvent<Message<Tuple<string, bool>>>()
                .Where(m => m.MessageType == SimplePLCMessages.SimplePLCConnectionStatus)
                .Subscribe(m =>
                {
                    called = true;
                    connected = m.Data.Item2;
                });

            new PLCHeartBeatCommunicator(webRequestFactory.Object, IqFusionMachineVariables.Instance, eventAggregator,
                mockLogger.Object);
            Retry.For(() => called, TimeSpan.FromSeconds(5));
            Specify.That(called).Should.BeTrue();
            Specify.That(connected).Should.BeTrue();
        }

        

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldShouldSendDisconnectedStatusWhenDisposed()
        {
            webRequestFactory
                .Setup(m => m.CreateGetRequest(It.IsAny<PacksizePlcVariable>()))
                .Returns(() => GetFailedToConnectRequest("Unable to connect to the remote server").Object);
            var called = false;
            var connected = true;
            var communicator = GetMockConnectedCommunicator();

            eventAggregator.GetEvent<Message<Tuple<string, bool>>>()
                .Where(m => m.MessageType == SimplePLCMessages.SimplePLCConnectionStatus)
                .Subscribe(m =>
                {
                    called = true;
                    connected = m.Data.Item2;
                });

            communicator.Dispose();

            Retry.For(() => called, TimeSpan.FromSeconds(2));
            Specify.That(called).Should.BeTrue();
            Specify.That(connected).Should.BeFalse();
        }

        private PLCHeartBeatCommunicator GetMockDisconnectedCommunicator()
        {
            var communicator = new PLCHeartBeatCommunicator(webRequestFactory.Object, IqFusionMachineVariables.Instance,
                eventAggregator, mockLogger.Object);
            return communicator;
        }

        private PLCHeartBeatCommunicator GetMockConnectedCommunicator(TimeSpan pingTime = default(TimeSpan), TimeSpan connectRetryTime = default(TimeSpan))
        {
            webRequestFactory.Setup(m => m.CreateGetRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.ConnectionStatus.VariableName)))
                .Returns(() => GetMockRequest("{\"PCCommunic:ConnectionStatus\":\"false\"}").Object);
            webRequestFactory
                .Setup(m => m.CreateGetRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.PcLifeSign.VariableName)))
                .Returns(() => GetMockRequest("{\"Main:state\":10}").Object);
            webRequestFactory
                .Setup(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.PcLifeSign.VariableName), It.IsAny<object>()))
                .Returns(() => GetMockRequest("{\"Main:state\":10}").Object);
            webRequestFactory
                .Setup(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.EventNotificationRegistrationBuffer.VariableName), It.IsAny<object>()))
                .Returns(() => GetMockRequest("{\"t:t\":true}").Object);
            webRequestFactory
                .Setup(m => m.CreatePostRequest(It.Is<PacksizePlcVariable>(v => v.VariableName == IqFusionMachineVariables.Instance.EventNotificationVariablesCounter.VariableName), It.IsAny<object>()))
                .Returns(() => GetMockRequest("{\"t:t\":true}").Object);

            var connected = false;
            eventAggregator.GetEvent<Message<Tuple<string, bool>>>()
                .Where(m => m.MessageType == SimplePLCMessages.SimplePLCConnectionStatus)
                .Subscribe(m =>
                {
                    connected = m.Data.Item2;
                });

            if (pingTime == default(TimeSpan))
                pingTime = TimeSpan.FromSeconds(2);

            if (connectRetryTime == default(TimeSpan))
                connectRetryTime = TimeSpan.FromSeconds(0.1);

            var communicator = new PLCHeartBeatCommunicator(
                webRequestFactory.Object,
                IqFusionMachineVariables.Instance,
                eventAggregator,
                mockLogger.Object);
            Retry.For(() => connected, TimeSpan.FromSeconds(5));
            Specify.That(connected).Should.BeTrue();
            return communicator;
        }

        [TestMethod]
        [TestCategory("Unit")]
        public void ShouldThrowReadonlyExceptionWhenWritingToReadonlyVariable()
        {
            var com = GetMockConnectedCommunicator();

            try
            {
                com.WriteValue(EmMachineVariables.Instance.CrossHeadSpeed, 100.0);
            }
            catch (Exception e)
            {
                var except = e as ReadOnlyException;
                Specify.That(except).Should.Not.BeNull();
            }
        }
    }
}
